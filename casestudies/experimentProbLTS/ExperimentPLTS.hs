module ExperimentPLTS where
    
import qualified Language.APSL.AISL.DataType.LTS as LTS    
import Language.APSL.AISL.ProbabilisticAnalyses.ProbLTS
import qualified Data.Map as M
import qualified Data.List
import Data.Time.Clock
import Control.DeepSeq

import qualified Debug.Trace as Debug

{- ======================================================
Benchmarking of Probabilistic LTS test analyses.

Experiment parameters to investigate:
  * Models : EX1, EX2 [0,2,8]
  * Test cases:
      tc3 [4..8] on EX1
      tc4 [4..8] on EX2 [0,2,8]
  * Analyses:
      f1_EX1: <>(s4 /\ <>s2) on EX1 
      f1_EX2: variant of f1 for EX2
      f2:  AtLeastN 1 8
      f3:  AtLeastN 1 10
  * Analyses algorithm: brute or label based.

Output set-1: models statistics
  * Model name
  * #nodes, #transitions
  * Non-determinism degree = averaage out-degree per node per out-label of that node

Output set-2: test cases statistics
  * Name of the model on which the test case is run
  * test case name
  * test case length
  * #nodes, #transitions in the resu;ting execution model
  * #paths
  * Non-determinism degree in the execution model
 
Output set-3: analyses performance
  * test case name
  * test case length
  * formula name
  * algorithm name
  * analyses result
  * time to construct the execution model
  * time to execute the analysis
  * total time 

====================================================== -}

-- The models: EX1 and EX2. EX1 is copied from ProbLTS. EX2 can be
-- parameterized to increase its non-determinism.
example_EX1 = ProbLTS s0 (M.fromList arrows) (M.fromList statelabels)
    where
    states = map LTS.Normal ["s0","s1","s2","s3","s4","s5","s6"]
    [s0,s1,s2,s3,s4,s5,s6] = states
    tau = LTS.Internal
    a = LTS.Send "a"
    b = LTS.Receive "b"
    c = LTS.Send "c"
    arrows =  [ 
      (s0, [(a, [(s1,0.5),(s2,0.5)])]),
      (s1, [(b, [(s0,0.9),(s3,0.1)])]),
      (s2, [(b, [(s0,1.0)]), (c, [(s2,1.0)])]), 
      (s3, [(tau, [(s4,0.9),(s5,0.1)])]),
      (s4, [(a, [(s1,1.0)])]), 
      (s5, [(c, [(s6,1.0)])]),
      (s6, [])
      ]
    statelabels = map (\s-> (s,())) states
    
example_EX2 n = ProbLTS s0 (M.fromList arrows) (M.fromList statelabels)
    where
    states_group1 = map LTS.Normal ["s0","nds1","nds2","s1","nds3","done"]
    states_group2 = [ndt i | i <- [0..(n-1)]]
    states = states_group1 ++ states_group2
    
    [s0,nds1,nds2,s1,nds3,done] = states_group1
    ndt i = LTS.Normal ("ndt" ++ show i)
    
    tau = LTS.Internal
    a = LTS.Send "a"
    b = LTS.Send "b"
    c = LTS.Receive "c"
    arrows =  [ 
      (s0,   [(a, [(nds1,1.0)])]),
      (nds1, [(c, [(nds1,0.7),(nds2,0.3)]), (a,[(s1,1.0)])]),
      (nds2, [(c, [(nds1,0.7),(nds2,0.3)])]),
      (s1,   [(b, [(s1,1.0)]), (a,[(nds3,1.0)])]),
      (nds3, [(c, (nds3,pRemainingAt_nds3) : arrows_to_ndti), (a,[(done,1.0)])]),
      (done, [])
      ]
      ++ arrows_from_ndti
    
    pRemainingAt_nds3 = if n==0 then 1.0 else 0.7
    pndt = (1.0 - pRemainingAt_nds3) / (fromIntegral $ toInteger n)
    arrows_to_ndti   = [ (ndt i, pndt)  | i <- [0..(n-1)]]  
    arrows_from_ndti = [(ndt i, [(c,[(nds3,1.0)])]) | i <- [0..(n-1)]]  
      
    statelabels = map (\s-> (s,())) states   
       
    
allModels = [ ("EX1", example_EX1),
              ("EX2-0", example_EX2 0),
              ("EX2-2", example_EX2 2),
              ("EX2-8", example_EX2 8)
            ]
            
getModel name = (M.fromList allModels) M.! name      
            
-- canculate the degree of non-determinism in a model. 1.0 means the model is
-- deterministic; higher means it is increasingly nd.            
nd_degree :: ProbLTS a -> Float
nd_degree model = (fromInteger $ toInteger $ totOutDegree) / n
    where
    outfans = concat $ M.elems $ transitions model   
    n :: Float
    n = fromInteger $ toInteger $ length $ outfans     
    totOutDegree = sum $ map length $ map snd $ outfans 
    
count_arrows model = sum $ map length $ map snd $ arrows 
   where    
   arrows = concat $ M.elems $ transitions model        
    
printLnRow_ :: [String] -> IO()
printLnRow_ row = putStrLn $ concat $ Data.List.intersperse ";" row
    
print_model_statistics = do
    printLnRow_ ["name","#nodes","#arrows","nd-degree"]
    sequence_ $ map printLnRow_ $ map getStatistics $ allModels
    
    where 
        
    getStatistics (name,model) = [name,
                                  show $ M.size $ slabels model, -- #nodes
                                  show $ count_arrows model, -- #arrows
                                  show $ nd_degree model
                                  ]
  
--       
-- The test cases: tc1 and tc2, both can take parameters to make them longer
-- tc1 is to be used on EX1, tc2 on EX2     
--      
tc1 k = concat (replicate k [a,b]) ++ [a] ++ replicate k c ++ [b,a,b,c]  -- total length =3b + 5
   where
   a = LTS.Send "a"
   b = LTS.Receive "b"
   c = LTS.Send "c"
   
tc2 k =  [a] 
         ++ replicate k c ++ [a] 
         ++ replicate k b ++ [b,a] 
         ++ replicate k c ++ [a]   -- total length =3b + 5
   where
   a = LTS.Send "a"
   b = LTS.Send "b"
   c = LTS.Receive "c"
    



all_testcases = -- tc1_ex1 ++ tc2_ex2_0 ++ tc2_ex2_2 ++ tc2_ex2_8 ++ 
                red_tc2_ex2_0 ++ red_tc2_ex2_8
   where
   tc1_ex1   = [ ("tc1-" ++ show k ++ "-EX1", (tc1 k, mkExecutionModel (tc1 k) (getModel "EX1")))  | k <- [4..8] ]
   tc2_ex2_0 = [ ("tc2-" ++ show k ++ "-EX2-0", (tc2 k, mkExecutionModel (tc2 k) (getModel "EX2-0")))  | k <- [4..8] ]
   tc2_ex2_2 = [ ("tc2-" ++ show k ++ "-EX2-2", (tc2 k, mkExecutionModel (tc2 k) (getModel "EX2-2")))  | k <- [4..8] ]
   tc2_ex2_8 = [ ("tc2-" ++ show k ++ "-EX2-8", (tc2 k, mkExecutionModel (tc2 k) (getModel "EX2-8")))  | k <- [4..8] ]
   
   reduce3 (tcname,(tc,emodel)) = ("red3-" ++ tcname, (tc,wordNormalize 3 emodel))
   -- some execution models after applying word3 reduction
   red_tc2_ex2_0 = map reduce3 tc2_ex2_0 
   red_tc2_ex2_8 = map reduce3 tc2_ex2_8           

get_emodel tcname = snd ((M.fromList all_testcases) M.! tcname)

print_testcases_statistics = do
   printLnRow_ ["name","length","#nodes", "#arrows", "nd-degree", "#paths"]
   sequence_ $ map printLnRow_ $ map getStatistics $ all_testcases
    
   where
   isCyclic model = worker [] (initState model)
      where
      worker visited s = if s `elem` visited then True
          else
          let 
          sucs = [ t | (a,nextstates) <- (transitions model) M.! s , (t,_) <- nextstates ]
          alreadyVisitedSucs = [ t | t <- sucs, u <- visited, t==u ]
          in
          if not(null alreadyVisitedSucs) then True
          else any (==True) $ map (worker (s:visited)) sucs
       
       
   getStatistics (name,(tc,emodel)) = [ name,
          show $ length tc,
          show $ M.size $ slabels emodel, -- #nodes
          show $ count_arrows emodel, -- #arrows
          show $ nd_degree emodel,
          show $ isCyclic emodel
          -- show $ length $ getAllExecs emodel
       ]           
    
tc1_emodel = mkExecutionModel (tc1 0) example_EX1
red_tc1_emodel = wordNormalize 3 tc1_emodel
  
--
-- The analyses/queries
--
f1_ex1 = LinFuture [clause1_ ["s4"] , clause1_ ["s2"]]     -- <>(s4 /\ <>s2)
f1_ex2 = LinFuture [clause1_ ["nds2"] , clause1_ ["ndt0"]]  -- <>(nds2 /\ <>ndt0)
f2 = AtLeastN 1 8
f3 = AtLeastN 1 10

all_analyses = map (\(aname,f,tcname) -> (aname, (f,get_emodel tcname))) 
   $ 
   f1_ex1_sets ++ f1_ex2_sets ++ f2_sets ++ f3_sets
   
   where
       
   f1_ex1_sets = [ ("f1-tc1-" ++ show k ++ "-EX1", f1_ex1, "tc1-" ++ show k ++ "-EX1")| k <- [4..8]]
   f1_ex2_sets = [ ("f1-tc2-" ++ show k ++ "-EX2-" ++ show n , f1_ex2, "tc2-" ++ show k ++ "-EX2-" ++ show n) 
                   | 
                   n <- [0,2,8], k <- [4..7] ]
                   
   f2_sets = [ ("f2-tc2-" ++ show k ++ "-EX2-" ++ show n , f2, "tc2-" ++ show k ++ "-EX2-" ++ show n) 
                   | 
                   n <- [0,2,8], k <- [4..8] ]
                   
   f3_sets = [ ("f3-tc2-" ++ show k ++ "-EX2-" ++ show n , f3, "tc2-" ++ show k ++ "-EX2-" ++ show n) 
                   | 
                   n <- [0,2,8], k <- [4..8] ]          
                   
             
run_analyses = do
   sequence_ $ map run_ all_analyses
   where
   epsilon = 1.0e-1
   dropLast s = take (length s - 1) s
   run_ (name,(f,emodel)) = do
       let numOfSatExecs = length $ getSatisfyingExecutions f emodel
       time0 <- getCurrentTime 
       putStr $ time0 `deepseq` " "
       let p1 = bruteAnalyzeTestCase f emodel  
       time1 <- p1 `deepseq` getCurrentTime
       putStr $ time1 `deepseq` " "
       let p2 = analyzeTestCase f emodel  
       time2 <- p2 `deepseq` getCurrentTime
       putStr $  time2 `deepseq` " "
       let ok = if abs(p1 - p2) < epsilon then "OK" else "X"
       let tbrute = 1.0 * (read $ dropLast $ show (diffUTCTime time1 time0))
       let tlabel = 1.0 * (read $ dropLast $ show (diffUTCTime time2 time1))
       let boost = tbrute/tlabel
       printLnRow_ [name,
               -- show numOfSatExecs, 
               show p2,
               ok,
               show tbrute, 
               show tlabel,
               show boost ]
     

-- the benchmarking function 
{-

benchmark args = 
   if null args then do 
      print_models_statistics 
      print_specs_statistics 
   else do 
      let [emodelName,k_,specName,algorithmName] = args 
      let k = read k_ - (4::Int)
      time0 <- getCurrentTime
      let emodelDesc = case emodelName of
                         "EX1"   -> executionModels_EX1 !! k
                         "EX2_0" -> executionModels_EX2_0 !! k
                         "EX2_2" -> executionModels_EX2_2 !! k
                         "EX2_8" -> executionModels_EX2_8 !! k
      let (_,_,_,emodel) = emodelDesc
      let count = cntNodes emodel
      time1 <- count `deepseq` getCurrentTime
      let spec = case specName of
                     "f1_ex1" -> snd f1_ex1
                     "f1_ex2" -> snd f1_ex2
                     "f2"     -> snd f2
                     "f3"     -> snd f3
      let algorithm = case algorithmName of 
                        "brute" -> bruteAnalyzeTestCase
                        "label" -> analyzeTestCase
      -- putStrLn ("\n============")
      let p = algorithm spec emodel
      time2 <- p `deepseq` getCurrentTime
      putStrLn $ concat (Data.List.intersperse ";" args) ++ ";" ++ show p ++ ";" 
                 ++ show (diffUTCTime time1 time0) ++ ";"
                 ++ show (diffUTCTime time2 time1)
      
   where   
   --        
   -- the execution models: 
   --      
   tc_lengths = [4..8]
   
   mkExecModel name k model testcase = (name, k, testcase, mkExecutionModel testcase model)
   executionModels_EX1   = [ mkExecModel "tc3 on EX1" k example_EX1 (tc3 k)    | k <- tc_lengths ]
   executionModels_EX2_0 = [ mkExecModel "tc4 on EX2 0" k (example_EX2 0) (tc4 k) | k <- tc_lengths ]
   executionModels_EX2_2 = [ mkExecModel "tc4 on EX2 2" k (example_EX2 2) (tc4 k) | k <- tc_lengths ]
   executionModels_EX2_8 = [ mkExecModel "tc4 on EX2 8" k (example_EX2 8) (tc4 k) | k <- tc_lengths ]

   executionModels = executionModels_EX1 ++ executionModels_EX2_0 ++ executionModels_EX2_2 ++ executionModels_EX2_8

   print_models_statistics = do 
      putStrLn "\n============"
      putStrLn "Models statistics:"
      putStrLn "Name | k | tc-length | #nodes | #execs" 
      sequence_ [print_model_stats $ mkModel_stats r | r <- executionModels]
   
   mkModel_stats (name,k,tc,emodel) = (name,k,length tc, cntNodes emodel,length $ getAllExecs emodel)    
   print_model_stats (name,k,tclen,nodesCnt,execsCnt) = putStrLn $ concat $ Data.List.intersperse ";" [name, show k , show tclen, show nodesCnt, show execsCnt]
  
   --
   -- the analyses:
   --
   f1_ex1 = ("f1_ex1",LinFuture [clause1_ ["s4"] , clause1_ ["s2"]])      -- <>(s4 /\ <>s2)
   f1_ex2 = ("f1_ex2",LinFuture [clause1_ ["nds2"] , clause1_ ["ndt0"]])  -- <>(nds2 /\ <>ndt0)
   f2 = ("f2", AtLeastN 1 8)
   f3 = ("f3", AtLeastN 1 10)
   specs = [f1_ex1, f1_ex2, f2, f3]
   
   print_specs_statistics = do
      putStrLn "\n============"
      putStrLn "Specs statistics:"
      putStrLn "Spec-name | emodel-name | k | #satisfying-execs" 
      let stats1 = [ (fst f1_ex1, emodelName, k, countSatisfyingExecs (snd f1_ex1) emodel)| (emodelName,k,_,emodel) <- executionModels_EX1 ]
      let modelsEX2 = executionModels_EX2_0 ++ executionModels_EX2_2 ++ executionModels_EX2_8
      let stats2 = [ (fst f1_ex2, emodelName, k, countSatisfyingExecs (snd f1_ex2) emodel)| (emodelName,k,_,emodel) <- modelsEX2 ]

      sequence_ $ map print_spec_stats  stats1
      sequence_ $ map print_spec_stats  stats2

      
   countSatisfyingExecs phi emodel = length $ getSatisfyingExecutions phi emodel 
   print_spec_stats (specName,emodelName,k,count) = putStrLn $ concat $ Data.List.intersperse ";"  [specName,emodelName,show k, show count]      
    
-} 

   
