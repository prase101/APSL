** Websocket case study

As the IUT, we use Autobahn implementation of Websocket. The implementation is
in Python, so it requires Python to run. I use Python3. To install Autobahn,
run:

   > pip3 install autobahn

pip3 is Python's package installer. Documentation and other information about
this software can be obtained at: https://crossbar.io/autobahn/

The specific instance used as the IUT is a modification of an echo server that
runs on top Websocket. This server is one of the standard examples supplied in
the autobahn package ---download its source code to see the original code.

To run the server do:

   > python3 server.py

Once running, APSL can interact with it.

* .aisl, .amsl : APSL models for the Websocket protocol.
* server.py : a websocket-based echo server.

