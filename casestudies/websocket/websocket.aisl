interactions module websocket

# Could be improved if explicit close or ping messages were present.
import * from 'websocket.amsl'


actor WebsocketClient with

    init state Start where
        anytime do send ClientOpenHandshake next Connecting
    end

    state Connecting where
        on ServerOpenHandshake do next Open
    end

    state Open where
        on MessageFrame do continue

        on MessageStartFrame do next ReceivingContinuation

        anytime 
           do send MaskedMessageFrame continue
           or do send MaskedMessageStartFrame next SendingContinuation
           or do send MaskedMessageFrame
        quit
    end

    state ReceivingContinuation where
        on ContinuationFrame do continue

        on MessageEndFrame do next Open

        anytime 
           do send MaskedMessageFrame continue
           or do send MaskedMessageStartFrame next SendingReceivingContinuation
    end

    state SendingContinuation where
        on MessageFrame do continue

        on MessageStartFrame do next SendingReceivingContinuation

        anytime 
           do send MaskedContinuationFrame continue
           or do send MaskedMessageEndFrame next Open
    end

    state SendingReceivingContinuation where
        on ContinuationFrame do continue

        on MessageEndFrame do next SendingContinuation

        anytime 
          do send MaskedContinuationFrame continue
          or do send MaskedMessageEndFrame next ReceivingContinuation
    end

end

actor WebsocketServer with

    init state Connecting where
        on ClientOpenHandshake do send ServerOpenHandshake next Open
    end

    state Open where
        on MaskedMessageFrame      do continue
        on MaskedMessageStartFrame do next ReceivingContinuation

        anytime 
            do send MessageFrame continue
            or do send MessageStartFrame next SendingContinuation
            # WP: commenting this out, since the echo protocol won't quit on its own:
            # or do send MessageFrame quit

    end

    state ReceivingContinuation where
        on MaskedContinuationFrame do continue
        on MaskedMessageEndFrame   do next Open

        anytime 
           do send MessageFrame continue
           or do send MessageStartFrame next SendingReceivingContinuation
    end

    state SendingContinuation where
        on MaskedMessageFrame do continue
        on MaskedMessageStartFrame do next SendingReceivingContinuation

        anytime 
          do send ContinuationFrame continue
          or do send MessageEndFrame next Open
    end

    state SendingReceivingContinuation where
        on MaskedContinuationFrame do continue
        on MaskedMessageEndFrame   do next SendingContinuation

        anytime 
           do send ContinuationFrame continue
           or do send MessageEndFrame next ReceivingContinuation
    end
    

end

   