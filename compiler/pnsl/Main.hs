module Main where

import Language.APSL.AsyncTestEngine.Test
import System.IO

main =
  hSetBuffering stdout NoBuffering >>
  hSetBuffering stderr NoBuffering >>
  myTestWebsocket 0 1 60000
