module Language.APSL.AISL.ProbabilisticAnalyses.ProbLTS where
    
import qualified Language.APSL.AISL.DataType.LTS as LTS

import           Data.Map (Map)
import qualified Data.Map as M
import           Data.Set (Set)
import qualified Data.Set as S
import Data.Maybe
import qualified Data.List
import qualified Debug.Trace as Debug



-- LTS, where arrows are annotated with probabilistic information. Note that probability is quantified over 
-- outgoing arrows over the same symbols. For example, if state s has three types of outgoing arrows, labelled
-- with e.g. a,b,c. .... blabla
--
data ProbLTS a = ProbLTS {
    initState   :: LTS.StateID,
    transitions :: Map LTS.StateID [OutgoingTransitionsGroup], 
    -- labels we put on every state:
    slabels     :: Map LTS.StateID a 
} deriving (Show)

-- getNormalStateName (LTS.Normal name) = name

-- Represeting outgoing arrows labelled with the same action.
-- For each destination, labelled with probability that destination is chosen,
-- when the action occurs.
type OutgoingTransitionsGroup = (LTS.Action, [(LTS.StateID,Double)])  -- assume ReceiveUnexpected action to have been eliminated

-- assign a default probability to a PLTS
assignEqualProb (ProbLTS s0 arrows slabs) = ProbLTS { initState = s0, transitions = M.map assign arrows, slabels = slabs}
   where
   assign outgoingGroups = map f outgoingGroups
   f (a,destinations) = (a, map ff destinations)
      where
      p :: Double
      p = 1.0 / (fromInteger $ toInteger $ length destinations)
      ff (dest,_) = (dest,p)

tauNormalize :: ProbLTS () -> ProbLTS ()
tauNormalize = undefined

-- Pretty print a PLTS
printPLTS :: Show a => ProbLTS a -> String
printPLTS (ProbLTS s0 arrows labels) = 
    "\n=============\n" ++
    "Init: " ++ getStName s0  ++  (concat $ map print1 states) ++
    "\n=============\n" 
    where
    states = M.keys arrows
    print1 st = "\n  " ++ getStName st ++ " : " ++ show (labels M.! st) ++ (concat $ map print2 (arrows M.! st))
    print2 (a,group) = concat $ map print3 group
        where
        print3 (st2,p) = "\n    " ++ show a ++ " --> " ++ getStName st2 ++ " (" ++ show p ++ ")"
    
    getStName (LTS.Normal st) = st

-- Count the number of nodes
cntNodes :: ProbLTS a -> Int
cntNodes (ProbLTS s0 arrows labels) = snd (dfs [] 0 s0)
   where
   sucs u =  Data.List.nub [ v | (a,groups)<- arrows M.! u, (v,p) <- groups ]
   dfs visited cnt u = if u `elem` visited then (visited,cnt)
                       else let
                             sucessors = sucs u
                             visited2 = u : visited
                             cnt2 = 1 + cnt 
                             in
                             foldr (\v (visited',cnt') -> dfs visited' cnt' v) (visited2,cnt2) sucessors 

-- An execution model is a PLTS representing all possible executions that would yield the same
-- observable trace. Note that an execution model should be acyclic.
-- If M is an PLTS and tr is a trace, and E is the execution model of tr, E has the same
-- structure of a PLTS, so we will also represent it with a PLTS. However, the same state in M
-- may occur multiple times in E. To represent this, note that E's "own" states have their own
-- unique ID. We will then use the "slabels" field to map every E's state u to M's state that
-- u represents.
--
-- In a later kind of execution models, slabels may map u to a sequence of states in M, rather
-- than just a single state.
--
type ExecutionModel = ProbLTS [LTS.StateID]

-- Given a trace of actions representing a test case, a PLTS, this function construct
-- the execution model of the trace. The PLTS is assumed to be tau-normalized.
-- The trace should only contain Send and Receive action (no Internal nor ReceiveUnexpected allowed)
--
mkExecutionModel :: [LTS.Action] -> ProbLTS () -> ExecutionModel
mkExecutionModel trace plts = case x of
    Nothing -> error ("The trace " ++ show trace ++ " is NOT a valid test case, given the model.")
    Just id -> ProbLTS { initState   = mkState id, 
                         transitions = M.fromList constructedTransitions, 
                         slabels     = M.fromList $ map (\(x,st,_)-> (mkState x, [st])) resultNodes 
                       }
   
    
    where
    -- represent a node with (unique-id, where it refers to in plts, suffix of the trace it represents) 
    -- represent arrows with (id-of-source, id-of-dest, probability)
    -- Invoke the worker function:
    (x, _ , (resultNodes,resultArrows)) = worker (nodes0,arrows0) 1 (initState plts) trace   
        
    sucs u = (transitions plts) M.! u  
    exitNodeId = 0 
    sharp = LTS.Normal "#"
    nodes0  = [ (exitNodeId,sharp,[])]
    arrows0 = [ ]
    
    mkState i = LTS.Normal $ show i
    
    -- converting the graph produced by the worker function to PLTS:
    constructedTransitions = map construct resultNodes
       where 
       construct (x,st,suffix) = if x == exitNodeId then (mkState x, [])
                                 else (mkState x, [(action, arrowsToSucs)])
          where
          action = if null suffix then LTS.Internal else head suffix
          arrowsToSucs = [ (mkState y,p) | (x_,y,p) <- resultArrows, x_ == x ]
    
    --
    -- The worker function has the following threaded parameters:
    --    * the graph constructed so far
    --    * next available fresh ID
    -- Inherited parameters:
    --    * a node/state u in the orginal PLTS
    --    * the remaining suffix of the original trace to execute
    -- Output/synthesized parameters:
    --    * Just id: the ID of the node in the execution model that represents u
    --    * Nothing, if it is not possible to execute the suffix on u
    --
    worker graphSoFar freshId u [] = 
        case visited of
           Just (id,_,_)  -> (Just id, freshId, graphSoFar)
           Nothing        -> if tauTransitionPossible then worker graphSoFar freshId u [LTS.Internal]
                             else (Just id_u, id_u + 1, (newnodes,newarrows))
        where
        visited = Data.List.find (\(id,v,suffix)-> (v,suffix) == (u,[])) $ fst graphSoFar
        -- check if a tau transition is possible
        tau_ = Data.List.find (\(b,_)-> b == LTS.Internal)  $ sucs u
        tauTransitionPossible = isJust tau_

        id_u = freshId
        (nodes,arrows) = graphSoFar
        newnodes  = (id_u,u,[]) : nodes
        newarrows = (id_u,exitNodeId,1.0) : arrows        
        
    -- case if the trace is not empty:
    worker graphSoFar freshId u trace@(a:remainingTrace) = 
        case visited of
           -- "visited" means that the execution model that corresponds to (u,trace) has been 
           -- constructed processed by some previous recursive call. So we are not going to
           -- construct it again, and simply share the constructed model:
           Just (id,_,_)  -> (Just id, freshId, graphSoFar)
           -- Else, we first check if the suffix can be executed on u at all. If this
           -- is not possible, we return essentially Nothing.
           -- Else we need to construct the execution model for (u,trace).
           Nothing -> if not transitionPossible || null sucArrows 
                      then (Nothing, freshId, graphSoFar)  -- not possible to execute trace on u
                      else (Just id_u, nextId, newGraph)   -- new graph/model for u
        where
        
        visited = Data.List.find (\(id,v,suffix)-> (v,suffix) == (u,trace)) $ fst graphSoFar
      
        -- The given PLTS is assume to be tau-normalized. So, on the state u, either only
        -- tau-transitions are possible, or only non-tau possible.
        
        -- Not nothing if u can (only) do a tau action
        tau_ = Data.List.find (\(b,_)-> b == LTS.Internal)  $ sucs u
        -- Not nothing if the first action a (in the trace) is possible
        a_   = Data.List.find (\(b,_)-> b == a) $ sucs u
        
        nextIsTau = isJust tau_
        transitionPossible = isJust tau_ || isJust a_
      
        -- the action and destinations, if either tau or a is possible on u:
        (action,successors) = if nextIsTau then fromJust tau_ else fromJust a_
        
        id_u = freshId
        traceTorecurseOn = if nextIsTau && (a /= LTS.Internal) then trace else remainingTrace
                                   
        -- assuming transition is possible, we process the successor of u one by one,
        -- to construct its execution graphs, and calculate the arrows from u to them.
        --   * sucArrows will contain all the new transitions from id_u to the nodes representing
        --     its successors.
        --   * newGraph_ is the total new graph accumolated from recursive calls to worker
        --
        (sucArrows,nextId,newGraph_) = foldr recurse ([],id_u + 1,graphSoFar) successors
        recurse (destination,p) (arrows_,freshId_,graphSoFar_) =
            case worker graphSoFar_ freshId_ destination traceTorecurseOn of
               (Nothing,_,_) -> (arrows_,freshId_,graphSoFar_)
               (Just id', freshId', graph' ) -> ((id_u,id',p) : arrows_, freshId', graph')
        
        -- assuming sucArrows not empty, normalize the probability in sucArrows:
        n :: Double
        n = fromIntegral $ toInteger $ length sucArrows
        totProbability = sum $ map (\(_,_,p)-> p) sucArrows
        sucArrows_ = map (\(x,y,p) -> (x,y,p/totProbability)) sucArrows
        
        -- now construct u's execution graph:
        suffixToLabel = if nextIsTau && (a /= LTS.Internal) then (LTS.Internal : trace) else trace
        newGraph = ((id_u, u, suffixToLabel) : fst newGraph_, sucArrows_ ++ snd newGraph_)

--
-- A representation of execution. It is essentially a sequence of states. In the representation,
-- it is a sequence of (s,a,p) where s is a state, a is the action that led from s to the next 
-- state in the sequence, and p is the probability that the action a was taken.
-- If the state is the final state of the original LTS model, then a is set to an artificial
-- tau transition, with p = 1.
--
type Execution state = [(state,LTS.Action,Double)]
  
--
-- Construct the set of possible executions of a given execution model. 
--
getAllExecs :: ExecutionModel -> [Execution [LTS.StateID]]
getAllExecs (ProbLTS s0 arrows labels) = result M.! s0
   where
   result = worker M.empty s0
   sucs u =  Data.List.nub [ (v,action,probability) | (action,groups)<- arrows M.! u, (v,probability) <- groups ]
   worker memo u =  case M.lookup u memo of
      Just _  ->  memo
      Nothing ->  let
                  sucessors = sucs u
                  sucessornodes = [ v | (v,_,_) <- sucessors ]
                  -- memo2  = foldr (flip worker) memo (Debug.trace ("\n## " ++ show u ++ " ==> " ++ show sucessors) sucessornodes)
                  memo2  = foldr (flip worker) memo sucessornodes
                  sigma  = [ (labels M.! u, a, p) : rho | (v,a,p) <- sucessors, rho <- memo2 M.! v ]  
                  execs_u = if null sucessors then [[(labels M.! u, LTS.Internal, 1.0)]]
                            --else Debug.trace ("\n  >> " ++ show u ++ ": " ++ show sigma) sigma   
                            else sigma                           
                  in
                  M.insert u execs_u memo2
                         
--
-- LTL-lin
--

-- Word formula [s1,s2,s3] means s1 /\ X(s2 /\ Xs3)
type WordFormula = [LTS.StateID]
-- A clause [w1,w2,w3] means w1 \/ w2 \/ w3
type Clause      = Set WordFormula
-- linear future formula [c1,c2,c3] means <>(c1 /\ <>(c2 /\ <>c3)) 
type LinFuture   = [Clause] 
data LTLLinAggr = LinFuture LinFuture
                | NegatedLinFuture LinFuture -- representing not f
                | AtLeastN Int Int           -- representing {k}>=N formula (covering at least N number of k-segments)

getClauses (LinFuture clauses) = clauses
getClauses (NegatedLinFuture clauses) = clauses
getWords linf = Data.List.nub $ concat $ map S.toList $ getClauses linf


-- Check if a given execution satisfies the LTL-lin formula.
satLTLLinAggr :: LTLLinAggr -> Execution [LTS.StateID] -> Bool
satLTLLinAggr (AtLeastN k n) execution = (length $ Data.List.nub $ getSegments sigma) >= n
    where
    sigma = [ s | ([s],a,p) <- execution ]
    getSegments sigma = if length sigma < k then []
                        else take k sigma : getSegments (tail sigma)

satLTLLinAggr (NegatedLinFuture f) execution = not $ satLTLLinAggr (LinFuture f) execution
satLTLLinAggr linf execution = linfHold
   where 
   clauses = getClauses linf
   words = getWords linf
   
   rho = [ state | ([state],a,p) <- execution ]

   linfHold = checkLinf clauses clausesLabel
   checkLinf [] z = True
   checkLinf (c1:rest) [] = False
   checkLinf clauses@(c1:restClauses) z@(satisfiedClauses:rest) 
             = 
             if c1 `elem` satisfiedClauses then checkLinf restClauses z
             else checkLinf clauses rest     
   
   clausesLabel = checkClauses [0.. (length rho - 1)]
      where
      checkClauses [] = []
      checkClauses (i:rest) = [ c | c <- clauses, check1 c (wordsLabel M.! i) ] : checkClauses rest
      check1 c validwords = not $ S.null $ (c `S.intersection` validwords)
          
   -- map i to the set of all words which hold on rho[i..]          
   wordsLabel = M.fromList $ zip [0..] (merge $ Data.List.transpose $ [ f w (checkWord w rho) | w <- words])
      where
      f w z = [ (w,indicator) | indicator <- z ]
      merge labels = [ foldr add_ S.empty s | s <- labels ]
        where
        add_ (word,True)  collected = S.insert word collected
        add_ (word,False) collected = collected

   checkWord w rho = checkw rho 
      where
      k = length w         
      checkw [] = []
      checkw z@(_:rest) = (take k z == w) : checkw rest

getSatisfyingExecutions :: LTLLinAggr -> ExecutionModel -> [Execution [LTS.StateID]]
getSatisfyingExecutions f emodel = satisfyingExecutions
    where
    allExecutions = getAllExecs emodel
    satisfyingExecutions = [ rho | rho <- allExecutions, satLTLLinAggr f rho ]

-- Calculate the analysis f on the given execution model. We use a brute approach, by quantifying
-- over all possible executions on the model.       
bruteAnalyzeTestCase :: LTLLinAggr -> ExecutionModel -> Double
bruteAnalyzeTestCase f emodel = --Debug.trace ("\n** Total number of executions: " ++ show nAll ++
                                --             "\n** Satisfying executions: " ++ show n
                                             -- ++ "\n## " ++ show satisfyingExecutions
                                --            )
                                (sum [ calcProbability rho | rho <- satisfyingExecutions ])
    where
    allExecutions = getAllExecs emodel
    satisfyingExecutions = [ rho | rho <- allExecutions, satLTLLinAggr f rho ] 
    nAll = length allExecutions
    n = length satisfyingExecutions
    calcProbability rho = product [ p | (s,a,p) <- rho ]

-- Calculate the analysis f on the given execution model. We use a much faster graph 
-- labelling approach. We impose the following restriction:
--   * if the words size in f is k >= 1, we assume the execution model to have been k-word expanded.
--   * For aggregate analysis of the form AtLeastN k n, we assume k=1. Analysis with
--     higher k is possible by first applying k-work expansion on the execution model.
--
analyzeTestCase :: LTLLinAggr -> ExecutionModel -> Double
analyzeTestCase (NegatedLinFuture f) emodel =  1 - analyzeTestCase (LinFuture f) emodel
analyzeTestCase f@(LinFuture cls) (ProbLTS u0 arrows labels) = (futuresLabel M.! u0) M.! cls
   where
   tailsOf_f = Data.List.tails cls       
       
   clauses = getClauses f
   words = getWords f
   
   nodes = M.keys labels
   
   -- maps every node u in the exec-model to the set of all the words that would hold on u
   wordsLabel =  M.fromList [ (u, S.fromList [ w | w <- words, w `Data.List.isPrefixOf` lab ]) 
                              | (u,lab) <- M.assocs labels ]
       
   -- maps every node u to the list of clauses that hold on that node u                               
   clausesLabel = M.fromList [ (u, [ c | c <- clauses, checkClause u c]) | u <- nodes]
   checkClause u c = not $ S.null $ (c `S.intersection` (wordsLabel M.! u))
      
   futuresLabel :: Map LTS.StateID (Map [Clause] Double)
   futuresLabel = worker M.empty u0
   
   sucs u =  Data.List.nub [ (v,p) | (a,groups)<- arrows M.! u, (v,p) <- groups ]
   
   worker memo u = case M.lookup u memo of
       Just _   -> memo
       Nothing  -> memo3
       where
       sucessors = sucs u
       
       -- recurse into the successors:
       memo2 = foldr (flip worker) memo [ v | (v,p)<- sucessors ]
      
       -- now add u into the memo:
       memo3  = M.insert u probs_at_u memo2
       probs_at_u = foldr calcProbability M.empty tailsOf_f
       
       calcProbability [] u_futuresLabel = u_futuresLabel
       
       -- calculating the chance of <>c
       calcProbability futureblock@[c] u_futuresLabel = M.insert futureblock p u_futuresLabel
           where
           c_holds_at_u = c `elem` (clausesLabel M.! u)
           p     = if c_holds_at_u then 1.0 else psucs
           psucs = sum [ p * ((memo2 M.! v) M.! futureblock) | (v,p) <- sucessors ]
   
   
       -- calculating the chance of <>(c /\ <>(...))    
       calcProbability futureblock@(c:nextblock) u_futuresLabel = M.insert futureblock p u_futuresLabel
           where
           c_holds_at_u = c `elem` (clausesLabel M.! u)
           p = if c_holds_at_u  then psucsNextBlock else psucsBlock
           psucsBlock = sum [ p * ((memo2 M.! v) M.! futureblock) | (v,p) <- sucessors ]
           psucsNextBlock = u_futuresLabel M.! nextblock
       
analyzeTestCase (AtLeastN 1 n) (ProbLTS u0 arrows labels) = sum [ p | (set,p) <- sets_at_u0 , S.size set >= n ]    
   where
       
   sets_at_u0 = (worker M.empty u0) M.! u0       
       
   sucs u =  Data.List.nub [ (v,p) | (a,groups)<- arrows M.! u, (v,p) <- groups ]
   
   worker memo u = case M.lookup u memo of          
      Just _   -> memo
      Nothing  -> memo3
      where
      sucessors = sucs u
      
      -- recurse into the successors:
      memo2 = foldr (flip worker) memo [ v | (v,p)<- sucessors ]
      state_repby_u = labels M.! u
      sets_at_u = if null sucessors then [ (S.singleton(state_repby_u), 1.0) ]
                  else [ (S.insert state_repby_u set, p*p') | (v,p) <- sucessors, (set,p') <- memo2 M.! v ]
                  
      merged_sets_at_u = concat $ map mergeGroup $ groups
          where
          groups = Data.List.groupBy (\(set1,_) (set2,_) -> set1 == set2) sets_at_u
          mergeGroup []    = []
          mergeGroup group = [(fst $ head $ group, sum [ p | (set,p) <- group ]) ]
     
      -- now add u into the memo:
      memo3  = M.insert u sets_at_u memo2
      
      
wordNormalize :: Int -> ExecutionModel -> ExecutionModel
-- wordNormalize ktop emode@(ProbLTS u0 arrows labels) = normalizedPLTS2
wordNormalize ktop emode@(ProbLTS u0 arrows labels) = Debug.trace (show words_labels) normalizedPLTS1
     where
       
   sucs u =  Data.List.nub [ (v,a,p) | (a,groups)<- arrows M.! u, (v,p) <- groups ]

   -- label each node u (in emodel) with words of length k that can be executed on u.
   -- We will do so by constructing a map where every u in the map's domain:
   -- 
   --    u |--> f, where f is a mapping that map length k |--> a list consisting of tuples (z,v,p)
   --
   -- where z is essentially a list of actions that can be executed on u, p is the probability
   -- that z is executed, and v is z's end node (in the original execution model).
   -- More precisely, z is a list of pairs (s,a) where s is a state in the original PLTS and a
   -- is an action that leads out from it, with tau is being used a dummy for the last a in z.
   --
   -- We will will do this recursively over k    
   words_labels :: Map LTS.StateID (Map Int [([(LTS.StateID,LTS.Action)],LTS.StateID,Double)]) 
   words_labels = topworker M.empty (ktop + 1)  -- trick... crucial
   
   topworker memo k = 
      if k<=0 then memo 
      else let memo2 = topworker memo (k-1) in worker memo2 k u0
           
   isDone memo (u,k) = case M.lookup u memo of
        Just y  ->  isJust $ M.lookup k y
        Nothing ->  False
       
   insertToMemo memo u k words = case M.lookup u memo of
       Just y  -> M.insert u (M.insert k words y) memo
       Nothing -> M.insert u (M.insert k words M.empty) memo               
       
   -- this will do the actual labeling of length k ; k>0  
   worker memo k u  = if isDone memo (u,k) then memo 
       else if k==1 then let
                         [state_u] = labels M.! u
                         words_u   = [([(state_u,LTS.Internal)],u,1.0)]
                         in 
                         insertToMemo memo u k words_u
                    
       else let -- k>1
            memo1 = worker memo (k-1) u
            sucessors = sucs u
            sucessornodes = [ v | (v,_,_) <- sucessors ]
            memo2     = foldr (\v memo_ -> worker memo_ k v) memo1 sucessornodes
            [state_u] = labels M.! u
            
            sharp = LTS.Normal "#"
            words_u = if null sucessors 
                      then {- u must be the end node -} [(replicate k (sharp,LTS.Internal), u, 1.0)]
                      else [ ((state_u,a):w, endnode, p*p')  | (v,a,p) <- sucessors, (w,endnode,p') <- memo2 M.! v M.! (k-1) ]  
            in
            insertToMemo memo2 u k words_u
                    
   mkState i = LTS.Normal $ show i
                       
                       
   -- construct the first normalized PLTS ... not intermediate words inside the edges have not been constructed                       
   normalizedPLTS1 = ProbLTS normalizedPLTS1_S0 normalizedPLTS1_arrows normalizedPLTS1_labels                 
   (normalizedPLTS1_arrows,normalizedPLTS1_labels,_,_) = constructNewExecutionModel keeptrack0 normalizedPLTS1_S0 u0
      where
      keeptrack0 = (M.singleton normalizedPLTS1_TerminalState [], 
                    M.fromList [(normalizedPLTS1_S0,[]), (normalizedPLTS1_TerminalState,[])],
                    M.empty, 
                    2) 
   
   normalizedPLTS1_S0 = mkState 1    
   normalizedPLTS1_TerminalState = mkState 0
      
   constructNewExecutionModel keeptrack x u  = 
       let
       
       -- nodesMapping map every node u in emodel to a set of newly created nodes
       -- u |-->  list of (x,w,p,v) x is the id of the new node labelled with w; the probability
       -- to traverse from u to x is p, and doing w will lead to node v (in the original emodel)
       --
       (arrows_,labels_,nodesMapping,freshId) = keeptrack
       
       -- all words that can be executed after the state u, along with the probability of taking them:
       words_on_u = if u==u0 then (words_labels M.! u) M.! ktop   
                             else (words_labels M.! u) M.! (ktop + 1) 
       
       -- new word-nodes that branch out from u, assuming u was unvisited:
       new_connections_on_u = [ (mkState x',z,v,p) | ((z,v,p),x') <- zip words_on_u [freshId ..]] 
      
       u_is_terminal = null (sucs u)
       u_was_visited = u `M.member` nodesMapping
                       
       nodesMapping2 = if u_was_visited then nodesMapping
                       else M.insert u new_connections_on_u nodesMapping  
       freshId2      = if u_was_visited then freshId else freshId + length words_on_u 
       
       isFinalNode = null $ sucs u 
       
       -- construct the set of words that branch out from u, obtain their probabilities,
       -- and assign unique ids to form fresh nodes holding these words:
       connections_on_u  = if isFinalNode then []
                           else [ if u==u0 then (x', snd $ head $ z, map fst z, v, p) 
                                           else (x', snd $ head $ z, map fst (tail z), v, p) 
                                  | (x',z,v,p) <- nodesMapping2 M.! u 
                                ]
       -- regroup the connections according to the first action that lead to them:
       connections_on_u_ = map f $ Data.List.groupBy p $ [ (a,x',p) | (x',a,_,_,p) <- connections_on_u ]
          where
          p (a,_,_) (a',_,_) = a == a'
          f group        = (fst_ $ head group, map rest_ group)
          fst_  (a,_,_)  = a
          rest_ (_,x',p) = (x',p)
       
       -- update the set of arrows and labels with the above calculated connections, 
       -- and use the new information to construct the new "keep-track" informatio
       arrows2 = if isFinalNode 
                 then M.insert x [(LTS.Internal,[(normalizedPLTS1_TerminalState,1.0)])] arrows_
                 else M.insert x connections_on_u_  arrows_
       labels2 = foldr f labels_ connections_on_u 
          where
          f (x',_,word,_,_) labelsTillNow = M.insert x' word labelsTillNow
          
       keeptrack2 = (arrows2,labels2,nodesMapping2,freshId2)      
              
       in
       if u_was_visited then {- no recursion is needed -} keeptrack2
       else let
            f (x',_,_,v,_) track = constructNewExecutionModel track x' v
            {- recurse over all nodes connected to by the words on u -}
            keeptrack3 = foldr f keeptrack2 connections_on_u 
            in 
            keeptrack3
            
            
   fillEdges keeptrack x =
      let
      (nodesMapping1,labels1,connections1,freshId1) = keeptrack  
      
      -- new id for x: (will only be evaluated if x is not yet visited)
      new_x :: Int
      new_x = freshId1
      freshId2 = freshId1 + 1
      words_x = normalizedPLTS1_labels M.! x
      nodesMapping2 = M.insert x new_x nodesMapping1
      labels2 = M.insert (mkState new_x) words_x labels1
      
      keeptrack2 = (nodesMapping2,labels2,connections1,freshId2)
       
      sucs = Data.List.nub [ (y,a,p) | (a,groups)<- normalizedPLTS1_arrows M.! x, (y,p) <- groups ]
      -- recursively fill the sucessors first:
      keeptrack3 = foldr (\y memo -> fillEdges memo y) keeptrack2 [ y | (y,_,_) <- sucs ]
      
      (nodesMapping3,labels3,connections3,freshId3) = keeptrack3
      

      mk_intermediates k word = if length word <= k then []
                                else take k word : mk_intermediates k (drop 1 word)         
                                 
      -- to construct a single chain from x to its original successor y:                       
      mk_chain freshId (y,a,p) =  if null intermediate_words then (freshId, [ (new_x, words_x, new_y, a, p) ])
          else
          (newFreshId, 
            (new_x, words_x, first_intermediate_id, LTS.Internal, p) :
              -- for now encoding transitions to intermediate nodes as tau; TODO: fix this
              [ (id1, w1, id2, LTS.Internal, 1.0) | ((w1,id1),(_,id2)) <- intermediate_words_chain ]
              ++
              [(last_intermediate_id, last_intermediate_word, new_y, a, 1.0)]
          )
          where
          new_y = nodesMapping3 M.! y
          combinedWord = words_x ++ normalizedPLTS1_labels M.! y
          intermediate_words = zip (mk_intermediates ktop (drop 1 combinedWord)) [freshId ..]                 
          intermediate_words_chain = zip intermediate_words (drop 1 intermediate_words)   
          (_,first_intermediate_id)  = head intermediate_words    
          (last_intermediate_word,last_intermediate_id) = last intermediate_words  
          newFreshId = freshId + length intermediate_words    
      
      -- construct all the intermediate chains that flow out from x, then flatten to new edges:
      (freshId4,newEdges) = foldr combine (freshId3, []) sucs
         where
         combine (y,a,p) (id_,collectedEdges) = (newid, chain ++ collectedEdges)
             where
             (newid,chain) = mk_chain id_ (y,a,p)
      
       
      (outgoings_x,otherNewEdges) = Data.List.partition p newEdges
         where
         p (id_,_,_,_,_) = id_ == new_x
           
                  
      outgoings_x_grouped = map f $ Data.List.groupBy sameAction outgoings_x
         where
         sameAction (_,_,_,a,_) (_,_,_,a',_) = (a == a')
         f group        = (fst_ $ head group, map rest_ group)
         fst_  (_,_,_,a,_)  = a
         rest_ (_,_,x',_,p) = (mkState x',p)
  
                       
      labels4 = foldr extend labels3 newEdges
         where
         extend (nd,word,_,_,_) labels = if nd == new_x then labels else M.insert (mkState nd) word labels
      
      connections4 = M.insert (mkState new_x) outgoings_x_grouped connections3
      connections4b = foldr extend connections4 otherNewEdges
         where
         extend (nd,_,suc,a,p) edges = M.insert (mkState nd) [(a,[(mkState suc,1.0)])] edges
      
      nodesMapping4 = nodesMapping3
      
      in
      if x `M.member` nodesMapping1 then keeptrack
      else (nodesMapping4, labels4, connections4b, freshId4)
       
   -- construct the final normalized PLTS ... with intermediate words added                   
   normalizedPLTS2 = ProbLTS normalizedPLTS2_S0 normalizedPLTS2_arrows normalizedPLTS2_labels             
   (nodesMapping,normalizedPLTS2_labels,normalizedPLTS2_arrows,_) = fillEdges keeptrack0 normalizedPLTS1_S0
      where
      keeptrack0 = (M.empty, M.empty, M.empty, 0) 

   normalizedPLTS2_S0 = mkState (nodesMapping M.! normalizedPLTS1_S0)

--
-- An example for testing the functions
--
example_EX1_ = ProbLTS s0 (M.fromList arrows) (M.fromList statelabels)
    where
    states = map LTS.Normal ["s0","s1","s2","s3","s4","s5","s6"]
    [s0,s1,s2,s3,s4,s5,s6] = states
    tau = LTS.Internal
    a = LTS.Send "a"
    b = LTS.Receive "b"
    c = LTS.Send "c"
    arrows =  [ 
      (s0, [(a, [(s1,0.5),(s2,0.5)])]),
      (s1, [(b, [(s0,0.9),(s3,0.1)])]),
      (s2, [(b, [(s0,1.0)]), (c, [(s2,1.0)])]), 
      (s3, [(tau, [(s4,0.9),(s5,0.1)])]),
      (s4, [(a, [(s1,1.0)])]), 
      (s5, [(c, [(s6,1.0)])]),
      (s6, [])
      ]
    statelabels = map (\s-> (s,())) states
  
-- some example test cases    
tc1_ = [LTS.Send "a", LTS.Receive "b", LTS.Send "a"]    
tc2_ k = LTS.Send "a" : concat (replicate k [LTS.Receive "b", LTS.Send "a"])
tc3_ k = concat (replicate k [a,b]) ++ [a] ++ replicate k c ++ [b,a,b,c]  -- total length =3b + 5
   where
   a = LTS.Send "a"
   b = LTS.Receive "b"
   c = LTS.Send "c"
    
word_ w    = map LTS.Normal w
clause1_ w = S.fromList $ [word_ w]
example_formula1 = LinFuture [clause1_ ["s3","s4"] , clause1_ ["s0","s2"]]
example_formula2 = LinFuture [clause1_ ["s4"] , clause1_ ["s2"]]

example_formula3 = LinFuture [clause1_ ["s1"] , clause1_ ["s0"]]


   
          
