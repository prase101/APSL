AISL stuffs:

  * Grammar.cf   : BNFC syntax specification of AMSL
  * /Grammar     : contains generated files from BNFC
  * /DataType    : high level datatypes representing an AISL module, the main 
                   part of this representation is that an AISL module defines
                   a labelled transition system (LTS).
  * Compiler.hs  : top-level AISL parser
