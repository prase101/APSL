{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}

-- | Representation of a labelled transition system derived from an AISL specification, of which the set of observable
--   actions consists of 'receive' and 'send' operations for messages with a textual identifier.
module Language.APSL.AISL.DataType.LTS where

import           Data.Map (Map)
import qualified Data.Map as M

import           Data.Set (Set)
import qualified Data.Set as S
import Data.List (nub)
import GHC.Generics
import Data.Serialize

import Language.APSL.AMSL.DataType.Module

-- | A state can either have a string label, be an intermediate output state, or be an unnamed 'exit' state that is
--   implicit in the specification.
data StateID = Normal String | Intermediate (String,String,String,Integer) | Exit
  -- WP: adding extra info into intermediate states.
  -- Orig: data StateID = Normal String | Intermediate Integer | Exit
  deriving (Eq, Ord, Show, Generic)

instance Serialize StateID

getStateName :: StateID -> String
getStateName (Normal name) = name
getStateName (Intermediate (from,a,to,i)) = "state_" ++ show i ++ " (" ++ from ++ "...." ++ show i ++ "--" ++ a ++ "->" ++ to ++ ")"
getStateName  Exit = "_Exit"

getStateUniqueName :: StateID -> String
getStateUniqueName (Intermediate (from,a,to,i)) = show i
getStateUniqueName s = getStateName s


-- | Actions are either internal (the 'anytime' construct in AISL), observable send/receive operations, or the
--   reception of a message for which no other action is defined.
data Action = Send MessageLabel | Receive MessageLabel | Internal | ReceiveUnexpected
    deriving (Eq, Ord, Show, Generic)

instance Serialize Action

-- | Outgoing transitions from one state to others, depending on the action that will occur.
--   Nondeterminism is modelled by one action mapping to multiple states.
type Transitions = Map Action [StateID]

-- | A system consists of an initial state and a map of transitions.
data LTS = LTS {
    initState   :: StateID,
    transitions :: Map StateID Transitions
} deriving (Show)

-- | Return the set of all states of an LTS.
allStates :: LTS -> [StateID]
-- allStates lts = M.keys $ transitions lts --> this would exclude the Exit state
allStates lts = nub $ concat [[from,to] | (from,_,to) <- allTransitions lts ]

-- | A single LTS transition is identified by an (action, from, to)-triple.
type Transition = (StateID, Action, StateID)

-- | Set of all possible transitions defined in the LTS.
--
allTransitions :: LTS -> [Transition]
allTransitions LTS{..} = [(from, act, to) | (from, ts) <- M.assocs transitions,
                                            (act, tos) <- M.assocs ts,
                                            to <- tos]



-- | The set of all outgoing transitions from a given state.
--
outGoingTransitions :: LTS -> StateID -> [Transition]
outGoingTransitions lts Exit   = []
outGoingTransitions lts state1 = [ (state1,act,state2) |  (act,sinks) <- M.assocs outgoings, state2 <- sinks ]
    where
    outgoings =  case M.lookup state1 (transitions lts) of
                   Just z -> z
                   _      -> M.empty

-- | The set of all outgoing send-transitions.
--
outGoingSends :: LTS -> StateID -> [Transition]
outGoingSends lts state = filter isSend (outGoingTransitions lts state)
   where
   isSend (_,Send a,_) = True
   isSend _            = False

-- | The set of all outgoing receive-transitions.
--
outGoingReceives :: LTS -> StateID -> [Transition]
outGoingReceives lts state = filter isReceive (outGoingTransitions lts state)
   where
   isReceive (_,Receive a,_) = True
   isReceive _            = False

-- | The set of all outgoing "ReceiveUnexpected" transitions. When such a transition is
-- present on a state, the state is assumed to have built-in ability to handle an invalid
-- message.
outGoingUnexpectedTransitions :: LTS -> StateID -> [Transition]
outGoingUnexpectedTransitions lts state = filter (\(_,act,_)-> act == ReceiveUnexpected) (outGoingTransitions lts state)

-- | Check if a is an allowed send-transition from a given state. This does not
-- take tau-transitions into account.
canSend :: LTS -> StateID -> MessageLabel -> Bool
canSend lts state a = not $ null $ filter match $ outGoingSends lts state
    where
    match (_,tr,_) = tr == Send a

-- | Check if a is an allowed receive-transition from a given state. This does not
-- take tau-transitions into account.
canReceive :: LTS -> StateID -> MessageLabel -> Bool
canReceive lts state a = not $ null $ filter match $ outGoingReceives lts state
    where
    match (_,tr,_) = tr == Receive a

canReceiveUnexpected :: LTS -> StateID -> Bool
canReceiveUnexpected lts state = not $ null $ outGoingUnexpectedTransitions lts state

-- | The set of all direct neighbors reachable through a single transitions.
--
nextStates :: LTS -> StateID -> [StateID]
nextStates lts state1 = nub [ state2 | (_,_,state2) <- outGoingTransitions lts state1 ]

-- | The set of all states that can be reaching from a given state s, by doing a transition labelled with act1
-- (either send or receive)
--
nextStatesAfterTransition :: LTS -> StateID -> Action -> [StateID]
nextStatesAfterTransition lts state1 act1 = nub [ state2 |  (_,act2,state2) <- outGoingTransitions lts state1, act2==act1 ]


-- | The set of all neigbors that are reachable with a single internal transition.
--
nextTauStates :: LTS -> StateID -> [StateID]
nextTauStates lts state = nextStatesAfterTransition lts state Internal


-- | Return the set of all states which are reachable with zero or more tau-steps from a given state s.
-- (note that it thus will always include s itself)
--
reachableTauStates :: LTS -> StateID -> [StateID]
reachableTauStates lts state1 = nub $ worker [state1] state1
   where
   worker :: [StateID] -> StateID -> [StateID]
   worker visited st1 =  st1 : concat [ worker (st2 : visited) st2
                                       |  st2 <- nextTauStates lts st1 ,
                                          st2 `notElem` visited ]


-- | All the message labels that are used in some action within the LTS.
possibleMessages :: LTS -> [MessageLabel]
possibleMessages lts =  [getMsg m | m <- possibleMessages_  lts ]
 where
    getMsg (Send msg)    = msg
    getMsg (Receive msg) = msg

-- | The set of labels of all send-transitions.
possibleSendMsgs :: LTS -> [MessageLabel]
possibleSendMsgs lts = [ msg | Send msg <- possibleMessages_ lts ]

-- | The set of labels of all receive-transitions.
possibleReceiveMsgs :: LTS -> [MessageLabel]
possibleReceiveMsgs lts = [ msg | Receive msg <- possibleMessages_ lts ]

possibleMessages_ :: LTS -> [Action]
possibleMessages_ lts = nub [act | (_, act, _) <- allTransitions lts, isSendOrReceive act ]
  where
  isSendOrReceive (Send msg)    = True
  isSendOrReceive (Receive msg) = True
  isSendOrReceive  _            = False


-- | Check if there is a path in the LTS that goes from the state s1 to the state s2.
canReach :: LTS -> StateID -> StateID -> Bool
canReach lts s1 s2 = s2 `elem` reachableStatesOf lts s1


-- | Return the set of all states in the LTS that are reachable from a given
-- state s.
reachableStatesOf :: LTS -> StateID -> [StateID]
reachableStatesOf lts stateOrigin = dfs [] stateOrigin
   where
   dfs visited s
      | s `elem` visited = visited
      | otherwise = let
                    nexts = [ t | t <- nextStates lts s, t `notElem` visited ]
                    visited' = s:visited
                    in
                    foldr (flip dfs) visited' nexts

-- for test
lts_test1 = LTS { initState = s0, transitions = arrows }
  where
  [s0,s1,s2,s3] = map Normal ["s0","s1","s2","s3"]
  tau = Internal
  x = Send "X"
  y = Send "Y"
  arrows = M.fromList [
    (s0, M.fromList [ (tau, [s0,s1]), (x, [s0,s1,s2]), (y, [s2,s3]) ] ) ,
    (s1, M.fromList [ (tau, [s0,s2]) ] ) ,
    (s2, M.fromList [ (ReceiveUnexpected, [s3])]) ,
    (s3, M.empty)
    ]

