{-# LANGUAGE RecordWildCards #-}

-- | Representation of an AISL module.
module Language.APSL.AISL.DataType.Module where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Module (Message, messageName)
import Language.APSL.AISL.DataType.LTS

-- | An AISL module, which includes the AMSL/AISL modules it imported.
data Module = Module {
    -- | The name of the module.
    moduleName :: String,

    -- | The messages that are used within this system, associated with their module environments.
    --   Their names are used as action labels with the LTS's.
    messages   :: [(Message, Env)],

    -- | An LTS associated with each actor described in this definition.
    actors     :: [(String, LTS)]
}

instance Show Module where
    show Module{..} = show (moduleName, map (messageName . fst) messages, actors)

