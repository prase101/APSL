module Language.APSL.AISL.DataType.TestLTS where

import Language.APSL.AISL.DataType.LTS 

import Language.APSL.AISL.Compiler hiding (actors)
import Language.APSL.AISL.DataType.Module

test1 dir = do {
   Right aisl <- compile dir "simple1.aisl" ;
   return $ head [lts_ | (name,lts_) <- actors aisl, name == "Server1" ] 
}

test2 dir = do {
   Right aisl <- compile dir "websocket.aisl" ;
   return $ head [lts_ | (name,lts_) <- actors aisl, name == "WebsocketServer" ] 
}

{- Websocket server internal representation:

LTS {initState = Normal "Connecting", 
     transitions = fromList [
         (Normal "Connecting",fromList [(Receive "ClientOpenHandshake",[Intermediate ("Connecting","ServerOpenHandshake","Open",10)])]),

         (Normal "Open",fromList [(Receive "MaskedMessageFrame",[Normal "Open"]),
                                  (Receive "MaskedMessageStartFrame",[Normal "ReceivingContinuation"]),
                                  (Internal,[Intermediate ("Open","MessageFrame","Open",11),
                                             Intermediate ("Open","MessageStartFrame","SendingContinuation",12),
                                             Intermediate ("Open","MessageFrame","_Exit",13)])]),

         (Normal "ReceivingContinuation",fromList [(Receive "MaskedContinuationFrame",[Normal "ReceivingContinuation"]),
                                                   (Receive "MaskedMessageEndFrame",[Normal "Open"]),
                                                   (Internal, [ Intermediate ("ReceivingContinuation","MessageFrame","ReceivingContinuation",14),
                                                                Intermediate ("ReceivingContinuation","MessageStartFrame","SendingReceivingContinuation",15)])]),

         (Normal "SendingContinuation",fromList [(Receive "MaskedMessageFrame",[Normal "SendingContinuation"]),(Receive "MaskedMessageStartFrame",[Normal "SendingReceivingContinuation"]),(Internal,[Intermediate ("SendingContinuation","ContinuationFrame","SendingContinuation",16),Intermediate ("SendingContinuation","MessageEndFrame","Open",17)])]),(Normal "SendingReceivingContinuation",fromList [(Receive "MaskedContinuationFrame",[Normal "SendingReceivingContinuation"]),(Receive "MaskedMessageEndFrame",[Normal "SendingContinuation"]),(Internal,[Intermediate ("SendingReceivingContinuation","ContinuationFrame","SendingReceivingContinuation",18),Intermediate ("SendingReceivingContinuation","MessageEndFrame","ReceivingContinuation",19)])]),

         (Intermediate ("Connecting","ServerOpenHandshake","Open",10),fromList [(Send "ServerOpenHandshake",[Normal "Open"])]),

         (Intermediate ("Open","MessageFrame","Open",11),fromList [(Send "MessageFrame",[Normal "Open"])]),
         (Intermediate ("Open","MessageFrame","_Exit",13),fromList [(Send "MessageFrame",[Exit])]),

         (Intermediate ("Open","MessageStartFrame","SendingContinuation",12),fromList [(Send "MessageStartFrame",[Normal "SendingContinuation"])]),

         (Intermediate ("ReceivingContinuation","MessageFrame","ReceivingContinuation",14),fromList [(Send "MessageFrame",[Normal "ReceivingContinuation"])]),
         (Intermediate ("ReceivingContinuation","MessageStartFrame","SendingReceivingContinuation",15),fromList [(Send "MessageStartFrame",[Normal "SendingReceivingContinuation"])]),
         (Intermediate ("SendingContinuation","ContinuationFrame","SendingContinuation",16),fromList [(Send "ContinuationFrame",[Normal "SendingContinuation"])]),
         (Intermediate ("SendingContinuation","MessageEndFrame","Open",17),fromList [(Send "MessageEndFrame",[Normal "Open"])]),
         (Intermediate ("SendingReceivingContinuation","ContinuationFrame","SendingReceivingContinuation",18),fromList [(Send "ContinuationFrame",[Normal "SendingReceivingContinuation"])]),
         (Intermediate ("SendingReceivingContinuation","MessageEndFrame","ReceivingContinuation",19),fromList [(Send "MessageEndFrame",[Normal "ReceivingContinuation"])])]
}
 
-}