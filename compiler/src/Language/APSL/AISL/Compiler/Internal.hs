{-# LANGUAGE TupleSections, RecordWildCards, PatternGuards #-}
module Language.APSL.AISL.Compiler.Internal (CompiledUnit(..), ImportUnit(..), ASTCompileError(..), transModule) where

import qualified Language.APSL.AISL.Grammar.Abs as G

import Language.APSL.AISL.DataType.LTS

import           Data.Map (Map)
import qualified Data.Map as M

import Control.Monad
import Control.Monad.Trans.State
import Data.Maybe


data CompiledUnit = CompiledUnit {
    moduleName :: String,
    imports    :: [ImportUnit],
    actors     :: [(String, LTS)]
}

data ImportUnit = ImportAll FilePath | ImportList [String] FilePath

data ASTCompileError 
    = InitStateRequired String
    | MultipleInitStates String [String]
    | InternalError String
    deriving (Show)

-- Contains a result or an error, and maintains a uniqueness generator.
newtype Result a = Result (State Integer (Either ASTCompileError a))

instance Functor Result where
    fmap = liftM

instance Applicative Result where
    pure x = Result $ return $ Right x
    (<*>) = ap

instance Monad Result where
    fail err = Result $ return $ Left $ InternalError err

    Result m >>= f = Result $ m >>= f'
     where 
        f' (Left err) = return $ Left err
        f' (Right x)  = let Result m = f x in m


uniqueID :: Result Integer
uniqueID = Result $ state (\i -> (Right i, i+1))

compileError :: ASTCompileError -> Result a
compileError = Result . return . Left

transModule :: G.Module -> Either ASTCompileError CompiledUnit
transModule m = let Result r = transModule' m in evalState r 0

transModule' :: G.Module -> Result CompiledUnit
transModule' (G.Module (G.LName (_, moduleName)) importList decls) = do
    imports <- sequence $ map transImport importList
    actors  <- sequence $ map transDecl decls
    return CompiledUnit {..}

transImport :: G.Import -> Result ImportUnit
transImport (G.Import G.ImportAll path) = liftM ImportAll $ parseLit path
transImport (G.Import (G.ImportSymbols syms) path) = liftM (ImportList (map getName syms)) $ parseLit path
 where 
    getName (G.ImportSymbol (G.CName (_, name))) = name

parseLit :: G.TextLit -> Result String
parseLit (G.TextLit (_, str)) 
        | '\'':str' <- str, last str' == '\'' = return $ init str'
        | otherwise = fail $ "Invalid string literal: " ++ str


transDecl :: G.Decl -> Result (String, LTS)
transDecl (G.ActorDecl (G.CName (_,name)) states) = do
    (inits, tss) <- liftM unzip $ sequence $ map transState states
    let transitions = combineStates tss
    case catMaybes inits of
        []               -> compileError $ InitStateRequired name
        [initState] -> do
            let lts = LTS {..}
            verifyLTS lts
            return (name, lts)
        is -> compileError $ MultipleInitStates name $ map show is

transState :: G.State -> Result (Maybe StateID, Map StateID Transitions)
transState s = case s of
    G.InitState (G.CName (_,n)) ts -> liftM (Just $ Normal n,) $ transState' n ts
    G.State (G.CName (_,n)) ts     -> liftM (Nothing,) $ transState' n ts
 where
    transState' n ts = do
        let state = Normal n
        (nexts, outputs) <- liftM unzip $ sequence $ map (transTransition state) ts
        return $ M.insert state (M.fromListWith (++) nexts) $ combineStates outputs

combineStates :: [Map StateID Transitions] -> Map StateID Transitions
combineStates = foldr (M.unionWith (M.unionWith (++))) M.empty

transTransition :: StateID -> G.Transition -> Result ((Action, [StateID]), Map StateID Transitions)
transTransition state (G.Transition sit opts) = do
    (nexts, outputs) <- liftM unzip $ transOptions opts
    return ((fromSit sit, nexts), combineStates outputs)
 where
    fromSit (G.OnMessage (G.CName (_,n))) = Receive n
    fromSit G.Otherwise = ReceiveUnexpected
    fromSit G.AnyTime = Internal

    transOptions (G.TFinal as ns) = liftM (:[]) $ transOption state as ns
    transOptions (G.TChoice as ns alt) = do
        o  <- transOption state as ns
        os <- transOptions alt
        return $ o:os

transOption :: StateID -> [G.Action] -> G.NextState -> Result (StateID, Map StateID Transitions)
transOption state sends next = transOption' sends
 where
    finalState = case next of 
        G.NextState (G.CName (_,n)) -> Normal n
        G.SameState -> state
        G.QuitState -> Exit

    transOption' [] = return (finalState, M.empty)
    transOption' ((G.Send (G.CName (_,output))):xs) = do
        -- WP: adding extra info to Intermediate state
        -- istate <- liftM Intermediate uniqueID
        -- 
        id <- uniqueID
        istate <- return $ Intermediate (getStateName state,output, getStateUniqueName finalState, id)
        (followUp, chain) <- transOption' xs
        let chain' = M.insert istate (M.singleton (Send output) [followUp]) chain
        return (istate, chain')

verifyLTS :: LTS -> Result ()
verifyLTS _ = return () -- TODO    
