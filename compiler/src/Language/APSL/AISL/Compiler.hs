{-# LANGUAGE RecordWildCards, TupleSections #-}

-- | Compiling AISL files.
module Language.APSL.AISL.Compiler (
    CompiledUnit (..),
    CompileError (..),
    ASTCompileError (..),
    compile,
    compileString
) where

import Language.APSL.Base
import Language.APSL.AISL.Compiler.Internal
import Language.APSL.AISL.DataType.Module
import qualified Language.APSL.AMSL.DataType.Module as AMSLModule
import qualified Language.APSL.AMSL.Compiler as AMSLCompiler

import qualified Language.APSL.AISL.Grammar.Abs as G
import Language.APSL.AISL.Grammar.Par (pModule, myLexer)
import Language.APSL.AISL.Grammar.ErrM (Err(..))

import Prelude hiding (catch)
import Control.Monad
import Control.Exception (catch)



data CompileError
    = SyntaxError String 
    | FileReadError FilePath IOError
    | ASTError ASTCompileError
    | UnknownMessage String
    | ImportedAMSLError FilePath AMSLCompiler.CompileError
    deriving (Show)


-- | Compile an AISL file into a module; the first argument is an import base path and the second is the path of the 
--   source file relative to it.
compile :: FilePath -> FilePath -> IO (Either CompileError Module)
compile dir path = do 
    unit' <- (liftM compileString $ readFile (dir ++ "/" ++ path)) 
                `catch` (return . Left . FileReadError path)
    case unit' of
        Left err -> return $ Left err
        Right (CompiledUnit {..}) -> do
            messages' <- liftM (liftM concat . sequence) $ sequence $ map compileImport imports
            case messages' of
                Left err -> return $ Left err
                Right messages -> return $ Right $ Module {..}

 where
    compileImport (ImportAll p) = compileImport' p
    compileImport (ImportList l p) = liftM2 (filter isImported) $ compileImport' p
     where isImported (m, _) = unTypeName (AMSLModule.messageName m) `elem` l

    compileImport' p = do
        result <- AMSLCompiler.compile dir p
        case result of 
            Left err -> return $ Left $ ImportedAMSLError p err
            Right m  -> return $ Right $ map (, AMSLModule.moduleEnv m) $ AMSLModule.messages m

    liftM2 f = liftM (liftM f)


-- | Parse a String to an AST, then compile it further.
compileString :: String -> Either CompileError CompiledUnit
compileString inp =
    case pModule $ myLexer inp of
        Bad err -> Left $ SyntaxError err
        Ok  ast -> either (Left . ASTError) Right $ transModule ast