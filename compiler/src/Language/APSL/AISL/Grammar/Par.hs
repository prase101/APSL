{-# OPTIONS_GHC -w #-}
{-# OPTIONS_GHC -fno-warn-incomplete-patterns -fno-warn-overlapping-patterns #-}
module Language.APSL.AISL.Grammar.Par where
import Language.APSL.AISL.Grammar.Abs
import Language.APSL.AISL.Grammar.Lex
import Language.APSL.AISL.Grammar.ErrM
import qualified Data.Array as Happy_Data_Array
import qualified Data.Bits as Bits
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.7

data HappyAbsSyn 
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 (CName)
	| HappyAbsSyn5 (LName)
	| HappyAbsSyn6 (TextLit)
	| HappyAbsSyn7 (Module)
	| HappyAbsSyn8 (Import)
	| HappyAbsSyn9 ([Import])
	| HappyAbsSyn10 (ImportList)
	| HappyAbsSyn11 (ImportSymbol)
	| HappyAbsSyn12 ([ImportSymbol])
	| HappyAbsSyn13 (Decl)
	| HappyAbsSyn14 ([Decl])
	| HappyAbsSyn15 (State)
	| HappyAbsSyn16 ([State])
	| HappyAbsSyn17 (Transition)
	| HappyAbsSyn18 (TransitionOptions)
	| HappyAbsSyn19 ([Transition])
	| HappyAbsSyn20 (Situation)
	| HappyAbsSyn21 (Action)
	| HappyAbsSyn22 ([Action])
	| HappyAbsSyn23 (NextState)

{- to allow type-synonyms as our monads (likely
 - with explicitly-specified bind and return)
 - in Haskell98, it seems that with
 - /type M a = .../, then /(HappyReduction M)/
 - is not allowed.  But Happy is a
 - code-generator that can just substitute it.
type HappyReduction m = 
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> m HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> m HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> m HappyAbsSyn
-}

action_0,
 action_1,
 action_2,
 action_3,
 action_4,
 action_5,
 action_6,
 action_7,
 action_8,
 action_9,
 action_10,
 action_11,
 action_12,
 action_13,
 action_14,
 action_15,
 action_16,
 action_17,
 action_18,
 action_19,
 action_20,
 action_21,
 action_22,
 action_23,
 action_24,
 action_25,
 action_26,
 action_27,
 action_28,
 action_29,
 action_30,
 action_31,
 action_32,
 action_33,
 action_34,
 action_35,
 action_36,
 action_37,
 action_38,
 action_39,
 action_40,
 action_41,
 action_42,
 action_43,
 action_44,
 action_45,
 action_46,
 action_47,
 action_48,
 action_49,
 action_50,
 action_51,
 action_52,
 action_53,
 action_54,
 action_55,
 action_56,
 action_57,
 action_58 :: () => Int -> ({-HappyReduction (Err) = -}
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (Err) HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (Err) HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> (Err) HappyAbsSyn)

happyReduce_1,
 happyReduce_2,
 happyReduce_3,
 happyReduce_4,
 happyReduce_5,
 happyReduce_6,
 happyReduce_7,
 happyReduce_8,
 happyReduce_9,
 happyReduce_10,
 happyReduce_11,
 happyReduce_12,
 happyReduce_13,
 happyReduce_14,
 happyReduce_15,
 happyReduce_16,
 happyReduce_17,
 happyReduce_18,
 happyReduce_19,
 happyReduce_20,
 happyReduce_21,
 happyReduce_22,
 happyReduce_23,
 happyReduce_24,
 happyReduce_25,
 happyReduce_26,
 happyReduce_27,
 happyReduce_28,
 happyReduce_29,
 happyReduce_30,
 happyReduce_31,
 happyReduce_32,
 happyReduce_33 :: () => ({-HappyReduction (Err) = -}
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (Err) HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (Err) HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> (Err) HappyAbsSyn)

happyExpList :: Happy_Data_Array.Array Int Int
happyExpList = Happy_Data_Array.listArray (0,86) ([0,0,2,0,0,4096,0,0,0,0,0,0,0,0,4,0,0,8192,0,0,0,0,0,0,0,32768,0,0,0,0,0,512,0,0,128,4096,0,0,0,0,16384,0,0,256,0,0,0,0,0,0,0,0,0,0,0,0,4096,0,0,2048,0,0,4096,0,0,16384,0,0,0,0,0,0,0,0,0,0,0,0,0,8192,513,0,0,0,0,0,0,0,0,512,0,0,4096,0,0,1024,0,0,4096,0,0,1024,0,0,0,0,9216,80,0,0,0,0,9216,80,0,0,0,0,4096,0,0,0,0,0,0,0,0,0,4096,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2048,392,0,0,0,0,0,32,0,0,0,0,0,4096,0,0,0,0,0,4096,0,0,0,0,0,0,0,4096,0,0,0,0,0
	])

{-# NOINLINE happyExpListPerState #-}
happyExpListPerState st =
    token_strs_expected
  where token_strs = ["error","%dummy","%start_pModule","CName","LName","TextLit","Module","Import","ListImport","ImportList","ImportSymbol","ListImportSymbol","Decl","ListDecl","State","ListState","Transition","TransitionOptions","ListTransition","Situation","Action","ListAction","NextState","'*'","','","'actor'","'anytime'","'continue'","'do'","'end'","'from'","'import'","'init'","'interactions'","'module'","'next'","'on'","'or'","'otherwise'","'quit'","'send'","'state'","'where'","'with'","L_CName","L_LName","L_TextLit","%eof"]
        bit_start = st * 48
        bit_end = (st + 1) * 48
        read_bit = readArrayBit happyExpList
        bits = map read_bit [bit_start..bit_end - 1]
        bits_indexed = zip bits [0..47]
        token_strs_expected = concatMap f bits_indexed
        f (False, _) = []
        f (True, nr) = [token_strs !! nr]

action_0 (34) = happyShift action_4
action_0 (7) = happyGoto action_3
action_0 _ = happyFail (happyExpListPerState 0)

action_1 (45) = happyShift action_2
action_1 _ = happyFail (happyExpListPerState 1)

action_2 _ = happyReduce_1

action_3 (48) = happyAccept
action_3 _ = happyFail (happyExpListPerState 3)

action_4 (35) = happyShift action_5
action_4 _ = happyFail (happyExpListPerState 4)

action_5 (46) = happyShift action_7
action_5 (5) = happyGoto action_6
action_5 _ = happyFail (happyExpListPerState 5)

action_6 (9) = happyGoto action_8
action_6 _ = happyReduce_6

action_7 _ = happyReduce_2

action_8 (32) = happyShift action_11
action_8 (8) = happyGoto action_9
action_8 (14) = happyGoto action_10
action_8 _ = happyReduce_14

action_9 _ = happyReduce_7

action_10 (26) = happyShift action_18
action_10 (13) = happyGoto action_17
action_10 _ = happyReduce_4

action_11 (24) = happyShift action_16
action_11 (45) = happyShift action_2
action_11 (4) = happyGoto action_12
action_11 (10) = happyGoto action_13
action_11 (11) = happyGoto action_14
action_11 (12) = happyGoto action_15
action_11 _ = happyFail (happyExpListPerState 11)

action_12 _ = happyReduce_10

action_13 (31) = happyShift action_21
action_13 _ = happyFail (happyExpListPerState 13)

action_14 (25) = happyShift action_20
action_14 _ = happyReduce_11

action_15 _ = happyReduce_9

action_16 _ = happyReduce_8

action_17 _ = happyReduce_15

action_18 (45) = happyShift action_2
action_18 (4) = happyGoto action_19
action_18 _ = happyFail (happyExpListPerState 18)

action_19 (44) = happyShift action_25
action_19 _ = happyFail (happyExpListPerState 19)

action_20 (45) = happyShift action_2
action_20 (4) = happyGoto action_12
action_20 (11) = happyGoto action_14
action_20 (12) = happyGoto action_24
action_20 _ = happyFail (happyExpListPerState 20)

action_21 (47) = happyShift action_23
action_21 (6) = happyGoto action_22
action_21 _ = happyFail (happyExpListPerState 21)

action_22 _ = happyReduce_5

action_23 _ = happyReduce_3

action_24 _ = happyReduce_12

action_25 (16) = happyGoto action_26
action_25 _ = happyReduce_18

action_26 (30) = happyShift action_28
action_26 (33) = happyShift action_29
action_26 (42) = happyShift action_30
action_26 (15) = happyGoto action_27
action_26 _ = happyFail (happyExpListPerState 26)

action_27 _ = happyReduce_19

action_28 _ = happyReduce_13

action_29 (42) = happyShift action_32
action_29 _ = happyFail (happyExpListPerState 29)

action_30 (45) = happyShift action_2
action_30 (4) = happyGoto action_31
action_30 _ = happyFail (happyExpListPerState 30)

action_31 (43) = happyShift action_34
action_31 _ = happyFail (happyExpListPerState 31)

action_32 (45) = happyShift action_2
action_32 (4) = happyGoto action_33
action_32 _ = happyFail (happyExpListPerState 32)

action_33 (43) = happyShift action_36
action_33 _ = happyFail (happyExpListPerState 33)

action_34 (19) = happyGoto action_35
action_34 _ = happyReduce_23

action_35 (27) = happyShift action_40
action_35 (30) = happyShift action_41
action_35 (37) = happyShift action_42
action_35 (39) = happyShift action_43
action_35 (17) = happyGoto action_38
action_35 (20) = happyGoto action_39
action_35 _ = happyFail (happyExpListPerState 35)

action_36 (19) = happyGoto action_37
action_36 _ = happyReduce_23

action_37 (27) = happyShift action_40
action_37 (30) = happyShift action_47
action_37 (37) = happyShift action_42
action_37 (39) = happyShift action_43
action_37 (17) = happyGoto action_38
action_37 (20) = happyGoto action_39
action_37 _ = happyFail (happyExpListPerState 37)

action_38 _ = happyReduce_24

action_39 (29) = happyShift action_46
action_39 (18) = happyGoto action_45
action_39 _ = happyFail (happyExpListPerState 39)

action_40 _ = happyReduce_27

action_41 _ = happyReduce_17

action_42 (45) = happyShift action_2
action_42 (4) = happyGoto action_44
action_42 _ = happyFail (happyExpListPerState 42)

action_43 _ = happyReduce_26

action_44 _ = happyReduce_25

action_45 _ = happyReduce_20

action_46 (22) = happyGoto action_48
action_46 _ = happyReduce_29

action_47 _ = happyReduce_16

action_48 (28) = happyShift action_51
action_48 (36) = happyShift action_52
action_48 (40) = happyShift action_53
action_48 (41) = happyShift action_54
action_48 (21) = happyGoto action_49
action_48 (23) = happyGoto action_50
action_48 _ = happyFail (happyExpListPerState 48)

action_49 _ = happyReduce_30

action_50 (38) = happyShift action_57
action_50 _ = happyReduce_22

action_51 _ = happyReduce_32

action_52 (45) = happyShift action_2
action_52 (4) = happyGoto action_56
action_52 _ = happyFail (happyExpListPerState 52)

action_53 _ = happyReduce_33

action_54 (45) = happyShift action_2
action_54 (4) = happyGoto action_55
action_54 _ = happyFail (happyExpListPerState 54)

action_55 _ = happyReduce_28

action_56 _ = happyReduce_31

action_57 (29) = happyShift action_46
action_57 (18) = happyGoto action_58
action_57 _ = happyFail (happyExpListPerState 57)

action_58 _ = happyReduce_21

happyReduce_1 = happySpecReduce_1  4 happyReduction_1
happyReduction_1 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn4
		 (CName (mkPosToken happy_var_1)
	)
happyReduction_1 _  = notHappyAtAll 

happyReduce_2 = happySpecReduce_1  5 happyReduction_2
happyReduction_2 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn5
		 (LName (mkPosToken happy_var_1)
	)
happyReduction_2 _  = notHappyAtAll 

happyReduce_3 = happySpecReduce_1  6 happyReduction_3
happyReduction_3 (HappyTerminal happy_var_1)
	 =  HappyAbsSyn6
		 (TextLit (mkPosToken happy_var_1)
	)
happyReduction_3 _  = notHappyAtAll 

happyReduce_4 = happyReduce 5 7 happyReduction_4
happyReduction_4 ((HappyAbsSyn14  happy_var_5) `HappyStk`
	(HappyAbsSyn9  happy_var_4) `HappyStk`
	(HappyAbsSyn5  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn7
		 (Language.APSL.AISL.Grammar.Abs.Module happy_var_3 (reverse happy_var_4) (reverse happy_var_5)
	) `HappyStk` happyRest

happyReduce_5 = happyReduce 4 8 happyReduction_5
happyReduction_5 ((HappyAbsSyn6  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn10  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn8
		 (Language.APSL.AISL.Grammar.Abs.Import happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_6 = happySpecReduce_0  9 happyReduction_6
happyReduction_6  =  HappyAbsSyn9
		 ([]
	)

happyReduce_7 = happySpecReduce_2  9 happyReduction_7
happyReduction_7 (HappyAbsSyn8  happy_var_2)
	(HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn9
		 (flip (:) happy_var_1 happy_var_2
	)
happyReduction_7 _ _  = notHappyAtAll 

happyReduce_8 = happySpecReduce_1  10 happyReduction_8
happyReduction_8 _
	 =  HappyAbsSyn10
		 (Language.APSL.AISL.Grammar.Abs.ImportAll
	)

happyReduce_9 = happySpecReduce_1  10 happyReduction_9
happyReduction_9 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn10
		 (Language.APSL.AISL.Grammar.Abs.ImportSymbols happy_var_1
	)
happyReduction_9 _  = notHappyAtAll 

happyReduce_10 = happySpecReduce_1  11 happyReduction_10
happyReduction_10 (HappyAbsSyn4  happy_var_1)
	 =  HappyAbsSyn11
		 (Language.APSL.AISL.Grammar.Abs.ImportSymbol happy_var_1
	)
happyReduction_10 _  = notHappyAtAll 

happyReduce_11 = happySpecReduce_1  12 happyReduction_11
happyReduction_11 (HappyAbsSyn11  happy_var_1)
	 =  HappyAbsSyn12
		 ((:[]) happy_var_1
	)
happyReduction_11 _  = notHappyAtAll 

happyReduce_12 = happySpecReduce_3  12 happyReduction_12
happyReduction_12 (HappyAbsSyn12  happy_var_3)
	_
	(HappyAbsSyn11  happy_var_1)
	 =  HappyAbsSyn12
		 ((:) happy_var_1 happy_var_3
	)
happyReduction_12 _ _ _  = notHappyAtAll 

happyReduce_13 = happyReduce 5 13 happyReduction_13
happyReduction_13 (_ `HappyStk`
	(HappyAbsSyn16  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn4  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn13
		 (Language.APSL.AISL.Grammar.Abs.ActorDecl happy_var_2 (reverse happy_var_4)
	) `HappyStk` happyRest

happyReduce_14 = happySpecReduce_0  14 happyReduction_14
happyReduction_14  =  HappyAbsSyn14
		 ([]
	)

happyReduce_15 = happySpecReduce_2  14 happyReduction_15
happyReduction_15 (HappyAbsSyn13  happy_var_2)
	(HappyAbsSyn14  happy_var_1)
	 =  HappyAbsSyn14
		 (flip (:) happy_var_1 happy_var_2
	)
happyReduction_15 _ _  = notHappyAtAll 

happyReduce_16 = happyReduce 6 15 happyReduction_16
happyReduction_16 (_ `HappyStk`
	(HappyAbsSyn19  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn4  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn15
		 (Language.APSL.AISL.Grammar.Abs.InitState happy_var_3 (reverse happy_var_5)
	) `HappyStk` happyRest

happyReduce_17 = happyReduce 5 15 happyReduction_17
happyReduction_17 (_ `HappyStk`
	(HappyAbsSyn19  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn4  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn15
		 (Language.APSL.AISL.Grammar.Abs.State happy_var_2 (reverse happy_var_4)
	) `HappyStk` happyRest

happyReduce_18 = happySpecReduce_0  16 happyReduction_18
happyReduction_18  =  HappyAbsSyn16
		 ([]
	)

happyReduce_19 = happySpecReduce_2  16 happyReduction_19
happyReduction_19 (HappyAbsSyn15  happy_var_2)
	(HappyAbsSyn16  happy_var_1)
	 =  HappyAbsSyn16
		 (flip (:) happy_var_1 happy_var_2
	)
happyReduction_19 _ _  = notHappyAtAll 

happyReduce_20 = happySpecReduce_2  17 happyReduction_20
happyReduction_20 (HappyAbsSyn18  happy_var_2)
	(HappyAbsSyn20  happy_var_1)
	 =  HappyAbsSyn17
		 (Language.APSL.AISL.Grammar.Abs.Transition happy_var_1 happy_var_2
	)
happyReduction_20 _ _  = notHappyAtAll 

happyReduce_21 = happyReduce 5 18 happyReduction_21
happyReduction_21 ((HappyAbsSyn18  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn23  happy_var_3) `HappyStk`
	(HappyAbsSyn22  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn18
		 (Language.APSL.AISL.Grammar.Abs.TChoice (reverse happy_var_2) happy_var_3 happy_var_5
	) `HappyStk` happyRest

happyReduce_22 = happySpecReduce_3  18 happyReduction_22
happyReduction_22 (HappyAbsSyn23  happy_var_3)
	(HappyAbsSyn22  happy_var_2)
	_
	 =  HappyAbsSyn18
		 (Language.APSL.AISL.Grammar.Abs.TFinal (reverse happy_var_2) happy_var_3
	)
happyReduction_22 _ _ _  = notHappyAtAll 

happyReduce_23 = happySpecReduce_0  19 happyReduction_23
happyReduction_23  =  HappyAbsSyn19
		 ([]
	)

happyReduce_24 = happySpecReduce_2  19 happyReduction_24
happyReduction_24 (HappyAbsSyn17  happy_var_2)
	(HappyAbsSyn19  happy_var_1)
	 =  HappyAbsSyn19
		 (flip (:) happy_var_1 happy_var_2
	)
happyReduction_24 _ _  = notHappyAtAll 

happyReduce_25 = happySpecReduce_2  20 happyReduction_25
happyReduction_25 (HappyAbsSyn4  happy_var_2)
	_
	 =  HappyAbsSyn20
		 (Language.APSL.AISL.Grammar.Abs.OnMessage happy_var_2
	)
happyReduction_25 _ _  = notHappyAtAll 

happyReduce_26 = happySpecReduce_1  20 happyReduction_26
happyReduction_26 _
	 =  HappyAbsSyn20
		 (Language.APSL.AISL.Grammar.Abs.Otherwise
	)

happyReduce_27 = happySpecReduce_1  20 happyReduction_27
happyReduction_27 _
	 =  HappyAbsSyn20
		 (Language.APSL.AISL.Grammar.Abs.AnyTime
	)

happyReduce_28 = happySpecReduce_2  21 happyReduction_28
happyReduction_28 (HappyAbsSyn4  happy_var_2)
	_
	 =  HappyAbsSyn21
		 (Language.APSL.AISL.Grammar.Abs.Send happy_var_2
	)
happyReduction_28 _ _  = notHappyAtAll 

happyReduce_29 = happySpecReduce_0  22 happyReduction_29
happyReduction_29  =  HappyAbsSyn22
		 ([]
	)

happyReduce_30 = happySpecReduce_2  22 happyReduction_30
happyReduction_30 (HappyAbsSyn21  happy_var_2)
	(HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn22
		 (flip (:) happy_var_1 happy_var_2
	)
happyReduction_30 _ _  = notHappyAtAll 

happyReduce_31 = happySpecReduce_2  23 happyReduction_31
happyReduction_31 (HappyAbsSyn4  happy_var_2)
	_
	 =  HappyAbsSyn23
		 (Language.APSL.AISL.Grammar.Abs.NextState happy_var_2
	)
happyReduction_31 _ _  = notHappyAtAll 

happyReduce_32 = happySpecReduce_1  23 happyReduction_32
happyReduction_32 _
	 =  HappyAbsSyn23
		 (Language.APSL.AISL.Grammar.Abs.SameState
	)

happyReduce_33 = happySpecReduce_1  23 happyReduction_33
happyReduction_33 _
	 =  HappyAbsSyn23
		 (Language.APSL.AISL.Grammar.Abs.QuitState
	)

happyNewToken action sts stk [] =
	action 48 48 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	PT _ (TS _ 1) -> cont 24;
	PT _ (TS _ 2) -> cont 25;
	PT _ (TS _ 3) -> cont 26;
	PT _ (TS _ 4) -> cont 27;
	PT _ (TS _ 5) -> cont 28;
	PT _ (TS _ 6) -> cont 29;
	PT _ (TS _ 7) -> cont 30;
	PT _ (TS _ 8) -> cont 31;
	PT _ (TS _ 9) -> cont 32;
	PT _ (TS _ 10) -> cont 33;
	PT _ (TS _ 11) -> cont 34;
	PT _ (TS _ 12) -> cont 35;
	PT _ (TS _ 13) -> cont 36;
	PT _ (TS _ 14) -> cont 37;
	PT _ (TS _ 15) -> cont 38;
	PT _ (TS _ 16) -> cont 39;
	PT _ (TS _ 17) -> cont 40;
	PT _ (TS _ 18) -> cont 41;
	PT _ (TS _ 19) -> cont 42;
	PT _ (TS _ 20) -> cont 43;
	PT _ (TS _ 21) -> cont 44;
	PT _ (T_CName _) -> cont 45;
	PT _ (T_LName _) -> cont 46;
	PT _ (T_TextLit _) -> cont 47;
	_ -> happyError' ((tk:tks), [])
	}

happyError_ explist 48 tk tks = happyError' (tks, explist)
happyError_ explist _ tk tks = happyError' ((tk:tks), explist)

happyThen :: () => Err a -> (a -> Err b) -> Err b
happyThen = (thenM)
happyReturn :: () => a -> Err a
happyReturn = (returnM)
happyThen1 m k tks = (thenM) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> Err a
happyReturn1 = \a tks -> (returnM) a
happyError' :: () => ([(Token)], [String]) -> Err a
happyError' = (\(tokens, _) -> happyError tokens)
pModule tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn7 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


returnM :: a -> Err a
returnM = return

thenM :: Err a -> (a -> Err b) -> Err b
thenM = (>>=)

happyError :: [Token] -> Err a
happyError ts =
  Bad $ "syntax error at " ++ tokenPos ts ++ 
  case ts of
    [] -> []
    [Err _] -> " due to lexer error"
    _ -> " before " ++ unwords (map (id . prToken) (take 4 ts))

myLexer = tokens
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}
{-# LINE 8 "<command-line>" #-}
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4














































{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "/usr/lib/ghc-8.0.2/include/ghcversion.h" #-}

















{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "/tmp/ghc23297_0/ghc_2.h" #-}




















































































































































































{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}

data Happy_IntList = HappyCons Int Happy_IntList







{-# LINE 68 "templates/GenericTemplate.hs" #-}

{-# LINE 78 "templates/GenericTemplate.hs" #-}

{-# LINE 87 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 140 "templates/GenericTemplate.hs" #-}

{-# LINE 150 "templates/GenericTemplate.hs" #-}
indexShortOffAddr arr off = arr Happy_Data_Array.! off








readArrayBit arr bit =
    Bits.testBit (indexShortOffAddr arr (bit `div` 16)) (bit `mod` 16)






-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             _ = nt :: Int
             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 268 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail explist (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ explist i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail explist i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 334 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
