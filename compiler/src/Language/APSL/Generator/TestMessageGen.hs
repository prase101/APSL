module Language.APSL.Generator.TestMessageGen where
    
import Language.APSL.Generator.MessageGen
import Language.APSL.AMSL.Compiler
import Language.APSL.AMSL.DataType.Module
import Language.APSL.AMSL.DataType.Expression(emptyEnv)
import Language.APSL.AMSL.MessagePrettyPrinter(prettyPrintMessage)
import Language.APSL.Generator.MessageCoverage
import Test.QuickCheck.Gen
import Text.Regex.TDFA ((=~))

-- give the dir where imap.amsl is located
myTest dir k = do {
   -- Right amslModule <-  compile dir "imap.amsl" ;
   Right amslModule <-  compile dir "websocket.amsl" ;
   putStrLn (show amslModule) ;
   putStrLn "=====" ;
   msgtype0 <- return $ (messages amslModule) !! k ;
   msg <- generate $ genMessage defaultParams emptyEnv msgtype0 ; -- generating a single message
   putStrLn . show . prettyPrintMessage $ msg ;
   -- cov <- return . moduleMsgCoverageAdd msg . moduleMsgCoverageBase . messages $ amslModule ;
   -- putStrLn . printModuleMsgCoverage $ cov -- reporting the coverage
}
