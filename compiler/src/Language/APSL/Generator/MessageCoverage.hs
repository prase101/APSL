{-# LANGUAGE RecordWildCards, GADTs, OverloadedStrings, PatternGuards #-}
-- | Tool for measuring how much of a message specification is covered by actual messages.
--   With the current measure, the branches of unions, values of enums and the presense of Optional fields are 
--   measured.
module Language.APSL.Generator.MessageCoverage (
    MsgCoverage (..),
    coverAmount,
    messageCoverage,
    combineMsgCoverage,
    printMsgCoverage,
    ModuleMsgCoverage (..),
    moduleMsgCoverageBase,
    moduleMsgCoverageAdd,
    moduleCoverAmount,
    printModuleMsgCoverage,
    showPercentage
) where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.TypeCodec
import Language.APSL.AMSL.DataType.Module

import Text.PrettyPrint

import Control.Arrow (first)
import Data.Maybe
import Data.Ratio
import           Data.Map (Map)
import qualified Data.Map as M

-- | Represents a message specification as a tree, with leafs that can be marked as being covered.
--   Both sum and product types are represented by branches, but in the latter case a single message can only cover one
--   direction.
data MsgCoverage
    = UncoveredLeaf
    | CoveredLeaf
    | Node MsgCoverage MsgCoverage
    | Label TypeName MsgCoverage
    deriving (Show)

-- | The fraction of leaves being covered.
coverAmount :: MsgCoverage -> Rational
coverAmount UncoveredLeaf = 0
coverAmount CoveredLeaf  = 1
coverAmount (Label _ c) = coverAmount c
coverAmount (Node a b)   = (numerator ca + numerator cb) % (denominator ca + denominator cb)
 where (ca, cb) = (coverAmount a, coverAmount b)

printMsgCoverage :: MsgCoverage -> String
printMsgCoverage c = showPercentage (coverAmount c) ++ "\n" ++ render (pp c)
 where
    pp UncoveredLeaf           = text "N"
    pp CoveredLeaf             = text "Y"
    pp (Label (TypeName l) c)  = text l $+$ pp c
    -- pp (Node l r)              = text "-" $+$ (text "." $$ nest 2 (pp l)) $+$ (text "." $$ nest 2 (pp r))
    pp (Node l r)              = (text "-" $$ nest 4 (pp l)) $+$ nest 0 (pp r)

-- | Determines how much a specification of a single message covers.
messageCoverage :: MessageInstance -> MsgCoverage
messageCoverage (MessageInstance tn r x) = typeCoverage (UserType $ Record tn [] r, Arguments []) $ Just x

typeCoverage :: (Type a, Arguments) -> Maybe a -> MsgCoverage
typeCoverage (ty, args) mx = case ty of
    BasicType Optional                           -> optionalCoverage mx
    UserType  (Record tn _ r)                    -> Label tn $ recordCoverage r mx
    UserType  (Union tn _ _ u)                   -> Label tn $ unionCoverage u mx
    UserType  (Enumeration tn tt (EnumValues e)) -> Label tn $ enumCoverage tt e mx
    UserType  (TypeAlias _ ty' _)                -> typeCoverage (ty', args) mx
    _ | Just _ <- mx                             -> CoveredLeaf
      | otherwise                                -> UncoveredLeaf
    
 where
    tn = typeName ty

    recordCoverage :: Record a -> Maybe a -> MsgCoverage
    recordCoverage Empty Nothing   = UncoveredLeaf
    recordCoverage Empty (Just ()) = CoveredLeaf
    recordCoverage (Field _ FieldTypeCodec{..} r) mxs = 
        Node (typeCoverage (fieldType, fieldTypeArgs) $ fmap fst mxs) (recordCoverage r $ fmap snd mxs)

    unionCoverage :: Union t a -> Maybe a -> MsgCoverage
    unionCoverage (Option _ _ FieldTypeCodec{..} u) mx = 
        let (xl, xr) = case mx of
                Nothing        -> (Nothing, Nothing)
                Just (Left x)  -> (Just x, Nothing)
                Just (Right x) -> (Nothing, Just x)
            cl = typeCoverage (fieldType, fieldTypeArgs) xl
         in 
            case u of
                None -> cl
                _    -> Node cl $ unionCoverage u xr

    enumCoverage :: TagType a -> [(FieldName, a)] -> Maybe a -> MsgCoverage
    enumCoverage tt ((_, y):ys) mx =
        let leaf | Just x <- mx, compareTags tt x y = CoveredLeaf
                 | otherwise                        = UncoveredLeaf
         in if null ys then leaf else Node leaf $ enumCoverage tt ys mx

    optionalCoverage :: Maybe (ContainerValue Maybe) -> MsgCoverage
    optionalCoverage mx | Just (TypeExpArg sty stargs) <- lookup "subject" $ unArguments args =
        let (cl, xr) = case mx of
                Nothing                                  -> (UncoveredLeaf, Nothing)
                Just (ContainerValue _ Nothing)          -> (CoveredLeaf, Nothing)
                Just (ContainerValue sty' (Just x)) 
                    | Just SameType <- sameType sty sty' -> (UncoveredLeaf, Just x)
         in Node cl $ typeCoverage (sty, stargs) xr


-- | Join the coverage of multiple instances of the same message or identically labelled structures.
combineMsgCoverage :: MsgCoverage -> MsgCoverage -> MsgCoverage
combineMsgCoverage = (<++>)
 where
    Label la a      <++> Label lb b    | la == lb = Label la $ a <++> b
    Node ll lr      <++> Node rl rr    = Node (ll <++> rl) (lr <++> rr)
    CoveredLeaf     <++> CoveredLeaf   = CoveredLeaf
    UncoveredLeaf   <++> CoveredLeaf   = CoveredLeaf
    CoveredLeaf     <++> UncoveredLeaf = CoveredLeaf
    UncoveredLeaf   <++> UncoveredLeaf = UncoveredLeaf
    a               <++> b             = error "Incompatible coverage trees."
  

-- | Maintain coverage for the various messages within a module.
data ModuleMsgCoverage = ModuleMsgCoverage {labelCoverage :: Map TypeName MsgCoverage, msgCoverage :: Map TypeName MsgCoverage}

-- Extend coverage of a certain message with the node coverage of other messages, and vice versa.
applyLabelledMsgCoverage :: MsgCoverage -> Map TypeName MsgCoverage -> (MsgCoverage, Map TypeName MsgCoverage)
applyLabelledMsgCoverage (Label l n) m 
    | Just n' <- M.lookup l m = 
        let (new, m') = applyLabelledMsgCoverage (combineMsgCoverage n n') m
         in (Label l new, M.insert l new m')
    | otherwise = 
        let (n', m') = applyLabelledMsgCoverage n m
         in (Label l n', M.insert l n' m')
applyLabelledMsgCoverage (Node l r) m =
    let (l', m1) = applyLabelledMsgCoverage l m
        (r', m2) = applyLabelledMsgCoverage r m1
     in (Node l' r', m2)
applyLabelledMsgCoverage leaf m = (leaf, m)


-- | 0% module coverage that can be extended with moduleCoverageAdd. The argument contains the messages in a (AMSL or 
--   AISL) module.
moduleMsgCoverageBase :: [Message] -> ModuleMsgCoverage
moduleMsgCoverageBase msgs = ModuleMsgCoverage M.empty $ M.fromList [(tn, recordCoverage tn r) | Message tn r <- msgs]
 where recordCoverage tn r = typeCoverage (UserType $ Record tn [] r, Arguments []) Nothing

-- | Add the coverage of a particular message to the total module coverage.
moduleMsgCoverageAdd :: MessageInstance -> ModuleMsgCoverage -> ModuleMsgCoverage
moduleMsgCoverageAdd msg@(MessageInstance tn _ _) (ModuleMsgCoverage lCov base) = 
    let (msgCov, lCov') = applyLabelledMsgCoverage (messageCoverage msg) lCov
     in ModuleMsgCoverage lCov' $ M.insert tn msgCov base

-- | Cover fraction for all messages.
moduleCoverAmount :: ModuleMsgCoverage -> Rational
moduleCoverAmount (ModuleMsgCoverage _ c) = sum $ map ((/ fromIntegral (M.size c)) . coverAmount) $ M.elems c

printModuleMsgCoverage :: ModuleMsgCoverage -> String
printModuleMsgCoverage c@(ModuleMsgCoverage _ c') = 
    "======\n"
    ++ "Coverage per record type is listed below.\n"
    -- ++ showPercentage (moduleCoverAmount c) ++ "\n" 
    ++ concatMap p (M.assocs c') ++ "\n"
    ++ "======\n"
    ++ "Overall message coverage = " ++ showPercentage (moduleCoverAmount c)
    ++ "\n======\n" 
 where
    p (TypeName tn, cov) = tn ++ ": " ++ printMsgCoverage cov ++ "\n"

showPercentage :: Rational -> String
showPercentage x = concat [show $ floor $ x * 100, ".", show $ floor (x * 10000) `mod` 100, "%"]