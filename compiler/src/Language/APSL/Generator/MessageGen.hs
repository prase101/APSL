{-# LANGUAGE RankNTypes, RecordWildCards, GADTs, OverloadedStrings, PatternGuards #-}
-- | Random message generator. TODO: move to seperate project.
module Language.APSL.Generator.MessageGen (genMessage, GenParams(..), defaultParams, genValue) where 

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Codecs
import Language.APSL.AMSL.DataType.TypeCodec
import Language.APSL.AMSL.DataType.Module
import Language.APSL.AMSL.ValueChecker
import Language.APSL.Generator.RegexGen
import Language.APSL.AMSL.MessagePrettyPrinter 

import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen

import Text.Regex.TDFA ((=~))

import Data.BitString.BigEndian (BitString)
import Data.ByteString.Lazy (ByteString)
import qualified Data.BitString.BigEndian as B
import qualified Data.ByteString.Lazy as By
import qualified Data.ByteString.Lazy.Char8 as BC8
import qualified Data.Map as M

import Text.PrettyPrint
import Control.Monad
import Data.Maybe
import System.Random
import Debug.Trace

data GenParams = GenParams {
    size :: Maybe Int,       
    -- ^ Unless Nothing, overrides the Quickcheck size parameter.
    intBounds :: Maybe (Integer, Integer)
    -- ^ Maximal/minimal integer values to consider; (-(2^size), 2^size) if Nothing.
}

defaultParams :: GenParams
defaultParams = GenParams {
    size = Nothing,
    intBounds = Nothing
}

-- | Generator for instances of a particular message within a certain environment.
genMessage :: GenParams -> Env -> Message -> Gen MessageInstance
genMessage gp env (Message tn r) = liftM (MessageInstance tn r) $ genValue gp env (UserType $ Record tn [] r, Arguments [])

parseBits :: String -> BitString
parseBits = B.fromList . map parseBit
 where 
    parseBit '0' = False
    parseBit '1' = True
    parseBit _ = error "parseBits: invalid bit"

parseChar8 :: String -> BitString
parseChar8 = B.bitStringLazy . BC8.pack

genValue :: GenParams -> Env -> (Type a, Arguments) -> Gen a
genValue gp env (ty, tyArgs@(Arguments args')) = maybe id resize (size gp) $ check $
    case ty of
        BasicType t -> genBasic t
        UserType (Record tn _ r) -> genRecord r
        UserType (Union tn _ tt u) -> genUnion tt u
        UserType (Enumeration tn tt e) -> genEnum tn tt e
        UserType (TypeAlias _ t args) -> genValue gp env (t, addArgs tyArgs args)
        ExtType _ -> error "TODO: generator for extension types."

 where
     
    -- defining a fail behavior for the Gen monad
    -- Using the pre-defined fail does not work; since it simply seems to do nothing
    -- fail_ = fail 
    fail_ errormsg = error errormsg
     
    unInt = liftM (fmap $ \(EInt n) -> n)

    arg name = return $ lookup name args'
    valArg name = do
        mexp <- arg name
        case mexp of
            Nothing -> return Nothing
            Just (ExpArg exp) -> do
                Right val <- return $ evaluate env exp
                return $ Just val

    withDefault :: a -> Gen (Maybe a) -> Gen a
    withDefault = liftM . fromMaybe

    check g = do 
        x <- g
        let xshow = renderStyle style (prettyPrint ty x)
        ok <- return $ checkValue env ty tyArgs x
        if ok then return x
              else fail_ ("genValue generates a value that does not satisfy its constraint:\n" ++ xshow)

    countArg :: ParamName -> Gen (Integer, Integer, Integer)
    countArg (ParamName name) = do
        val <- unInt $ valArg $ ParamName name
        minVal <- withDefault 0 $ unInt $ valArg $ ParamName $ "min_" ++ name
        maxVal <- unInt $ valArg $ ParamName $ "max_" ++ name
        factor <- withDefault 1 $ unInt $ valArg $ ParamName $ name ++ "_factor"
        if factor == 0 then fail_ "factor=0 (not allowed!)."
           else do
             size <- sized return
             let maxSize = minVal + toInteger size
             case val of
                Just v | v >= minVal,
                         maybe True (v<=) maxVal,
                         v `rem` factor == 0
                                   -> return (v, v, factor)
                       | otherwise -> fail_ "Value inconsistant with size constraints."
                Nothing -> do
                           let ma = maybe maxSize (min maxSize) maxVal
                           if ma < minVal then fail_ "Empty range." else return (minVal, ma, factor)


    genBasic :: BasicType a -> Gen a
    genBasic t = 
        case t of
            Binary    -> fixValue BinaryTag $ do
                length <- countArg "length"
                bpat <- valArg "bit_pattern"
                xbpat <- valArg "exclude_bit_pattern"
                cpat <- valArg "char8_pattern"
                xcpat <- valArg "exclude_char8_pattern"

                let notExcluded str = 
                        maybe True (\(ERegex r) -> not $ bitMatch r str) xbpat
                            && maybe True (\(ERegex r) -> not $ char8Match r str) xcpat

                case (bpat, cpat) of
                    (Nothing, Nothing) -> do
                        len <- liftM fromInteger $ chooseFromCount length
                        liftM  B.fromList
                               -- (\s-> trace ("\n#### Binary len=" ++ show (B.length (B.fromList s))) (B.fromList s))  
                               (vectorOf len (elements [False, True])) `suchThat` notExcluded
                    (Just (ERegex p), Nothing) -> 
                        liftM parseBits (genByRegex Nothing p length) `suchThat` notExcluded
                    (Nothing, Just (ERegex p)) -> do
                        let (mi, ma, f) = length
                        liftM parseChar8 (genByRegex Nothing p (mi `quot` 8, ma `quot` 8, f `quot` 8)) `suchThat` notExcluded
                    (Just _, Just _) -> fail_ "Combining bit_pattern and char8_pattern not supported."                

            Bool      -> fixValue BoolTag $ elements [False, True]

            Integer   -> fixValue IntTag $ do
                maxSize <- sized return
                let (minInt, maxInt) = fromMaybe (-1 * (2 ^ maxSize), 2 ^ maxSize) $ intBounds gp
                val_ <- unInt $ valArg "value"
                mi_ <- liftM (maybe minInt (max minInt)) $ unInt $ valArg "min"
                ma_ <- liftM (maybe maxInt (min maxInt)) $ unInt $ valArg "max"
                let mi  = maybe mi_ id val_
                let ma  = maybe ma_ id val_         
                factor <- withDefault 1 $ unInt $ valArg "factor"
                if ma < mi then fail_ $ "Impossible integer range: min=" ++ show mi ++ ", max=" ++ show ma
                           -- else trace ("\n#### min=" ++ show mi ++ ",max=" ++ show ma ++ ",factor=" ++ show factor) $ chooseFromCount (mi, ma, factor)
                           else chooseFromCount (mi, ma, factor)

            Text      -> fixValue TextTag $ do 
                count <- countArg "count"
                ERegex pattern <- withDefault (ERegex ".*") $ valArg "pattern"
                xpat <- valArg "exclude_pattern"
                let notExcluded str | Just (ERegex p) <- xpat = not (str =~ p)
                                    | Nothing <- xpat = True

                charset <- valArg "charset"
                let ranges | Just (EText cs) <- charset,
                             Just result <- M.lookup cs charsets = Just result
                           | Nothing <- charset = Nothing

                genByRegex ranges pattern count `suchThat` notExcluded

            Optional  -> 
                withSubType "subject" $ \st sargs -> do
                    Just (EBool empty) <- valArg "is_empty"
                    liftM (ContainerValue st) $
                        if empty
                            then return Nothing
                            else liftM Just $ genValue gp env (st, sargs)

            List      -> 
                withSubType "elem" $ \et eargs ->
                    liftM (ContainerValue et) . fixListValue et $ do
                        count' <- countArg "count"
                        count <- chooseFromCount count'
                        EList excludeList <- liftM (fromMaybe $ EList []) $ valArg "exclude_values"

                        excludedValues <- sequence $ map (compareValue et) excludeList
                        let notExcluded x = not $ any ($x) excludedValues

                        vectorOf (fromInteger count) $ genValue gp env (et, eargs) `suchThat` notExcluded
                        

    compareValue :: Type a -> ExpValue -> Gen (a -> Bool)
    compareValue et v = 
        maybe (fail_ "Excluded value not a tag type.") return $ do 
            ett <- toTagType et
            x <- readTag ett v
            return $ compareTags ett x

    fixValue :: TagType a -> Gen a -> Gen a
    fixValue tt alt = do
        mval <- valArg "value"
        case mval of
            Nothing    -> alt
            Just ENull -> alt
            Just val | Just x <- readTag tt val -> return x
            _ -> fail_ "Invalid value argument."

    fixListValue :: Type e -> Gen [e] -> Gen [e]
    fixListValue et alt = do
        mval <- valArg "value"
        case mval of
            Nothing -> alt
            Just vals' -> do
                EList vals <- return vals'
                let makeVal val = genValue gp env (et, Arguments [("value", ExpArg $ Constant val)])
                sequence $ map makeVal vals


    genRecord :: Record a -> Gen a
    genRecord = buildRecordArgs tyArgs env (\e t _ -> genValue gp e t)

    genUnion :: TagType t -> Union t a -> Gen a
    genUnion tt u = do
        tagarg <- valArg "tag"
        tag <- case tagarg of
            Nothing   -> elements $ unionTags u
            Just val  | Just t <- readTag tt val -> return t
        genOption tt u tag

    unionTags :: Union t a -> [t]
    unionTags None = []
    unionTags (Option t _ _ r) = t : unionTags r

    genOption :: TagType t -> Union t a -> t -> Gen a
    genOption tt (Option t1 _ FieldTypeCodec {..} r) t2 
        | compareTags tt t1 t2 = do
            Right env2 <- return $ argsToEnv tyArgs env
            liftM Left $ genValue gp env2 (fieldType, fieldTypeArgs)
        | otherwise = liftM Right $ genOption tt r t2

    genEnum :: TypeName -> TagType a -> Enumeration a -> Gen a
    genEnum tn tt e@(EnumValues vals) = fixValue (EnumTag tn tt e) $ oneof $ map (return . snd) vals

    withSubType :: ParamName -> (forall s. Type s -> Arguments -> Gen b) -> Gen b
    withSubType p f = do
        Just (TypeExpArg st sargs) <- arg p
        f st sargs

    chooseFromCount :: (Integer, Integer, Integer) -> Gen Integer
    chooseFromCount (mi, ma, f) = do
        pick <- liftM (*f) $ choose (mi `quot` f, ma `quot` f)
        return $ case () of () | pick < mi -> mi
                               | pick > ma -> ma
                               | otherwise -> pick