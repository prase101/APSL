{-# LANGUAGE PatternGuards #-}
-- | Provides a tool for generating test strings that match a given regular expression.
--   Uses a custom dynamic programming algorithm for string selection.
-- Supported regex operators: \\|*+?{}()^.[] See https://en.wikipedia.org/wiki/Regular_expression
module Language.APSL.Generator.RegexGen (genByRegex) where 

import Language.APSL.Base

import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen

import Control.Arrow
import Control.Monad
import Control.Applicative
import Data.Char
import Data.Ix
import Data.List

-- A terminal is a sequence of inclusive code point ranges that match.
-- E.g. "a" -> [('a','a')]; "[0-9A-Z]" -> [('0','9'),('A','Z')]; "." -> [(minBound, maxBound)]
type Terminal = [(Char, Char)]

-- Simple AST of non-empty regexes.
data Regex 
    = Term Terminal
    | Seq Regex Regex
    | Choice Regex Regex
    | Quant (Int, Maybe Int) Regex
    deriving Show

-- To parse a string representing a regular expression. 
-- Supported regex operators: \\|*+?{}()^.[] See https://en.wikipedia.org/wiki/Regular_expression
parseRegex :: String -> Regex
parseRegex "" = error "parseRegex: empty patterns not allowed"
parseRegex str = 
    case p1 $ filter (not . (`elem` "^$")) str of
        Just (r,"") -> r
        _ -> error $ "parseRegex: invalid regular expression: " ++ str
 where
    p1 s | Just (l, '|':s1) <- p2 s,
           Just (r, s2) <- p1 s1
            = Just (Choice l r, s2)
         | otherwise = p2 s
    p2 s | Just (l, s1) <- p3 s,
           Just (r, s2) <- p2 s1
            = Just (Seq l r, s2)
         | otherwise = p3 s
    p3 s | Just (x, s1) <- p4 s,
           Just (q, s2) <- pQuant s1 
            = Just (Quant q x, s2)
         | otherwise = p4 s
    p4 s | '(':s1 <- s,
           Just (x, ')':s2) <- p1 s1
            = Just (x, s2)
         | otherwise = fmap (first Term) $ pTerm s

    pQuant s =
        case s of
            '*':s1 -> Just ((0, Nothing), s1)
            '+':s1 -> Just ((1, Nothing), s1)
            '?':s1 -> Just ((0, Just 1), s1)
            '{':s1 | [(n,'}':s2)] <- reads s1
                      -> Just ((n, Just n), s2)
                   | [(n, ',':'}':s2)] <- reads s1
                         -> Just ((n, Nothing), s2)
                   | [(n, ',':s2)] <- reads s1,
                     [(m, '}':s3)] <- reads s2
                      -> Just ((n, Just m), s3)
            _ -> Nothing

    pTerm ('.':s) = Just ([(minBound, maxBound)], s)
    pTerm ('[':'^':s) | (t, ']':s1) <- pUnits s = Just (negateTerm t, s1)
    pTerm ('[':s)  | (t, ']':s1) <- pUnits s = Just (t, s1)
    pTerm s = fmap (first $ \x -> [(x,x)]) $ pUnit s

    pUnits s | Just (l, '-':s1) <- pUnit s,
               Just (r, s2) <- pUnit s1
                = first ((l,r) :) $ pUnits s2
             | Just (x, s1) <- pUnit s
                = first ((x,x) :) $ pUnits s1
             | otherwise 
                 = ([], s)

    pUnit ('\\':s) | [(i, s1)] <- reads s = Just (chr i, s1)
    pUnit ('\\':c:s) | c `elem` operators  || c == '-' = Just (c,s)
                     | [(c', "")] <- reads ['\'', c, '\''] = Just (c', s)
                     | otherwise = Nothing
    pUnit (c:s) | c `elem` operators = Nothing 
                | otherwise = Just (c,s)
    pUnit [] = Nothing

    operators = "\\|*+?{}()^.[]"


negateTerm :: Terminal -> Terminal
negateTerm = filter (uncurry (<=)) . foldr remove [(minBound, maxBound)]
 where
    remove _ [] = []
    remove (nmi, nma) ((mi,ma):xs) 
        | inRange (mi,ma) nmi || inRange (mi,ma) nma
           = (mi, min ma $ pred nmi) : (max mi $ succ nma, ma) : remove (nmi, nma) xs
        | otherwise 
           = remove (nmi, nma) xs


restrictCharset :: Charset -> Regex -> Regex
restrictCharset cs r = case r of
    Term t -> Term $ intersection (sort cs) (sort t)
    Seq a b -> Seq (restrictCharset cs a) (restrictCharset cs b)
    Choice a b -> Choice (restrictCharset cs a) (restrictCharset cs b)
    Quant (i, j) a -> Quant (i, j) $ restrictCharset cs a

intersection :: Charset -> Charset -> Charset
intersection (a@(a1, a2):as) (b@(b1, b2):bs) 
    | a2 < b1 = intersection as (b:bs)
    | b2 < a1 = intersection (a:as) bs
    | a1 < b1 = intersection ((b1, a2):as) (b:bs)
    | b1 < a1 = intersection (a:as) ((a1, b2):bs)
    | a2 < b2 = a : intersection as ((a2,b2):bs)
    | b2 < a2 = b : intersection ((b2,a2):as) bs
    | otherwise = a : intersection as bs
intersection _ _ = []


-- | Given a pattern, a length range, and a length divisor, obtain a generator that provides random strings, with a 
--   size within this range, that matches the pattern; fails if no such string exists.
genByRegex :: Maybe Charset -> String -> (Integer, Integer, Integer) -> Gen String
genByRegex cs pat (mi, ma, fa) = 
    pickOne $ map snd 
            $ filter (\(l,_) -> l `rem` fromInteger fa == 0)
            $ takeWhile ((<= fromInteger ma) . fst)
            $ dropWhile ((< fromInteger mi) . fst)  
            $ rexGen 
            $ maybe id restrictCharset cs
            $ parseRegex pat
 where
    pickOne [] = fail $ "No matching strings of this length exist." ++ show (pat, mi, ma, fa)
    pickOne xs = oneof xs

-- Provides generators for certain lengths, when possible. Result is sorted on keys and may be infinitely long.
rexGen :: Regex -> [(Int, Gen String)]
rexGen (Term t) = [(1, liftM (:[]) $ termGen t)]
rexGen (Choice a b) = merge (\x y -> oneof [x, y]) (rexGen a) (rexGen b)
rexGen (Quant r x) = 
    [(l, oneof gens) 
        | (l, gens) <- qGens r (rexGen x), 
          length gens > 0]
rexGen (Seq a b) = 
    [(l, oneof gens) 
        | (l, gens) <- seqGens (listify $ rexGen a) (listify $ rexGen b), 
          length gens > 0]
    

seqGens :: [(Int, [Gen String])] -> [(Int, [Gen String])] -> [(Int, [Gen String])]
seqGens (a@(ai, ags):as) (b@(bi, bgs):bs) =
    (ai + bi, [(++) <$> ag <*> bg | ag <- ags, bg <- bgs])
        : merge (++) (seqGens (a:as) bs) 
            (seqGens as (b:bs))
seqGens _ _ = []

qGens :: (Int, Maybe Int) -> [(Int, Gen String)] -> [(Int, [Gen String])]
qGens (0, ma) ((0,_):x) = qGens (0, ma) x
qGens (0, ma) x = qGens' ma [(0, [return ""])]
 where
    prependX = seqGens (listify x)
    qGens' (Just 0) s = s
    qGens' n (s:ss) = s : (merge (++) ss $ qGens' (fmap pred n) $ prependX (s:ss))
qGens (mi, ma) x = seqGens (listify x) $ qGens (mi - 1, fmap pred ma) x

listify :: [(Int, Gen String)] -> [(Int, [Gen String])]
listify = map $ second (:[])

merge :: Ord i => (a -> a -> a) -> [(i, a)] -> [(i, a)] -> [(i, a)]
merge f ass@((ia, a):as) bss@((ib, b):bs) =
    case compare ia ib of
        LT -> (ia, a) : merge f as bss
        EQ -> (ia, f a b) : merge f as bs
        GT -> (ib, b) : merge f ass bs
merge _ as [] = as
merge _ [] bs = bs

termGen :: Terminal -> Gen Char
termGen ranges = frequency [(1 + ord ma - ord mi, choose (mi, ma)) | (mi, ma) <- ranges]

testMe regex = sample (genByRegex Nothing regex (1,8,3))

