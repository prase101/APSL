-- A module for calculating probabilistic-based transition coverage.
-- Based on an algorithm by Rick Klomp & Wishnu Prasetya
--

module Language.APSL.TestEngine.LTSCoverage where 

import Language.APSL.AISL.DataType.LTS

import           Data.Set (Set)
import qualified Data.Set as S
import           Data.Map (Map)
import qualified Data.Map as M
import Data.List (nub,partition)
import Data.Maybe


import Debug.Trace

-- | Representing transition-coverage information of an LTS. We will later on calculate
-- this on on the original LTS, but on its tau-star LTS. The coverage information is
-- represented as a mapping that specifies for each transition in the (tau-star) LTS,
-- the probability that it has been covered. 1.0 means that it is definitely covered,
-- 0.0 means it is definitely not covered. Any value in between means that it might be
-- covered with the given estimated probability.
--
type TransitionCoverage = Map (StateID, Action, StateID) Float

-- | Calculate the overall transition coverage in the sense of how much of the total
-- set of the transitions is covered. If every transition is either covered (1.0) or
-- not, the we the overall coverage number is simply the ratio between the number of
-- transitions covered and the total number of transitions. 
--
-- However, some transitions may only have a fragment of 1.0 to indicate the probability
-- that it is covered. We will calculate the overall coverage simply by summing all
-- the coverage probability of all transitions. This is consistent with the intuition that
-- if two transitions that are siblings, each is observed with probability 0.5, then
-- at least one of them will be covered, which is one the sum of their probabilities also
-- say.
--
overallTransitionCoverage :: TransitionCoverage -> Float
overallTransitionCoverage m = if n==0 then 0.0 else p / fromIntegral n
    where
    p = sum $ map snd $ M.toList m
    n = M.size m

printTransitionCoverageMetric :: TransitionCoverage -> String
printTransitionCoverageMetric m 
    = 
    "=====\n"
    ++ "Coverage per transition is shown below:: \n"
    ++ worker (M.toList m) 
    ++ "\n  TOTAL transition coverage = " ++ show(overallTransitionCoverage m)
    ++ "\n=====\n"
    
    where
    worker [] = ""
    worker [(t,p)] = print (t,p)
    worker ((t,p) : therest) = print (t,p) ++ "\n" ++ worker therest    
    print (t,p) = "  " ++ show t ++ ": " ++ show (100.0 * p)
    
       
-- | Combine two transition coverage information, e.g. they may come from two separate
-- test cases. The coverage infos are assumed to quantify over the same set of transitions.
--  
mergeTransitionCoverage :: TransitionCoverage -> TransitionCoverage -> TransitionCoverage
mergeTransitionCoverage cov1 cov2 = M.fromList [ (tr,calculate tr p) | (tr,p) <- M.assocs cov1 ]
   where
   -- Calculate the probability that tr is covered in either cov1 or cov2
   -- The probability of tr in cov1 = p
   -- Let the probability of tr in cov2 = q
   -- The probability that tr is NOT convered by cov1 nor cov2 = (1-p)(1-q)
   -- The probability that tr is covered in cov1 or cov1 = 1 - (1-p)(1-q) = p+q - pq
    calculate tr p = p + q - p*q
        where
        q = cov2 M.! tr

-- | Combine a non-empty set of transition coverage information.
--    
mergeTransitionCoverages :: [TransitionCoverage] -> TransitionCoverage
mergeTransitionCoverages covs = foldr1 mergeTransitionCoverage covs
   
   
-- | A represetation of a reduced LTS, with tau-star reduction. 
--  
-- The tau-closure of a state s consists of all states that can be reaches from s with
-- zero or more tau-steps. Such a closure is represented by a pair (s,U) where U is the
-- closure. We also call (s,U) a tau=star state.
--
-- A transition with label a between (s,U) and (t,V) exists if there exists a transition
-- from some state s' in U, labelled with a, to t.
--
-- An lts M can be reduced to a tau-star LTS: 
--    (1) calculate the tau-star states of every state in M. Take only (s,U) if
--        s is either an initial state, or a target of a non-tau transition in M.
--    (2) Connect the tau-star states. Only allow non-tau transitions between tau-star
--        states.
--
data TauStarLTS   = TauStarLTS {
          -- the underlying LTS:
          baseLTS_ :: LTS,  
          -- The star-states
          tstarStates :: Map StateID [StateID],
          -- Non-tau transitions between tau-star states: 
          tstarTransitions :: [(StateID, Action, StateID)]
       }
       deriving Show

-- | Construct a tau-star LTS from a base LTS.
--
tauStarReduction :: LTS -> TauStarLTS 
tauStarReduction lts = TauStarLTS lts (M.fromList newstates) newtransitions 
    where
    baseStates      = allStates lts
    baseTransitions = allTransitions lts
    baseInitState   = initState lts
    
    from_   (s,a,t) = s
    to_     (s,a,t) = t
    action_ (s,a,t) = a
    
    -- construct the tau-star states:
    newstates = [ (s, reachableTauStates lts s) | s <- baseStates,
                   s == baseInitState
                   || any (\(_,a,to)-> a/= Internal && to==s) baseTransitions ]
    
    -- Calculate (non-tau) transitions between tau-star states; 
    newtransitions = nub [ (s1,a,s2) |  
                            (s1,closure) <- newstates, 
                            (s2,_) <- newstates,
                            s1x <- closure,
                            (_,a,to_) <- outGoingTransitions lts s1x, 
                            a /= Internal  &&  to_ == s2
                         ]
                                     

-- | Obtain all outgoing transitions that are possible from a tau-star state s, by doing
-- a send or receive action a. The calculation also takes into account that a Receive m
-- can also be taken via ReceiveUnexpected transition on a base state sx, if normal
-- Receive m is not possible on sx.
--
-- This results the set of all outgoing transisions from s, with the given action, and
-- may also include ReceiveUnexpected transitions as pointed out above.
--
tauStarOutgongTransitions :: TauStarLTS -> StateID -> Action -> [(StateID,Action,StateID)]
tauStarOutgongTransitions tslts s action = case action of
       Send _     -> normalTransitions
       Receive _  -> normalTransitions ++ receiveUnexpectedTransitions
    where
    normalTransitions  = [ t | t@(from_,a,_) <- tstarTransitions tslts, from_ == s && a==action ]

    tauclosure = tstarStates tslts M.! s
    
    receiveUnexpectedTransitions =  nub [(s,ReceiveUnexpected,s2) | sx <- tauclosure, s2 <- checkReceiveUnexpected sx ]
    
    -- a ReceiveUnexpected transition from a state sx is only taken if "action" above is not an
    -- option for sx
    checkReceiveUnexpected sx =
       if canReceive (baseLTS_ tslts) sx m then []
       else [ s2 | (_,_,s2) <- outGoingUnexpectedTransitions (baseLTS_ tslts) sx ]
       where
       Receive m = action
        
    
-- | Given a potentially non-deterministic (tau-star) lts, and a trace tr of 
-- observable actions, the execution tree of tr is a tree that describes all 
-- possible paths through the lts that would produce the same observable trace as tr.
--
data ExecutionTree = ExecutionTree StateID [(Action,ExecutionTree)]  

instance Show ExecutionTree where
   show etree = worker "" "" etree
      where
      worker indent a (ExecutionTree s subtrees) = indent ++ a ++ " -> " ++ show s ++ "  ["  
                                                    ++ kids ++ "]"
          where
          kids = concat $ map (\(a,t)-> "\n" ++ worker ("   " ++ indent) (show a) t) subtrees

-- | In the process of constructing an execution tree, it may become necessary
-- to remove certain leaves, because they cannot be continued further. Removing
-- such a leaf may alse trigger the removal of its parent etc. Performich such
-- an operation of a data type representation of a tree is not very efficient
-- to do in Haskell. Being a functional language, we cannot just cut off "pointers"
-- within a tree to severe some some paths.
--
-- The data type ExecutionTree__ below provides a low level representation of 
-- execution trees, where severing paths can be done more efficiently.
--
data ExecutionTree__ = ExecutionTree__ { 
       root_      :: TreeNodeID,
       nodes_     :: Map TreeNodeID StateID,
       children_  :: Map TreeNodeID [(Action,TreeNodeID)], 
       parent_    :: Map TreeNodeID (Maybe TreeNodeID),
       lastID_    :: Int }    
       deriving Show 

type TreeNodeID = Int



-- | Construct an initial execution tree with the given state as the root state.
initialExecutionTree__ :: StateID -> ExecutionTree__
initialExecutionTree__ rootState = ExecutionTree__ {
      root_     = 0,
      nodes_    = M.insert 0 rootState M.empty,
      children_ = M.insert 0 [] M.empty,
      parent_   = M.empty,
      lastID_   = 0
   }


-- | Convert ExecutionTree__ to ExecutionTree.
reconstructTree :: ExecutionTree__ -> ExecutionTree
reconstructTree etree = worker (root_ etree)
   where
   worker i =  ExecutionTree (nodes_ etree M.! i) [ (a,worker j)| (a,j) <- children_ etree M.! i ]
   

-- | Construct a readable string representation of a low-level execution tree   
printExecutionTree__ :: ExecutionTree__ -> String   
printExecutionTree__ etree = show $ reconstructTree etree



-- | Obtain the transition coverage information from a given execution tree.
--
getTransitionCoverage :: TauStarLTS -> ExecutionTree__ -> TransitionCoverage
getTransitionCoverage tslts etree 
   = 
   M.fromList [ (e, calculateCov 1.0 e dummy etree_) | e <- tstarTransitions tslts ]   
   
   where
   etree_ = reconstructTree etree
   
   dummy = (Normal "______", Internal)    
   
   calculateCov probability transition (from_,a) (ExecutionTree to_ subtrees) 
      =
      if (from_,a,to_) == transition then probability
      else sum [ calculateCov newprobability transition (to_,action2) tree | (action2,tree) <- subtrees ]
      where 
      newprobability = probability / fromIntegral (length subtrees)
  

-- get all the leaves in an execution tree
leaves_ :: ExecutionTree__ -> Map TreeNodeID StateID
leaves_ etree = M.filterWithKey p $ nodes_ etree
   where
   p id _ = null (children_ etree M.! id)

-- get all siblings of a node in an execution tree
siblings_ :: ExecutionTree__ -> TreeNodeID -> [TreeNodeID]
siblings_ etree id = 
   if id == root_ etree 
   then []
   else let
        parent = fromJust $ parent_ etree M.! id
        in
        [ id2 | (a,id2) <- children_ etree M.! parent, id2/=id ]


-- A "linage" is a partial path from a leaf x towards the root, such that every ancestor of
-- of x in the path (including x itself) has no sibling. The root cannot be part of a linage.
-- The function below remove the linage of a given leaf x from the execution tree.
--    
deleteLinage :: ExecutionTree__ -> TreeNodeID -> ExecutionTree__
deleteLinage etree leaf = etree {
    nodes_    = new_nodes,
    parent_   = parent_ etree `M.withoutKeys` tobeDeleted,
    children_ = M.update remove_z parent_of_z  $ (children_ etree `M.withoutKeys` tobeDeleted),
    lastID_   = maximum $ M.keys new_nodes
    }
    where
    
    remove_z kids = Just [ (a,y) | (a,y)<-kids, y /= z ]
        
    (z,tobeDeleted)  = (last tobeDeleted_, S.fromList tobeDeleted_)
       where
       tobeDeleted_ = getLinage leaf
       
    parent_of_z = fromJust (parent_ etree M.! z)
       
    getLinage id = if id == root_ etree then []
                   else if null $ siblings_ etree id 
                        then id : getLinage (fromJust (parent_ etree M.! id))
                        else [id]
      
    new_nodes = nodes_  etree `M.withoutKeys` tobeDeleted

 
-- | Exapand an execution tree with the execution of a single observable action.
-- The action must be either a Send or a Receive. 
--
expand :: TauStarLTS -> ExecutionTree__ -> Action -> ExecutionTree__
expand trlts etree action = etree3
    where
    
    ltsEdges  = tstarTransitions trlts
    
    leaves    = [ (i,newkids s) | (i,s) <- M.toList $ leaves_ etree ]
    newkids s = tauStarOutgongTransitions trlts s action
    
    (tobeDeleted,extendables) = partition (\(_,kids)-> null kids) leaves
    
    deleteDeadLinages t [] = t
    deleteDeadLinages t ((i,_) : therest) = deleteDeadLinages (deleteLinage t i) therest
    
    etree2 = deleteDeadLinages etree tobeDeleted
    etree3 = expand etree2 extendables
    
    expand t [] = t
    expand t ((i,newkids) : therest) = expand (expand1 t i newkids) therest
    
    expand1 t i newkids = t {
           nodes_     = nodes_ t `M.union`  M.fromList [ (j,to) | (j,(_,_,to)) <- newkids_ ] ,
           children_  = M.insert i  [ (a,j) | (j,(_,a,_)) <- newkids_ ] (children_ t) 
                        `M.union`
                        M.fromList [ (j,[]) | (j,_) <- newkids_ ]
                         ,
           parent_    = parent_ t `M.union` M.fromList [(j, Just i) | (j,_) <- newkids_ ] ,
           lastID_ = new_last_id
        }
        where
        last_id  = lastID_ t
        newkids_ = zip [last_id + 1..] newkids 
        new_last_id = last_id + length newkids_
  
-- | Exapand an execution tree with the execution of a trace of observable actions.
-- The actions must be either a Send or a Receive. 
expandTr :: TauStarLTS -> ExecutionTree__ -> [Action] -> ExecutionTree__
expandTr trlts etree trace = worker etree trace
    where
    worker t [] = t
    worker t (a:therest) = worker (expand trlts t a) therest
 
    
getExecutionTree__ :: LTS -> [Action] -> ExecutionTree__
getExecutionTree__ lts trace = expandTr (tauStarReduction lts) (initialExecutionTree__ (initState lts)) trace

    
-- | Calculate the transition coverage over an LTS, after executing a trace of
-- observable actions. The trace should consist of only Send and Receive actions.
--
calculateTransitionCoverage lts actiontrace = getTransitionCoverage tauStarLTS etree
    where
    tauStarLTS = tauStarReduction lts
    etree0 = initialExecutionTree__ (initState lts)
    etree  = expandTr tauStarLTS etree0 actiontrace

    
    
etree_example1 = ExecutionTree__ {
      root_     = 0,
      nodes_    = M.fromList [ (0, Normal "S0"), (1, Normal "S1"), (2, Normal "S0"), (3, Normal "S2"), (4,Normal "S0"),
                               (5, Normal "S0"), (6,Normal "S0")],
      children_ = M.fromList [ (0, [(Send "a",1), (Send "b",2)]),
                               (1, [(Send "a",3), (Send "b",4)]),
                               (2, []), (3, []), (4, [(Send "c",5)]), (5, [(Send "d",6)]), (6, [])
                             ],
      parent_   = M.fromList [ (1,Just 0), (2, Just 0), (3, Just 1), (4, Just 1), (5, Just 4), (6, Just 5)],
      lastID_   = 0
   }
        
etree_example0 = initialExecutionTree__ (Normal "S0")

  
convertToLTS :: StateID -> [Transition] -> LTS   
convertToLTS s0 trlist = LTS s0 (M.fromList transitions_)
   where
   states = nub $ concat $ [ [s1,s2] | (s1,_,s2)<-trlist ]
   outGoingActions s = [ a | (s1,a,_) <- trlist, s1 == s ]
   targetStates s a  = [ s2 | (s1,a_,s2) <- trlist, s1 == s && a_ == a]
   transitions_ = [ (s, M.fromList [ (a, targetStates s a) | a <- outGoingActions s ]) |  s <- states ]
   
lts_example0 = convertToLTS (Normal "S0")
    [
        (Normal  "S0", Send "a", Normal "S0"),
        (Normal  "S0", Send "a", Normal "S1"),
        (Normal  "S1", Send "a", Normal "S0"),
        (Normal  "S1", Send "a", Normal "S2"),
        (Normal  "S1", Send "b", Normal "S2"),
        (Normal  "S2", Send "a", Normal "S0"),
        (Normal  "S2", Send "a", Normal "S3"),
        (Normal  "S3", Send "c", Normal "S4"),
        (Normal  "S4", Send "c", Normal "S5")
     ] 

lts_example1 = convertToLTS (Normal "S0") 
    [
        (Normal  "S0", Internal, Normal "S1"),
        (Normal  "S0", Receive "a", Normal "S2"),
        (Normal  "S0", ReceiveUnexpected , Normal "S5"),
        (Normal  "S1", Receive "a", Normal "S3"),
        (Normal  "S1", Receive "a", Normal "S3"),
        (Normal  "S1", Internal, Normal "S0"),
        (Normal  "S3", Internal , Normal "S1"),
        (Normal  "S3", Receive "b", Normal "S4")
     ] 


testExpand_ lts trace = expandTr (tauStarReduction lts)  etree_example0 trace
testExpand  lts trace = putStrLn $ printExecutionTree__ $ testExpand_ lts trace         

testExpand2 lts trace  =  do {
     putStrLn $ printExecutionTree__ etree ;
     putStrLn "====" ;
     putStrLn (printTransitionCoverageMetric (getTransitionCoverage (tauStarReduction lts) etree))
   }
   where
   etree = testExpand_ lts trace         



