{-# LANGUAGE FunctionalDependencies, FlexibleInstances, RecordWildCards,
             PatternGuards, GeneralizedNewtypeDeriving, DeriveGeneric    #-}

-- | Provides a general IUT traversal algorithm. A "traversal" is a finite sequence of interactions with the IUT,
--   guided by the interaction model (actor model) of the IUT. A traversal may end sucessfully (it does not reveal
--   any error in the IUT), or it may end in some step revealing an error.
--
--   Essentially, the algorithm produces a traversal by constructing a path through the interaction model that
--   it uses as the guide. The algorithm is parameterized with a traversal strategy. When there are multiple ways to
--   extend a traversal with the next interactions, this strategy is invoked to decide which next-interaction to
--   choose.
--   To do its work, the algorithm  maintains a "traversal state". It contains information such as the traversal
--   "current location" / "current state" in the model, and the trace of interactions produced so far.
--   Since APSL allows models to be non-deterministic, it is not always possible for the algorithm to uniquely
--   determine IUT's current state (in terms of the states of its interaction model). So, the algorithm maintains
--   a "set" of possible current states, rather than just a single current state.
--
module Language.APSL.TestEngine.Traversal where

import Language.APSL.AISL.DataType.LTS

import Language.APSL.AISL.Compiler hiding (actors)
import Language.APSL.AISL.DataType.Module
import Language.APSL.AMSL.DataType.Module hiding (Module, messages)
import Language.APSL.AMSL.DataType.Expression(Env)
import Language.APSL.Generator.MessageGen
import Language.APSL.Channel.Encoder
import Language.APSL.Base
import Language.APSL.Channel.MessageChannel
import Language.APSL.Channel.SocketChannel
import Language.APSL.Generator.MessageCoverage
import Language.APSL.TestEngine.LTSCoverage
import Language.APSL.TestEngine.CoverageMergeUtil
import Language.APSL.AMSL.MessagePrettyPrinter

import           Data.Set (Set)
import qualified Data.Set as S
import           Data.Map (Map)
import qualified Data.Map as M
import Data.List (nub)
import Data.Maybe

import Data.Functor.Identity
import Data.Monoid
import Data.Ratio

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State (StateT)
import Control.Monad.State
import Data.Binary.Put
import Data.Binary.Bits.Put
import Control.Monad.IO.Class
import Data.Time.Clock
import System.Random
import Test.QuickCheck.Gen
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString as StrictByteString
import Network.Socket
import Network.BSD
import GHC.Generics
import qualified Data.Serialize as Serialize
import Text.PrettyPrint
import Debug.Trace

{- -------------------------------------------------------------------------------------
   Main data structures involved in traversal.
------------------------------------------------------------------------------------- -}

-- | Representing the current traversal state.
data TraversalState info = TraversalState {
    possibleCurrentStates :: Set StateID,
    trtrace :: [(Action, Maybe MessageInstance, Maybe ByteString)],
                    -- the trace of observable interactions with the IUT so far, in reversed order.
                    -- note that this can also be a "ReceiveUnexpected" msg.
    msgcov  :: ModuleMsgCoverage, -- the achieved msg coverage so far
    info    :: info,
    cyclecount :: Int    -- the number of traversal count so far (this is equal or larger than the length or trtrace)
    }
    -- deriving Show

incrCount info = info { cyclecount = cyclecount info + 1 }

-- | To construct the initial traversal state.
initialTraversalState :: Module -> String -> info -> TraversalState info
initialTraversalState aislModule nameOfActorUnderTheTest info0 = TraversalState {
      possibleCurrentStates = S.fromList $ reachableTauStates lts (initState lts),
      trtrace = [],
      msgcov = moduleMsgCoverageBase [msg | (msg,_) <- messages aislModule],
      info = info0,
      cyclecount = 0
   }
   where
   -- the lts of the actor under test
   lts = head [lts_ | (name,lts_) <- actors aislModule, name == nameOfActorUnderTheTest ]

-- | Describing the result of a full traversal.
data FullTraversalStatus = IUTSendInvalidFormatMsg String -- with the error msg
                         | IUTSendInvalidMsgType [MessageLabel] MessageLabel
                         | ConnectionError
                         | ConnectionTimeOut
                         | TraversalCompleted String
                         deriving (Eq,Show)

-- | To hold various parameters to customize a traversal.
data TraversalParameters info = TraversalParameter {
       channel           :: MessageChannel,             -- a channel, for interacting with the IUT
       traversalStrategy :: TraversalStrategy info,     -- strategy that decides what to do next
       msgGenerator      :: TraversalState info -> Env -> Message -> IO(Maybe MessageInstance,ByteString),  -- the msg-generator to use
       customInfoUpdater :: CustomInfoUpdater info,     -- a function specifying how to update the custom traversal state
       initialCustomInfo :: info,                       -- initial custom traversal state
       writelog          :: String -> IO(),             -- function to write to log
       reportResult      :: TraversalState info -> String,
       reportVerbosity   :: Int                         -- 10 reports everything, 9 reports less etc
       }


type CustomInfoUpdater info = LTS -> Action -> TraversalState info -> info

-- | A traversal delegates some decisions to a "traversal strategy", which we will represent as the
-- following recursive datatype.
data TraversalStrategy info =
       -- continously apply a primitive strategy (until it says to stop)
       Sustain (PrimitiveTraversalStrategy info)
       -- apply a primitive strategy once, then stop
     | Once (PrimitiveTraversalStrategy info)
       -- go through s1 until it fails, then continue with s2
     | Then   (TraversalStrategy info) (TraversalStrategy info)
       -- if s1's first step works, do that s1, else do s2
     | OrElse (TraversalStrategy info) (TraversalStrategy info)
       -- if s' first step works, do the whole s and repeat, else we stop
     | Repeat (TraversalStrategy info)

-- | In the most primitive form, a traversal strategy is a function of this type.
type PrimitiveTraversalStrategy info = StrategyDirective
                              -> LTS
                              -> TraversalState info
                              -> IO StrategyDecision



data StrategyDirective = FavorSendToIUT | FavorReceiveFromIUT | DontCare deriving Eq

data StrategyDecision = DecideToWaitForInputFromIUT
                      | DecideToSendToIUT MessageLabel
                      | DecideToStop String
                      | Undecided
                      deriving (Eq,Show)


{- -------------------------------------------------------------------------------------
   Providing combinators to compose strategies to build up more complicated strategies.
------------------------------------------------------------------------------------- -}


-- | A strategy that stops.
stop_ :: String -> TraversalStrategy info
stop_ reason = Sustain (\sdir lts stateinfo -> return $ DecideToStop reason)

invokeStrategy :: TraversalStrategy info
                  -> StrategyDirective
                  -> LTS
                  -> TraversalState info
                  -> IO (StrategyDecision,TraversalStrategy info)

invokeStrategy strategy sdir lts stateinfo = case strategy of
    Sustain s0 -> do { d <- s0 sdir lts stateinfo ; return (d,strategy)}
    Once s0    -> do { d <- s0 sdir lts stateinfo ; return (d,stop_ "Once s has been applied once and done.")}
    OrElse s1 s2 -> do {
         (d1,d1next) <- invokeStrategy s1 sdir lts stateinfo ;
         if d1 /= Undecided
             then return (d1,d1next)
             else invokeStrategy s2 sdir lts stateinfo
       }
    Then s1 s2  -> do {
          (d1,d1next) <- invokeStrategy s1 sdir lts stateinfo ;
          case d1 of
            DecideToStop _ -> invokeStrategy s2 sdir lts stateinfo -- continue with s2 and discard s1 forever
            _  -> return (d1,Then d1next s2)
       }
    Repeat s0  -> do {
         (d,snext) <- invokeStrategy s0 sdir lts stateinfo ;
         case d of
           DecideToStop _ -> return (d,stop_ "A repeated strategy has decided to stop")
           _              -> return (d,Then snext (Repeat s0))
       }

-- Determies which msgs can be SOUNDLY sent to the IUT. Note that currentstates may contain
-- multiple states due to non-determinism. Only a msg that are allowed by all current states
-- are sound to send. We will however exclude non-Exit current states that can only send.
--
msgTypesTo_SOUNDLY_SendToIUT :: LTS -> [StateID] -> [MessageLabel]
msgTypesTo_SOUNDLY_SendToIUT lts currentstates =
  let
  allPossibleMsgTypesToSendToIUT  = possibleReceiveMsgs lts -- we'll limit to quantifying over rec-msgs

  -- exclude non-Exit current states, which can only send:
  currentstates2 = [ s | s<- currentstates, canReceiveUnexpected lts s || s==Exit || not (null (outGoingReceives lts s))]
  in
  if Exit `elem` currentstates2
     -- if Exit is in the remaining current then it is not sound to send any msg to the IUT:
     then []
     -- else we check which receive msgs can be soundly send to the IUT. Since the IUT may contain
     -- a "ReceiveUnexpected" transition, we will quantify over all possible receive msgs in the
     -- whole actor:
     else [  m | m <-allPossibleMsgTypesToSendToIUT,
                     all (\state-> canReceiveUnexpected lts state || canReceive lts state m) currentstates2  ]

-- | A strategy that decides to send a message of the given type to the IUT, if it is safe to do
--   so, and else it stops.
strategySend1 :: MessageLabel -> TraversalStrategy info
strategySend1 msgty = Once send
   where
   send sdir lts stateinfo =
      if msgty `elem` msgTypesTo_SOUNDLY_SendToIUT lts (S.toList $ possibleCurrentStates stateinfo)
         then return $ DecideToSendToIUT msgty
         else return $ DecideToStop "strategySend1 cannot send"



{- -------------------------------------------------------------------------------------
   We first define the underlying single-step traversal over an LTS.
------------------------------------------------------------------------------------- -}

type TraversalStep info = TraversalState info -> Maybe (TraversalState info)

-- | Traverse a single send-transition in the LTS. This only traverse the lts model. Note that
-- "sending" here means that the Implementation Under Test sends something. This function's role
-- is only to traverse the LTS; it is not responsible for the actual interaction with the
-- IUT.
--
traverseSend :: CustomInfoUpdater info -> LTS
                -> (MessageLabel,Maybe MessageInstance,Maybe ByteString)
                -> TraversalStep info
traverseSend infoUpdater lts (a,concrete_a,bytes) travStatus
           = traverse1_ infoUpdater lts (Send a, concrete_a,bytes) travStatus

-- | Traverse a single receive-transition in the LTS. This only traverse the lts model. Note that
-- "receive" here means that the Implementation Under Test receives something. This function's role
-- is only to traverse the LTS; it is not responsible for the actual interaction with the
-- IUT.
-- Now, importantly, this function will calculate the set of possible current IUT states in the LTS.
-- If the the protocol is deterministic, the current state is unique; else it may not be unique.
--
traverseReceive :: CustomInfoUpdater info -> LTS
                   -> (MessageLabel,Maybe MessageInstance,Maybe ByteString)
                   -> TraversalStep info
traverseReceive infoUpdater lts (a,concrete_a,bytes) travStatus
              = traverse1_ infoUpdater lts (Receive a, concrete_a,bytes) travStatus

-- The worker function of the two functions above.
traverse1_ :: CustomInfoUpdater info -> LTS -> (Action,Maybe MessageInstance, Maybe ByteString) -> TraversalStep info
traverse1_ infoUpdater lts (a,concrete_a,bytes) travStatus =

   if not $ S.null nextStates2 then Just newStatus else Nothing

   where

   currentStates = possibleCurrentStates travStatus

   nextStates st a@(Send _)    = nextStatesAfterTransition lts st a
   -- if it is a receive action, it is a bit more complicated; some states may have
   -- a "ReceiveUnexpected" which needs to be treated as a special case:
   nextStates st a@(Receive _) = let
                                 nexts = nextStatesAfterTransition lts st a
                                 in
                                 -- If nexts is not empty, then a is an expected action, so we return
                                 -- nexts. Else, check if a "ReceiveUnexpected" is possible.
                                 if not (null nexts) then nexts
                                 else [ t | (_,_,t) <- outGoingUnexpectedTransitions lts st ]

   -- possible next states:
   nextStates1   = nub $ concat [ nextStates st a | st <- S.toList currentStates ]
   -- add tau-transition closures:
   nextStates2   = S.fromList $ concat [ reachableTauStates lts st | st <- nextStates1 ]

   newStatus = TraversalState {
      possibleCurrentStates = nextStates2,
      trtrace = (a,concrete_a,bytes) : trtrace travStatus,
      info = infoUpdater lts a travStatus,
      msgcov = case concrete_a of
                 Nothing  -> msgcov travStatus
                 Just a_  -> a_ `moduleMsgCoverageAdd` msgcov travStatus ,
      cyclecount = cyclecount travStatus + 1
      }

{- -------------------------------------------------------------------------------------
   Defining the main traversal function.
------------------------------------------------------------------------------------- -}

-- | This is the traversal engine. It will produce a single traversal over the IUT.
-- At each step, it invokes a "traversal strategy" (which is part of the parameter to the traversal)
-- to decide whether to wait for a message from the IUT or to send a message to the IUT.
-- If the startegy decides to send a message, it only decides which message type to send.
-- The traversal function then invokes a msg-generator (which is one of its parameters) to
-- create a valid instance of the message type.
--
traversal :: TraversalParameters info  -- record specifying various traversal parameters
             -> Module                 -- AISL module containing the interaction model of the IUT
             -> String                 -- the name of the actor in the AISL model that describes the IUT
             -> IO (FullTraversalStatus, TraversalState info)

traversal tparams
          aislModule
          nameOfActorUnderTheTest
          =
          do {
            writeln ("** Starting a traversal of " ++ nameOfActorUnderTheTest ++ "\n=====") ;
            -- construct the initial traversal state:
            state0 <- return $ initialTraversalState aislModule nameOfActorUnderTheTest (initialCustomInfo tparams) ;

            -- run the traversal (the worker function):
            r@(status,finalTraversalState) <- worker (traversalStrategy tparams) DontCare state0   ;

            --
            -- Now, produce reports and save the trace ...
            --

            -- reporting msg coverage:
            doWhen (verbosity >= 9) $ writeln $ printModuleMsgCoverage $ msgcov finalTraversalState ;

            -- reporting and saving the trace:
            trace <- return $ reverse $ map (\(mtype,_,_)->mtype) $ trtrace finalTraversalState ;
            -- writeln ("\n\n### taustar LTS =\n" ++ show (tauStarReduction lts)) ;
            doWhen (verbosity >= 8) $ writeln ("\n\n### trace =\n" ++ show trace) ;
            -- etree <- return $ getExecutionTree__ lts trace) ;
            -- writeln ("\n\n### execution tree =\n" ++ printExecutionTree__ etree) ;
            saveTrace "." $ trtrace finalTraversalState ;

            -- reporting and saving transition coverage:
            trcov <- return $ calculateTransitionCoverage lts $ trace  ;
            doWhen (verbosity >= 8) $ writeln $ printTransitionCoverageMetric trcov ;
            saveTransitionCoverageInfo_ "." trcov ;

            -- reporting verdict:
            writeln $ reportResult tparams finalTraversalState ;
            (case status of
               TraversalCompleted why -> writeln ("SUCCESS. The traversal was completed because " ++ why ++ ". No error was found.")
               ConnectionError    -> do { reportTraversalState finalTraversalState ; writeln "PROBLEM. Connection error." }
               ConnectionTimeOut  -> do { reportTraversalState finalTraversalState ; writeln "PROBLEM. Connection has timed out." }
               IUTSendInvalidFormatMsg err -> do { reportTraversalState finalTraversalState ;
                                                   writeln "FORMAT ERROR. The IUT sent a message with an invalid format:" ;
                                                   writeln err }
               IUTSendInvalidMsgType expected a -> do {
                    reportTraversalState finalTraversalState ;
                    writeln "IO CONFORMANCE ERROR. The IUT sent a message of an unexpected type." ;
                    writeln ("   One of these is expected: " ++ show expected) ;
                    writeln ("   But the IUT sent an " ++ a)
                  }
               ) ;
            writeln ("=====") ;
            return r
          }

          where
          -- the lts of the actor under test
          lts = head [lts_ | (name,lts_) <- actors aislModule, name == nameOfActorUnderTheTest ]
          -- all messages' labels and types in the module
          msgs = M.fromList [(unTypeName $ messageName msg, (msg, env)) | (msg, env) <- messages aislModule ]

          fromJust (Just x) = x

          customUpdater   = customInfoUpdater tparams
          receiveFromIUT_ = receiveFromIUT $ channel tparams
          sendToIUT_      = sendToIUT $ channel tparams

          possibleNextMsgTypesFromIUT currentStates = nub [  m | s <- S.toList currentStates, (_,Send m,_) <- outGoingSends lts s ]

          -- the actual worker function of traversal
          worker strategy strategyDirective travstate = do {
             -- the idea is simple:
             --    (1) invoke the traversal strategy to decide what to do next
             --    (2) carry out the decision
             --    (3) recurse
             --
             currentstates <- return $ possibleCurrentStates travstate ;
             printProgress travstate ;
             printCurrentStates $ S.toList currentstates ;
             (decision, strategyContinuation) <- invokeStrategy strategy strategyDirective lts travstate ;
             case decision of
               DecideToStop why -> {- the strategy wants to stop -} return (TraversalCompleted why, travstate)

               DecideToWaitForInputFromIUT -> {- the stratgy wants to wait for msg from the IUT -}
                  do { status <- receiveFromIUT_ $ possibleNextMsgTypesFromIUT $ currentstates ; -- get a msg from the IUT
                       case status of
                         ReceiveOK a a_ bs -> do {
                              doWhen (isJust a_ && verbosity >= 10)
                                  $ writeln ("\n### message instance:\n" ++ renderStyle style (prettyPrintMessage $ fromJust a_)) ;
                              -- updte the traversal state, then recurse:
                              worker strategyContinuation DontCare $ fromJust $ traverseSend customUpdater lts (a,a_,bs) travstate ;


                            }
                         -- case when the received msg is not among what is expected:
                         Receive_WrongMsgError expected a ->  return (IUTSendInvalidMsgType expected a, travstate)
                         -- case then the recived msg has invalid format:
                         Receive_MsgWithIllegalFormatError err -> return (IUTSendInvalidFormatMsg err,travstate)
                         -- othe problems:
                         ReceiveConnectionError -> return (ConnectionError,travstate)
                         ReceiveTimeOut         -> {- the IUT is silent; see if we can send something to it -} do {
                                writeln "... the IUT has timed out." ;
                                worker strategyContinuation FavorSendToIUT (incrCount travstate)
                             }
                  }


               DecideToSendToIUT a -> {- the strategy wants to send a message of type a to the IUT -}
                  -- should lead to somewhere
                  do  { (a_,env)        <- return $ fromJust $ M.lookup a msgs ;
                        -- invoke the msg-generator to create an instance of a
                        (mconcrete_a,bytestring) <- (msgGenerator tparams) travstate env a_ ;
                        -- calculate the new traversal state:
                        Just travstate2 <- return $ traverseReceive customUpdater lts (a,mconcrete_a, Just bytestring) travstate ;
                        -- now send the msg:
                        status <- sendToIUT_ (a,bytestring) ;
                        doWhen (isJust mconcrete_a && verbosity >= 10)
                            $ writeln ("\n### message instance:\n" ++ renderStyle style (prettyPrintMessage $ fromJust mconcrete_a)) ;
                        case status of
                          SendOK -> worker strategyContinuation DontCare travstate2
                          SendConnectionError -> return (ConnectionError,travstate)
                  }
          }
          -- for logging the string s
          write s   = writelog tparams $ s
          writeln s = writelog tparams $ (s ++ "\n")

          verbosity = reportVerbosity tparams
          doWhen g f = if g then f else return ()


          printProgress travstate = write ("... [" ++ show (length (trtrace travstate))
                                                 ++ "|" ++ show (cyclecount travstate)
                                                 ++ "]" )

          printCurrentStates []  = writeln    " possible current states: [ ]"
          printCurrentStates s   = do { write " possible current states: [ " ; print_ s ; writeln " ]" }
             where
             print_  [s]      =  write (getStateName s)
             print_  (s:rest) =  do { write (getStateName s) ; write ", " ; print_ rest }

          reportTraversalState travstate = do {
             writeln ("=====") ;
             writeln ("Traversal state:") ;
             writeln ("   > IUT possible current states: (" ++ show n ++ "): " ++ show cstates) ;
             writeln ("   > Traces so far (" ++ show k ++ "): " ++ show (reverse trace_)) ;
             writeln ("=====")
          }
              where
              cstates = possibleCurrentStates travstate
              n       = S.size cstates
              trace_  = map (\(mtype,_,_)->mtype) $ trtrace travstate
              k       = length trace_

type Trace = [(Action, Maybe MessageInstance, Maybe ByteString)]

saveTrace :: FilePath -> Trace -> IO()
saveTrace dir trace = do
   fn <- mk_tstamp_fname
   let nfname = dir ++ pathSeparator ++ fn ++ ".trace"
   -- unfortunately, MessageInstance cannot be made serializable (at least, I don't know how)
   -- So we will only save the msgtype and the bytestrings.
   -- Furthermore, getting the bytestrings sent by the IUT from the channel is more difficult
   -- than I thought. So for now, we don't /can't put them into the trace.
   let trace_ = map (\(mtype,minstance,bytes) -> (mtype,bytes)) trace
   let bytes  =  Serialize.encode $ trace_
   -- StrictByteString.putStrLn bytes
   StrictByteString.writeFile nfname $ Serialize.encode $ trace_
   putStrLn $ "** Saved a trace to " ++ nfname


loadTrace :: FilePath -> IO Trace
loadTrace fname = do {
    bs <- StrictByteString.readFile (normalizeFileName_ fname ".trace") ;
    case Serialize.decode bs of
        Right trace -> return $ map (\(mtype,bytes) -> (mtype,Nothing,bytes)) trace
        Left  e     -> error e
    }

{- -------------------------------------------------------------------------------------
   Providing two primitive strategies: random and goto-state.
------------------------------------------------------------------------------------- -}

-- This is a strategy simply randomly chooses the next transition from the set of valid
-- transitions in the given LTS.
--
-- The strategy is parameterized by an aggresiveness level. On the aggressiveness level 0, the
-- startegy will only send a msg m to the IUT when it is sound to do so. That is,
-- if sending m to the IUT is allowed by all possible IUT's current states.
--
-- On aggressiveness level 1, it sends a message to the IUT if there is one possible current
-- state that can accept it as an expected message.
--
-- On aggressiveness level 2, it is as level 1, but it can also send any rec-message to the IUT,
-- if one of its current state has an outgoing ReceiveUnexpected transition.
--
randomStrategy :: Int->Int -> TraversalStrategy info

-- Note: TraversalStrategy info is defined as: StrategyDirective  -> LTS -> TraversalState info -> IO StrategyDecision

randomStrategy aggresiveness maximumCycleCount
   =
   Sustain $ (\directive lts travstate  -> genericrandomStrategy aggresiveness maximumCycleCount choser directive lts travstate)
   where
   choser lts currentstates possibleNextMsgTypesToSendToIUT = do {
     n <- return $ length possibleNextMsgTypesToSendToIUT ;
     k <- getStdRandom (randomR (0,n-1)) ; -- using system random, not ideal!
                                           -- using pseudo random requires it to be threaded, which
                                           -- does not really fit in the current architecture. TO DO.
     return $ (possibleNextMsgTypesToSendToIUT !! k)
   }

-- a support type to represent the function that decides which message type to send to the IUT,
-- given a selection to choose from.
type Choser = LTS -> [StateID] -> [MessageLabel] -> IO MessageLabel

genericrandomStrategy :: Int -> Int -> Choser -> PrimitiveTraversalStrategy info
genericrandomStrategy aggresiveness maximumCycleCount choser directive lts travstate
     =
     --
     -- This is very error prone unfortunately, we will divide this into a number of cases.
     --
     --
     -- Case (1) is easy: when the maximum number of cycles is reached:
     if cyclecount travstate >= maximumCycleCount    then return $ DecideToStop "maximum number of traversal count is reached"
     --
     -- Case (2) when "Exit" is the only possible current state. Then we know for sure that the IUT is done.
     -- Note that when "Exit" is not the only possibility, it is not sound to infer that IUT is done.
     else if currentstates == [Exit]                 then return $ DecideToStop "the model has definitely arrives at its exit state"
     --
     -- Case (3) is when either the IUT cannot send or a directive "FavorSendToIUT" has been given
     else if null possibleNextMsgTypesFromIUT  || directive == FavorSendToIUT then
          (
          --
          -- Case (3a) We infer that IUT has become stuck (it can neither receive nor send, but it MAY NOT be in the Exit state)
          if sendingToIUTisNotPossibleAtAll
             && null possibleNextMsgTypesFromIUT         then return $ DecideToStop "the model indicates that no further interaction is possible while it might not be in the Exit state."
          --
          -- Case (3b) No msg can be (soundly, depending on the aggresiveness) sent to the IUT, but the IUT can send. Then wait for a msg from IUT:
          else if null possibleNextMsgTypesToSendToIUT
               && not (null possibleNextMsgTypesFromIUT) then trace ("### " ++ show possibleNextMsgTypesToSendToIUT) $ return DecideToWaitForInputFromIUT
          --
          -- Case (3c) No msg can be (soundly) sent to the IUT, but the IUT cannot send anything either:
          else if null possibleNextMsgTypesToSendToIUT
               && null possibleNextMsgTypesFromIUT then return $ DecideToStop "the IUT cannot be soundly further tested. The model indicates that a msg should be sent to the IUT, but no msg can be soundly sent to it."
          --
          -- Case (3d) It is possible to (soundly) send a msg to the IUT; let's do so:
          else do {
              msgTypeToSend <- choser lts currentstates possibleNextMsgTypesToSendToIUT ;
              return $ DecideToSendToIUT msgTypeToSend
              }
          )
     --
     -- Case (4) the IUT can send and no directive to favor "sending to IUT" was given.
     -- Decide to wait for a msg from IUT:
     else return DecideToWaitForInputFromIUT

     where

     currentstates = S.toList $ possibleCurrentStates travstate
     possibleNextMsgTypesFromIUT     = [  m | s <- currentstates, (_,Send m,_) <- outGoingSends lts s ]


     possibleNextMsgTypesToSendToIUT =
          if aggresiveness<=0 then possibleNextMsgTypesTo_SOUNDLY_SendToIUT
          else if aggresiveness==1 then nub [  m | s <- currentstates, (_,Receive m,_) <- outGoingReceives lts s ]
          else [  m | m <-allPossibleMsgTypesToSendToIUT,
                      any (\state-> canReceiveUnexpected lts state || canReceive lts state m) currentstates ]

     -- Determines which msgs can be SOUNDLY sent to the IUT. Note that currentstates may contain
     -- multiple states due to non-determinism. Only a msg that are allowed by all current states
     -- are sound to send. We will however exclude non-Exit current states that can only send.
     --
     possibleNextMsgTypesTo_SOUNDLY_SendToIUT = msgTypesTo_SOUNDLY_SendToIUT lts currentstates

     allPossibleMsgTypesToSendToIUT  = possibleReceiveMsgs lts -- we'll limit to quantifying over rec-msgs

     -- check if sending a message to the IUT is at all possible
     sendingToIUTisNotPossibleAtAll = all (\state-> not (canReceiveUnexpected lts state)
                                                    && null (outGoingReceives lts state))
                                          currentstates


--
-- A startegy that tries to reach a given target state. If the target state is (definitely) reached,
-- the strategy stops. Else it re-use the generic random strategy above. Whenever the random strategy
-- decides to send a message to the IUT, this gotoState strategy below will choose a message type
-- that will keep the chance of reaching the target state non-zero. If no such action is possible on
-- the current state(s), the one message type will be randomly chosen, from the choices offered by
-- the underlying random strategy.
--
gotoStateStrategy :: Int->Int -> StateID -> TraversalStrategy info
gotoStateStrategy aggresiveness maximumCycleCount targetState = Sustain goto_
   where
   goto_ directive lts travstate =
      if currentStates == [targetState]
      then return $ DecideToStop ("Target state " ++ show targetState ++ " is definitely reached.")
      else if aggresiveness >= 1 && targetState `elem` currentStates
           -- on an aggressive setting, stop if the target state is a possible current state
           then return $ DecideToStop ("Target state " ++ show targetState ++ " is possibly reached.")
           else genericrandomStrategy aggresiveness maximumCycleCount choser directive lts travstate

      where
      currentStates = S.toList (possibleCurrentStates travstate)

   choser lts currentstates possibleNextMsgTypesToSendToIUT = do
       let z = [ m | m <- possibleNextMsgTypesToSendToIUT, or [ canReachTarget st m | st <- currentstates ] ]
       let choices = if null z then possibleNextMsgTypesToSendToIUT else z
       n <- return $ length choices
       k <- getStdRandom (randomR (0,n-1))
       return $ (choices !! k)

       where

       statesThatCanReachTarget = [s | s <- allStates lts, canReach lts s targetState ]
       -- check if doing ?msgLabel on the state st *can* get us to a state from where the target is reachable
       canReachTarget st msglabel =
            or [ to `elem` statesThatCanReachTarget
                    | (from,a,to) <- allTransitions lts,
                      (from==st) && ((a==ReceiveUnexpected) || (a==Receive msglabel))
               ]




{- -------------------------------------------------------------------------------------
   Providing an echo traversal; useful to test out the working of our own traversal function.
------------------------------------------------------------------------------------- -}

-- | Top level API. An instance of the traversal engine that uses of the echo channel. You don't
-- use this to test a real IUT.
--
echoTraversal dir aislFile nameOfActorUnderTheTest aggressiveness maximumCycleCount = do {
       Right aisl <- aislCompiler dir aislFile ;
       echoTraversal_ aggressiveness maximumCycleCount aisl nameOfActorUnderTheTest
   }
   where
   aislCompiler = compile


echoTraversal_ aggressiveness maximumCycleCount aislModule nameOfActorUnderTheTest = traversal tvparams aislModule nameOfActorUnderTheTest
   where
   msggenparams = defaultParams

   tvparams = TraversalParameter {
       channel = echoMessageChannel aislModule msggenparams,
       traversalStrategy = randomStrategy aggressiveness maximumCycleCount,
       msgGenerator = (\traversalstate env mty -> asmlRandomMsgGenerator defaultParams env mty) , -- use ASML's default msg-gen with default configuration
       customInfoUpdater = trivialInfoupdater,
       initialCustomInfo = (),
       writelog = putStr,
       reportResult = resultreporter,
       reportVerbosity = 10
     }

   trivialInfoupdater x y z = ()
   resultreporter _ = "-- no custom state"


asmlRandomMsgGenerator :: GenParams -> Env -> Message -> IO(Maybe MessageInstance,ByteString)
asmlRandomMsgGenerator genparams env mty = do {
          concrete_a <- generate $ genMessage genparams env mty ;
          -- convert to bytestring:
          MessageInstance tn r x <- return concrete_a  ;

          -- putStrLn ("####\n" ++ renderStyle style (prettyPrintMessage concrete_a))  ;

          bytestring <- return $ runPut $ runBitPut $ encodeRecord arbitraryGenerator env tn r x ;
          return (Just concrete_a,bytestring)
       }


{- -------------------------------------------------------------------------------------
   Some top-level APIs
------------------------------------------------------------------------------------- -}

-- | Top level API. An instance of the traversal engine that uses socket-based channel, and random traversal
-- strategy.
-- TO DO: separate this to a new file.
--
randomTraversal :: FilePath -> FilePath -> String -> (HostName, PortNumber)
                   -> GenParams -- parameters for the underlying msg-generator
                   -> Int       -- aggresiveness
                   -> Timeout
                   -> Int       -- max. number of cycles to run
                   -> Int       -- reporting verbosity, 10 is very verbose, 7 would surpress all coverage reports
                   -> IO (FullTraversalStatus, TraversalState ())
randomTraversal dir aislFile nameOfActorUnderTheTest (hostname,port)
                msggenparams aggresiveness timeout maximumCycleCount verbosity
    =
    traversal_using_socketchannel__ dir aislFile nameOfActorUnderTheTest (hostname,port) msggenparams timeout verbosity strategy
    where
    strategy = randomStrategy aggresiveness maximumCycleCount

-- top level API
gotoStateTraversal :: FilePath -> FilePath -> String -> (HostName, PortNumber)
                      -> StateID
                      -> GenParams -- parameters for the underlying msg-generator
                      -> Int -> Timeout -> Int
                      -> Int       -- report verbosity
                      -> IO (FullTraversalStatus, TraversalState ())
gotoStateTraversal dir aislFile nameOfActorUnderTheTest (hostname,port) targetState
                   msggenparams aggresiveness timeout maximumCycleCount verbosity
    =
    traversal_using_socketchannel__ dir aislFile nameOfActorUnderTheTest (hostname,port) msggenparams timeout verbosity strategy
    where
    strategy = gotoStateStrategy aggresiveness maximumCycleCount targetState

-- an instance of the generic traversal that uses socket-channel
traversal_using_socketchannel__ dir aislFile nameOfActorUnderTheTest (hostname,port)
                                msggenparams timeout verbosity strategy
     = do {
     -- connecting to the host:
     socket_   <- socket AF_INET Stream defaultProtocol ;
     hostEntry <- getHostByName hostname ;
     connect socket_ $ SockAddrInet port $ hostAddress hostEntry ;

     -- setting up the traversal:
     Right aislModule <- aislCompiler dir aislFile ;
     socketchannel <- socketChannel aislModule socket_ timeout (const writelog) ;
     tvparams      <- return $ TraversalParameter {
         channel = socketchannel,
         traversalStrategy = strategy,
         msgGenerator = (\traversalstate env mty -> asmlRandomMsgGenerator msggenparams env mty)  , -- use ASML's default msg-gen with default configuration
         customInfoUpdater = trivialInfoupdater,
         initialCustomInfo = (),
         writelog = putStr,
         reportResult = resultreporter,
         reportVerbosity = verbosity
         } ;
     -- running the traversal:
     traversalStatus <- traversal tvparams aislModule nameOfActorUnderTheTest ;
     -- closing the connection to the server and returning the final traversal info:
     close socket_ ;
     return traversalStatus

   }
   where
   aislCompiler = compile
   writelog = putStr
   trivialInfoupdater x y z = ()
   resultreporter _ = "-- no custom state"


--
-- Few test tiles to test the protocols from the case studies.
--
myTestWebsocket aggresiveness timeout maximumCycleCount verbosity
   =
   randomTraversal "../casestudies/websocket" "websocket.aisl" "WebsocketServer" ("localhost",9000)
                   -- parameters for the msg-generator; in particular don't change the max-int bound
                   -- to trigger websocker 3rd tier payload:
                   (GenParams { size = Nothing, intBounds = Just (0, 2^16 + 1)})
                   aggresiveness
                   timeout
                   maximumCycleCount verbosity

myTestWebsocketToState targetState aggresiveness timeout maximumCycleCount verbosity
   =
   gotoStateTraversal "../casestudies/websocket" "websocket.aisl" "WebsocketServer" ("localhost",9000)
                   targetState
                   -- parameters for the msg-generator; in particular don't change the max-int bound
                   -- to trigger websocker 3rd tier payload:
                   (GenParams { size = Nothing, intBounds = Just (0, 2^16 + 1)})
                   timeout
                   maximumCycleCount verbosity


wsockStrategy1 =  gotoStateStrategy 2 200 (Normal "SendingContinuation")
                  `Then`
                  strategySend1 "MaskedMessageFrame"

wsockStrategy2 =  gotoStateStrategy 2 200 (Normal "SendingContinuation")
                  `Then`
                  strategySend1 "MaskedMessageStartFrame"

wsockStrategy3 =  gotoStateStrategy 2 200 (Normal "SendingReceivingContinuation")
                  `Then`
                  strategySend1 "MaskedContinuationFrame"

wsockStrategy4 =  gotoStateStrategy 2 200 (Normal "SendingReceivingContinuation")
                  `Then`
                  strategySend1 "MaskedMessageEndFrame"


myTestWebsocketWithStrategy strategy timeout verbosity
   =
   traversal_using_socketchannel__ "../../casestudies/websocket" "websocket.aisl" "WebsocketServer" ("localhost",9000)
                                   (GenParams { size = Nothing, intBounds = Just (0, 2^16 + 1)})
                                   timeout
                                   verbosity
                                   strategy

{- Have not put back the IMAP case study.
myTestIMAP timeout maximumCycleCount
   =
   randomTraversal "../../casestudies/" "imap.aisl" "IMAPServer" ("localhost",143)
                   timeout
                   maximumCycleCount
-}



