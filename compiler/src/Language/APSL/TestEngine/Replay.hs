-- | Provide functionalities to replay and reduce a trace.
--
--
module Language.APSL.TestEngine.Replay where
    
    
import Language.APSL.TestEngine.Traversal  
import Language.APSL.AISL.DataType.LTS  
import Language.APSL.AMSL.DataType.Module hiding (Module, messages)
import Language.APSL.AISL.Compiler hiding (actors)
import Language.APSL.AISL.DataType.Module
import Language.APSL.Channel.SocketChannel

import Data.List

import Network.Socket
import Network.BSD
import Data.Time.Clock
import Data.ByteString.Lazy (ByteString)     

import Debug.Trace
    
-- | API. Replay a saved trace file.
--   
replay :: FilePath -> FilePath -> String -> (HostName, PortNumber) -> Timeout -> FilePath
          -> IO (FullTraversalStatus, TraversalState ())
replay dir aislFile nameOfActorUnderTheTest (hostname,port) timeout trfile = do {
      trace <- loadTrace trfile ;
      replay_ dir aislFile nameOfActorUnderTheTest (hostname,port) timeout trace
   }

-- The worker function of replay.
replay_ ::  FilePath -> FilePath -> String -> (HostName, PortNumber)
            -> Timeout 
            -> Trace
            -> IO (FullTraversalStatus, TraversalState ())

replay_ dir aislFile nameOfActorUnderTheTest (hostname,port) timeout mytrace = do {

    -- connecting to the host:
    socket_   <- socket AF_INET Stream defaultProtocol ;
    hostEntry <- getHostByName hostname ;
    connect socket_ $ SockAddrInet port $ hostAddress hostEntry ;
    
    -- setting up the traversal:
    Right aislModule <- aislCompiler dir aislFile ; 
    socketchannel <- socketChannel aislModule socket_ timeout writelog ;
    tvparams      <- return $ TraversalParameter {
        channel           = socketchannel,      
        traversalStrategy = replayStrategy,     
        msgGenerator      = nextMsg,  
        customInfoUpdater = trivialInfoupdater,
        initialCustomInfo = (),     
        writelog = putStr,
        reportResult = resultreporter
        } ;
        
    -- now do the traversal:
    traversal tvparams aislModule nameOfActorUnderTheTest
   }
    
   where
       
   aislCompiler = compile
   writelog = putStr
   
   trivialInfoupdater x y z = ()
   resultreporter _ = "-- no custom state"
    
   n = length mytrace        
   revtrace = reverse mytrace
   
   isSendAction (Send m) = True
   isSendAction _ = False
   
   replayStrategy _ _ travstate = 
       if n==0 || k==n      then return $ DecideToStop "Replay is complete." 
       else if k>0 && mty1 /= mty2 then return $ DecideToStop ("Replay is aborted because inconsistent " ++ show (k-1) ++ "-th step, expecting " 
                                                         ++ show mty2 ++ ", receiving " ++ show mty1)
       else do {
         (a,_,_) <- return $ revtrace !! k ;
         case a of
            Receive atyName -> trace ("## " ++ show a) $ return $ DecideToSendToIUT atyName
            _               -> return DecideToWaitForInputFromIUT
       }
       
       where
       k = length $ trtrace travstate
       (mty1,_,_) = head $ trtrace travstate 
       (mty2,_,_) = revtrace !! (k-1)
                   
   
   nextMsg travstate _ _ = case revtrace !! k of
                             (Receive _ ,_, Just bytestr) -> return (Nothing,bytestr)
                             a   -> error ("Replay is aborted because of inconsistent " ++ show k ++ "-th step.")
       where
       k = length $ trtrace travstate

-- Calculate all idempotent sequence of actions in an lts. A sequence z is idempotent if:
--
--   (1) on all states s in the lts, it is either not possible fully execute z, or the only
--       possible state after z is s itself (also taking tau-transitions into account).
--
--   (2) on at least one state s in the lts, it is possible to fully execute z, and results
--       in s itself as the only possible final state.
--
--  We will only calculate idempotent sequences of length at most 2.
--  We will also filter the resulting sequences to throw away sequences that are redundant
--  (their idempotencies follow from other sequences' idempotencies).
--
getIdempotents :: LTS -> [[Action]]
getIdempotents lts = filterRedundant $ filter isIdempotent candidates
    where
    -- true only when the action can fire on all of the given states
    demonicEnabled action states = case action of
        Send _     ->  all ok1 states
        Receive _  ->  all (\s -> ok1 s || ok2 s) states
        where
        ok1 s = not $ null $ nextStatesAfterTransition lts s action
        ok2 s = not $ null $ nextStatesAfterTransition lts s ReceiveUnexpected
        
    -- if the action can be executed on all the given states, return all the states
    -- that can be reached by executing it,followed by tau-transitions after it.
    -- Else returns empty.
    demonicExec1 :: Action -> [StateID] -> [StateID] 
    demonicExec1 action states = if demonicEnabled action states then nextstates else []
        where
        nextstates1 = concat $ map (\s-> nextStatesAfterTransition lts s action) states
        nextstates2 = concat $ map (\s-> nextStatesAfterTransition lts s ReceiveUnexpected) states
        nextstates3 = case action of
            Send _    -> nub $ nextstates1
            Receive _ -> nub $ nextstates1 ++ nextstates2
    
        nextstates  = nub $ concat $ map (reachableTauStates lts) nextstates3
    
    demonicExec :: [Action] -> [StateID] -> [StateID] 
    demonicExec [] states       = nub $ concat $ map (reachableTauStates lts) states
    demonicExec (a:rest) states = demonicExec1 a $ demonicExec rest states
    
    allstates = allStates lts
    allactions = nub [ a | (_,a,_) <- allTransitions lts, isSendOrReceive a ]
       where
       isSendOrReceive (Send _)    = True
       isSendOrReceive (Receive _) = True
       isSendOrReceive _ = False
        
    
    allActionPairs = [[a,b] | a<-allactions, b<-allactions ]   
      
    candidates = map (\a->[a]) allactions ++ allActionPairs
     
    isIdempotent actions = all (\s-> let v = demonicExec actions [s] in null v || v==[s]) allstates
                           &&
                           any (\s-> demonicExec actions [s] == [s]) allstates
    
    filterRedundant :: [[Action]] -> [[Action]] 
    filterRedundant z = singletons ++ filter (not . isRedundant) doubles
        where
        isSingle [a] = True
        isSingle _   = False
        singletons = filter isSingle z
        doubles = filter (not . isSingle) z
        
        isRedundant [a,b] = [a] `elem` singletons && [b] `elem` singletons
        
--
-- Trace minimizers, employing algebraic reduction a la Elyasov et al. Given a trace,
-- this will extract all idempotent sequences of length max. 2 that are possible on
-- the given lts, and uses these to rewrite/reduce the target trace.
-- The result is a list of trace on various stadia of reduction, with the first one
-- being the most reduced one.
--
minimize :: LTS -> Trace -> [Trace]      
minimize lts trace = worker [trace] trace
   where     
   idempotents = getIdempotents lts

   isSingle [a] = True
   isSingle _   = False

   singletonRules = map (\[a]->a) $ filter isSingle idempotents
   pairRules = filter (not . isSingle) idempotents
   
   minimalize1 trace = filter (not . canDrop) trace
      where
      canDrop (Receive mty, _ , _ ) =  Receive mty `elem` singletonRules
      canDrop _ = False
      
   minimalize2 trace = case trace of
      []  -> trace
      [_] -> trace
      ((Send mty,mi,bs) : rest) -> (Send mty,mi,bs) : minimalize2 rest
      (a:b:rest) -> if canDrop [a,b] then minimalize2 rest else a: minimalize2 (b:rest)
      where
      canDrop [(mtya,_,_),(mtyb,_,_)] = [mtya,mtyb] `elem` pairRules
      
   getActions trace = map (\(a,_,_)->a) trace
         
   worker :: [Trace] -> Trace -> [Trace]
   worker collected trace = if trace2_ == trace_ then collected
                            else if trace1_ == trace_ then worker (trace2:collected) trace2
                            else worker (trace2:trace1:collected) trace2
       where
       trace_ = getActions trace
       trace1 = minimalize1 trace
       trace2 = minimalize2 trace1
       trace1_ = getActions trace1
       trace2_ = getActions trace2
   
 ---
 --- 
 ---
replayWebsocket timeout trfile = replay "../../casestudies/websocket" "websocket.aisl" "WebsocketServer" 
                                        ("localhost",9000) 
                                        timeout trfile


testGetIdempotents dir aislFile actorName = do {
       Right aislModule <- aislCompiler dir aislFile ; 
       lts <- return $ head [lts_ | (name,lts_) <- actors aislModule, name == actorName ] ;
       putStrLn "========" ;
       putStrLn (show (allTransitions lts)) ;
       putStrLn "========" ;
       
       return $ getIdempotents lts 
   }
   where
   aislCompiler = compile
