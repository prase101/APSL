{-# LANGUAGE DeriveGeneric #-}

-- | Provides utilities to save the coverage information to a file and to merge
--   coverage information from multiple files to calculate an aggregate coverage
--   information.
--   
--   Currently, only trasition coverage information can be saved and merged.
--
module Language.APSL.TestEngine.CoverageMergeUtil where 

import Language.APSL.TestEngine.LTSCoverage
import Language.APSL.AISL.DataType.LTS

import           Data.Map (Map)
import qualified Data.Map as M
import           Data.Set (Set)
import qualified Data.Set as S
import qualified Data.ByteString as ByteString
import Data.List (nub)

import System.IO
import System.Time
import GHC.Generics
import Data.Serialize
import System.Directory


-- serializable representation of transition coverage info
data TransitionCoverage__ = TransitionCoverage__ [ ((StateID, Action, StateID), Float) ] deriving (Show, Generic)
instance Serialize TransitionCoverage__

normalizeFileName_ fn extension = if ext == extension then fn else fn ++ extension
   where
   n = length fn
   ext = drop (n- length extension) fn
   
-- how to make this platform independent??
pathSeparator = "/"   

-- | Use this to save a given transition coverage info to a file. You need to
-- specify the name of the file.
--
saveTransitionCoverageInfo :: FilePath -> TransitionCoverage -> IO()
saveTransitionCoverageInfo fname tc = ByteString.writeFile nfname $ encode tc__
   where
   nfname = normalizeFileName_ fname ".tcov"
   tc__ = TransitionCoverage__ $ M.assocs tc
   
mk_tstamp_fname = do {
    TOD seconds picos <-  getClockTime ;
    return ("F" ++ show seconds ++ "_" ++ show picos)
}
  
-- | Use this to save a given transition coverage info to a file. The 
-- file name will be generated form system time. It will be saved to 
-- the given directory.
--
saveTransitionCoverageInfo_ :: FilePath -> TransitionCoverage -> IO()
saveTransitionCoverageInfo_ dir tc = do {
   fn <- mk_tstamp_fname ;
   saveTransitionCoverageInfo (dir ++ pathSeparator ++ fn ++ ".tcov") tc
}
     

loadTransitionCoverageInfo :: FilePath -> IO TransitionCoverage
loadTransitionCoverageInfo fname = do { 
    bs <- ByteString.readFile nfname ; 
    case decode bs of 
        Right (TransitionCoverage__ tc__) -> return $ M.fromList tc__
        Left  e  -> error e    
    }
    where 
    nfname = normalizeFileName_ fname ".tcov"
   
 
-- | Find all transition coverage info files in the given directory. Load them,
-- then merge the coverage infos into a single coverage info, and return the
-- later.
--  
mergeTransitionCoverageInfo :: FilePath -> IO(Int,TransitionCoverage)
mergeTransitionCoverageInfo dir = do {
     dircontent <- listDirectory dir ;
     tcovfiles  <- return $ filter isTransitionCovFile dircontent ;
     tcovs <- sequence [ loadTransitionCoverageInfo (dir ++ pathSeparator ++ fn) | fn <- tcovfiles ] ;
     return $ (length tcovs, mergeTransitionCoverages tcovs)
   } 
   where
   isTransitionCovFile fname = drop (length fname - 5) fname == ".tcov"

printMergedTransitionCoverageInfo :: FilePath -> IO()
printMergedTransitionCoverageInfo dir = do {
      (n,tcov) <- mergeTransitionCoverageInfo dir ;
      putStrLn ("\n** Obtained " ++ show n ++ " coverage files.") ;
      putStrLn $ printTransitionCoverageMetric tcov
   }

testDir dir = do {
     dircontent <- listDirectory dir ;
     sequence_ [ putStrLn fn | fn <- dircontent ]
}
   