{-# LANGUAGE FunctionalDependencies, FlexibleInstances, RecordWildCards,
             PatternGuards, GeneralizedNewtypeDeriving, DeriveGeneric    #-}

-- | TODO: write description
module Language.APSL.AsyncTestEngine.Traversal where

import Prelude hiding (traverse, log)

import Language.APSL.PNSL.DataType.Types
import Language.APSL.PNSL.API
import Language.APSL.PNSL.Visualize
import qualified Data.Set as S
import Data.Maybe

import qualified Data.List as L

import qualified Language.APSL.AISL.DataType.LTS as LTS (StateID(..))
import Language.APSL.Analyses.ProbLTS
import Language.APSL.Analyses.Tracker

import Language.APSL.AMSL.Compiler
import Language.APSL.AMSL.DataType.Module
import Language.APSL.AMSL.DataType.Expression(Env, emptyEnv)
import qualified Language.APSL.AMSL.DataType.Expression as E
import Language.APSL.Generator.MessageGen
import Language.APSL.Channel.Encoder
import Language.APSL.Base
import Language.APSL.Channel.MessageChannel
import Language.APSL.Channel.SocketChannel
import Language.APSL.Generator.MessageCoverage
--import Language.APSL.TestEngine.LTSCoverage
--import Language.APSL.TestEngine.CoverageMergeUtil

import           Data.Set (Set)
import qualified Data.Set as S
import           Data.Map (Map)
import qualified Data.Map as M
import Data.List (nub)

import Data.Functor.Identity
import Data.Monoid
import Data.Ratio

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State (StateT)
import Control.Monad.State
import Data.Binary.Put
import Data.Binary.Bits.Put
import Control.Monad.IO.Class
import Data.Time.Clock
import System.Random
import Test.QuickCheck.Gen
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString as StrictByteString
import Network.Socket
import Network.BSD
import GHC.Generics
import qualified Data.Serialize as Serialize
import qualified Debug.Trace as D

--
-- Verbosity levels:
--   0: only essentials
--   1: everything
--
verbosity = 0

{- -------------------------------------------------------------------------------------
   Main data structures involved in traversal.
------------------------------------------------------------------------------------- -}

-- | Representing the current traversal state.
data TraversalState info = TraversalState {
  pnslConf :: PNSLConf,
  trace :: MTrace, -- _Observed_ trace in reversal. e.g. with trace [a?, x!], a? was the last observed action
  info :: info,
  plts :: ProbLTS [Place] (Int,Action),
  cycleCount :: Int
} --deriving Show

incrCount st = st { cycleCount = cycleCount st + 1 }

initProbLTSState = LTS.Normal "0"
initProbLTS initConf = ProbLTS initProbLTSState [] M.empty (M.singleton initProbLTSState (initConf))

-- | To construct the initial traversal state.
initialTraversalState :: PNSL -> info -> TraversalState info
initialTraversalState pn info0
  = let initConf = PNSLConf {
                     protocol = pn,
                     meta = buildPNMeta pn,
                     confs = S.fromList [ConfM initProbLTSState (map (flip Marker emptyEnv) $ pn_inits pn) []]
                   }
    in TraversalState {
  pnslConf = initConf,
  trace = [],
  info = info0,
  plts = initProbLTS (pn_inits pn),
  cycleCount = 0
}

-- | Describing the result of a full traversal.
data FullTraversalStatus = IUTSendInvalidFormatMsg
                         | IUTSendInvalidMsgType [MessageLabel] MessageLabel
                         | ConnectionError
                         | ConnectionTimeOut
                         | TraversalCompleted String
                         deriving (Eq,Show)

-- | To hold various parameters to customize a traversal.
data TraversalParameters info = TraversalParameter {
  channel           :: MessageChannel,             -- a channel, for interacting with the IUT
  traversalStrategy :: TraversalStrategy info,     -- strategy that decides what to do next
  msgGenerator      :: TraversalState info -> Env -> Message -> IO(Maybe MessageInstance,ByteString),  -- the msg-generator to use
  customInfoUpdater :: CustomInfoUpdater info,     -- a function specifying how to update the custom traversal state
  initialCustomInfo :: info,                       -- initial custom traversal state
  log               :: Int -> String -> IO(),             -- function to write to log
  reportResult      :: TraversalState info -> String
}

type CustomInfoUpdater info = Action -> TraversalState info -> info

-- | A traversal delegates some decisions to a "traversal strategy", which is a function of
--   this type.
type TraversalStrategy info = StrategyDirective
                              -> TraversalState info
                              -> IO StrategyDecision

data StrategyDirective = FavorSendToIUT | FavorReceiveFromIUT | DontCare deriving Eq

data StrategyDecision = DecideToWaitForInputFromIUT
                      | DecideToSendToIUT [MessageLabel]
                      | DecideToStop String
                      | Undecided
                      deriving (Eq,Show)

{- -------------------------------------------------------------------------------------
   We first define the underlying single-step traversal over an LTS.
------------------------------------------------------------------------------------- -}

-- | Traverse a single receive-transition in the LTS. This only traverse the lts model. Note that
-- "receive" here means that the Implementation Under Test receives something. This function's role
-- is only to traverse the LTS; it is not responsible for the actual interaction with the
-- IUT.
-- Now, importantly, this function will calculate the set of possible current IUT states in the LTS.
-- If the the protocol is deterministic, the current state is unique; else it may not be unique.
--
traverse :: CustomInfoUpdater info
         -> (Action, Maybe MessageInstance, Maybe ByteString)
         -> TraversalState info -> TraversalState info
traverse infoUpdater (a,concrete_a,bytes) travStatus
   = let pnslConf' = pushGlobalStmt (pnslConf travStatus)
                   $ Statement ("cycle") (E.Constant (E.EInt $ toInteger (cycleCount travStatus)))
         pnslConf'' = analyseLess (updatePNSLConf (a,fromJust concrete_a)
                                                  (pnslConf'))
     in travStatus {
--      possibleCurrentStates = nextStates2,
      pnslConf = pnslConf'',
      trace = (a,concrete_a,bytes) : trace travStatus,
      info = infoUpdater a travStatus,
{-      msgcov = case concrete_a of
                 Nothing  -> msgcov travStatus
                 Just a_  -> a_ `moduleMsgCoverageAdd` msgcov travStatus ,-}
      --plts = plts',
      cycleCount = cycleCount travStatus + 1
      }

type PNProbLTS = ProbLTS [Place] (Int,Action)
calculatePLTS :: TraversalState info -> PNProbLTS
calculatePLTS travStatus =
  let pn = protocol $ pnslConf $ travStatus
      initConf = pnslConf $ initialTraversalState pn (info travStatus)
      retrace = do
        (foldM step initConf (zip [0..] $ (reverse $ (Observe "End", Just undefined, Nothing) : (trace travStatus))))
        --confMs' <- mapM (tryReachEnd pn) (S.toList $ confs conf)
      (conf',problts) = analyse (plts travStatus) retrace
  in problts { finalStates = map c_pid (S.toList $ confs $ conf') }
  where step :: PNSLConf -> (Int,(Action, Maybe MessageInstance, Maybe ByteString)) -> Analyzer [Place] (Int,Action) PNSLConf
        step conf (cycle,(a, Just concrete_a,_)) = do
          let conf' = pushGlobalStmt conf
                    $ Statement ("cycle") (E.Constant (E.EInt $ toInteger cycle))
          --D.trace ("analysis step: " ++ show cycle ++ ", " ++ show a) (return ())
          conf'' <- updatePNSLConf (a, concrete_a) conf'
          return conf''

analyseProbLTS :: Show evTy => PN evTy -> PNProbLTS -> String
analyseProbLTS pn probLTS =
  let execs = getAllExecs probLTS (-30,Tau)
      stats = nub $ map execCovs execs
  -- in show tsCov
  in if length stats == 1
      then head stats
      else "ExecCovs length: " ++ show (length stats) ++ "\n.." ++ L.intercalate "\n.. or ..\n" stats
  where tCov exec t = (t_id t, length $ filter (\(_,(t_id',_),_) -> t_id t == t_id') exec, t_ev t)
        execCovs exec = let tsCovs = map (tCov exec) (pn_transitions pn)
                            cov = filter (\(_,n,_) -> n > 0) tsCovs
                            n1 = length cov
                            n2 = length tsCovs
                            stats = show (fromIntegral (n1*100) / fromIntegral n2)
                                    ++ "% (" ++ show n1 ++ "/" ++ show n2 ++ ")"
                            sum   = foldr (+) 0 (map (\(_,x,_) -> x) tsCovs)
                        in "******\nTransition coverage:\n" ++
                           L.intercalate "\n" (map (\(a,b,c) -> strip '\n' $ "\t" ++ show a ++ "\t" ++ show b ++ "\t" ++ show c ) tsCovs) ++
                           "\t" ++ stats ++ "\t" ++ show sum ++
                           "\n******\n"


traverses :: CustomInfoUpdater info
         -> [((Maybe MessageInstance, ByteString), Action)]
         -> TraversalState info -> (TraversalState info, Int)
traverses infoUpdater concretes travStatus =
  case foldl singleStep (Right (travStatus,0)) concretes of
    Right (status,i) -> (status,i)
    Left (status,i) -> (status,i)
  where singleStep (Right (status,i)) ((msgInstance, bs),a) =
          let status' = traverse infoUpdater (a,msgInstance,Just bs) status
          in if i > 0 && null (confs $ pnslConf status')
               then Left (status,i)
               else Right (status',i + 1)
        singleStep (Left (status,i)) _ = Left (status,i)
{- -------------------------------------------------------------------------------------
   Defining the main traversal function.
------------------------------------------------------------------------------------- -}

-- | This is the traversal engine. It will produce a single traversal over the IUT.
-- At each step, it invokes a "traversal strategy" (which is part of the parameter to the traversal)
-- to decide whether to wait for a message from the IUT or to send a message to the IUT.
-- If the startegy decides to send a message, it only decides which message type to send.
-- The traversal function then invokes a msg-generator (which is one of its parameters) to
-- create a valid instance of the message type.
--
traversal :: TraversalParameters info  -- record specifying various traversal parameters
             -> PNSL                   -- AISL module containing the interaction model of the IUT
             -> Module
             -- -> String                 -- the name of the actor in the AISL model that describes the IUT
             -> IO (FullTraversalStatus, TraversalState info)

traversal tparams
          pn
          amsl
          -- nameOfActorUnderTheTest
          =
          do {
            writeln 0 ("** Starting a traversal of " ++ "name disabled" ++ {- nameOfActorUnderTheTest ++ -} "\n=====") ;
            initVisual <- visualizationNum ;
            -- construct the initial traversal state:
            state0 <- return $ initialTraversalState pn (initialCustomInfo tparams) ;

            visualizePNConfs (pnslConf state0) [] ;
            openVisualizations initVisual (initVisual + 1) ;

            fromVisual <- visualizationNum ;

            writeln 0 "Starting communication with SUT";
            -- run the traversal (the worker function):
            r@(status,finalTraversalState) <- worker DontCare state0   ;
            writeln 0 "Finished communication with SUT";

            --
            -- Now, produce reports and save the trace ...
            --

            -- reporting msg coverage:
            -- writeln $ printModuleMsgCoverage $ msgcov finalTraversalState ;

            -- reporting and saving the trace:
            tr <- return $ reverse $ map (\(mtype,_,_)->mtype) $ trace finalTraversalState ;
            -- writeln ("\n\n### taustar LTS =\n" ++ show (tauStarReduction lts)) ;

            writeln 1 ("\n\n### trace =\n" ++ show tr) ;
            -- etree <- return $ getExecutionTree__ lts trace) ;
            -- writeln ("\n\n### execution tree =\n" ++ printExecutionTree__ etree) ;
            -- saveTrace "." $ trace finalTraversalState ;

            -- reporting and saving transition coverage:
            -- trcov <- return $ calculateTransitionCoverage lts $ trace  ;
            -- writeln $ printTransitionCoverageMetric trcov ;
            -- saveTransitionCoverageInfo_ "." trcov ;

            -- reporting verdict:
            writeln 0 $ reportResult tparams finalTraversalState ;
            reportTraversalState finalTraversalState ;
            (case status of
               TraversalCompleted why -> writeln 0 ("SUCCESS. The traversal was completed because " ++ why ++ ". No error was found.")
               ConnectionError    -> do { writeln 0 "PROBLEM. Connection error." }
               ConnectionTimeOut  -> do { writeln 0 "PROBLEM. Connection has timed out." }
               IUTSendInvalidFormatMsg -> do { writeln 0 "FORMAT ERROR. The IUT sent a message with an invalid format."  }
               IUTSendInvalidMsgType expected a -> do {
                    writeln 0 "IO CONFORMANCE ERROR. The IUT sent a message of an unexpected type." ;
                    writeln 0 ("   One of these is expected: " ++ show expected) ;
                    writeln 0 ("   But the IUT sent an " ++ a)
                  }
               ) ;
            writeln 0 ("=====") ;
            toVisual <- visualizationNum ;
            openVisualizations (if toVisual - fromVisual > 10 then toVisual - 10 else fromVisual) toVisual ;
            return r
          }

          where
          msgsEnv = moduleEnv amsl
          msgs = M.fromList [(unTypeName $ messageName msg, (msg, msgsEnv)) | msg <- messages amsl ]

          fromJust (Just x) = x

          strategy        = traversalStrategy tparams
          customUpdater   = customInfoUpdater tparams
          receiveFromIUT_ = receiveFromIUT $ channel tparams
          sendToIUT_      = sendToIUT $ channel tparams

          possibleObservableMsgs currentPlaces travstate = nub
                                                         $ map getMessageLabel
                                                         $ filter isObserveAction
                                                         $ possibleNextEvents
                                                         $ pnslConf travstate
          -- the actual worker function of traversal
          worker strategyDirective travstate = do {
             -- the idea is simple:
             --    (1) invoke the traversal strategy to decide what to do next
             --    (2) carry out the decision
             --    (3) recurse
             --
             currentPlaces <- return $ possibleCurrentPlaces (pnslConf travstate) ;
             printProgress travstate ;
             printCurrentState travstate ;
             writeln 1 (show $ possibleObservableMsgs currentPlaces travstate) ;

             decision <- strategy strategyDirective travstate ;
             case decision of
               DecideToStop why -> {- the strategy wants to stop -} return (TraversalCompleted why, travstate)

               DecideToWaitForInputFromIUT -> {- the stratgy wants to wait for msg from the IUT -}
                  do { status <- receiveFromIUT_ $ possibleObservableMsgs currentPlaces travstate ; -- get a msg from the IUT
                       case status of
                         ReceiveOK a a_ bs -> do
                              -- updte the traversal state, then recurse:
                              let travstate' = traverse customUpdater (Observe a, a_, bs) travstate
                              --visualizePNConfs (pnslConf travstate') (map (\(a,_,_) -> a) $ trace travstate')
                              -- worker DontCare travstate'
                              worker FavorSendToIUT travstate'
                         -- case when the received msg is not among what is expected:
                         Receive_WrongMsgError expected a ->  return (IUTSendInvalidMsgType expected a, travstate)
                         -- case then the recived msg has invalid format:
                         Receive_MsgWithIllegalFormatError errmsg -> return (IUTSendInvalidFormatMsg,travstate)
                         -- othe problems:
                         ReceiveConnectionError -> return (ConnectionError,travstate)
                         ReceiveTimeOut         -> {- the IUT is silent; see if we can send something to it -} do {
                                writeln 1 "... the IUT has timed out." ;
                                worker FavorSendToIUT (incrCount travstate)
                             }
                  }


               DecideToSendToIUT as -> do {- the strategy wants to send a message of type a to the IUT -}
                  -- should lead to somewhere
                  let as' = map (\a -> fromJust $ M.lookup a msgs) as
                  -- invoke the msg-generator to create an instance of a
                  concretes <- mapM (\(a_,env) -> (msgGenerator tparams) travstate env a_) as'
                  -- calculate the new traversal state:
                  (travstate2,i) <- return $ traverses customUpdater (zip concretes (map Control as)) travstate
                  -- now send the msgs:
                  let msgs = zip as (map snd concretes)
                  status <- mapM sendToIUT_ (take i msgs)
                  if allOK status
                    then --visualizePNConfs (pnslConf travstate2) (map (\(a,_,_) -> a) $ trace travstate2) >>
                         worker DontCare travstate2
                    else return (ConnectionError,travstate)
                  where allOK [] = True
                        allOK (SendOK : ss) = allOK ss
                        allOK _ = False
          }
          -- for logging the string s
          write n s = (log tparams) n s
          writeln n s = (log tparams) n (s ++ "\n")

          printProgress travstate = write 1 ("... [" ++ show (length (trace travstate))
                                                 ++ "|" ++ show (cycleCount travstate)
                                                 ++ "|" ++ show (S.size (confs $ pnslConf travstate))
                                                 ++ "]" )

          printCurrentState travstate = return () {-do
                                        writeln "Current petrinet state:"
                                        write (show $ pnslConf travstate)
-}
          reportTraversalState travstate = do {
             writeln 0 ("=====") ;
             writeln 0 ("Traversal state:") ;
             writeln 0 ("\t" ++ show (confs $ pnslConf travstate));
             writeln 0 ("   > IUT possible current states: (" ++ show n ++ "): " ++ show cstates) ;
             writeln 1 ("   > Traces so far (" ++ show k ++ "): " ++ show (reverse trace_)) ;
             writeln 0 ("=====") ;
             writeln 0 ("******** ProbLTS ********") ;
             let problts = assignEqualProb $ purgeDanglings $ calculatePLTS travstate
             in do
               -- writeln (printPLTS (assignEqualProb $ purgeDanglings $ calculatePLTS travstate))
               writeln 0 (analyseProbLTS (protocol $ pnslConf travstate) problts)
          }
              where
              cstates = possibleCurrentPlaces (pnslConf travstate)
              n       = length cstates
              trace_  = map (\(mtype,_,_)->mtype) $ trace travstate
              k       = length trace_
              markFinal state problts =
                problts { finalStates = map c_pid (S.toList $ confs $ pnslConf state) }

type MTrace = [(Action, Maybe MessageInstance, Maybe ByteString)]

{-
saveTrace :: FilePath -> MTrace -> IO()
saveTrace dir trace = do
   fn <- mk_tstamp_fname
   let nfname = dir ++ pathSeparator ++ fn ++ ".trace"
   -- unfortunately, MessageInstance cannot be made serializable (at least, I don't know how)
   -- So we will only save the msgtype and the bytestrings.
   -- Furthermore, getting the bytestrings sent by the IUT from the channel is more difficult
   -- than I thought. So for now, we don't /can't put them into the trace.
   let trace_ = map (\(mtype,minstance,bytes) -> (mtype,bytes)) trace
   let bytes  =  Serialize.encode $ trace_
   -- StrictByteString.putStrLn bytes
   StrictByteString.writeFile nfname $ Serialize.encode $ trace_
   putStrLn $ "** Saved a trace to " ++ nfname


loadTrace :: FilePath -> IO Trace
loadTrace fname = do {
    bs <- StrictByteString.readFile (normalizeFileName_ fname ".trace") ;
    case Serialize.decode bs of
        Right trace -> return $ map (\(mtype,bytes) -> (mtype,Nothing,bytes)) trace
        Left  e     -> error e
    }
-}


{- -------------------------------------------------------------------------------------
   Providing two simple strategies: random and goto-state.
------------------------------------------------------------------------------------- -}

-- This is a strategy simply randomly chooses the next transition from the set of valid
-- transitions in the given model.
--
-- The strategy is parameterized by an aggresiveness level. On the aggressiveness level 0, the
-- startegy will only send a msg m to the IUT when it is sound to do so. That is,
-- if sending m to the IUT is allowed by all possible IUT's current places.
--
-- On aggressiveness level 1, it sends a message to the IUT if there is one possible current
-- place that can accept it as an expected message.
--
-- On aggressiveness level 2, it is as level 1, but it can also send any rec-message to the IUT,
-- if one of its current state has an outgoing ReceiveUnexpected transition.
--
randomStrategy :: Int->Int -> TraversalStrategy info

-- Note: TraversalStrategy info is defined as: StrategyDirective  -> LTS -> TraversalState info -> IO StrategyDecision

randomStrategy aggresiveness maximumCycleCount directive travstate
   =
   genericrandomStrategy aggresiveness maximumCycleCount (\lbls -> mapM choser (filter (not . null) lbls)) directive travstate
   where
   choser possibleNextMsgTypesToSendToIUT = do {
     n <- return $ length possibleNextMsgTypesToSendToIUT ;
     k <- getStdRandom (randomR (0,n-1)) ; -- using system random, not ideal!
                                           -- using pseudo random requires it to be threaded, which
                                           -- does not really fit in the current architecture. TO DO.
     return $ (possibleNextMsgTypesToSendToIUT !! k)
   }

-- a support type to represent the function that decides which message type to send to the IUT,
-- given a selection to choose from.
type Choser = [[MessageLabel]] -> IO [MessageLabel]

genericrandomStrategy :: Int -> Int -> Choser -> TraversalStrategy info
genericrandomStrategy aggresiveness maximumCycleCount choser directive travstate
     =
     --
     -- This is very error prone unfortunately, we will divide this into a number of cases.
     --
     --
     -- Case (1) is easy: when the maximum number of cycles is reached:
     if cycleCount travstate >= maximumCycleCount    then return $ DecideToStop "maximum number of traversal count is reached"
     --
     -- Case (2) when "Exit" is the only possible current state. Then we know for sure that the IUT is done.
     -- Note that when "Exit" is not the only possibility, it is not sound to infer that IUT is done.
     else if all (isExitPlace (protocol $ pnslConf travstate)) currentPlaces then return $ DecideToStop ("the model has definitely arrives at its exit state: " ++ show currentPlaces)
     --
     -- Case (3) is when either the IUT cannot send or a directive "FavorSendToIUT" has been given
     else if null possibleNextMsgTypesFromIUT  || directive == FavorSendToIUT then
          (
          --
          -- Case (3a) We infer that IUT has become stuck (it can neither receive nor send, but it MAY NOT be in the Exit state)
          if sendingToIUTisNotPossibleAtAll
             && null possibleNextMsgTypesFromIUT         then return $ DecideToStop "the model indicates that no further interaction is possible while it might not be in the Exit state."
          --
          -- Case (3b) No msg can be (soundly, depending on the aggresiveness) sent to the IUT, but the IUT can send. Then wait for a msg from IUT:
          else if null possibleNextMsgTypesToSendToIUT
               && not (null possibleNextMsgTypesFromIUT) then return DecideToWaitForInputFromIUT
          --
          -- Case (3c) No msg can be (soundly) sent to the IUT, but the IUT cannot send anything either:
          else if null possibleNextMsgTypesToSendToIUT
               && null possibleNextMsgTypesFromIUT then return $ DecideToStop "the IUT cannot be soundly further tested. The model indicates that a msg should be sent to the IUT, but no msg can be soundly sent to it."
          --
          -- Case (3d) It is possible to (soundly) send a msg to the IUT; let's do so:
          else do {
              msgTypesToSend <- choser possibleNextMsgTypesToSendToIUT ;
              return $ DecideToSendToIUT msgTypesToSend
              }
          )
     --
     -- Case (4) the IUT can send and no directive to favor "sending to IUT" was given.
     -- Decide to wait for a msg from IUT:
     else return DecideToWaitForInputFromIUT

     where

     currentPlaces = possibleCurrentPlaces
                   $ pnslConf travstate
     possibleNextMsgTypesFromIUT = map getMessageLabel
                                 $ filter isObserveAction
                                 $ possibleNextEvents
                                 $ pnslConf travstate


     possibleNextMsgTypesToSendToIUT =
          if aggresiveness<=0 then (map . map) getMessageLabel $ soundControlEvents (pnslConf travstate)
          else if aggresiveness==1 then undefined -- map getMessageLabel $ possibleControlEvents (pnslConf travstate)
          else undefined -- [  m | m <-allPossibleMsgTypesToSendToIUT,
                      -- any (\state-> canReceiveUnexpected lts state || canReceive lts state m) currentstates ]

     -- Determites which msgs can be SOUNDLY sent to the IUT. Note that currentstates may contain
     -- multiple states due to non-determinism. Only a msg that are allowed by all current states
     -- are sound to send. We will however exclude non-Exit current states that can only send.
     --
     {-
     possibleNextMsgTypesTo_SOUNDLY_SendToIUT =
         let
         -- exclude non-Exit current states, which can only send:
         currentstates2 = [ s | s<- currentstates, canReceiveUnexpected lts s || s==Exit || not (null (outGoingReceives lts s))]
         in
         if Exit `elem` currentstates2
            -- if Exit is in the remaining current then it is not sound to send any msg to the IUT:
            then []
            -- else we check which receive msgs can be soundly send to the IUT. Since the IUT may contain
            -- a "ReceiveUnexpected" transition, we will quantify over all possible receive msgs in the
            -- whole actor:
            else [  m | m <-allPossibleMsgTypesToSendToIUT,
                        all (\state-> canReceiveUnexpected lts state || canReceive lts state m) currentstates2  ]

     allPossibleMsgTypesToSendToIUT  = possibleReceiveMsgs lts -- we'll limit to quantifying over rec-msgs
      -}
     -- check if sending a message to the IUT is at all possible
     sendingToIUTisNotPossibleAtAll = null $ possibleControlEvents (pnslConf travstate)
{-

--
-- A startegy that tries to reach a given target state. If the target state is (definitely) reached,
-- the strategy stops. Else it re-use the generic random strategy above. Whenever the random strategy
-- decides to send a message to the IUT, this gotoState strategy below will choose a message type
-- that will keep the chance of reaching the target state non-zero. If no such action is possible on
-- the current state(s), the one message type will be randomly chosen, from the choices offered by
-- the underlying random strategy.
--
gotoStateStrategy :: Int->Int -> StateID -> TraversalStrategy info
gotoStateStrategy aggresiveness maximumCycleCount targetState directive lts travstate
   =
   if currentStates == [targetState]
   then return $ DecideToStop ("Target state " ++ show targetState ++ " is definitely reached.")
   else if aggresiveness >= 1 && targetState `elem` currentStates
           -- on an aggressive setting, stop if the target state is a possible current state
   then return $ DecideToStop ("Target state " ++ show targetState ++ " is possibly reached.")
   else genericrandomStrategy aggresiveness maximumCycleCount choser directive lts travstate
   where
   currentStates = S.toList (possibleCurrentStates travstate)

   choser lts currentstates possibleNextMsgTypesToSendToIUT = do
       let z = [ m | m <- possibleNextMsgTypesToSendToIUT, or [ canReachTarget st m | st <- currentstates ] ]
       let choices = if null z then possibleNextMsgTypesToSendToIUT else z
       n <- return $ length choices
       k <- getStdRandom (randomR (0,n-1))
       return $ (choices !! k)

       where

       statesThatCanReachTarget = [s | s <- allStates lts, canReach lts s targetState ]
       -- check if doing ?msgLabel on the state st *can* get us to a state from where the target is reachable
       canReachTarget st msglabel =
            or [ to `elem` statesThatCanReachTarget
                    | (from,a,to) <- allTransitions lts,
                      (from==st) && ((a==ReceiveUnexpected) || (a==Receive msglabel))
               ]


{- -------------------------------------------------------------------------------------
   Providing combinators to compose strategies to build up more complicated strategies.
------------------------------------------------------------------------------------- -}

orElse_ :: TraversalStrategy info -> TraversalStrategy info -> TraversalStrategy info
(s1 `orElse_` s2) sdir lts stateinfo = do
    d1 <- s1 sdir lts stateinfo
    if d1 /= Undecided then return d1 else s2 sdir lts stateinfo

default_ :: StrategyDecision -> TraversalStrategy info -> TraversalStrategy info
default_ defaultdecision strategy sdir lts stateinfo = do
    d <- strategy sdir lts stateinfo
    if d == Undecided then return defaultdecision else return d

-- Note that this combinator is not stateful (it does not remember if s2 has previously decided to stop)
--
then_ :: TraversalStrategy info -> TraversalStrategy info -> TraversalStrategy info
(s1 `then_` s2) sdir lts stateinfo = do
    d1 <- s1 sdir lts stateinfo
    case d1 of
      DecideToStop _ -> s2 sdir lts stateinfo
      _              -> return d1



{- -------------------------------------------------------------------------------------
   Providing an echo traversal; useful to test out the working of our own traversal function.
------------------------------------------------------------------------------------- -}

-- | Top level API. An instance of the traversal engine that uses of the echo channel. You don't
-- use this to test a real IUT.
echoTraversal dir aislFile nameOfActorUnderTheTest aggressiveness maximumCycleCount = do {
       Right aisl <- aislCompiler dir aislFile ;
       echoTraversal_ aggressiveness maximumCycleCount aisl nameOfActorUnderTheTest
   }
   where
   aislCompiler = compile


echoTraversal_ aggressiveness maximumCycleCount aislModule nameOfActorUnderTheTest = traversal tvparams aislModule nameOfActorUnderTheTest
   where
   msggenparams = defaultParams

   tvparams = TraversalParameter {
       channel = echoMessageChannel aislModule msggenparams,
       traversalStrategy = randomStrategy aggressiveness maximumCycleCount,
       msgGenerator = (\traversalstate env mty -> asmlRandomMsgGenerator defaultParams env mty) , -- use ASML's default msg-gen with default configuration
       customInfoUpdater = trivialInfoupdater,
       initialCustomInfo = (),
       writelog = \n -> if n <= verbosity then putStr else const $ return (),
       reportResult = resultreporter
     }

   trivialInfoupdater x y z = ()
   resultreporter _ = "-- no custom state"

-}

asmlRandomMsgGenerator :: GenParams -> Env -> Message -> IO(Maybe MessageInstance,ByteString)
asmlRandomMsgGenerator genparams env mty = do {
          concrete_a <- generate $ genMessage genparams env mty ;
          -- convert to bytestring:
          MessageInstance tn r x <- return concrete_a  ;
          bytestring <- return $ runPut $ runBitPut $ encodeRecord arbitraryGenerator env tn r x ;
          return (Just concrete_a,bytestring)
       }



{- -------------------------------------------------------------------------------------
   Some top-level APIs
------------------------------------------------------------------------------------- -}

-- | Top level API. An instance of the traversal engine that uses socket-based channel, and random traversal
-- strategy.
-- TO DO: separate this to a new file.
--
randomTraversal :: PN String -> FilePath -> FilePath -> (HostName, PortNumber)
                   -> Int -> Timeout -> Int
                   -> IO (FullTraversalStatus, TraversalState ())
randomTraversal pn dir amslFile (hostname,port) aggresiveness timeout maximumCycleCount
    =
    traversal_using_socketchannel__ pn dir amslFile (hostname,port) timeout strategy
    where
    strategy = randomStrategy aggresiveness maximumCycleCount

{-
-- top level API
gotoStateTraversal :: FilePath -> FilePath -> String -> (HostName, PortNumber)
                      -> StateID
                      -> Int -> Timeout -> Int
                      -> IO (FullTraversalStatus, TraversalState ())
gotoStateTraversal dir aislFile nameOfActorUnderTheTest (hostname,port) targetState aggresiveness timeout maximumCycleCount
    =
    traversal_using_socketchannel__ dir aislFile nameOfActorUnderTheTest (hostname,port) timeout strategy
    where
    strategy = gotoStateStrategy aggresiveness maximumCycleCount targetState
-}

-- an instance of the generic traversal that uses socket-channel
traversal_using_socketchannel__ :: PN String -> FilePath -> FilePath -> (HostName, PortNumber)
                   -> Timeout -> TraversalStrategy ()
                   -> IO (FullTraversalStatus, TraversalState ())
traversal_using_socketchannel__ pn dir amslFile (hostname,port) timeout strategy = do

     -- connecting to the host:
     socket_   <- socket AF_INET Stream defaultProtocol
     hostEntry <- getHostByName hostname
     connect socket_ $ SockAddrInet port $ hostAddress hostEntry

     -- setting up the traversal:
     compiled <- amslCompiler dir amslFile
     writelog 0 (show compiled)
     let amslModule = case compiled of
                        Right m -> m
                        Left e -> error $ show e
     socketchannel <- socketChannel_ (msgs amslModule) socket_ timeout writelog
     tvparams      <- return $ TraversalParameter {
         channel = socketchannel,
         traversalStrategy = strategy,
         msgGenerator = (\traversalstate env mty -> asmlRandomMsgGenerator defaultParams env mty)  , -- use ASML's default msg-gen with default configuration
         customInfoUpdater = trivialInfoupdater,
         initialCustomInfo = (),
         log = \n -> if n <= verbosity then putStr else const $ return (),
         reportResult = resultreporter
         } ;
     -- running the traversal:
     traversalStatus <- traversal tvparams compiledPN amslModule
     -- closing the connection to the server and returning the final traversal info:
     close socket_
     return traversalStatus
   where
   amslCompiler = compile
   compiledPN = compilePN pn
   msgs amslModule = zip (messages amslModule) (take (length (messages amslModule)) $ repeat $ moduleEnv amslModule)
   msggenparams = defaultParams -- just use the default msg-gen configuration
   writelog n
    | n <= verbosity = putStrLn
    | otherwise      = const $ return ()
   trivialInfoupdater x y = ()
   resultreporter _ = "-- no custom state"



--
-- Few test tiles to test the protocols from the case studies.
--

{-
myTestWebsocketToState targetState aggresiveness timeout maximumCycleCount
   =
   gotoStateTraversal "../../casestudies/websocket" "websocket.aisl" "WebsocketServer" ("localhost",9000)
                   targetState
                   aggresiveness
                   timeout
                   maximumCycleCount
-}

{- Have not put back the IMAP case study.
myTestIMAP timeout maximumCycleCount
   =
   randomTraversal "../../casestudies/" "imap.aisl" "IMAPServer" ("localhost",143)
                   timeout
                   maximumCycleCount

-}
