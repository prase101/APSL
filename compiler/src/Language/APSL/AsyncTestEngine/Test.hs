module Language.APSL.AsyncTestEngine.Test where

import Language.APSL.PNSL.DataType.Types
import Control.Applicative hiding (optional)
import Language.APSL.PNSL.EDSL
import Language.APSL.AsyncTestEngine.Traversal

myTestWebsocket aggresiveness timeout maximumCycleCount
   =
   randomTraversal websocketPN
                   "../casestudies/websocket"
                   "websocket.amsl"
                   --("localhost",9000)
                   ("80.60.84.110",9000)
                   aggresiveness
                   timeout
                   maximumCycleCount

websocketPN
  = optimizePN "theta"
  $ buildPN wsPNBuilder "theta"

wsPNBuilder = do
  markInit
  transition "ClientOpenHandshake?"
  transition "ServerOpenHandshake!"
  parallel [cli2serv,
            serv2cli,
            cliPing,
            servPing]
           "theta"
  transition "End!"
  markExit

cycleEnd = "15000"

cli2serv = do
  cliData
  transition ("MaskedClose? `cycle > " ++ cycleEnd ++ "`")

serv2cli = do
  servData
  transition "Close!"


cliData = do
  loop $ do
    catch "theta" (label "closeCliData") $ do
      continue "MaskedMessageFrame?"
      transition "MaskedMessageStartFrame?"
      continue "MaskedContinuationFrame?"
      transition "MaskedMessageEndFrame?"
  transitionTo "theta" (label "closeCliData")

servData = do
  loop $ do
    catch "theta" (label "closeServData") $ do
      continue "MessageFrame!"
      transition "MessageStartFrame!"
      continue "ContinuationFrame!"
      transition "MessageEndFrame!"
  transitionTo "theta" (label "closeServData")

cliPing = do
  loop $ do
    catch "theta" (label "endCliPing") $ do
      transition ("MaskedPing? {payload = msg.frame.payload_data}")
      transition "Pong! [payload == msg.frame.payload_data]"
  transitionTo "theta" (label "endCliPing")

servPing = do
  loop $ do
    catch "theta" (label "endServPing") $ do
      transition "Ping! {payload = msg.frame.payload_data}"
      transition "MaskedPong? [payload == msg.frame.payload_data]"
  transitionTo "theta" (label "endServPing")
