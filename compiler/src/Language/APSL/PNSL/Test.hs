module Language.APSL.PNSL.Test where

import Language.APSL.PNSL.EDSL
import Language.APSL.PNSL.Visualize
import Language.APSL.PNSL.DataType.Types

import Control.Applicative


pnTest pnBuilder
  = do
  let pn = buildPN pnBuilder
  putStrLn (show pn)
  putStrLn "\n\ndotcode: "
  putStrLn $ pn2dot pn
  visualizePNConfs pn



lotsofts n pnb = foldl (<**>) initPN (take n $ repeat pnb)
funBuild1 c pnb = initPN <**> parallel (Observe "a") (take c $ repeat pnb) (Control "fff")


pn2Builder
  = initPN <**>
    choice' [(Control "aaa", [pn1Builder, initPN <**> transition (Observe "forked")]),
             (Observe "bbb", [initPN <**>
                              wait ["lowerMutex"] (Control "ggg")])] <**>
    transition (Observe "beautiful")


pnExample :: PNBuilder (PN Action)
pnExample
  = initPN <**>
    parallel (Observe "a") [initPN <**> transition (Control "c1"),
                            initPN <**> transition (Control "c2")]
             (Observe "b")

pn1Builder ::PNBuilder (PN Action)
pn1Builder
  = initPN <**>
    parallel (Control "a") [upperTrack, lowerTrack] (Observe "y")

upperTrack :: PNBuilder (PN Action)
upperTrack
  = initPN <**>
    transition (Observe "c") <**>
    forkT (Observe "b") [initPN <**> labelTail "lowerMutex",
                         initPN <**> transition (Control "z")]

lowerTrack :: PNBuilder (PN Action)
lowerTrack
  = initPN <**>
    transition (Control "x") <**>
    wait ["lowerMutex"] (Observe "z") <**>
    transition (Control "f")
