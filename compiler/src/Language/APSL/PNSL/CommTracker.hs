module Language.APSL.PNSL.CommTracker (
  updateConfM
) where

--------------------------------------------------------------------------------
-- Inspired by the following articles:
--  [1] Automatically Tracking Model Based Testing of Asynchronously
--      Communicated Protocol Implementations
--        - Klomp, R. & Prasetya, S.W.B
--  [2] A formal approach to conformance testing
--        - Tretmans, J.
--------------------------------------------------------------------------------


import Language.APSL.PNSL.DataType.Types
import Language.APSL.AMSL.DataType.Expression (Env)

import qualified Language.APSL.AMSL.DataType.Module as M
import Language.APSL.Base

import Data.List
import Data.Maybe
import qualified Data.Map.Lazy as M
import qualified Data.Set as S
import qualified Debug.Trace as D

-- Update a possible configuration C^? based on a new observed trace \sigma
--
-- Either applies mu or zeta, depending on if we stay sound when ignoring
-- asynchronous effects
--
-- The trace argument _evs_ must be ordered as follows:
--    [oldestRegisteredEvent, ... , latestRegisteredEvent]
updateConfM :: PN EventTy -> PNMeta -> [(Action, M.MessageInstance)] -> ConfM -> PNAnalyzer (S.Set ConfM)
updateConfM _ _ [] confM =
  return (S.singleton confM)
updateConfM pn pnm ((ev@(Control _), msg):evs) confm@(ConfM pid ms [])
  | not $ any (== ev) asyncEvs = do
    confMs' <- zeta pn confm ((ev, msg) : [])
    confMss'' <- mapM (updateConfM pn pnm evs) confMs'
    return $ S.unions confMss''
  where asyncEvs = concatMap ((M.!) (pnm_async pnm)) (map m_place ms)
updateConfM pn pnm ((ev, msg):evs) (ConfM pid ms q) = do
  confMs' <- mu pn pnm (ConfM pid ms []) ((ev, msg) : q)
  confMss'' <- mapM (updateConfM pn pnm evs) confMs'
  return $ S.unions confMss''


-- Update a possible configuration C^? on an observed trace \sigma (_evs_)
--
-- mu considers asynchronous effects
--
-- See Definition 1. in [1] for a mathematical definition (however, do
-- note: that definition does not include the Analyzer functionality).
-- Original defined by Tretmans in [2]
--
-- \sigma is in reverse in this function.
-- The head element in \sigma is the latest registered event, tail of
-- \sigma the first registered event. Aka:
-- \sigma = [mostRecentEvent, ... , mostOldestEvent]
mu :: PN EventTy -> PNMeta -> ConfM -> [(Action, M.MessageInstance)] -> PNAnalyzer [ConfM]
mu pn _ confM [] =
  fireTaus pn confM
mu pn pnm confM ((a@(Control _), msg):evs) = do
  confMs' <- mu pn pnm confM evs
  confMs'' <- concat <$> mapM (applyControl (a, msg)) confMs'
  return $ map (\confM' -> queueEv confM' (a, msg)) confMs'
           `union` confMs''
  where applyControl :: (Action, M.MessageInstance) ->
                        ConfM ->
                        PNAnalyzer [ConfM]
        applyControl (a,msg) confM'
          | null (c_queue confM') =
            consumeEv pn (a,msg) confM'
          | otherwise =
            return []
mu pn pnm confM ((x@(Observe _), msg):evs) = do
  confMs' <- mu pn pnm confM evs
  concat <$> mapM findOutput confMs'
  where findOutput :: ConfM -> PNAnalyzer [ConfM]
        findOutput (ConfM pid ms q) = do
          confMs' <- consumeEv pn (x,msg) (ConfM pid ms [])
          S.toList
            <$> S.unions
            <$> mapM (updateConfM pn pnm (reverse q)) confMs'

-- Update a possible configuration C^? on an observed trace \sigma
--
-- zeta does _not_ consider asynchronous effects. It disregards/ignores them
-- all and acts as if all communication events in \sigma were communicated
-- synchronously.
--
-- See Definition 2. in [1] for a mathematical definition (however, do
-- note: that definition does not include the Analyzer functionality).
--
-- Theorem 3. in [1] specifies when we can safely neglect asynchronous
-- effects, and thus safely apply zeta instead of mu.
--
-- \sigma is in reverse in this function (similar to \sigma in mu).
-- The head element in \sigma is the latest registered event, tail of
-- \sigma the first registered event. Aka:
-- \sigma = [mostRecentEvent, ... , mostOldestEvent]
zeta :: PN EventTy -> ConfM -> [(Action, M.MessageInstance)] -> PNAnalyzer [ConfM]
zeta pn confM [] =
  fireTaus pn confM
zeta pn confM ((a, msg):evs) = do
  confMs' <- zeta pn confM evs
  concat <$> mapM (consumeEv pn (a,msg)) confMs'

-- Implementation of: M ==a \in L==> M'
-- Thus consumeEv is also responsible for firing any necessary theta transitions
-- . Additionally, it is responsible for firing tau transitions after
-- consuming the event.
--
-- See Definition 3. in [1] for a mathematical definition
consumeEv :: PN EventTy ->
             (Action, M.MessageInstance) ->
             ConfM ->
             PNAnalyzer [ConfM]
consumeEv pn (ev,msg) confM = do
  confMs' <- fireThetas pn ev confM
  confMs'' <- concat <$> mapM (fireTransitionsOnEv pn (ev,msg)) confMs'
  concat <$> mapM (fireTaus pn) confMs''

canFireOnEv :: Action -> [Transition EventTy] -> [Transition EventTy]
canFireOnEv ev
  = filter (\t -> (e_action . t_ev) t == ev)
