{-# LANGUAGE RecordWildCards #-}
module Language.APSL.PNSL.API (
  fireableTransitions,
  updatePNSLConf,
  possibleCurrentPlaces,
  PNSLConf(..),
  PNMeta(..),
  tryReachEnd,
  PNSL,
  buildPNMeta,
  possibleControlEvents,
  soundControlEvents,
  compilePN,
  Action(..),
  EventTy(..)
) where

import Control.Monad
import Data.List
import Data.Maybe
import qualified Data.Set.Extra as S
import qualified Data.Set as S
import qualified Data.Map as M

import Language.APSL.Analyses.Tracker

import Language.APSL.PNSL.DataType.Types
import Language.APSL.PNSL.CommTracker
import qualified Language.APSL.AMSL.Compiler as C
import qualified Language.APSL.AMSL.Compiler.Internal as C
import qualified Language.APSL.AMSL.DataType.Module as M
import qualified Language.APSL.AMSL.Grammar.Abs as G
import qualified Debug.Trace as D

compilePN :: PN String -> PNSL
compilePN pn
  = fmap compileStr pn
  where compileStr str = case C.compileActionString str of
                           Left err -> error (show err)
                           Right ast -> ast2Act ast
        ast2Act (C.TrEvent pAction pTesterGuard pGuard pStatements)
          = let act = case pAction of
                        G.TrActionObs cname -> Observe $ C.identCName cname
                        G.TrActionCtl cname -> Control $ C.identCName cname
                        G.TrActionTau       -> Tau
                        G.TrActionTheta     -> Theta
                g   = case pGuard of
                        G.TrGuard (G.ListedExp e) -> Just $ C.transExp e
                        G.TrNoGuard               -> Nothing
                tG  = case pTesterGuard of
                        G.TrTGuard (G.ListedExp e) -> Just $ C.transExp e
                        G.TrTNoGuard               -> Nothing
                ss  = case pStatements of
                        G.TrStatements stmts -> map (\(G.TrStatement v e) -> Statement (C.identLName v) (C.transExp e)) stmts
                        G.TrNoStatements     -> []
            in EventTy act tG g ss


-- buildPNMeta assumes:
--    * pn does not have any tau loops
buildPNMeta :: PNSL -> PNMeta
buildPNMeta pn =
  PNMeta {
    pnm_async = M.fromList
              $ map (\p -> (p, buildPlaceMeta p))
              $ pn_places pn
  }
  where buildPlaceMeta :: Place -> [Action]
        buildPlaceMeta p =
          let traces = filter (isObserveAction . head)
                     $ contTraces pn [p]
          in nub $ mapMaybe (find isControlAction) traces

updatePNSLConf :: (Action, M.MessageInstance) -> PNSLConf -> PNAnalyzer PNSLConf
updatePNSLConf (ev, msg) pnConf
  = PNSLConf (protocol pnConf) (meta pnConf)
      <$> S.unions
      <$> mapM updateConfM' (S.toList $ confs pnConf)
  where updateConfM' confM
          = do
          confMs' <- updateConfM (protocol pnConf) (meta pnConf) [(ev, msg)] confM
          if S.null confMs'
            then unregisterNode (c_pid confM)
            else return ()
          return confMs'

confMPlaces :: PNSL -> ConfM -> [Place]
confMPlaces pn c@(ConfM pid ms q)
  = concatMap currentPlaces' [c]
  -- $ asyncInfer pn (ConfM pid ms []) q
  where currentPlaces' :: ConfM -> [Place]
        currentPlaces' (ConfM _ ms _) = map m_place ms

possibleCurrentPlaces :: PNSLConf -> [Place]
possibleCurrentPlaces pnConf
  = concatMap (confMPlaces (protocol pnConf)) (confs pnConf)

soundControlEvents :: PNSLConf -> [[Action]]
soundControlEvents pnConf
  = nub
  $ map nub
  $ map (filter isControlAction)
  $ (map . map) (e_action . t_ev)
  $ map (fireableTransitions $ protocol pnConf) confs'
  where confs' = filter (\(ConfM _ _ q) -> null q) (S.elems $ confs pnConf)

possibleControlEvents :: PNSLConf -> [Action]
possibleControlEvents pnConf
  = nub
  $ filter isControlAction
  $ possibleNextEvents pnConf


tryReachEnd :: PNSLConf -> PNAnalyzer PNSLConf
--updatePNSLConf :: (Action, M.MessageInstance) -> PNSLConf -> PNAnalyzer PNSLConf
tryReachEnd conf = do
  updatePNSLConf (Observe "End", undefined) conf
  {-
  let tFilter = (\a -> a == Theta || a == Tau) . e_action . t_ev
  confMs' <- fireTransitionOnEv
    fireSome pn tFilter confM
  let confMs'' = filter (\cM -> null (map m_place (c_markers cM) \\ pn_exits pn) && null (pn_exits pn \\ map m_place (c_markers cM))) confMs'
  let confMsDiscontinued = filter (\cM -> (not . null) (map m_place (c_markers cM) \\ pn_exits pn) || (not . null) (pn_exits pn \\ map m_place (c_markers cM))) confMs'
  if null confMs''
    then return $ confM
    else mapM (\cM -> unregisterNode (c_pid cM)) (confMsDiscontinued) >>
         return (head confMs'')
-}


