{-# LANGUAGE GADTs, OverloadedStrings, RankNTypes, ScopedTypeVariables, PatternGuards #-}

-- | Functionality for serializing Haskell objects matching a specification into bytes that can be send over the wire.
module Language.APSL.Channel.Encoder (encodeRecord, encode, TerminatorGenerator, arbitraryGenerator) where

import Language.APSL.Base
import Language.APSL.Generator.MessageGen
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Codecs
import Language.APSL.AMSL.DataType.TypeCodec

import Test.QuickCheck.Gen (unGen)
import Test.QuickCheck.Random (mkQCGen)

import Data.Binary.Put (runPut)
import Data.Binary.Bits.Put

import Data.BitString.BigEndian (BitString)
import qualified Data.BitString.BigEndian as B
import qualified Data.ByteString.Lazy as By
import qualified  Data.Text as T
import Data.Text.Encoding
import Control.Monad
import Data.Maybe
import Data.Char


putBitString :: BitString -> BitPut ()
putBitString b = do
    let (bytes, rest) = B.splitAt (B.length b `quot` 8 * 8) b
    putByteString $ B.realizeBitStringStrict bytes
    forM_ (B.toList rest) putBool

flipBits :: BitString -> BitString
flipBits = B.fromList . map not . B.toList

putUIntBe :: Integer -> Integer -> BitPut ()
putUIntBe n x | x < 0 = putUIntBe n $ 2^n + x
              | n > 64 = let (ws,w) = x `quotRem` 64 in putWord64be 64 (fromInteger w) >> putUIntBe (n - 64) ws
              | otherwise = putWord64be (fromInteger n) $ fromInteger x

putUIntLe :: Integer -> Integer -> BitPut ()
putUIntLe n x | x < 0 = putUIntLe n $ 2^n + x
              | n > 8 = let (bs,b) = x `quotRem` 8 in putWord8 8 (fromInteger b) >> putUIntLe (n - 8) bs
              | otherwise = putWord8 (fromInteger n) $ fromInteger x

putText :: String -> String -> BitPut ()
putText "utf-8" str = putByteString $ encodeUtf8 $ T.pack str
putText enc str = forM_ str $ putCodePoint enc . ord

putCodePoint :: String -> Int -> BitPut ()
putCodePoint "ascii" = putWord8 8 . fromIntegral
putCodePoint enc = error $ "Unknown encoding: " ++ enc

-- | In the case of a TerminatedUnionList, the encoder needs to provide some arbitrary union or record value of a 
--   particular type with which to signify that a list has ended. Usually such a terminator has only one value.
--   A TerminatorGenerator provides a valid value for any type that can then be used as a terminator; one can either 
--   use the 'arbitraryGenerator' for this purpose or provide a custom one.
type TerminatorGenerator = forall a. Env -> (Type a, Arguments) -> a

-- | Provides terminators of a particular type: if a terminator could have more than one value, which of them is chosen 
--   is not defined.
arbitraryGenerator :: TerminatorGenerator
arbitraryGenerator env tyArgs = unGen (genValue defaultParams env tyArgs) (mkQCGen 0) 0


-- | Encoder for a particular type. Should not fail when the value passes the checker, or when it was produced by the 
--   decoder.
encode :: TerminatorGenerator -> Env -> (Type a, Arguments) -> (Codec, Arguments) -> a -> BitPut ()
encode termGen env (ty, tyArgs) (co, coArgs) x = case ty of
    UserType (TypeAlias _ t args) -> encode' env (t, addArgs args tyArgs) (co, coArgs) x
    UserType (Enumeration _ tt _) -> encode' env (fromTagType tt, tyArgs) (co, coArgs) x
    _ -> run co
 where 
    encode' :: Env -> (Type b, Arguments) -> (Codec, Arguments) -> b -> BitPut ()
    encode' = encode termGen

    arg (Arguments args) name = return $ lookup name args

    valArg args name = do
        mexp <- arg args name
        case mexp of
            Nothing -> return Nothing
            Just (ExpArg exp) -> case evaluate env exp of
                Right val -> return $ Just val
                Left err  -> error $ "Error evaluating expression: " ++ show (exp, err)

    valArgDefault args name def = liftM (fromMaybe def) $ valArg args name

    run (BasicCodec c) = do
        BasicType t <- return ty
        case (t,c) of
            (Binary, FixedLengthBinary) -> putBitString x

            (Binary, LengthPrefixBinary) -> do
                putCount "length" $ B.length x
                putBitString x

            (Binary, TerminatedBinary) -> do
                Just (EBinary term) <- valArg coArgs "terminator"
                putBitString x
                putBitString term

            (Bool, BoolBits) -> do
                mtrue  <- valArg coArgs "truth_string"
                mfalse <- valArg coArgs "falsehood_string"
                case (mtrue, mfalse) of
                    (Just (EBinary t), _) | x     -> putBitString t
                    (_, Just (EBinary f)) | not x -> putBitString f
                    (Just (EBinary t), _) | not x -> putBitString $ flipBits t
                    (_, Just (EBinary f)) | x     -> putBitString $ flipBits f
                    _ -> fail "Invalid BoolBits arguments."

            (Integer, BigEndian) -> do
                Just (EInt len) <- valArg coArgs "length"
                EInt multiplier <- valArgDefault coArgs "multiplier" $ EInt 1
                EInt offset <- valArgDefault coArgs "offset" $ EInt 0

                putUIntBe ((len - offset) `quot` multiplier) x

            (Integer, LittleEndian) -> do
                Just (EInt len) <- valArg coArgs "length"
                EInt multiplier <- valArgDefault coArgs "multiplier" $ EInt 1
                EInt offset <- valArgDefault coArgs "offset" $ EInt 0

                putUIntLe ((len - offset) `quot` multiplier) x

            (Integer, LengthPrefixInteger) -> do
                Just (EBool signed) <- valArg coArgs "signed"
                EInt factor <- valArgDefault coArgs "length_multiplier" $ EInt 1

                let bitCount = floor $ logBase 2 (fromIntegral $ abs x :: Double)
                let (q,r) = bitCount `quotRem` factor
                let unitCount = if r == 0 then q else q + 1
                let len = unitCount * factor

                when signed $ putBool $ x < 0
                putCount "length" unitCount
                putUIntBe len $ if x >= 0 then x else 2^len + x

            (Integer, TextInteger) -> do
                Just (CodecExpArg cco cargs) <- arg coArgs "text_codec"
                encode' env (BasicType Text, Arguments []) (cco, cargs) $ show x

            (Text, FixedCountText)      -> do
                Just (EText encoding) <- valArg coArgs "encoding"
                putText encoding x

            (Text, CountPrefixText)     -> do
                Just (EText encoding) <- valArg coArgs "encoding"
                putCount "count" $ length x
                putText encoding x

            (Text, TerminatedText)      -> do
                Just (EText encoding) <- valArg coArgs "encoding"
                Just (EText term) <- valArg coArgs "terminator"
                putText encoding x
                putText encoding term

            (List, TerminatedList) -> do
                Just termVal <- valArg coArgs "terminator"
                putList x
                withSubTypeCodec "elem" $ \(et, eta) (ec, eca) -> do
                    Just term <- return $ toTagType et >>= flip readTag termVal
                    encode' env (et, eta) (ec, eca) term

            (List, FixedCountList)      -> putList x

            (List, CountPrefixList)     -> do
                ContainerValue _ xs <- return x
                putCount "count" $ length xs
                putList x

            (List, TerminatedUnionList) -> do
                Just termVal <- valArg coArgs "terminator_tag"
                uField <- valArg coArgs "union_field"
                putList x
                withSubTypeCodec "elem" $ \(et, eta) _ -> do
                    
                    case uField of
                        Nothing -> do 
                            UserType (Union _ _ tagTy u) <- return et
                            Just termTag <- return $ readTag tagTy termVal
                            putOptionWithTag tagTy termTag u
                        Just (EText field) -> undefined -- TODO

            (Optional, OptionalCodec)   -> 
                case x of
                    ContainerValue _ Nothing -> do
                        EBinary nil <- valArgDefault coArgs "null_string" $ EBinary B.empty
                        putBitString nil
                    ContainerValue st (Just x') ->
                        withSubTypeCodec "subject" $ \(st', sta) (sc, sca) -> do
                            Just SameType <- return $ sameType st st'
                            encode' env (st, sta) (sc, sca) x'


    run RecordCodec = do
        UserType (Record _ _ r) <- return ty
        putRecord r x

    run (UnionCodec uc) = do
        UserType (Union _ _ tagTy u) <- return ty
        putUnion uc tagTy u x

    run (UserCodec (CodecAlias _ co' coArgs')) = encode' env (ty, tyArgs) (co', addArgs coArgs' coArgs) x

    run DoubleCodec = do
        Just (CodecExpArg fc fca) <- arg coArgs "field"
        Just (CodecExpArg ac aca) <- arg coArgs "additional"
        -- TODO FIXME: make this work with codecs that do not have output length divisible by eight. Unfortunately this would require BitPut hacking.
        let firstPass = runPut $ runBitPut $ encode' env (ty, tyArgs) (fc, fca) x
        encode' env (BasicType Binary, Arguments []) (ac, aca) $ B.bitStringLazy firstPass


    putCount :: Integral i => String -> i -> BitPut ()
    putCount name n = do
        Just (CodecExpArg cco cargs) <- arg coArgs $ ParamName $ name ++ "_codec"
        EInt multiplier <- valArgDefault coArgs (ParamName $ name ++ "_multiplier") $ EInt 1
        EInt offset <- valArgDefault coArgs (ParamName $ name ++ "_offset") $ EInt 0
        encode' env (BasicType Integer, Arguments []) (cco, cargs) $ (toInteger n - offset) `div` multiplier

    withSubTypeCodec :: String -> (forall e. (Type e, Arguments) -> (Codec, Arguments) -> BitPut ()) -> BitPut ()
    withSubTypeCodec name f = do
        Just (TypeExpArg sty stargs) <- arg tyArgs $ ParamName name
        mSubCodec <- arg coArgs $ ParamName $ name ++ "_codec"
        subCodec <-
            case mSubCodec of
                Just (CodecExpArg co args) -> return (co, args)
                Nothing | Just co <- implicitCodec sty -> return (co, Arguments [])
                        | otherwise -> error $ "No implicit codec for: " ++ show (typeName sty)
        f (sty, stargs) subCodec

    putList :: ContainerValue [] -> BitPut ()
    putList (ContainerValue st xs) = 
        withSubTypeCodec "elem" $ \(st', sta) (sc, sca) ->
            case sameType st st' of
                Just SameType -> sequence_ $ map (encode' env (st, sta) (sc, sca)) xs
                Nothing       -> fail "List element type does not match argument."

    putRecord :: Record b -> b -> BitPut ()
    putRecord r x = do 
        Right env2 <- return $ argsToEnv tyArgs env
        foldRecord env2 encode' r x

    putUnion :: UnionTagCodec -> TagType t -> Union t b -> b -> BitPut ()
    putUnion uc tagTy (Option _ _ _ others) (Right x') = putUnion uc tagTy others x'
    putUnion uc tagTy (Option tag _ (FieldTypeCodec ft fta fc fca) _) (Left x') = do
        putTag uc tagTy tag
        Right env2 <- return $ argsToEnv tyArgs env
        encode' env2 (ft, fta) (fc, fca) x'

    putTag :: UnionTagCodec -> TagType t -> t -> BitPut ()
    putTag TagPrefixUnion tagTy tag = do
        let tagTy' = fromTagType tagTy
        tagCodec <- arg coArgs "tag_codec"
        tagCodecArgs <-
            case tagCodec of
                Just (CodecExpArg tco tcargs) -> return (tco, tcargs)
                Nothing | Just tco <- implicitCodec tagTy' -> return (tco, Arguments [])
        encode' env (tagTy', Arguments []) tagCodecArgs tag
    putTag _ _ _ = return ()

    putOptionWithTag :: TagType t -> t -> Union t b -> BitPut ()
    putOptionWithTag _ _ None = fail "Invalid tag"
    putOptionWithTag tagTy tag u@(Option tag' _ (FieldTypeCodec ft fta fc fca) others)
        | compareTags tagTy tag tag' = encode' env (ft, fta) (fc, fca) $ termGen env (ft, fta)
        | otherwise = putOptionWithTag tagTy tag others

-- | Standard encoder for record with no parameters.
encodeRecord :: TerminatorGenerator -> Env -> TypeName -> Record a -> a -> BitPut ()
encodeRecord tgen env n r = encode tgen env (UserType (Record n [] r), Arguments []) (RecordCodec, Arguments [])
