{-# LANGUAGE GADTs, OverloadedStrings, RankNTypes, ScopedTypeVariables, PatternGuards #-}
-- | Functionality for parsing and checking binary messages into a value of an appropriate type. 
--   Note that the types used here should have been validated first.
module Language.APSL.Channel.Decoder (decodeRecord, decode) where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Codecs
import Language.APSL.AMSL.DataType.TypeCodec
import Language.APSL.AMSL.ValueChecker

import Data.BitString.BigEndian (BitString)
import qualified Data.BitString.BigEndian as B
import qualified Data.ByteString.Lazy as By

import Control.Monad
import Data.Char
import Data.List
import Data.Bits
import Data.Maybe
import Debug.Trace

import Data.Binary.Get (runGetOrFail)
import Data.Binary.Bits.Get


getBitString :: Integer -> BitGet BitString
getBitString n = do
    guardB $ n >= 0
    let (byteCount, restCount) = (fromIntegral n) `quotRem` 8
    bytes <- getLazyByteString byteCount
    getRest (B.bitStringLazy bytes) restCount
 where
    getRest s 0 = return s
    getRest s n = getBool >>= \b -> getRest (s `B.append` B.fromList [b]) (n - 1)

getUIntBe :: Integer -> BitGet Integer
getUIntBe 0 = return 0
getUIntBe len | len <= 64 = liftM toInteger $ getWord64be $ fromInteger len
              | otherwise = do
    word <- liftM toInteger $ getWord64be 64
    rest <- getUIntBe (len - 64)
    return $ word `shiftL` (fromInteger len) + rest

getUIntLe :: Integer -> BitGet Integer
getUIntLe 0 = return 0
getUIntLe len | len <= 8 = liftM toInteger $ getWord8 $ fromInteger len
              | otherwise = do
    byte <- liftM toInteger $ getWord8 8
    rest <- getUIntLe (len - 8)
    return $ rest `shiftL` 8 + byte

-- Try running a parser without consuming its input.
peek :: BitGet a -> BitGet a
peek = undefined -- TODO: extend Binary.Get in order to support this.

-- replicateM, except with big integers.
replicateB :: Integer -> BitGet a -> BitGet [a]
replicateB 0 _ = return []
replicateB n p = do
    x  <- p
    xs <- replicateB (n-1) p
    return $ x : xs

guardB :: Bool -> BitGet ()
guardB True = return ()
guardB False = fail "Assertion failed."

getText :: String -> Integer -> BitGet String
getText enc 0 = return ""
getText enc n = do 
    x  <- getCodePoint (map toLower enc) 
    xs <- getText enc (n - 1)
    return (x:xs)

getUntilTerminator :: Eq a => [a] -> BitGet a -> BitGet [a]
getUntilTerminator term p = fetch [] term
 where 
    -- TODO: better string matching algorithm
    fetch _ [] = return []
    fetch tseen tleft@(t:ts)  = do
        c <- p
        if c == t
            then fetch (t:tseen) ts
            else do
                let buffered = reverse tseen
                cs <- fetch [] term
                return $ buffered ++ c : cs


getCodePoint :: String -> BitGet Char
getCodePoint "ascii" = do
    False <- getBool
    liftM (chr . fromIntegral) $ getWord8 7
getCodePoint "utf-8" = do
    multibyte <- getBool
    if multibyte
     then do
        True <- getBool
        ones <- countOnes
        firstBits <- liftM fromIntegral $ getWord8 $ 6 - ones
        rest <- replicateM ones $ do
            True <- getBool
            False <- getBool
            liftM fromIntegral $ getWord8 6
        return $ chr $ foldl (\a b -> a `shiftL` 6 + b) firstBits rest

     else 
        liftM (chr . fromIntegral) $ getWord8 7
 where 
    countOnes = do
        bit <- getBool
        if bit
         then liftM (+1) countOnes
         else return 0

getCodePoint enc = fail $ "Unsupported text encoding: " ++ enc -- TODO: other text encodings (hook into some library)

-- Debugging helper.
dump :: Show a => a -> BitGet ()
-- dump x = trace (show x) $ return ()
dump _ = return ()

-- | Standard decoder for a record with no parameters.
decodeRecord :: Env -> TypeName -> Record a -> BitGet a
decodeRecord env n r = decode env (UserType (Record n [] r), Arguments []) (RecordCodec, Arguments [])

-- | Decoder for a particular type. Fails when input is incorrectly formatted or the parsed structures are not 
--   permissable according to type arguments in the specification.
decode :: forall a. Env -> (Type a, Arguments) -> (Codec, Arguments) -> BitGet a
decode env (ty, tyArgs) (co, coArgs) = do
    dump (typeName ty, codecName co)
    x <- decode'
    dump [typeName ty, codecName co]
    when (not $ checkValue env ty tyArgs x) $ fail "Failed argument check."
    return x
 where
    decode' 
        | (UserType (TypeAlias _ t args)) <- ty = decode env (t, addArgs tyArgs args) (co, coArgs)
        | (UserType (Enumeration _ tt _)) <- ty = decode env (fromTagType tt, Arguments []) (co, coArgs)
        | otherwise = run co

    arg (Arguments args) name = return $ lookup name args

    valArg args name = do
        mexp <- arg args name
        case mexp of
            Nothing -> return Nothing
            Just (ExpArg exp) -> do
                dump ("arg" :: String, name, '=', evaluate env exp)
                Right val <- return $ evaluate env exp
                return $ Just val

    valArgDefault args name def = liftM (fromMaybe def) $ valArg args name

    run :: Codec -> BitGet a

    run (UserCodec (CodecAlias cn co' args)) = decode env (ty, tyArgs) (co', addArgs args coArgs)

    run (BasicCodec c) = do
        BasicType bt <- return ty
        case (bt, c) of
            (Binary, FixedLengthBinary) -> do
                lenArg <- valArg tyArgs "length"
                case lenArg of
                    Just (EInt len) -> getBitString len
                    Nothing -> do
                        Just (EBinary val) <- valArg tyArgs "value"
                        getBitString $ toInteger $ B.length val

            (Binary, LengthPrefixBinary) -> getCount "length" >>= getBitString                

            (Binary, TerminatedBinary) -> do
                Just (EBinary term) <- valArg coArgs "terminator"
                bits <- getUntilTerminator (B.toList term) getBool
                return $ B.fromList bits

            (Bool, BoolBits) -> do
                mtrue  <- valArg coArgs "truth_string"
                mfalse <- valArg coArgs "falsehood_string"
                case (mtrue, mfalse) of
                    (Just (EBinary t), Nothing) -> do
                        dump [t]
                        str <- getBitString (toInteger $ B.length t)
                        return $ str == t
                    (Nothing, Just (EBinary f)) -> do
                        str <- getBitString (toInteger $ B.length f)
                        return $ str /= f
                    (Just (EBinary t), Just (EBinary f))
                        | B.length t <= B.length f -> do
                            str1 <- getBitString (toInteger $ B.length t)
                            if str1 == t
                             then 
                                return True
                             else do
                                    str2 <- getBitString (toInteger $ B.length f - B.length t)
                                    guardB $ f == (str1 `B.append` str2)
                                    return False
                        | otherwise -> do
                            str1 <- getBitString (toInteger $ B.length f)
                            if str1 == f 
                             then 
                                return False
                             else do
                                str2 <- getBitString (toInteger $ B.length t - B.length f)
                                guardB $ t == (str1 `B.append` str2)
                                return True

            (Integer, BigEndian) -> do
                Just (EInt baseLen) <- valArg coArgs "length"
                Just (EBool signed) <- valArg coArgs "signed"
                EInt multiplier <- valArgDefault coArgs "multiplier" $ EInt 1
                EInt offset <- valArgDefault coArgs "offset" $ EInt 0

                let len = baseLen * multiplier + offset
                int <- getUIntBe len
                return $ if signed && int > 2^(len-1) then 2^len - int else int

            (Integer, LittleEndian) -> do
                Just (EInt baseLen) <- valArg coArgs "length"
                Just (EBool signed) <- valArg coArgs "signed"
                EInt multiplier <- valArgDefault coArgs "multiplier" $ EInt 1
                EInt offset <- valArgDefault coArgs "offset" $ EInt 0

                let len = baseLen * multiplier + offset
                int <- getUIntLe len
                return $ if signed && int > 2^(len-1) then 2^len - int else int

            (Integer, LengthPrefixInteger) -> do
                Just (EBool signed) <- valArg coArgs "signed"
                len <- getCount "length"
                int <- getUIntBe len
                return $ if signed && int > 2^(len-1) then 2^len - int else int

            (Integer, TextInteger) -> do 
                Just (CodecExpArg cco cargs) <- arg coArgs "text_codec"
                [(int, "")] <- liftM reads $ decode env (BasicType Text, Arguments []) (cco, cargs)
                return int

            (Text, FixedCountText)      -> do
                mcount <- valArg tyArgs "count"
                count <- case mcount of
                    Just (EInt c) -> return c
                    Nothing -> do
                        Just (EText val) <- valArg tyArgs "value"
                        return $ toInteger $ length val

                Just (EText encoding) <- valArg coArgs "encoding"
                getText encoding count

            (Text, CountPrefixText)     -> do
                Just (EText encoding) <- valArg coArgs "encoding"
                getCount "count" >>= getText encoding

            (Text, TerminatedText)      -> do
                Just (EText encoding) <- valArg coArgs "encoding"
                Just (EText term) <- valArg coArgs "terminator"
                getUntilTerminator term (getCodePoint encoding)

            (List, FixedCountList)      -> do
                mcount <- valArg tyArgs "count"
                count <- case mcount of
                    Just (EInt c) -> return c
                    Nothing -> do
                        Just (EList val) <- valArg tyArgs "value"
                        return $ toInteger $ length val
                getList count

            (List, CountPrefixList)     -> getCount "count" >>= getList

            (List, TerminatedList)      ->
                withSubTypeCodec "elem" $ \(et, ea) ec -> do
                    Just ett <- return $ toTagType et
                    Just termVal <- valArg coArgs "terminator"
                    Just term <- return $ readTag ett termVal

                    liftM (ContainerValue et) $ getTermList (compareTags ett term) $ decode env (et, ea) ec

            (List, TerminatedUnionList) ->
                withSubTypeCodec "elem" $ \(et, ea) ec -> do
                    Just termVal <- valArg coArgs "terminator_tag"
                    uField <- valArg coArgs "union_field"
                    isTerm <- 
                        case uField of
                            Nothing ->
                                withUnion et $ \tt ut -> do
                                    Just term <- return $ readTag tt termVal
                                    return $ \u -> compareTags tt term $ unionTag ut u
                            Just (EText field) ->
                                withRecordField et (FieldName field) $ \ft ext ->
                                    withUnion ft $ \tt ut -> do
                                        Just term <- return $ readTag tt termVal
                                        return $ \r -> compareTags tt term $ unionTag ut $ ext r

                    liftM (ContainerValue et) $ getTermList isTerm $ decode env (et, ea) ec

            (Optional, OptionalCodec)       -> do
                withSubTypeCodec "subject" $ \(st, stargs) (sco, scargs) -> do
                    Just (EBool empty) <- valArg tyArgs "is_empty"
                    EBinary nil <- valArgDefault tyArgs "null_string" $ EBinary B.empty
                    val <- 
                        if empty 
                         then do
                            nil' <- getBitString $ toInteger $ B.length nil
                            guardB $ nil == nil'
                            return Nothing
                         else liftM Just $ decode env (st, stargs) (sco, scargs)
                    return $ (ContainerValue st val :: ContainerValue Maybe)

            
    run RecordCodec = do
        UserType (Record _ _ r) <- return ty
        buildRecordArgs tyArgs env decode r

    run (UnionCodec uc) = do
        UserType (Union _ _ tagTy u) <- return ty
        Right env2 <- return $ argsToEnv tyArgs env
        tag <- case uc of
            TagPrefixUnion   -> decodeTag tagTy
            EmbeddedTagUnion -> do
                EInt offset <- valArgDefault coArgs "offset" $ EInt 0
                guardB $ offset >= 0
                getBitString offset
                peek (decodeTag tagTy)
            ContextTagUnion  -> do
                Just t <- liftM (>>= readTag tagTy) $ valArg tyArgs "tag"
                return t
        Just opt <- getOption env2 tagTy tag u
        return opt

    run DoubleCodec = do
        Just (CodecExpArg fc fca) <- arg coArgs "field"
        Just (CodecExpArg ac aca) <- arg coArgs "additional"

        -- TODO FIXME: make this work with codecs that do not have output length divisible by eight.
        secondPass <- decode env (BasicType Binary, Arguments []) (ac, aca)
        case runGetOrFail (runBitGet $ decode env (ty, tyArgs) (fc, fca)) $ B.realizeBitStringLazy secondPass of
            Left (_, _, msg) -> fail msg
            Right (left, _, x) | By.null left -> return x
                               | otherwise -> fail "DoubleCodec: unconsumed bytes."

    decodeTag :: TagType t -> BitGet t
    decodeTag tagTy = do 
        let tagTy' = fromTagType tagTy
        tagCodec <- arg coArgs "tag_codec"
        tagCodecArgs <-
            case tagCodec of
                Just (CodecExpArg tco tcargs) -> return (tco, tcargs)
                Nothing | Just tco <- implicitCodec tagTy' -> return (tco, Arguments [])
        decode env (tagTy', Arguments []) tagCodecArgs

    getOption :: Env -> TagType t -> t -> Union t b -> BitGet (Maybe b)
    getOption _ _ _ None = return Nothing
    getOption env tagTy tag (Option optTag _ (FieldTypeCodec ft fta fc fca) r) 
        | compareTags tagTy tag optTag = liftM (Just . Left) $ decode env (ft, fta) (fc, fca)
        | otherwise                    = liftM (fmap Right) $ getOption env tagTy tag r

    withSubTypeCodec :: String -> (forall e. (Type e, Arguments) -> (Codec, Arguments) -> BitGet b) -> BitGet b
    withSubTypeCodec name f = do
        Just (TypeExpArg sty stargs) <- arg tyArgs $ ParamName name
        mSubCodec <- arg coArgs $ ParamName $ name ++ "_codec"
        subCodec <-
            case mSubCodec of
                Just (CodecExpArg co args) -> return (co, args)
                Nothing | Just co <- implicitCodec sty -> return (co, Arguments [])
        f (sty, stargs) subCodec

    getList :: Integer -> BitGet (ContainerValue [])
    getList count = do
        guardB $ count >= 0
        withSubTypeCodec "elem" $ \(st, stargs) (sco, scargs) ->
            liftM (ContainerValue st) $ replicateB count $ decode env (st, stargs) (sco, scargs)

    getTermList :: (e -> Bool) -> BitGet e -> BitGet [e]
    getTermList isTerm p = do
        x <- p
        if isTerm x
            then return []
            else getTermList isTerm p >>= \xs -> return (x:xs)

    getCount :: String -> BitGet Integer
    getCount name = do 
        Just (CodecExpArg cco cargs) <- arg coArgs $ ParamName $ name ++ "_codec"
        EInt multiplier <- valArgDefault coArgs (ParamName $ name ++ "_multiplier") $ EInt 1
        EInt offset <- valArgDefault coArgs (ParamName $ name ++ "_offset") $ EInt 0
        val <- decode env (BasicType Integer, Arguments []) (cco, cargs)
        return $ val * multiplier + offset

    withRecordField :: Type r -> FieldName -> (forall f. Type f -> (r -> f) -> BitGet b) -> BitGet b
    withRecordField (UserType (TypeAlias _ t _)) fn f = withRecordField t fn f
    withRecordField (UserType (Record _ _ r)) fn f = extractField f r
     where
        extractField :: (forall f. Type f -> (r -> f) -> BitGet b) -> Record r -> BitGet b
        extractField _ Empty = fail "Field not found"
        extractField f (Field name ftc rest) | name == fn = f (fieldType ftc) fst
                                             | otherwise = extractField (\t ext -> f t $ ext . snd) rest
    withRecordField _ _ _ = fail "Not a record type."

    withUnion :: Type u -> (forall t. TagType t -> Union t u -> BitGet b) -> BitGet b
    withUnion (UserType (TypeAlias _ t _)) f = withUnion t f
    withUnion (UserType (Union _ _ tt ut)) f = f tt ut
    withUnion _ _ = fail "Not a union type."
    
