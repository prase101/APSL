{-# LANGUAGE DeriveDataTypeable #-}

-- | A message generator based on AMSL message specifications.
--   It detects message labels by parsing incoming bytes, and transmits randomly generated messages that match the
--   description.
module Language.APSL.Channel.SocketChannel
where

import Prelude hiding (getContents)

import Language.APSL.Base
import Language.APSL.Channel.Encoder
import Language.APSL.Channel.Decoder
import Language.APSL.Generator.MessageGen
import Language.APSL.Generator.MessageCoverage
import Language.APSL.AMSL.DataType.Module hiding (Module, messages)
import Language.APSL.AMSL.DataType.Expression (Env)
import Language.APSL.Channel.MessageChannel
import Language.APSL.AISL.DataType.Module

import Network.Socket hiding (sendAll, send)
import Network.Socket.ByteString.Lazy (sendAll, getContents)
import Test.QuickCheck.Gen
import Data.Binary.Get
import Data.Binary.Bits.Get
import Data.Binary.Put
import Data.Binary.Bits.Put
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Writer

import Data.Typeable
import Control.Exception
import Control.Concurrent
import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Concurrent.STM.TChan
import Control.Applicative
import           Data.Map (Map)
import qualified Data.Map as M
import qualified Data.Set as S
import           Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as B

import Text.PrettyPrint
import Language.APSL.AMSL.MessagePrettyPrinter
import Data.Hex
import System.Random


-- | Receive timeout in milliseconds.
type Timeout = Int


-- | Construct a socket-based message channel. This channel facilitates communication between
-- APSL traversal engine and an IUT, through TCP socket.
--
socketChannel_ :: [(Message, Env)] -> Socket -> Timeout -> (Int -> String -> IO()) -> IO MessageChannel
socketChannel_ msgsenv
              socket -- the socket through which the communication will go through
              timeout
              writelog -- function to write to log
              =
  do{inbox <- newTQueueIO ;
     inp   <- getContents socket ;
     forkIO $ receiveLoop inbox $ B.toChunks inp ;
     let
        send (msglabel,bytestring) = do
             {-
             -- This need to be moved elsewhere:

             Just (msg,env) <- return $ M.lookup msglabel msgs ;
             MessageInstance tn r x <- return msginstance ;
             bytes <- return $ runPut $ runBitPut $ encodeRecord arbitraryGenerator env tn r x ;
             -}
             writelog 1 ("??? IUT is receiving " ++ msglabel ++ " ... ")
             sendAll socket bytestring
             writelog 1 (" successful.\n")
             return SendOK
             -- TO DO: catch things like connection error

        receive expectedMsgLabels = do {
             -- randomize the time out with some addition
             -- d <- newRandom ;
             -- timeout_ <- return $ fromInteger $ toInteger $ round $ d * timeOutAdditionRange ;
             result <- race (threadDelay $ timeout ) (atomically $ peekTQueue inbox) ;
             (case result of
                 Left ()  -> return ReceiveTimeOut
                 Right (Left (msglabel, inst)) -> do {
                     atomically $ readTQueue inbox ;
                     writelog 1 ("!!! IUT sent " ++ msglabel ++ ".\n") ;
                     if msglabel `elem` expectedMsgLabels
                        then -- returning the parsed message-instance; we also pass along its message type
                             -- and the original bytestring:
                             return $ ReceiveOK msglabel (Just inst) Nothing -- need to pass on the bitstring here... but how???
                        else return $ Receive_WrongMsgError expectedMsgLabels msglabel
                     }
                 Right (Right (DecodeError err)) -> return $ Receive_MsgWithIllegalFormatError err)
             }
     in return MessageChannel { sendToIUT=(\f -> catch (send f) returnSendError), receiveFromIUT=(\f -> catch (receive f) returnRecvError) }
   }
   where
   returnSendError :: IOException -> IO IOSendToIUTStatus
   returnSendError e = error "crasdh" -- return SendConnectionError
   returnRecvError :: IOException -> IO IOReceiveFromIUTstatus
   returnRecvError e = error "crashrsv" -- return ReceiveConnectionError
   -- all messages' labels and types in the module
   -- msgs = M.fromList [(unTypeName $ messageName msg, (msg, env)) | (msg, env) <- messages aislModule]
   msgs = M.fromList [(unTypeName $ messageName msg, (msg, env)) | (msg, env) <- msgsenv ]

   -- note to Rick: well, if you can dechiper the part below... it looks like a magic box to me :D

   --- this is where we decode an incoming bitstring to a message instance
   getMsg (l, (Message tn r, env)) = runBitGet (decodeRecord env tn r) >>= \x -> return (l, MessageInstance tn r x)

   receiveLoop inbox bs = receiver inbox bs $ runGetIncremental $ foldr1 (<|>) $ reverse $ map getMsg $ M.assocs msgs
   receiver inbox bs (Done b' _ x) = atomically (writeTQueue inbox $ Left x) >> receiveLoop inbox (b':bs)
   -- the case when the incoming bitstring cannot be decoded:
   receiver inbox _ (Fail _ _ err)  = atomically (writeTQueue inbox $ Right $ DecodeError err)
   receiver inbox [] _ = return ()
   receiver inbox (b:bs) (Partial f) = receiver inbox bs $ f $ Just b

   --newRandom = randomIO :: IO Float
   --timeOutAdditionRange :: Float
   --timeOutAdditionRange = 100 -- fromInteger $ toInteger $ timeout `div` 2


socketChannel :: Module -> Socket -> Timeout -> (Int -> String -> IO()) -> IO MessageChannel
socketChannel aislModule = socketChannel_ $ messages aislModule

data DecodeError = DecodeError String
 deriving (Show, Typeable)

instance Exception DecodeError
