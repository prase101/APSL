-- | Provide a generic representation of a message channel.
-- A message channel is the interface between APSL traversal engine and the Implementation
-- Under Test. It provides two primitives: one for sending a message to the IUT, and one
-- for receiving a message from the IUT.
--

module Language.APSL.Channel.MessageChannel where
    
import Language.APSL.AISL.DataType.LTS
import Language.APSL.AISL.DataType.Module
import Language.APSL.AMSL.DataType.Module hiding (Module, messages)
import Language.APSL.AMSL.DataType.Expression (Env)
import Language.APSL.Generator.MessageGen
import Language.APSL.Base

import Data.Set (Set)
import qualified Data.Set as S
import qualified Data.Map as M
import Data.ByteString.Lazy (ByteString)
import Test.QuickCheck.Gen

-- | A record representing a communication channel for interacting with the Implementation
-- Under Test (IUT). The record holds a send and receive IO action.
--
data MessageChannel = MessageChannel {
    -- | an IO function to send a byestring message to the IUT.
    sendToIUT :: (MessageLabel,ByteString) -> IO IOSendToIUTStatus,

    -- | IO action for receiving msg from the IUT. The argument specifies the set of
    -- expected message labels. Receiving a message which is not in the set counts as
    -- an error.
    receiveFromIUT ::  [MessageLabel] -> IO IOReceiveFromIUTstatus
}

data IOReceiveFromIUTstatus = ReceiveOK MessageLabel (Maybe MessageInstance) (Maybe ByteString)
                            | Receive_MsgWithIllegalFormatError String -- with the error msg
                            | Receive_WrongMsgError [MessageLabel] MessageLabel -- expected, received
                            | ReceiveConnectionError
                            | ReceiveTimeOut
                            
data IOSendToIUTStatus = SendOK | SendConnectionError   

-- | A simple echo channel that does NOT interact with a real IUT. Instead it interacts with the user
-- that takes the role of the IUT.
-- Obviously, this channel cannot be used to test a real IUT. It is used mainly for debugging/testing
--  a traversal strategy.
--
-- When asked to provide a msg, the human-IUT only needs to specify a message label from a set of
-- expected labels. Currently, the actual message instance (of the message label that the human-IUT
-- says he wants to send) is not generated. TO DO.
--
echoMessageChannel_ :: [(Message, Env)] -> GenParams -> MessageChannel
echoMessageChannel_ msgsenv msgGenParams = MessageChannel { sendToIUT = send , receiveFromIUT = receive }
   where
       
   -- all messages' labels and types in the module
   --msgs = M.fromList [(unTypeName $ messageName msg, (msg, env)) | (msg, env) <- messages aislModule ]
   msgs = M.fromList [(unTypeName $ messageName msg, (msg, env)) | (msg, env) <- msgsenv ]
   
   fromJust (Just x) = x
          
   send (mlabel,_) = do {
      putStrLn ("??? IUT received " ++ mlabel ++ ".") ;
      return SendOK ;
   }   
       
   receive expected = do { 
       putStrLn ("... Expecting one of " ++ show expected) ;
       putStr    "... Enter what you want the IUT to send: " ;
       mlabel <- getLine ;
       putStrLn ("!!! IUT sent " ++ mlabel ++ ".") ;
       if mlabel `elem` expected 
          then do { (a,env)    <- return $ fromJust $ M.lookup mlabel msgs ;
                    concrete_a <- generate $ genMessage msgGenParams env a ;
                    return $ ReceiveOK mlabel (Just concrete_a) Nothing
                  }
          else return $ Receive_WrongMsgError expected mlabel  
   }

echoMessageChannel :: Module -> GenParams -> MessageChannel
echoMessageChannel aislModule = echoMessageChannel_ $ messages aislModule