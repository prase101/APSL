module Language.APSL.Analyses.Tracker where

import Language.APSL.Analyses.ProbLTS
import Language.APSL.Base

import qualified Language.APSL.AISL.DataType.LTS as LTS
import Control.Monad.State
import           Data.Map (Map)
import qualified Data.Map as M
import Data.Maybe
import Data.List
import Debug.Trace


type Analyzer state ev a = State (Bool,(ProbLTS state ev), Int) a

analyseLess :: Analyzer state ev a -> a
analyseLess noTrack
  = evalState noTrack (False,undefined,0)

analyse :: ProbLTS state ev -> Analyzer state ev a -> (a,ProbLTS state ev)
analyse plts track
  = let (r,(_,plts',_)) = runState track (True,plts,nextUniqueID plts)
    in (r,plts')

nextUniqueID :: ProbLTS state ev -> Int
nextUniqueID plts
  = 1 + (maximum $ 0 : mapMaybe toInt (M.keys $ slabels plts))
  where toInt :: LTS.StateID -> Maybe Int
        toInt (LTS.Normal i) = Just $ read i
        toInt _ = Nothing

genProbStateID :: Analyzer state ev LTS.StateID
genProbStateID
  = do
  (enabled,plts, i) <- get
  put (enabled,plts, i + 1)
  return (LTS.Normal (show i))

unregisterNode :: LTS.StateID -> Analyzer state ev ()
unregisterNode node
  = do
  (enabled,plts,i) <- trace ("unregistering " ++ show node) get
  let transitions' = M.map (map gTransPurgeNode)
                   $ M.delete node (transitions plts)
      plts' = plts { transitions = transitions'
                   , slabels = M.delete node (slabels plts)}
  if enabled
    then put (enabled,plts',i)
    else return ()
  where gTransPurgeNode :: OutgoingTransitionsGroup ev -> OutgoingTransitionsGroup  ev
        gTransPurgeNode (ev,arrows)
          = (ev, filter ((/=) node . fst) arrows)

markFinal :: LTS.StateID -> Analyzer state ev ()
markFinal node = do
  (enabled,plts,i) <- get
  if enabled
    then put (enabled,plts {finalStates = node : finalStates plts},i)
    else return ()

registerTransition :: Eq ev => LTS.StateID -> ev -> state -> Analyzer state ev LTS.StateID
registerTransition fromNode ev st
  = do
  pid <- genProbStateID
  (enabled,plts, i) <- get
  let trans' = case M.lookup fromNode (transitions plts) of
                Just g -> case findIndex ((==ev) . fst) g of
                  Just i  -> replaceIndex i (ev, (pid,1.0) : (snd $ g !! i)) g
                  Nothing -> (ev, [(pid,1.0)]) : g
                Nothing -> [(ev, [(pid,1.0)])]
  let plts' = plts { transitions = M.insert fromNode trans' (transitions plts)
                   , slabels = M.insert pid st (slabels plts)}
  if enabled
    then put (enabled,plts',i) >> return pid
    else return pid
