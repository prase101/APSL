module Language.APSL.Analyses.ProbLTS where

import qualified Language.APSL.AISL.DataType.LTS as LTS

import           Data.Map (Map)
import qualified Data.Map as M
import           Data.Set (Set)
import qualified Data.Set as S
import Data.Maybe
import qualified Data.List
import qualified Debug.Trace as D

import Data.Time.Clock
import Control.DeepSeq


-- LTS, where arrows are annotated with probabilistic information. Note that probability is quantified over
-- outgoing arrows over the same symbols. For example, if state s has three types of outgoing arrows, labelled
-- with e.g. a,b,c. .... blabla
--
data ProbLTS state ev = ProbLTS {
    initState   :: LTS.StateID,
    finalStates :: [LTS.StateID],
    transitions :: Map LTS.StateID [OutgoingTransitionsGroup ev],
    -- labels we put on every state:
    slabels     :: Map LTS.StateID state
} deriving (Show)


transitionsToState :: ProbLTS state ev -> LTS.StateID -> [LTS.StateID]
transitionsToState plts node =
  M.keys
  $ M.filter (any (\(_,ts) -> any ((==) node . fst) ts))
  $ (transitions plts)

purgeDanglings :: ProbLTS state ev -> ProbLTS state ev
purgeDanglings plts =
  let finals = finalStates plts
      markedStates = concatMap markBack finals
  in plts {transitions = M.restrictKeys (transitions plts) (S.fromList markedStates)}
  where markBack :: LTS.StateID -> [LTS.StateID]
        markBack s =
          let ss = transitionsToState plts s
          in s : concatMap markBack ss

-- getNormalStateName (LTS.Normal name) = name

-- Represeting outgoing arrows labelled with the same action.
-- For each destination, labelled with probability that destination is chosen,
-- when the action occurs.
type OutgoingTransitionsGroup ev = (ev, [(LTS.StateID,Double)])  -- assume ReceiveUnexpected action to have been eliminated

-- assign a default probability to a PLTS
assignEqualProb (ProbLTS s0 finals arrows slabs) = ProbLTS { initState = s0, finalStates = finals, transitions = M.map assign arrows, slabels = slabs}
   where
   assign outgoingGroups = map f outgoingGroups
   f (a,destinations) = (a, map ff destinations)
      where
      p :: Double
      p = 1.0 / (fromInteger $ toInteger $ length destinations)
      ff (dest,_) = (dest,p)

tauNormalize :: ProbLTS () () -> ProbLTS () ()
tauNormalize = undefined

-- Pretty print a PLTS
printPLTS :: (Show state, Show ev) => ProbLTS state ev -> String
printPLTS (ProbLTS s0 finals arrows labels) =
    "\n=============\n" ++
    "Init: " ++ getStName s0  ++  (concat $ map print1 states) ++
    "\nFinals: " ++ show (map getStName finals) ++
    "\n=============\n"
    where
    states = M.keys arrows
    print1 st = "\n  " ++ getStName st ++ " : " ++ show (labels M.! st) ++ (concat $ map print2 (arrows M.! st))
    print2 (a,group) = concat $ map print3 group
        where
        print3 (st2,p) = "\n    " ++ show a ++ " --> " ++ getStName st2 ++ " (" ++ show p ++ ")"

    getStName (LTS.Normal st) = st

-- Count the number of nodes
cntNodes :: ProbLTS a ev -> Int
cntNodes (ProbLTS s0 _ arrows labels) = snd (dfs [] 0 s0)
   where
   sucs u =  Data.List.nub [ v | (a,groups)<- arrows M.! u, (v,p) <- groups ]
   dfs visited cnt u = if u `elem` visited then (visited,cnt)
                       else let
                             sucessors = sucs u
                             visited2 = u : visited
                             cnt2 = 1 + cnt
                             in
                             foldr (\v (visited',cnt') -> dfs visited' cnt' v) (visited2,cnt2) sucessors




-- An execution model is a PLTS representing all possible executions that would yield the same
-- observable trace. Note that an execution model should be acyclic.
--
type ExecutionModel state ev = ProbLTS state ev

-- Given a trace of actions representing a test case, a PLTS, this function construct
-- the execution model of the trace. The PLTS is assumed to be tau-normalized.
-- The trace should only contain Send and Receive action (no Internal nor ReceiveUnexpected allowed)
--
mkExecutionModel :: [LTS.Action] -> ProbLTS () LTS.Action -> ExecutionModel [LTS.StateID] LTS.Action
mkExecutionModel trace plts = case x of
    Nothing -> error ("The trace " ++ show trace ++ " is NOT a valid test case, given the model.")
    Just id -> ProbLTS { initState   = mkState id,
                         finalStates = [],
                         transitions = M.fromList constructedTransitions,
                         slabels     = M.fromList $ map (\(x,st,_)-> (mkState x, [st])) resultNodes
                       }


    where
    -- represent a node with (unique-id, where it refers to in plts, suffix of the trace it represents)
    -- represent arrows with (id-of-source, id-of-dest, probability)
    -- Invoke the worker function:
    (x, _ , (resultNodes,resultArrows)) = worker (nodes0,arrows0) 1 (initState plts) trace

    sucs u = (transitions plts) M.! u
    exitNodeId = 0
    sharp = LTS.Normal "#"
    nodes0  = [ (exitNodeId,sharp,[])]
    arrows0 = [ ]

    mkState i = LTS.Normal $ show i

    -- converting the graph produced by the worker function to PLTS:
    constructedTransitions = map construct resultNodes
       where
       construct (x,st,suffix) = if x == exitNodeId then (mkState x, [])
                                 else (mkState x, [(action, arrowsToSucs)])
          where
          action = if null suffix then LTS.Internal else head suffix
          arrowsToSucs = [ (mkState y,p) | (x_,y,p) <- resultArrows, x_ == x ]

    --
    -- The worker function has the following threaded parameters:
    --    * the graph constructed so far
    --    * next available fresh ID
    -- Inherited parameters:
    --    * a node/state u in the orginal PLTS
    --    * the remaining suffix of the original trace to execute
    -- Output/synthesized parameters:
    --    * Just id: the ID of the node in the execution model that represents u
    --    * Nothing, if it is not possible to execute the suffix on u
    --
    worker graphSoFar freshId u [] =
        case visited of
           Just (id,_,_)  -> (Just id, freshId, graphSoFar)
           Nothing        -> if tauTransitionPossible then worker graphSoFar freshId u [LTS.Internal]
                             else (Just id_u, id_u + 1, (newnodes,newarrows))
        where
        visited = Data.List.find (\(id,v,suffix)-> (v,suffix) == (u,[])) $ fst graphSoFar
        -- check if a tau transition is possible
        tau_ = Data.List.find (\(b,_)-> b == LTS.Internal)  $ sucs u
        tauTransitionPossible = isJust tau_

        id_u = freshId
        (nodes,arrows) = graphSoFar
        newnodes  = (id_u,u,[]) : nodes
        newarrows = (id_u,exitNodeId,1.0) : arrows

    -- case if the trace is not empty:
    worker graphSoFar freshId u trace@(a:remainingTrace) =
        case visited of
           -- "visited" means that the execution model that corresponds to (u,trace) has been
           -- constructed processed by some previous recursive call. So we are not going to
           -- construct it again, and simply share the constructed model:
           Just (id,_,_)  -> (Just id, freshId, graphSoFar)
           -- Else, we first check if the suffix can be executed on u at all. If this
           -- is not possible, we return essentially Nothing.
           -- Else we need to construct the execution model for (u,trace).
           Nothing -> if not transitionPossible || null sucArrows
                      then (Nothing, freshId, graphSoFar)  -- not possible to execute trace on u
                      else (Just id_u, nextId, newGraph)   -- new graph/model for u
        where

        visited = Data.List.find (\(id,v,suffix)-> (v,suffix) == (u,trace)) $ fst graphSoFar

        -- The given PLTS is assume to be tau-normalized. So, on the state u, either only
        -- tau-transitions are possible, or only non-tau possible.

        -- Not nothing if u can (only) do a tau action
        tau_ = Data.List.find (\(b,_)-> b == LTS.Internal)  $ sucs u
        -- Not nothing if the first action a (in the trace) is possible
        a_   = Data.List.find (\(b,_)-> b == a) $ sucs u

        nextIsTau = isJust tau_
        transitionPossible = isJust tau_ || isJust a_

        -- the action and destinations, if either tau or a is possible on u:
        (action,successors) = if nextIsTau then fromJust tau_ else fromJust a_

        id_u = freshId
        traceTorecurseOn = if nextIsTau && (a /= LTS.Internal) then trace else remainingTrace

        -- assuming transition is possible, we process the successor of u one by one,
        -- to construct its execution graphs, and calculate the arrows from u to them.
        --   * sucArrows will contain all the new transitions from id_u to the nodes representing
        --     its successors.
        --   * newGraph_ is the total new graph accumolated from recursive calls to worker
        --
        (sucArrows,nextId,newGraph_) = foldr recurse ([],id_u + 1,graphSoFar) successors
        recurse (destination,p) (arrows_,freshId_,graphSoFar_) =
            case worker graphSoFar_ freshId_ destination traceTorecurseOn of
               (Nothing,_,_) -> (arrows_,freshId_,graphSoFar_)
               (Just id', freshId', graph' ) -> ((id_u,id',p) : arrows_, freshId', graph')

        -- assuming sucArrows not empty, normalize the probability in sucArrows:
        n :: Double
        n = fromIntegral $ toInteger $ length sucArrows
        totProbability = sum $ map (\(_,_,p)-> p) sucArrows
        sucArrows_ = map (\(x,y,p) -> (x,y,p/totProbability)) sucArrows

        -- now construct u's execution graph:
        suffixToLabel = if nextIsTau && (a /= LTS.Internal) then (LTS.Internal : trace) else trace
        newGraph = ((id_u, u, suffixToLabel) : fst newGraph_, sucArrows_ ++ snd newGraph_)

--
-- A representation of execution. It is essentially a sequence of states. In the representation,
-- it is a sequence of (s,a,p) where s is a state, a is the action that led from s to the next
-- state in the sequence, and p is the probability that the action a was taken.
-- If the state is the final state of the original LTS model, then a is set to an artificial
-- tau transition, with p = 1.
--
type Execution state ev = [(state,ev,Double)]

--
-- Construct the set of possible executions of a given execution model.
--
getAllExecs :: Eq ev => ExecutionModel state ev -> ev -> [Execution state ev]
getAllExecs (ProbLTS s0 _ arrows labels) tau = result M.! s0
   where
   result = worker M.empty s0
   sucs u | any (==u) (M.keys arrows) = Data.List.nub [ (v,action,probability) | (action,groups)<- arrows M.! u, (v,probability) <- groups ]
          | otherwise = []
   worker memo u =  case M.lookup u memo of
      Just _  ->  memo
      Nothing ->  let
                  sucessors = sucs u
                  sucessornodes = [ v | (v,_,_) <- sucessors ]
                  -- memo2  = foldr (flip worker) memo (D.trace ("\n## " ++ show u ++ " ==> " ++ show sucessors) sucessornodes)
                  memo2  = foldr (flip worker) memo sucessornodes
                  sigma  = [ (labels M.! u, a, p) : rho | (v,a,p) <- sucessors, rho <- memo2 M.! v ]
                  execs_u = if null sucessors then [[(labels M.! u, tau, 1.0)]]
                            --else D.trace ("\n  >> " ++ show u ++ ": " ++ show sigma) sigma
                            else sigma
                  in
                  M.insert u execs_u memo2

--
-- LTL-lin
--

-- Word formula [s1,s2,s3] means s1 /\ X(s2 /\ Xs3)
type WordFormula = [LTS.StateID]

-- A clause [w1,w2,w3] means w1 \/ w2 \/ w3
type Clause wordFormula = Set [wordFormula]
-- linear future formula [c1,c2,c3] means <>(c1 /\ <>(c2 /\ <>c3))
type LinFuture wordFormula = [Clause wordFormula]
data LTLLinAggr wordFormula
    = LinFuture (LinFuture wordFormula)
    | NegatedLinFuture (LinFuture wordFormula) -- representing not f
    | AtLeastN Int Int           -- representing {k}>=N formula (covering at least N number of k-segments)

getClauses (LinFuture clauses) = clauses
getClauses (NegatedLinFuture clauses) = clauses
getWords linf = Data.List.nub $ concat $ map S.toList $ getClauses linf


-- Check if a given execution satisfies the LTL-lin formula.
satLTLLinAggr :: Ord state => LTLLinAggr state -> Execution [state] ev -> Bool
satLTLLinAggr (AtLeastN k n) execution = (length $ Data.List.nub $ getSegments sigma) >= n
    where
    sigma = [ s | ([s],a,p) <- execution ]
    getSegments sigma = if length sigma < k then []
                        else take k sigma : getSegments (tail sigma)

satLTLLinAggr (NegatedLinFuture f) execution = not $ satLTLLinAggr (LinFuture f) execution
satLTLLinAggr linf execution = linfHold
   where
   clauses = getClauses linf
   words = getWords linf

   rho = [ state | ([state],a,p) <- execution ]

   linfHold = checkLinf clauses clausesLabel
   checkLinf [] z = True
   checkLinf (c1:rest) [] = False
   checkLinf clauses@(c1:restClauses) z@(satisfiedClauses:rest)
             =
             if c1 `elem` satisfiedClauses then checkLinf restClauses z
             else checkLinf clauses rest

   clausesLabel = checkClauses [0.. (length rho - 1)]
      where
      checkClauses [] = []
      checkClauses (i:rest) = [ c | c <- clauses, check1 c (wordsLabel M.! i) ] : checkClauses rest
      check1 c validwords = not $ S.null $ (c `S.intersection` validwords)

   -- map i to the set of all words which hold on rho[i..]
   wordsLabel = M.fromList $ zip [0..] (merge $ Data.List.transpose $ [ f w (checkWord w rho) | w <- words])
      where
      f w z = [ (w,indicator) | indicator <- z ]
      merge labels = [ foldr add_ S.empty s | s <- labels ]
        where
        add_ (word,True)  collected = S.insert word collected
        add_ (word,False) collected = collected

   checkWord w rho = checkw rho
      where
      k = length w
      checkw [] = []
      checkw z@(_:rest) = (take k z == w) : checkw rest

getSatisfyingExecutions :: (Ord state, Eq ev) => LTLLinAggr state -> ev -> ExecutionModel [state] ev -> [Execution [state] ev]
getSatisfyingExecutions f tau emodel = satisfyingExecutions
    where
    allExecutions = getAllExecs emodel tau
    satisfyingExecutions = [ rho | rho <- allExecutions, satLTLLinAggr f rho ]

-- Calculate the analysis f on the given execution model. We use a brute approach, by quantifying
-- over all possible executions on the model.
bruteAnalyzeTestCase :: (Eq ev, Ord state) => ev -> LTLLinAggr state -> ExecutionModel [state] ev -> Double
bruteAnalyzeTestCase tau f emodel = --D.trace ("\n** Total number of executions: " ++ show nAll ++
                                --             "\n** Satisfying executions: " ++ show n
                                             -- ++ "\n## " ++ show satisfyingExecutions
                                --            )
                                (sum [ calcProbability rho | rho <- satisfyingExecutions ])
    where
    allExecutions = getAllExecs emodel tau
    satisfyingExecutions = [ rho | rho <- allExecutions, satLTLLinAggr f rho ]
    nAll = length allExecutions
    n = length satisfyingExecutions
    calcProbability rho = product [ p | (s,a,p) <- rho ]

-- Calculate the analysis f on the given execution model. We use a much faster graph
-- labelling approach. We impose the following restriction:
--   * if the words size in f is k >= 1, we assume the execution model to have been k-word expanded.
--   * For aggregate analysis of the form AtLeastN k n, we assume k=1. Analysis with
--     higher k is possible by first applying k-work expansion on the execution model.
--
analyzeTestCase :: (Ord state, Eq ev) => LTLLinAggr state -> ExecutionModel [state] ev -> Double
analyzeTestCase (NegatedLinFuture f) emodel =  1 - analyzeTestCase (LinFuture f) emodel
analyzeTestCase f@(LinFuture cls) (ProbLTS u0 _ arrows labels) = (futuresLabel M.! u0) M.! cls
   where
   tailsOf_f = Data.List.tails cls

   clauses = getClauses f
   words = getWords f

   nodes = M.keys labels

   -- maps every node u in the exec-model to the set of all the words that would hold on u
   wordsLabel =  M.fromList [ (u, S.fromList [ w | w <- words, w `Data.List.isPrefixOf` lab ])
                              | (u,lab) <- M.assocs labels ]

   -- maps every node u to the list of clauses that hold on that node u
   clausesLabel = M.fromList [ (u, [ c | c <- clauses, checkClause u c]) | u <- nodes]
   checkClause u c = not $ S.null $ (c `S.intersection` (wordsLabel M.! u))

   --futuresLabel :: Ord state => Map LTS.StateID (Map [Clause state] Double)
   futuresLabel = worker M.empty u0

   sucs u =  Data.List.nub [ (v,p) | (a,groups)<- arrows M.! u, (v,p) <- groups ]

   worker memo u = case M.lookup u memo of
       Just _   -> memo
       Nothing  -> memo3
       where
       sucessors = sucs u

       -- recurse into the successors:
       memo2 = foldr (flip worker) memo [ v | (v,p)<- sucessors ]

       -- now add u into the memo:
       memo3  = M.insert u probs_at_u memo2
       probs_at_u = foldr calcProbability M.empty tailsOf_f

       calcProbability [] u_futuresLabel = u_futuresLabel

       -- calculating the chance of <>c
       calcProbability futureblock@[c] u_futuresLabel = M.insert futureblock p u_futuresLabel
           where
           c_holds_at_u = c `elem` (clausesLabel M.! u)
           p     = if c_holds_at_u then 1.0 else psucs
           psucs = sum [ p * ((memo2 M.! v) M.! futureblock) | (v,p) <- sucessors ]


       -- calculating the chance of <>(c /\ <>(...))
       calcProbability futureblock@(c:nextblock) u_futuresLabel = M.insert futureblock p u_futuresLabel
           where
           c_holds_at_u = c `elem` (clausesLabel M.! u)
           p = if c_holds_at_u  then psucsNextBlock else psucsBlock
           psucsBlock = sum [ p * ((memo2 M.! v) M.! futureblock) | (v,p) <- sucessors ]
           psucsNextBlock = u_futuresLabel M.! nextblock

analyzeTestCase (AtLeastN 1 n) (ProbLTS u0 _ arrows labels) = sum [ p | (set,p) <- sets_at_u0 , S.size set >= n ]
   where

   sets_at_u0 = (worker M.empty u0) M.! u0

   sucs u =  Data.List.nub [ (v,p) | (a,groups)<- arrows M.! u, (v,p) <- groups ]

   worker memo u = case M.lookup u memo of
      Just _   -> memo
      Nothing  -> memo3
      where
      sucessors = sucs u

      -- recurse into the successors:
      memo2 = foldr (flip worker) memo [ v | (v,p)<- sucessors ]
      state_repby_u = labels M.! u
      sets_at_u = if null sucessors then [ (S.singleton(state_repby_u), 1.0) ]
                  else [ (S.insert state_repby_u set, p*p') | (v,p) <- sucessors, (set,p') <- memo2 M.! v ]

      merged_sets_at_u = concat $ map mergeGroup $ groups
          where
          groups = Data.List.groupBy (\(set1,_) (set2,_) -> set1 == set2) sets_at_u
          mergeGroup []    = []
          mergeGroup group = [(fst $ head $ group, sum [ p | (set,p) <- group ]) ]

      -- now add u into the memo:
      memo3  = M.insert u sets_at_u memo2

wordNormalize k emode@(ProbLTS u0 _ arrows labels) = undefined
   where

   sucs u =  Data.List.nub [ (v,p) | (a,groups)<- arrows M.! u, (v,p) <- groups ]

   -- label each node u with words of length k that can be executed on u
   -- will do this recursively over k
   --words_labels :: Map LTS.StateID (Map Int [([LTS.StateID],LTS.StateID,Double)])
   words_labels = topworker M.empty k

   topworker memo k =
      if k<=0 then memo
      else let memo2 = topworker memo (k-1) in worker memo2 k u0

   isDone memo (u,k) = case M.lookup u memo of
        Just y  ->  isJust $ M.lookup k y
        Nothing ->  False

   insertToMemo memo u k words = case M.lookup u memo of
       Just y  -> M.insert u (M.insert k words y) memo
       Nothing -> M.insert u (M.insert k words M.empty) memo

   -- this will do the actual labeling of length k ; k>0
   worker memo k u  = if isDone memo (u,k) then memo
       else if k==1 then let
                         state_u = labels M.! u
                         words_u = [([state_u],u,1.0)]
                         in
                         insertToMemo memo u k words_u

       else let -- k>1
            sucessors = sucs u
            sucessornodes = [ v | (v,_) <- sucessors ]
            memo2  = foldr (\v memo_ -> worker memo_ k v) memo sucessornodes
            state_u = labels M.! u

            sharp = LTS.Normal "#"
            words_u = if null sucessors
                      then {- u must be the end node -} [(replicate k sharp, u, 1.0)]
                      else [ (state_u:w, endnode, p*p')  | (v,p) <- sucessors, (w,endnode,p') <- memo2 M.! v M.! (k-1) ]
            in
            insertToMemo memo2 u k words_u



--
-- An example for testing the functions
--
example_EX1 = ProbLTS s0 [] (M.fromList arrows) (M.fromList statelabels)
    where
    states = map LTS.Normal ["s0","s1","s2","s3","s4","s5","s6"]
    [s0,s1,s2,s3,s4,s5,s6] = states
    tau = LTS.Internal
    a = LTS.Send "a"
    b = LTS.Receive "b"
    c = LTS.Send "c"
    arrows =  [
      (s0, [(a, [(s1,0.5),(s2,0.5)])]),
      (s1, [(b, [(s0,0.9),(s3,0.1)])]),
      (s2, [(b, [(s0,1.0)]), (c, [(s2,1.0)])]),
      (s3, [(tau, [(s4,0.9),(s5,0.1)])]),
      (s4, [(a, [(s1,1.0)])]),
      (s5, [(c, [(s6,1.0)])]),
      (s6, [])
      ]
    statelabels = map (\s-> (s,())) states

-- some example test cases
tc1 = [LTS.Send "a", LTS.Receive "b", LTS.Send "a"]
tc2 k = LTS.Send "a" : concat (replicate k [LTS.Receive "b", LTS.Send "a"])
tc3 k = concat (replicate k [a,b]) ++ [a] ++ replicate k c ++ [b,a,b,c]  -- total length =3b + 5
   where
   a = LTS.Send "a"
   b = LTS.Receive "b"
   c = LTS.Send "c"

word_ w    = map LTS.Normal w
clause1_ w = S.fromList $ [word_ w]
example_formula1 = LinFuture [clause1_ ["s3","s4"] , clause1_ ["s0","s2"]]
example_formula2 = LinFuture [clause1_ ["s4"] , clause1_ ["s2"]]

example_formula3 = LinFuture [clause1_ ["s1"] , clause1_ ["s0"]]

-- ======================================================
-- Benchmarking, to be moved elsewhere!
--
--  We will use the model EX1 above and EX2 below.
-- ======================================================

example_EX2 n = ProbLTS s0 [] (M.fromList arrows) (M.fromList statelabels)
    where
    states_group1 = map LTS.Normal ["s0","nds1","nds2","s1","nds3","done"]
    states_group2 = [ndt i | i <- [0..(n-1)]]
    states = states_group1 ++ states_group2

    [s0,nds1,nds2,s1,nds3,done] = states_group1
    ndt i = LTS.Normal ("ndt" ++ show i)

    tau = LTS.Internal
    a = LTS.Send "a"
    b = LTS.Send "b"
    c = LTS.Receive "c"
    arrows =  [
      (s0,   [(a, [(nds1,1.0)])]),
      (nds1, [(c, [(nds1,0.7),(nds2,0.3)]), (a,[(s1,1.0)])]),
      (nds2, [(c, [(nds1,0.7),(nds2,0.3)])]),
      (s1,   [(b, [(s1,1.0)]), (a,[(nds3,1.0)])]),
      (nds3, [(c, (nds3,pRemainingAt_nds3) : arrows_to_ndti), (a,[(done,1.0)])]),
      (done, [])
      ]
      ++ arrows_from_ndti

    pRemainingAt_nds3 = if n==0 then 1.0 else 0.7
    pndt = (1.0 - pRemainingAt_nds3) / (fromIntegral $ toInteger n)
    arrows_to_ndti   = [ (ndt i, pndt)  | i <- [0..(n-1)]]
    arrows_from_ndti = [(ndt i, [(c,[(nds3,1.0)])]) | i <- [0..(n-1)]]

    statelabels = map (\s-> (s,())) states

-- some test cases
tc4 k =  [a]
         ++ replicate k c ++ [a]
         ++ replicate k b ++ [b,a]
         ++ replicate k c ++ [a]   -- total length =3b + 5
   where
   a = LTS.Send "a"
   b = LTS.Send "b"
   c = LTS.Receive "c"


-- the benchmarking function
benchmark args tau =
   if null args then do
      print_models_statistics
      print_specs_statistics
   else do
      let [emodelName,k_,specName,algorithmName] = args
      let k = read k_ - (4::Int)
      time0 <- getCurrentTime
      let emodelDesc = case emodelName of
                         "EX1"   -> executionModels_EX1 !! k
                         "EX2_0" -> executionModels_EX2_0 !! k
                         "EX2_2" -> executionModels_EX2_2 !! k
                         "EX2_8" -> executionModels_EX2_8 !! k
      let (_,_,_,emodel) = emodelDesc
      let count = cntNodes emodel
      time1 <- count `deepseq` getCurrentTime
      let spec = case specName of
                     "f1_ex1" -> snd f1_ex1
                     "f1_ex2" -> snd f1_ex2
                     "f2"     -> snd f2
                     "f3"     -> snd f3
      let algorithm = case algorithmName of
                        "brute" -> bruteAnalyzeTestCase tau
                        "label" -> analyzeTestCase
      -- putStrLn ("\n============")
      let p = algorithm spec emodel
      time2 <- p `deepseq` getCurrentTime
      putStrLn $ concat (Data.List.intersperse ";" args) ++ ";" ++ show p ++ ";"
                 ++ show (diffUTCTime time1 time0) ++ ";"
                 ++ show (diffUTCTime time2 time1)

   where
   --
   -- the execution models:
   --
   tc_lengths = [4..8]

   mkExecModel name k model testcase = (name, k, testcase, mkExecutionModel testcase model)
   executionModels_EX1   = [ mkExecModel "tc3 on EX1" k example_EX1 (tc3 k)    | k <- tc_lengths ]
   executionModels_EX2_0 = [ mkExecModel "tc4 on EX2 0" k (example_EX2 0) (tc4 k) | k <- tc_lengths ]
   executionModels_EX2_2 = [ mkExecModel "tc4 on EX2 2" k (example_EX2 2) (tc4 k) | k <- tc_lengths ]
   executionModels_EX2_8 = [ mkExecModel "tc4 on EX2 8" k (example_EX2 8) (tc4 k) | k <- tc_lengths ]

   executionModels = executionModels_EX1 ++ executionModels_EX2_0 ++ executionModels_EX2_2 ++ executionModels_EX2_8

   print_models_statistics = do
      putStrLn "\n============"
      putStrLn "Models statistics:"
      putStrLn "Name | k | tc-length | #nodes | #execs"
      sequence_ [print_model_stats $ mkModel_stats r | r <- executionModels]

   mkModel_stats (name,k,tc,emodel) = (name,k,length tc, cntNodes emodel,length $ getAllExecs emodel tau)
   print_model_stats (name,k,tclen,nodesCnt,execsCnt) = putStrLn $ concat $ Data.List.intersperse ";" [name, show k , show tclen, show nodesCnt, show execsCnt]

   --
   -- the analyses:
   --
   f1_ex1 = ("f1_ex1",LinFuture [clause1_ ["s4"] , clause1_ ["s2"]])      -- <>(s4 /\ <>s2)
   f1_ex2 = ("f1_ex2",LinFuture [clause1_ ["nds2"] , clause1_ ["ndt0"]])  -- <>(nds2 /\ <>ndt0)
   f2 = ("f2", AtLeastN 1 8)
   f3 = ("f3", AtLeastN 1 10)
   specs = [f1_ex1, f1_ex2, f2, f3]

   print_specs_statistics = do
      putStrLn "\n============"
      putStrLn "Specs statistics:"
      putStrLn "Spec-name | emodel-name | k | #satisfying-execs"
      let stats1 = [ (fst f1_ex1, emodelName, k, countSatisfyingExecs (snd f1_ex1) emodel)| (emodelName,k,_,emodel) <- executionModels_EX1 ]
      let modelsEX2 = executionModels_EX2_0 ++ executionModels_EX2_2 ++ executionModels_EX2_8
      let stats2 = [ (fst f1_ex2, emodelName, k, countSatisfyingExecs (snd f1_ex2) emodel)| (emodelName,k,_,emodel) <- modelsEX2 ]

      sequence_ $ map print_spec_stats  stats1
      sequence_ $ map print_spec_stats  stats2


   countSatisfyingExecs phi emodel = length $ getSatisfyingExecutions phi tau emodel
   print_spec_stats (specName,emodelName,k,count) = putStrLn $ concat $ Data.List.intersperse ";"  [specName,emodelName,show k, show count]






