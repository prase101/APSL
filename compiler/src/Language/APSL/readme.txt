* /AMSL  : language def. and parser of AMSL, and related utilities
* /AISL  : language def. and parser of AISL, and related utilities
* /Generator : contains generators e.g. message generator, and related stuffs
* /Channel   : 
    - contains the message decoder and encoder
    - implementation of channels to connect to Implementation Under Test

* Base.hs : providing representation of character sets; a bit weird to be put here...

* /TestEngine : 
     - this is where you can find APSL test engine
     - putting LTS coverage calculator here
