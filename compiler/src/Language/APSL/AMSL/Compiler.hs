-- | Compile the contents of one or more specification files into an AMSL Module.
module Language.APSL.AMSL.Compiler (
    CompiledUnit(..),
    compile,
    CompileError(..),
    ASTCompileError(..),
    compileUnvalidated,
    compileString,
    compileActionString,
    G.TrEvent(..),
    G.TrAction(..),
    G.TrGuard(..),
    compileAST
) where

import Prelude hiding (catch)

import Language.APSL.AMSL.Compiler.Internal
import Language.APSL.AMSL.DataType.Module
import Language.APSL.AMSL.Validator

import Language.APSL.AMSL.Grammar.Par (pModule, myLexer, pTrEvent)
import Language.APSL.AMSL.Grammar.ErrM (Err(..))
import qualified Language.APSL.AMSL.Grammar.Abs as G

import Data.Set (Set)
import qualified Data.Set as S
import Control.Monad
import Control.Exception (catch)


-- | The primary compiler function, and likely the only one you will need.
--   Compiles an AMSL file into a module, along with its dependencies, and then validates them.
--   The first argument should point to a directory (optionally relative to the current working
--   directory) describing the root from where imported modules should be looked up.
--   The second argument is the path of the AMSL file; if it is a relative path, it is relative to
--   the afformentioned lookup root.
--   The result is either an error or a valid 'Module'.
compile :: FilePath -> FilePath -> IO (Either CompileError Module)
compile dir path = compileUnvalidated dir path >>= return . validate
 where
    validate (Left err) = Left err
    validate (Right m)  =
        case validateModule m of
            Left err -> Left $ InvalidModule err
            Right () -> Right m


-- | The various kinds of compiler errors.
data CompileError
    = SyntaxError String
    | InvalidModule ValidationError
    | FileReadError FilePath IOError
    | CyclicImport
    | ASTError ASTCompileError
    deriving (Show)

-- | Same as 'compile', except that the resulting module has not been validated, so it may still
--   contain errors.
compileUnvalidated :: FilePath -> FilePath -> IO (Either CompileError Module)
compileUnvalidated dir path = compileFile S.empty $ dir ++ "/" ++ path
 where
    compileFile done f | f `S.member` done = return $ Left CyclicImport
                       | otherwise = do
        unit <- (liftM compileString $ readFile f) `catch` (return . Left . FileReadError f)
        either (return . Left) (resolveUnit $ S.insert f done) unit

    resolveUnit done (CompiledUnit deps makeMod) = do
        cdeps <- liftM sequence $ sequence $ map (compileFile done) deps
        return $ cdeps >>= either (Left . ASTError) Right . makeMod


-- | Parse a String to an AST, then compile it further.
compileString :: String -> Either CompileError CompiledUnit
compileString inp =
    case pModule $ myLexer inp of
        Bad err -> Left $ SyntaxError err
        Ok  ast -> Right $ compileAST ast

compileActionString :: String -> Either CompileError G.TrEvent
compileActionString str
  = case pTrEvent $ myLexer str of
      Bad err -> Left $ SyntaxError err
      Ok action -> Right $ action

-- | Compiles a file's abstract syntax tree.
compileAST :: G.Module -> CompiledUnit
compileAST = transModule
