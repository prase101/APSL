-- TODO 
-- We don't have a validator yet.

module Language.APSL.AMSL.Validator where

import Language.APSL.AMSL.DataType.Module
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Expression


validateModule :: Module -> Either ValidationError ()
validateModule _ = Right ()

validateType   :: Type a -> Either ValidationError ()
validateType _ = Right ()

data ValidationError = ValidationError deriving (Show)