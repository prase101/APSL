{-# LANGUAGE OverloadedStrings, GADTs, PatternGuards #-}

-- | Tool for verifying whether a value matches a type instance.
module Language.APSL.AMSL.ValueChecker (checkValue, bitMatch, char8Match) where 

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Codecs
import Language.APSL.AMSL.DataType.TypeCodec

import           Data.BitString.BigEndian (BitString)
import qualified Data.BitString.BigEndian as B

import Text.Regex.TDFA ((=~))

import           Data.Map (Map)
import qualified Data.Map as M
import Data.List
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as BC8
import Debug.Trace


matchWhole :: String -> String
matchWhole pat | not $ "^" `isPrefixOf` pat = matchWhole $ '^' : pat
               | not $ "$" `isSuffixOf` pat = pat ++ "$"
               | otherwise = pat

bitMatch :: String -> BitString -> Bool
bitMatch pat bits = map (\b -> if b then '1' else '0') (B.toList bits) =~ pat

char8Match :: String -> BitString -> Bool
char8Match pat bits = B.length bits `mod` 8 == 0 && BC8.unpack (B.realizeBitStringLazy bits) =~ pat

-- | Checks whether a value with a certain type satisfies the constraints expressed by the provided 
--   type arguments. The arguments and type should already have been validated.
--   For enum values, it also checks if the input is a member of the enumeration.
--   Does not recursively check the arguments of record/union fields.
checkValue :: Env -> Type a -> Arguments -> a -> Bool
checkValue env ty args@(Arguments args') x = 
    case ty of
        BasicType t -> checkBasic t
        UserType (Record _ ps r) -> checkRecord r ps x
        UserType (Union _ _ tt u) -> checkUnion tt u x
        UserType (Enumeration _ tt e) -> checkEnum tt e x
        UserType (TypeAlias _ t' args') -> checkValue env t' (addArgs args args') x
        ExtType   m -> error "TODO: extension type checker"
 where
    valArg n | Just (ExpArg ex) <- lookup n args' = either (const Nothing) noNull $ evaluate env ex 
             | otherwise = Nothing
    arg n pr = maybe True pr $ valArg n
    noNull ENull = Nothing
    noNull x = Just x
    allTrue = all id :: [Bool] -> Bool

    checkBasic t =
        allTrue $ case t of
            Binary    -> [
                arg "value"  $ \(EBinary val) -> val == x,
                arg "length" $ \(EInt len) -> len == toInteger (B.length x),
                arg "min_length" $ \(EInt len) -> len <= toInteger (B.length x),
                arg "max_length" $ \(EInt len) -> len >= toInteger (B.length x),
                arg "length_factor" $ \(EInt f) -> toInteger (B.length x) `mod` f == 0,
                arg "bit_pattern" $ \(ERegex pat) -> bitMatch (matchWhole pat) x,
                arg "exclude_bit_pattern" $ \(ERegex pat) -> not $ bitMatch pat x,
                arg "char8_pattern" $ \(ERegex pat) -> char8Match (matchWhole pat) x,
                arg "exclude_char8_pattern" $ \(ERegex pat) -> not $ char8Match pat x
             ]
            Bool      -> [
                arg "value" $ \(EBool val) -> val == x
             ]
            Integer   -> [
                arg "value" $ \(EInt val) -> val == x,
                arg "min" $ \(EInt lower) -> lower <= x,
                arg "max" $ \(EInt upper) -> x <= upper,
                arg "factor" $ \(EInt factor) -> x `mod` factor == 0
             ]
            Text      -> [
                arg "value" $ \(EText val) -> val == x,
                arg "count" $ \(EInt cnt) -> cnt == (toInteger $ length x),
                arg "min_count" $ \(EInt minc) -> minc <= (toInteger $ length x),
                arg "max_count" $ \(EInt maxc) -> maxc >= (toInteger $ length x),
                arg "pattern" $ \(ERegex pat) -> x =~ matchWhole pat, -- (trace ("### matching on " ++ show pat) pat),
                arg "exclude_pattern" $ \(ERegex pat) -> not $ x =~ pat, -- trace ("### matching on NOT " ++ show pat) pat,
                arg "charset" $ \(EText set) -> all (inCharset set) x
             ]
            Optional  -> 
                case x of 
                    ContainerValue st mx -> [
                        arg "is_empty" $ \(EBool empt) -> empt == isNothing mx,
                        maybe True (subType "subject" st) mx
                     ]
            List      -> 
                case x of
                    ContainerValue et xs -> [
                        arg "count" $ \(EInt cnt) -> cnt == (toInteger $ length xs),
                        arg "min_count" $ \(EInt minc) -> minc <= (toInteger $ length xs),
                        arg "max_count" $ \(EInt maxc) -> maxc >= (toInteger $ length xs),
                        arg "value" $ \(EList vals) -> 
                            length vals == length xs && 
                                all id (zipWith (subValue et) vals xs),
                        all (subType "elem" et) xs
                     ]

    checkRecord :: Record b -> [ParamName] -> b -> Bool
    checkRecord rec pars x = foldRecord env2 checkField rec x == Just ()
     where 
        Right env2 = argsToEnv args env
        checkField :: Env -> (Type t, Arguments) -> (Codec, Arguments) -> t -> Maybe ()
        checkField env (ty, args) _ f | checkValue env ty args f = Just ()
                                      | otherwise = Nothing


    checkUnion :: TagType t -> Union t b -> b -> Bool
    checkUnion tt (Option _ _ _ rest)  (Right x') = checkUnion tt rest x'
    checkUnion tt (Option tag _ ftc _) (Left x') = 
        let Right env2 = argsToEnv args env
         in arg "tag" (maybe False (compareTags tt tag) . readTag tt)
             && checkValue env2 (fieldType ftc) (fieldTypeArgs ftc) x'

    checkUnion _ None _ = False

    checkEnum :: TagType b -> Enumeration b -> b -> Bool
    checkEnum tt e val = 
        case enumField tt e val of
            Nothing -> False
            Just p  -> arg "value" $ \(EEnum _ p') -> p == p'

    subValue :: Type b -> ExpValue -> b -> Bool
    subValue ty val = checkValue env ty $ Arguments [("value", ExpArg $ Constant val)]

    subType :: ParamName -> Type b -> b -> Bool
    subType n ty = checkValue env ty (subTypeArgs n)

    subTypeArgs n | Just (TypeExpArg _ args) <- lookup n args' = args
                  | otherwise = error "Missing subtype"


    