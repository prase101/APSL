module Language.APSL.AMSL.Grammar.Skel where

-- Haskell module generated by the BNF converter

import Language.APSL.AMSL.Grammar.Abs
import Language.APSL.AMSL.Grammar.ErrM
type Result = Err String

failure :: Show a => a -> Result
failure x = Bad $ "Undefined case: " ++ show x

transCName :: CName -> Result
transCName x = case x of
  CName string -> failure x
transLName :: LName -> Result
transLName x = case x of
  LName string -> failure x
transTDecimal :: TDecimal -> Result
transTDecimal x = case x of
  TDecimal string -> failure x
transTHexInt :: THexInt -> Result
transTHexInt x = case x of
  THexInt string -> failure x
transTDQText :: TDQText -> Result
transTDQText x = case x of
  TDQText string -> failure x
transTSQText :: TSQText -> Result
transTSQText x = case x of
  TSQText string -> failure x
transTHexBin :: THexBin -> Result
transTHexBin x = case x of
  THexBin string -> failure x
transTBitBin :: TBitBin -> Result
transTBitBin x = case x of
  TBitBin string -> failure x
transRegexLit :: RegexLit -> Result
transRegexLit x = case x of
  RegexLit string -> failure x
transTrEvent :: TrEvent -> Result
transTrEvent x = case x of
  TrEvent traction trtesterguard trguard trstatements -> failure x
transTrAction :: TrAction -> Result
transTrAction x = case x of
  TrActionObs cname -> failure x
  TrActionCtl cname -> failure x
  TrActionTau -> failure x
  TrActionTheta -> failure x
transTrTesterGuard :: TrTesterGuard -> Result
transTrTesterGuard x = case x of
  TrTGuard listedexp -> failure x
  TrTNoGuard -> failure x
transTrGuard :: TrGuard -> Result
transTrGuard x = case x of
  TrGuard listedexp -> failure x
  TrNoGuard -> failure x
transTrStatements :: TrStatements -> Result
transTrStatements x = case x of
  TrStatements trstatements -> failure x
  TrNoStatements -> failure x
transTrStatement :: TrStatement -> Result
transTrStatement x = case x of
  TrStatement lname exp -> failure x
transModule :: Module -> Result
transModule x = case x of
  Module lname imports decls -> failure x
transImport :: Import -> Result
transImport x = case x of
  Import importlist textlit -> failure x
transImportList :: ImportList -> Result
transImportList x = case x of
  ImportAll -> failure x
  ImportSymbols importsymbols -> failure x
transImportSymbol :: ImportSymbol -> Result
transImportSymbol x = case x of
  ImportSymbol cname -> failure x
transDecl :: Decl -> Result
transDecl x = case x of
  RecordDecl cname fields -> failure x
  ParamRecordDecl cname paramnames fields -> failure x
  MessageDecl cname fields -> failure x
  TaggedUnionDecl cname1 cname2 taggedoptions -> failure x
  ParamUnionDecl cname1 paramnames cname2 taggedoptions -> failure x
  EnumDecl cname1 cname2 enummembers -> failure x
  TypeDecl cname typeexp -> failure x
  CodecDecl cname typeexp -> failure x
  DefaultDecl cname typeexp -> failure x
  ExtDecl ext -> failure x
transExt :: Ext -> Result
transExt x = case x of
  TypeExt cname paramnames -> failure x
  CodecExt cname1 paramnames cname2 -> failure x
transParamName :: ParamName -> Result
transParamName x = case x of
  ParamName lname -> failure x
transField :: Field -> Result
transField x = case x of
  Field lname fieldinfo -> failure x
transFieldInfo :: FieldInfo -> Result
transFieldInfo x = case x of
  FieldType typeexp -> failure x
  FieldTypeCodec typeexp1 typeexp2 -> failure x
transOption :: Option -> Result
transOption x = case x of
  NamedOption field -> failure x
  UnnamedOption fieldinfo -> failure x
transTaggedOption :: TaggedOption -> Result
transTaggedOption x = case x of
  TaggedOption exp option -> failure x
transEnumMember :: EnumMember -> Result
transEnumMember x = case x of
  EnumMember lname exp -> failure x
transTypeExp :: TypeExp -> Result
transTypeExp x = case x of
  TypeByName cname -> failure x
  TypeWithParams cname parameterset -> failure x
transParameterSet :: ParameterSet -> Result
transParameterSet x = case x of
  ParamList namedparams -> failure x
transNamedParam :: NamedParam -> Result
transNamedParam x = case x of
  NamedParam lname argument -> failure x
transArgument :: Argument -> Result
transArgument x = case x of
  TypeArg typeexp -> failure x
  ExpArg exp -> failure x
transExp :: Exp -> Result
transExp x = case x of
  ECond exp1 exp2 exp3 -> failure x
  EOr exp1 exp2 -> failure x
  EXor exp1 exp2 -> failure x
  EAnd exp1 exp2 -> failure x
  EEq exp1 exp2 -> failure x
  ENeq exp1 exp2 -> failure x
  ELt exp1 exp2 -> failure x
  EGt exp1 exp2 -> failure x
  ELte exp1 exp2 -> failure x
  EGte exp1 exp2 -> failure x
  ECons exp1 exp2 -> failure x
  EShiftl exp1 exp2 -> failure x
  EShiftr exp1 exp2 -> failure x
  EPlus exp1 exp2 -> failure x
  EMinus exp1 exp2 -> failure x
  ETimes exp1 exp2 -> failure x
  EIntDiv exp1 exp2 -> failure x
  EMod exp1 exp2 -> failure x
  EPow exp1 exp2 -> failure x
  ENeg exp -> failure x
  ENot exp -> failure x
  EList listedexps -> failure x
  EField lname subfieldnames -> failure x
  EVar lname -> failure x
  ENull -> failure x
  EBool boollit -> failure x
  EInt intlit -> failure x
  EText textlit -> failure x
  EBin binlit -> failure x
  ERegex regexlit -> failure x
transListedExp :: ListedExp -> Result
transListedExp x = case x of
  ListedExp exp -> failure x
transSubFieldName :: SubFieldName -> Result
transSubFieldName x = case x of
  SubFieldName lname -> failure x
transBoolLit :: BoolLit -> Result
transBoolLit x = case x of
  LTrue -> failure x
  LFalse -> failure x
transIntLit :: IntLit -> Result
transIntLit x = case x of
  LDecimal tdecimal -> failure x
  LHexInt thexint -> failure x
transTextLit :: TextLit -> Result
transTextLit x = case x of
  LDQText tdqtext -> failure x
  LSQText tsqtext -> failure x
transBinLit :: BinLit -> Result
transBinLit x = case x of
  LHexBin thexbin -> failure x
  LBitBin tbitbin -> failure x

