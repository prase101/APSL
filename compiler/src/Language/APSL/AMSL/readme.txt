AMSL stuffs:

  * Grammar.cf   : BNFC syntax specification of AMSL
  * /Grammar     : contains generated files from BNFC
  * /DataType    : high level datatypes representing an AMSL module,
  * Compiler.hs  : top-level AMSL parser
  * Validator.hs : AMSL language validator --> still dummy

Message related stuffs:

  * MessagePrettyPrinter.hs : utility to pretty print message instances
  * ValueChecker.hs : utility to check is a value is a consistent instance of an APSL type.

