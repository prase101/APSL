{-# LANGUAGE RankNTypes, TupleSections, OverloadedStrings, ScopedTypeVariables, GADTs, PatternGuards #-}

module Language.APSL.AMSL.Compiler.Internal (transModule, transExp, ASTCompileError(..), Result, CompiledUnit(..), identCName, identLName) where

import qualified Language.APSL.AMSL.Grammar.Abs as G

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Module
import Language.APSL.AMSL.DataType.Types hiding (typeName)
import qualified Language.APSL.AMSL.DataType.Types as Types
import Language.APSL.AMSL.DataType.Codecs
import Language.APSL.AMSL.DataType.TypeCodec
import Language.APSL.AMSL.DataType.Expression

import Data.BitString.BigEndian (BitString)
import qualified Data.BitString.BigEndian as B

import Data.Hex

import qualified Data.ByteString.Lazy.Char8 as BC8
import Data.Map (Map)
import qualified Data.Map as M
import Data.Set (Set)
import qualified Data.Set as S
import Data.Char
import Data.Ord
import Data.List
import Data.Either
import Control.Monad
import Control.Arrow


-- | A single compiled AMSL. It may depend on other files that will have to be compiled first,
--   before a 'Module' object can be obtained. Note that this Module is not yet validated.
data CompiledUnit = CompiledUnit {
    requiredDependencies :: [FilePath],
    compiledModule       :: [Module] -> Result Module
    -- ^ The argument should contain a compiled module for each depandant module in
    --   /requiredDependencies/.
}

-- | The result of compilation.
type Result a = Either ASTCompileError a

data ASTCompileError
    = ImportedDeclNotFound {decName :: TypeName, modName :: String}
    | UnknownName {decName :: TypeName}
    | CyclicDependency {decName :: TypeName}
    | NameConflict {decName :: TypeName}
    | ExpressionError InvalidExpression
    | InvalidTagType {tyName :: TypeName}
    | InvalidEnumRepType {tyName :: TypeName}
    | CodecUsedAsType {coName :: TypeName}
    | TypeUsedAsCodec {tyName :: TypeName}
    | InvalidTypeExpression
    | CodecRequired {tyName :: TypeName}
    deriving (Show)

-- An environment of names, mapping to possibly incomplete declarations and default codecs.
data DeclTable = DeclTable {
    declarations  :: Map TypeName (Dependant Declaration),
    defaultCodecs :: Map TypeName DefaultCodec
}

-- A result monad of which the value may be dependant on another declaration.
data Dependant a
    = Final a
    | Partial TypeName (Declaration -> Dependant a)
    | RequiresDefaultCodec TypeName (DefaultCodec -> Dependant a)
    | Error ASTCompileError

-- A compiled declaration.
data Declaration
    = MessageDecl Message
    | TypeDecl DefinedType
    | CodecDecl DefinedCodec

newtype DefaultCodec = DefaultCodec (Dependant (Codec, Arguments))

instance Functor Dependant where
    fmap = liftM

instance Applicative Dependant where
    pure = Final
    (<*>) = ap

instance Monad Dependant where
 (Error err) >>= _  = Error err
 (Final x)     >>= mf = mf x
 (Partial n l) >>= mf = Partial n $ (>>= mf) . l
 (RequiresDefaultCodec n l) >>= mf = RequiresDefaultCodec n $ (>>= mf) . l

-- Compiles the AST of a module.
transModule :: G.Module -> CompiledUnit
transModule x =
    case x of
        G.Module (G.LName (_, name)) imports decls ->
            -- Import statements.
            let (iNames, paths) = unzip $ map transImport imports
             in CompiledUnit {
                requiredDependencies = paths,
                compiledModule = \mods -> do
                    -- Imported declarations.
                    importedDecls <- liftM concat $ sequence $ zipWith fetchDecls iNames mods

                    -- Declarations from within the module.
                    table   <- transDecls decls
                    declList <- resolveDecls table (importedDecls :: [(TypeName, Declaration)])
                    return $ makeModule name $ map snd $ importedDecls ++ declList
            }

transImport :: G.Import -> (Maybe [TypeName], FilePath)
transImport (G.Import symbols path) = (names, textLiteral path)
 where
    names = case symbols of
        G.ImportAll          -> Nothing
        G.ImportSymbols syms -> Just $ map (\(G.ImportSymbol s) -> typeName s) syms

-- Fetch certain (or all) declarations from an already compiled module.
fetchDecls :: Maybe [TypeName] -> Module -> Result [(TypeName, Declaration)]
fetchDecls names m =
    case names of
        Nothing     -> return decls
        Just names' -> sequence $ map getName names'
 where
    decls = map (\d -> (declName d, d)) $ concat [
                map MessageDecl $ messages m,
                map TypeDecl $ types m,
                map CodecDecl $ codecs m
            ]
    getName n = case lookup n decls of
                    Nothing -> Left ImportedDeclNotFound {decName = n, modName = moduleName m}
                    Just d  -> return (n, d)

-- Get the name of a declaration.
declName :: Declaration -> TypeName
declName (MessageDecl (Message name _)) = name
declName (TypeDecl (DefinedType t)) = Types.typeName $ UserType t
declName (TypeDecl (DeclaredExtType t)) = Types.typeName $ ExtType t
declName (CodecDecl (DefinedCodec (CodecAlias name _ _))) = name
declName (CodecDecl (DeclaredExtCodec _ c)) = extCodecName c


-- Resolves the dependency graph of a declaration table, given (already resolved) imported declarations. Cycles within
-- the dependency graph are not allowed, and detected.
resolveDecls :: DeclTable -> [(TypeName, Declaration)] -> Result [(TypeName, Declaration)]
resolveDecls table imports = do
    -- Check for naming conflicts with imported declarations.
    forM_ imports $ \(n,_) -> if n `M.member` declarations table then Left $ NameConflict n
                                                                 else return ()

    -- Start building a dependency graph, which is gradually adjusted while resolving them.
    let graph = table {declarations = M.union (declarations table) $ M.fromList $ map (second Final) imports}
    sequence $ snd $ mapAccumL resolve (S.empty, graph) $ M.toList $ declarations table
 where
    resolve :: (Set TypeName, DeclTable)
               -> (TypeName, Dependant Declaration)
               -> ((Set TypeName, DeclTable), Result (TypeName, Declaration))
    resolve (p, g) (n, Final d) = ((p, g), return (n, d))
    resolve (p, g) (n, RequiresDefaultCodec tyName f) =
        case M.lookup tyName $ defaultCodecs g of
            Just dc -> resolve (p, g) (n, f dc)
            Nothing -> ((p, g), Left $ CodecRequired tyName)
    resolve (p, g) (n, Partial depName f) | depName `S.member` p = ((p,g), Left $ CyclicDependency depName)
                                          | Just pDep <- M.lookup depName $ declarations g,
                                            ((_, g'), res) <- resolve (S.insert depName p, g) (depName, pDep)
                                          =
        case liftM (second f) res of
            Left err           -> ((p,g), Left err)
            Right (_, newDecl) ->
                let newG = g' {declarations = M.insert n newDecl $ declarations g'}
                 in resolve (p, newG) (n, newDecl)
    resolve acc (n, Partial depName _) = (acc, Left $ UnknownName depName)
    resolve acc (_, Error err) = (acc, Left err)


-- Compile a list of declarations, making sure there are no duplicate names.
transDecls :: [G.Decl] -> Result DeclTable
transDecls decls = do
    let (tDecls, tDefCodecs) = partitionEithers $ map (extractEither . transDecl) decls
    noDuplicates $ sort $ map fst tDecls
    noDuplicates $ sort $ map fst tDefCodecs
    return DeclTable {declarations = M.fromList tDecls, defaultCodecs = M.fromList tDefCodecs}
 where
    noDuplicates (a:b:_) | a == b = Left $ NameConflict a
    noDuplicates (_:xs)           = noDuplicates xs
    noDuplicates []               = return ()
    extractEither (a, Left  b) = Left (a,b)
    extractEither (a, Right b) = Right (a,b)

-- Turn a list of declarations into a Module object.
makeModule :: String -> [Declaration] -> Module
makeModule name = foldr addDecl (Module name [] [] [])
 where
    addDecl d m =
        case d of
            MessageDecl msg -> m {messages = msg : messages m}
            TypeDecl ty     -> m {types = ty : types m}
            CodecDecl co    -> m {codecs = co : codecs m}

-- Compile a single declaration or default codec. It may depend upon the values of other declarations.
transDecl :: G.Decl -> (TypeName, Either (Dependant Declaration) DefaultCodec)
transDecl x = case x of
    G.MessageDecl cname fields ->
        withName cname $ \n -> transFields fields (return . MessageDecl . Message n)
    G.RecordDecl cname fields  -> transRecord cname [] fields
    G.ParamRecordDecl cname params fields -> transRecord cname params fields
    G.TaggedUnionDecl cname tagname taggedoptions -> transUnion cname [] tagname taggedoptions
    G.ParamUnionDecl cname params tagname taggedoptions -> transUnion cname params tagname taggedoptions
    G.EnumDecl cname typename enummembers  ->
        withName cname $ \n -> withTagType typename $ \enumType -> do
            liftM (userType . Enumeration n enumType) $ transEnumeration enummembers enumType
    G.TypeDecl cname typeexp ->
        withName cname $ \n -> liftM (uncurry ($)) $
            transTypeExp typeexp $ \ty -> return $ userType . TypeAlias n ty
    G.CodecDecl cname typeexp ->
        withName cname $ \n -> liftM (uncurry ($)) $
            transCodecExp typeexp $ \co -> return $ userCodec . CodecAlias n co
    G.DefaultDecl cname typeexp ->
        (typeName cname, Right $ DefaultCodec $ transCodecExp typeexp $ return)
    G.ExtDecl (G.TypeExt cname paramnames) ->
        withName cname $ \n -> return $ TypeDecl $ DeclaredExtType $ ExtTypeMeta n $ map transParamName paramnames
    G.ExtDecl (G.CodecExt cname1 paramnames cname3) ->
        withName cname1 $ \n -> transType (typeName cname3) $ \ty ->
            return $ CodecDecl $ DeclaredExtCodec ty $ ExtCodecMeta n $ map transParamName paramnames

 where
    userType :: UserType a -> Declaration
    userType = TypeDecl . DefinedType

    userCodec :: UserCodec -> Declaration
    userCodec = CodecDecl . DefinedCodec

    withName cname f = let n = typeName cname in (n, Left $ f n)
    transParamName (G.ParamName p) = paramName p

    transRecord cname params fields =
        withName cname $ \n -> transFields fields (return . userType . Record n (map transParamName params))
    transUnion cname params tagname taggedoptions =
        withName cname $ \n -> withTagType tagname $ \tagType ->
            transOptions taggedoptions tagType (return . userType . Union n (map transParamName params) tagType)

    withTagType :: G.CName -> (forall t. TagType t -> Dependant a) -> Dependant a
    withTagType cname f = transType (typeName cname) $ \ty ->
        case ty of
            BasicType Binary  -> f BinaryTag
            BasicType Bool    -> f BoolTag
            BasicType Integer -> f IntTag
            BasicType Text    -> f TextTag
            UserType (Enumeration n t e) -> f $ EnumTag n t e
            _ -> Error $ InvalidTagType (typeName cname)

identCName :: G.CName -> String
identCName (G.CName (_, str)) = str

identLName :: G.LName -> String
identLName (G.LName (_, str)) = str

typeName :: G.CName -> TypeName
typeName (G.CName (_, name)) = TypeName name

fieldName :: G.LName -> FieldName
fieldName (G.LName (_, name)) = FieldName name

paramName :: G.LName -> ParamName
paramName (G.LName (_, name)) = ParamName name

-- Transforms a type or codec name.
transTypeOrCodec :: forall a. TypeName -> (forall t. Type t -> Dependant a) -> (Codec -> Dependant a) -> Dependant a
transTypeOrCodec n tt tc =
    case n of
        -- Basic types.
        "Binary"    -> bt Binary
        "Bool"      -> bt Bool
        "Integer"   -> bt Integer
        "Text"      -> bt Text
        "Optional"  -> bt Optional
        "List"      -> bt List

        -- Basic codecs.
        "FixedLengthBinary"     -> bc FixedLengthBinary
        "LengthPrefixBinary"    -> bc LengthPrefixBinary
        "BoolBits"              -> bc BoolBits
        "BigEndian"             -> bc BigEndian
        "LittleEndian"          -> bc LittleEndian
        "LengthPrefixInteger"   -> bc LengthPrefixInteger
        "TextInteger"           -> bc TextInteger
        "FixedCountText"        -> bc FixedCountText
        "CountPrefixText"       -> bc CountPrefixText
        "TerminatedText"        -> bc TerminatedText
        "FixedCountList"        -> bc FixedCountList
        "CountPrefixList"       -> bc CountPrefixList
        "TerminatedList"        -> bc TerminatedList
        "TerminatedUnionList"   -> bc TerminatedUnionList
        "OptionalCodec"         -> bc OptionalCodec

        -- The record codec.
        "RecordCodec"           -> tc RecordCodec

        -- Union codecs.
        "ContextTagUnion"       -> uc ContextTagUnion
        "TagPrefixUnion"        -> uc TagPrefixUnion
        "EmbeddedTagUnion"      -> uc EmbeddedTagUnion

        -- Double codec.
        "DoubleCodec"           -> tc DoubleCodec

        -- User-defined names.
        _ -> Partial n $ \decl ->
            case decl of
                MessageDecl (Message _ r)         -> tt $ UserType $ Record n [] r
                TypeDecl (DefinedType ut)         -> tt $ UserType ut
                TypeDecl (DeclaredExtType m)      -> tt $ ExtType m
                CodecDecl (DefinedCodec uc)       -> tc $ UserCodec uc
                CodecDecl (DeclaredExtCodec ty m) -> tc $ ExtCodec (Types.typeName ty) m
 where
    bt :: BasicType t -> Dependant a
    bt = tt . BasicType
    bc :: BasicCodec -> Dependant a
    bc = tc . BasicCodec
    uc :: UnionTagCodec -> Dependant a
    uc = tc . UnionCodec


-- Transforms a type name.
transType :: TypeName -> (forall t. Type t -> Dependant a) -> Dependant a
transType n t = transTypeOrCodec n t (const $ Error $ CodecUsedAsType n)


-- Transforms a codec name.
transCodec :: TypeName -> (Codec -> Dependant a) -> Dependant a
transCodec n t = transTypeOrCodec n (const $ Error $ TypeUsedAsCodec n) t

transFields :: [G.Field] -> (forall a. Record a -> Dependant b) -> Dependant b
transFields [] t = t Empty
transFields ((G.Field lname fieldinfo):fs) t =
    transFields fs $ \rest ->
        transFieldInfo fieldinfo $ \tyco ->
            t $ Field (fieldName lname) tyco rest

transOptions :: [G.TaggedOption] -> TagType t -> (forall a. Union t a -> Dependant b) -> Dependant b
transOptions [] _ t = t None
transOptions ((G.TaggedOption exp option):os) tagTy t = do
    let expEnv | EnumTag name _ enum <- tagTy = addEnumEnv name enum emptyEnv
               | otherwise = emptyEnv
    let tagExp = transExp exp
    tagVal <- (either (Error . ExpressionError) return $ evaluate expEnv tagExp) >>= transTag tagTy
    transOptions os tagTy $ \rest ->
        transFieldInfo fieldinfo $ \tyco ->
            t $ Option tagVal fname tyco rest
 where
    (fname, fieldinfo) =
        case option of
            G.NamedOption (G.Field lname info) -> (Just $ fieldName lname, info)
            G.UnnamedOption info -> (Nothing, info)

transEnumeration :: [G.EnumMember] -> TagType a -> Dependant (Enumeration a)
transEnumeration members tagTy = liftM EnumValues $ sequence $ map transMember members
 where
    transMember (G.EnumMember lname exp) = do
        let memberExp = transExp exp
        val <- either (Error . ExpressionError) (return . id) $ evaluate emptyEnv memberExp
        x <- transTag tagTy val
        return (fieldName lname, x)

transFieldInfo :: forall b. G.FieldInfo -> (forall t. FieldTypeCodec t -> Dependant b) -> Dependant b
transFieldInfo info t =
    case info of
        G.FieldTypeCodec typeexp codecexp -> transInfo typeexp $ Just codecexp
        G.FieldType typeexp -> transInfo typeexp Nothing
 where
    transInfo typeexp mcodecexp = do
        ((argEater, coArgs), tyArgs) <-
            transTypeExp typeexp $ \ty ->
                case mcodecexp of
                    Just codecexp -> transCodecExp codecexp (handler ty)
                    Nothing | Just co <- implicitCodec ty -> liftM (, Arguments []) $ handler ty co
                            | otherwise ->
                                    RequiresDefaultCodec (Types.typeName ty) $ \(DefaultCodec d) -> do
                                        (co, args) <- d
                                        x <- handler ty co
                                        return (x, args)
        argEater tyArgs coArgs

    handler :: Type t -> Codec -> Dependant (Arguments -> Arguments -> Dependant b)
    handler ty co = return $ \tyArgs coArgs -> t $ FieldTypeCodec ty tyArgs co coArgs


transTag :: TagType a -> ExpValue -> Dependant a
transTag tt val | Just x <- readTag tt val = return x
                | otherwise = Error $ InvalidEnumRepType $ Types.typeName $ fromTagType tt


transTypeExp :: G.TypeExp -> (forall a. Type a -> Dependant b) -> Dependant (b, Arguments)
transTypeExp exp t = do
    (name, args@(Arguments argl)) <- transTypeOrCodecExp exp
    x <- transType name t
    return (x, args)


transCodecExp :: G.TypeExp -> (Codec -> Dependant b) -> Dependant (b, Arguments)
transCodecExp exp t = do
    (name, args@(Arguments argl)) <- transTypeOrCodecExp exp
    x <- transCodec name t
    return (x, args)

transTypeOrCodecExp :: G.TypeExp -> Dependant (TypeName, Arguments)
transTypeOrCodecExp (G.TypeByName cname) = return (typeName cname, Arguments [])
transTypeOrCodecExp (G.TypeWithParams cname parameterset) =
    transParameterSet parameterset >>= return . (typeName cname,)


transParameterSet :: G.ParameterSet -> Dependant (Arguments)
transParameterSet (G.ParamList namedparams) = liftM Arguments $ sequence $ map transParam namedparams
 where
    transParam (G.NamedParam name exp) = transArg exp >>= return . (paramName name,)

transArg :: G.Argument -> Dependant Argument
transArg (G.ExpArg exp) = return $ ExpArg $ transExp exp
transArg (G.TypeArg typeexp) = do
    (tn, args) <- transTypeOrCodecExp typeexp
    transTypeOrCodec tn (return . flip TypeExpArg args) (return . flip CodecExpArg args)

transExp :: G.Exp -> Expression
transExp = t
 where
    notExp e = Operator e BXor (Constant $ EBool True)
    opExp op a b = Operator (t a) op (t b)

    t exp =
        case exp of
            G.ECond exp1 exp2 exp3 -> Conditional (t exp2) (t exp1) (t exp3)
            G.EOr exp1 exp2   -> opExp BOr exp1 exp2
            G.EXor exp1 exp2  -> opExp BXor exp1 exp2
            G.EAnd exp1 exp2  -> opExp BAnd exp1 exp2
            G.EEq exp1 exp2   -> opExp BEq exp1 exp2

            G.ENeq exp1 exp2 -> notExp $ t $ G.EEq exp1 exp2
            G.ELt exp1 exp2  -> opExp BLess exp1 exp2
            G.EGt exp1 exp2  -> opExp BLess exp2 exp1
            G.ELte exp1 exp2  -> notExp $ opExp BLess exp2 exp1
            G.EGte exp1 exp2  -> notExp $ opExp BLess exp1 exp2
            G.ECons exp1 exp2 -> opExp BConcat exp1 exp2

            G.EShiftl exp1 exp2  -> error "TODO: shifts" -- TODO: define shift semantics
            G.EShiftr exp1 exp2  -> error "TODO: shifts"

            G.EPlus exp1 exp2  -> opExp BPlus exp1 exp2
            G.EMinus exp1 exp2  -> opExp BMinus exp1 exp2
            G.ETimes exp1 exp2  -> opExp BMul exp1 exp2
            G.EIntDiv exp1 exp2  -> opExp BDiv exp1 exp2
            G.EMod exp1 exp2  -> opExp BMod exp1 exp2
            G.EPow exp1 exp2  -> opExp BPow exp1 exp2
            G.ENeg exp  -> Operator (Constant (EInt 0)) BMinus (t exp)
            G.ENot exp  -> notExp $ t exp

            G.EField lname subfields  ->
                let (fn:fs) = reverse $ fieldName lname : map (\(G.SubFieldName l) -> fieldName l) subfields
                in FieldAccess (reverse fs) fn

            G.EList listedexps  -> Listed $ map (\(G.ListedExp e) -> t e) listedexps
            G.EVar lname -> Var $ fieldName lname
            G.ENull  -> Constant ENull

            G.EBool G.LTrue -> Constant $ EBool True
            G.EBool G.LFalse -> Constant $ EBool False
            G.EInt (G.LDecimal (G.TDecimal (_,dec))) | Just i <- parseDecLit dec -> Constant $ EInt i
            G.EInt (G.LHexInt (G.THexInt (_,hex)))   | Just i <- parseHexLit hex -> Constant $ EInt i
            G.EInt _ -> error "Can't parse integer literal. The compiler either has a bug or the AST is invalid."

            G.EText tl -> Constant $ EText $ textLiteral tl

            G.EBin (G.LHexBin (G.THexBin (_, 'X':'\'':s))) | last s == '\'',
                                                             hex <- filter (not . (`elem` [' ', '_'])) $ init s,
                                                             Just i <- parseHexBinaryLit hex
                                                             -> Constant $ EBinary i
            G.EBin (G.LBitBin (G.TBitBin (_, 'b':'\'':s))) | last s == '\'',
                                                             bin <- filter (not . (`elem` [' ', '_'])) $ init s,
                                                             all (`elem` ['0', '1']) bin
                                                             -> Constant $ EBinary $ B.fromList $ map (=='1') bin
            G.ERegex (G.RegexLit (_, '/':s)) | last s == '/',
                                               [(r,"")] <- reads $ "\"" ++ fixSlashes (init s) ++ "\""
                                               -> Constant $ ERegex r
            G.ERegex (G.RegexLit (_,r)) -> error $ "Can't parse regex literal: " ++ show r

parseDecLit, parseHexLit :: String -> Maybe Integer
parseDecLit dec | [(i, "")] <- reads $ filter (/= '_') dec = Just i
parseDecLit _ = Nothing
parseHexLit ('0':'x':l@(_:_)) =
    liftM (foldl1 (\n d -> d * 16 + n)) $ sequence $ map (fmap toInteger . hexDigit) $ filter (/= '_') l
 where hexDigit d | '0' <= d && d <= '9' = Just $ digitToInt d
                  | 'A' <= d && d <= 'F' = Just $ 10 + ord d - ord 'A'
                  | 'a' <= d && d <= 'f' = Just $ 10 + ord d - ord 'a'
                  | otherwise = Nothing
parseHexLit _ = Nothing

textLiteral :: G.TextLit -> String
textLiteral (G.LDQText (G.TDQText (_, str)))    | [(s,"")] <- reads str = s
textLiteral (G.LSQText (G.TSQText (_, '\'':s))) | last s == '\'' = init s
textLiteral _ = error "Can't parse string literal. The compiler either has a bug or the AST is invalid."

-- TODO: better and consistent regex parser.
fixSlashes :: String -> String
fixSlashes ('\\':'/':xs) = '/' : fixSlashes xs
fixSlashes ('\\':'\\':xs) = "\\\\\\\\" ++ fixSlashes xs
fixSlashes ('\\':c:xs) | c `elem` ( "|*+?{}()^.[]-" :: String) = "\\\\" ++ c : fixSlashes xs
fixSlashes (x:xs) = x : fixSlashes xs
fixSlashes [] = []

parseHexBinaryLit :: String -> Maybe BitString
parseHexBinaryLit str = liftM B.bitStringLazy $ unhex (BC8.pack str)
