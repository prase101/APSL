{-# LANGUAGE GADTs, PatternGuards #-}
-- | Contain functions to prety print a message instance.
module Language.APSL.AMSL.MessagePrettyPrinter (prettyPrint, prettyPrintMessage) where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Module

import Text.PrettyPrint

prettyPrintMessage :: MessageInstance -> Doc
prettyPrintMessage (MessageInstance tn r x) = prettyPrint (UserType $ Record tn [] r) x

prettyPrint :: Type a -> a -> Doc
prettyPrint ty x = text (unTypeName $ typeName ty) <+> braces (content ty)
 where
    content (BasicType t) =
        case t of
            Binary    -> showD x
            Bool      -> showD x
            Integer   -> showD x
            Text      -> showD x
            Optional  | ContainerValue _ Nothing    <- x -> text "null"
                      | ContainerValue t' (Just x') <- x -> prettyPrint t' x'
            List      | ContainerValue t' xs <- x -> hsep $ punctuate (char ',') $ map (prettyPrint t') xs

    content (UserType t) =
        case t of
            Record _ _ r         -> nest 5 $ ppFields r x
            Union _ _ _ u        -> ppOption u x
            Enumeration _ tt e | Just (FieldName f) <- enumField tt e x -> text f
            TypeAlias _ t' _   -> content t'
            _ -> text "???"

    content (ExtType _) = text "..."

    ppFields :: Record a -> a -> Doc
    ppFields Empty _ = empty
    ppFields (Field (FieldName name) ftc rest) (x,xs) =
        (text name <> char ':' <+> prettyPrint (fieldType ftc) x) $+$ ppFields rest xs

    ppOption :: Union t a -> a -> Doc
    ppOption None _ = text "???"
    ppOption (Option _ Nothing ftc _) (Left x) = prettyPrint (fieldType ftc) x
    ppOption (Option _ (Just (FieldName name)) ftc _) (Left x) = text name <> char ':' <+> prettyPrint (fieldType ftc) x
    ppOption (Option _ _ _ others) (Right x) = ppOption others x


showD :: Show a => a -> Doc
showD = text . show
