{-# LANGUAGE RankNTypes, GADTs #-}

-- | A AMSL specification module, that contains the messages, types and codecs defined within a parsed AMSL file.
module Language.APSL.AMSL.DataType.Module where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Codecs
import Language.APSL.AMSL.DataType.TypeCodec
import Data.Maybe
import Debug.Trace


type MessageLabel = String

-- | The most important field: records (along with their names) that are marked to be messages.
data Message where
    Message :: forall a. TypeName -> Record a -> Message

messageName :: Message -> TypeName
messageName (Message n _) = n

-- | An instance of a particular message, of which all of its fields have been given a particular value.
data MessageInstance where
    MessageInstance :: forall a. TypeName -> Record a -> a -> MessageInstance

-- | User-defined (non-message) records, unions, aliases, and enums; along with extensions.
data DefinedType where
    DefinedType :: forall a. UserType a -> DefinedType
    DeclaredExtType :: ExtTypeMeta -> DefinedType

-- | Codec aliases and extensions.
data DefinedCodec where
    DefinedCodec :: UserCodec -> DefinedCodec
    DeclaredExtCodec :: forall a. Type a -> ExtCodecMeta -> DefinedCodec

-- | Represents the relevant definitions from a module.
data Module = Module {
    moduleName  :: String,
    messages    :: [Message],
    types       :: [DefinedType],
    codecs      :: [DefinedCodec]
}

-- | Expression environment providing access to constants (currently only enumeration fields) defined within a module.
moduleEnv :: Module -> Env
moduleEnv m = foldr (.) id (map updater (types m)) $ emptyEnv
 where
    updater (DefinedType (Enumeration tn _ e)) = addEnumEnv tn e
    updater _ = id

instance Show Module where
    show (Module name msgs tys cos) =
        concat [
            "\nModule: " ++ name,
            "\nMessages: " ++ show (map (\(Message n _) -> n) msgs),
            "\nTypes: " ++ show (map (\(DefinedType t) -> typeName $ UserType t) tys),
            "\nCodecs: " ++ show (map (\(DefinedCodec (CodecAlias n _ _)) -> n) cos)
        ]

