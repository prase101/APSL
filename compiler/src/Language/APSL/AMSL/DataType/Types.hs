{-# LANGUAGE GADTs, KindSignatures, RankNTypes, PatternGuards #-}

module Language.APSL.AMSL.DataType.Types where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Codecs
import Data.BitString.BigEndian (BitString)

import Data.List
import qualified Data.Set as S
import qualified Data.Map as M

import Debug.Trace

type Codec = Codec' Type
type UserCodec = UserCodec' Type
type Arguments = Arguments' Type Codec
type Argument = Argument' Type Codec

data Type a where
    BasicType :: BasicType a -> Type a
    UserType  :: UserType  a -> Type a
    ExtType   :: ExtTypeMeta -> Type ExtData

data BasicType :: * -> * where
    Binary     :: BasicType BitString
    Bool       :: BasicType Bool
    Integer    :: BasicType Integer
    Text       :: BasicType String
    Optional   :: BasicType (ContainerValue Maybe)
    List       :: BasicType (ContainerValue [])

data ContainerValue :: (* -> *) -> * where
    ContainerValue :: forall f a. Type a -> f a -> ContainerValue f

data UserType a where
    Record      ::               TypeName -> [ParamName] -> Record a                     -> UserType a
    Union       :: forall a tag. TypeName -> [ParamName] -> TagType tag   -> Union tag a -> UserType a
    Enumeration ::               TypeName -> TagType a   -> Enumeration a                -> UserType a
    TypeAlias   ::               TypeName -> Type a      -> Arguments                    -> UserType a


data TagType :: * -> * where
    BinaryTag :: TagType BitString
    BoolTag   :: TagType Bool
    IntTag    :: TagType Integer
    TextTag   :: TagType String
    EnumTag   :: forall a. TypeName -> TagType a -> Enumeration a -> TagType a

data ExtTypeMeta = ExtTypeMeta {
    extTypeName   :: TypeName,
    extParameters :: [ParamName]
}


data Record :: * -> * where
    Empty :: Record ()
    Field :: forall t c r. FieldName -> FieldTypeCodec t -> Record r -> Record (t, r)

data Union tag :: * -> * where
    None   :: Union tag Void
    Option :: forall tag t c r. tag -> Maybe FieldName -> FieldTypeCodec t
                                    -> Union tag r     -> Union tag (Either t r)

data Enumeration a where
    EnumValues :: [(FieldName, a)] -> Enumeration a


data FieldTypeCodec t = FieldTypeCodec {
    fieldType :: Type t,
    fieldTypeArgs :: Arguments,
    codecType :: Codec,
    codecTypeArgs :: Arguments
}

typeName :: Type a -> TypeName
typeName (BasicType b) =
    TypeName $ case b of
        Binary     -> "Binary"
        Bool       -> "Bool"
        Integer    -> "Integer"
        Text       -> "Text"
        Optional   -> "Optional"
        List       -> "List"
typeName (UserType u) =
    case u of
        Record      name _ _   -> name
        Union       name _ _ _ -> name
        Enumeration name _ _   -> name
        TypeAlias   name _ _   -> name
typeName (ExtType e) = extTypeName e

fromTagType :: TagType a -> Type a
fromTagType t = case t of
    BinaryTag     -> BasicType Binary
    BoolTag       -> BasicType Bool
    IntTag        -> BasicType Integer
    TextTag       -> BasicType Text
    EnumTag n t e -> UserType $ Enumeration n t e

toTagType :: Type a -> Maybe (TagType a)
toTagType t = case t of
    BasicType Binary             -> Just BinaryTag
    BasicType Bool               -> Just BoolTag
    BasicType Integer            -> Just IntTag
    BasicType Text               -> Just TextTag
    UserType (Enumeration n t e) -> Just $ EnumTag n t e
    UserType (TypeAlias _ t _)   -> toTagType t
    _                            -> Nothing

-- | Extend an expression environment with the values of an enumeration.
addEnumEnv :: TypeName -> Enumeration a -> Env -> Env
addEnumEnv ty (EnumValues fields) env
  = foldr (\(v,_) -> M.insert ([], unFieldName v) (Success $ EEnum ty v)) env fields

addExpValue2Env :: FieldName -> ExpValue -> Env -> Env
addExpValue2Env v e env
  = M.insert ([], unFieldName v) (Success e) env

-- | Try transforming a value of a certain type into an expression value. Nothing if that is not
--   possible for this type, because it is not a basic or enum type (or a container around an invalid type).
toExpValue :: Type a -> a -> Maybe ExpValue
toExpValue (BasicType ty) x =
    case ty of
        Binary    -> Just $ EBinary x
        Bool      -> Just $ EBool x
        Integer   -> Just $ EInt x
        Text      -> Just $ EText x
        Optional  | ContainerValue el x' <- x -> maybe (Just ENull) (toExpValue el) x'
        List      | ContainerValue el x' <- x -> fmap EList $ sequence $ map (toExpValue el) x'
        _         -> Nothing
toExpValue (UserType (Enumeration n ty (EnumValues vals))) x = do
    (field, _) <- find ((compareTags ty x) . snd) vals
    return $ EEnum n field
toExpValue (UserType (TypeAlias _ ty _)) x = toExpValue ty x
toExpValue _ _ = Nothing

-- | Read an expression value associated with a tag.
readTag :: TagType a -> ExpValue -> Maybe a
readTag ty val = case (ty, val) of
    (BinaryTag, EBinary x) -> Just x
    (BoolTag,   EBool x)   -> Just x
    (IntTag,    EInt x)    -> Just x
    (TextTag,   EText x)   -> Just x
    (EnumTag n t (EnumValues vals), EEnum n' field) | n == n' -> lookup field vals
    _ -> Nothing

compareTags :: TagType a -> a -> a -> Bool
compareTags ty a b =
    case ty of
        BinaryTag     -> a == b
        BoolTag       -> a == b
        IntTag        -> a == b
        TextTag       -> a == b
        EnumTag _ t _ -> compareTags t a b


unionTag :: Union t u -> u -> t
unionTag (Option t _ _ _) (Left _) = t
unionTag (Option _ _ _ os) (Right u) = unionTag os u

enumField :: TagType a -> Enumeration a -> a -> Maybe FieldName
enumField tt (EnumValues vals) x = fmap fst $ find (compareTags tt x . snd) vals

