{-# LANGUAGE GADTs, KindSignatures, RankNTypes, OverloadedStrings #-}

module Language.APSL.AMSL.DataType.Codecs where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression

import Data.Binary

-- | A codec that provides encoders and decoders for values of a particular type.
data Codec' ty
    = BasicCodec BasicCodec
    | RecordCodec
    | UnionCodec UnionTagCodec
    | DoubleCodec
    | UserCodec (UserCodec' ty)
    | ExtCodec TypeName ExtCodecMeta

data BasicCodec
    = FixedLengthBinary
    | LengthPrefixBinary
    | TerminatedBinary
    | BoolBits
    | BigEndian
    | LittleEndian
    | LengthPrefixInteger
    | TextInteger
    | FixedCountList
    | CountPrefixList
    | TerminatedList
    | TerminatedUnionList
    | OptionalCodec
    | FixedCountText
    | CountPrefixText
    | TerminatedText
    deriving (Eq, Show)

data UnionTagCodec 
    = ContextTagUnion
    | TagPrefixUnion
    | EmbeddedTagUnion

data UserCodec' ty = CodecAlias CodecName (Codec' ty) (Arguments' ty (Codec' ty))

data ExtCodecMeta = ExtCodecMeta {
    extCodecName  :: TypeName,
    extParameters :: [ParamName]
}

codecName :: Codec' ty -> TypeName
codecName (BasicCodec c) = TypeName $ show c
codecName RecordCodec = "RecordCodec"
codecName (UnionCodec t) =
    case t of
        ContextTagUnion  -> "ContextTagUnion"
        TagPrefixUnion   -> "TagPrefixUnion"
        EmbeddedTagUnion -> "EmbeddedTagUnion"
codecName (UserCodec (CodecAlias n _ _)) = n
codecName (ExtCodec _ m) = extCodecName m
codecName DoubleCodec = "DoubleCodec"

