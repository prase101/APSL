{-# LANGUAGE RankNTypes, KindSignatures, GADTs, PatternGuards #-}

-- | Utilities for managaging the relationships between types and codecs.
module Language.APSL.AMSL.DataType.TypeCodec where

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Codecs

import Control.Monad
import qualified Data.Map as M

import Debug.Trace

-- | When the codec can be applied to the given type.
matchTypeCodec :: Type a -> Codec -> Bool
matchTypeCodec ty co =
    case (ty, co) of
        (_, UserCodec (CodecAlias n c args))      -> matchTypeCodec ty c
        (BasicType t, BasicCodec c)               -> matchBasic t c
        (UserType (Record _ _ r), RecordCodec)    -> True
        (UserType (Union  _ _ _ r), UnionCodec _) -> True
        (UserType (Enumeration _ t _), _)         -> matchTypeCodec (fromTagType t) co
        (UserType (TypeAlias _ t _), _)           -> matchTypeCodec t co
        (_, ExtCodec tn m)                        -> typeName ty == tn
        _                                         -> False
 where
    matchBasic :: BasicType a -> BasicCodec -> Bool
    matchBasic t c =
        case (c,t) of
            (FixedLengthBinary,   Binary    ) -> True
            (LengthPrefixBinary,  Binary    ) -> True
            (TerminatedBinary,    Binary    ) -> True
            (BoolBits,            Bool      ) -> True
            (BigEndian,           Integer   ) -> True
            (LittleEndian,        Integer   ) -> True
            (LengthPrefixInteger, Integer   ) -> True
            (TextInteger,         Integer   ) -> True
            (FixedCountText,      Text      ) -> True
            (CountPrefixText,     Text      ) -> True
            (TerminatedText,      Text      ) -> True
            (FixedCountList,      List      ) -> True
            (CountPrefixList,     List      ) -> True
            (OptionalCodec,       Optional  ) -> True
            _                                 -> False


data SameType :: * -> * -> * where
    SameType :: forall a. SameType a a

-- | Tests equality of AMSL types; resolves aliases first.
sameType :: Type a -> Type b -> Maybe (SameType a b)
sameType a b =
    case (a,b) of
        (UserType (TypeAlias _ a _), _) -> sameType a b
        (_, UserType (TypeAlias _ b _)) -> sameType a b
        (BasicType a, BasicType b)      -> sameBasicType a b
        (UserType a, UserType b)        -> sameUserType a b
        (ExtType ma, ExtType mb)        |  extTypeName ma == extTypeName mb -> Just SameType
        (_, _)                          -> Nothing
 where
    sameBasicType :: BasicType a -> BasicType b -> Maybe (SameType a b)
    sameBasicType a b =
        case (a,b) of
            (Binary, Binary)        -> Just SameType
            (Bool, Bool)            -> Just SameType
            (Integer, Integer)      -> Just SameType
            (Text, Text)            -> Just SameType
            (Optional, Optional)    -> Just SameType
            (List, List)            -> Just SameType
            (_, _)                  -> Nothing
    sameUserType a b =
        case (a,b) of
            (Record na _ ra, Record nb _ rb)           | na == nb -> Just $ assertRecord ra rb
            (Union na _ ta ua,  Union nb _ tb ub)      | na == nb,
                                                         Just SameType <- sameType (fromTagType ta) (fromTagType tb)
                                                          -> Just $ assertUnion ua ub
            (Enumeration na ta _, Enumeration nb tb _) | na == nb,
                                                         Just SameType <- sameType (fromTagType ta) (fromTagType tb)
                                                         -> Just SameType
            (_, _) -> Nothing

    assertRecord :: Record a -> Record b -> SameType a b
    assertRecord Empty Empty = SameType
    assertRecord (Field na ta ra) (Field nb tb rb) | na == nb,
                                                     Just SameType <- sameType (fieldType ta) (fieldType tb),
                                                     SameType <- assertRecord ra rb
                                                     = SameType
    assertRecord _ _ = error "Different records sharing name."

    assertUnion :: Union t a -> Union t b -> SameType a b
    assertUnion None None = SameType
    assertUnion (Option _ na ta ra) (Option _ nb tb rb) | na == nb,
                                                          Just SameType <- sameType (fieldType ta) (fieldType tb),
                                                          SameType <- assertUnion ra rb
                                                          = SameType
    assertUnion _ _ = error "Different unions sharing name."


-- | Codec that should be used for a field for which no explicit or default codec is provided. If Nothing, no implicit
--   codec is available and the user needs to provide one.
implicitCodec :: Type a -> Maybe Codec
implicitCodec t =
    case t of
        BasicType Binary              -> Just $ BasicCodec FixedLengthBinary
        UserType (Record _ _ r)       -> Just $ RecordCodec
        UserType (Union _ _ _ u)      -> Just $ UnionCodec ContextTagUnion
        UserType (Enumeration _ et _) -> implicitCodec $ fromTagType et
        UserType (TypeAlias _ at _)   -> implicitCodec at
        _                             -> Nothing


-- | Construct the value of a record, within some monad, by producing its fields one by one. Updates the expression
--   environment with previously produced field values.
buildRecord :: Monad m => Env -> (forall t. Env -> (Type t, Arguments) -> (Codec, Arguments) -> m t) -> Record a -> m a
buildRecord _ _ Empty = return ()
buildRecord env f (Field fn (FieldTypeCodec ft fta fc fca) rest) = do
    let scope = []
    x  <- f env (ft, fta) (fc, fca)
    xs <- buildRecord (updateEnv scope (unFieldName fn) ft x env) f rest
    return (x,xs)

-- | buildRecord, given a set of type arguments for the record in question.
buildRecordArgs :: Monad m => Arguments -> Env -> (forall t. Env -> (Type t, Arguments) -> (Codec, Arguments) -> m t) -> Record a -> m a
buildRecordArgs args env f r = do
    Right env2 <- return $ argsToEnv args env
    buildRecord env2 f r

-- | Process the fields of a record one-by-one in some monad, while the expression environment is updated accordingly.
foldRecord :: Monad m => Env -> (forall t. Env -> (Type t, Arguments) -> (Codec, Arguments) -> t -> m ())
                             -> Record a -> a -> m ()
foldRecord _ _ Empty () = return ()
foldRecord env f (Field fn (FieldTypeCodec ft fta fc fca) rest) (x, xs) = do
    let scope = []
    f env (ft, fta) (fc, fca) x
    foldRecord (updateEnv [] (unFieldName fn) ft x env) f rest xs

-- | Add a field's value with a certain type to an expression environment.
updateEnv :: ScopeTy -> Ident -> Type b -> b -> Env -> Env
updateEnv scope var ty x env
  = let env'   = M.insert (scope, var) (maybe NoExpressionType Success $ toExpValue ty x) env
    in case ty of
         UserType (Record fn _ r) -> subFieldEnv (unTypeName fn:scope) r x env'
         _ -> env'
 where
    subFieldEnv :: [Ident] -> Record r -> r -> Env -> Env
    subFieldEnv _ Empty _ env = env
    subFieldEnv scope (Field fn (FieldTypeCodec ft _ _ _) r) (x,xs) env = updateEnv scope (unFieldName fn) ft x $ subFieldEnv scope r xs env

-- | Updates the environment with the given expressions, which will be evaluated.
evaluateToEnv :: ScopeTy -> [(FieldName, Expression)] -> Env -> Either InvalidExpression Env
evaluateToEnv scope es env = do
    vals <- sequence $ map (\(fn, ex) -> evaluate env ex >>= \v -> return (fn,v)) es
    return $ foldl (\env' (fn,v) -> M.insert (scope, unFieldName fn) (Success v) env') env vals

-- | Calls evaluateToEnv, given only ExpArgs.
argsToEnv :: Arguments -> Env -> Either InvalidExpression Env
argsToEnv (Arguments args) env = do
    exps <- sequence $ map argExp args
    evaluateToEnv [] exps env
 where
    argExp (ParamName n, ExpArg ex) = Right (FieldName n, ex)
    argExp _ = Left NotAValueExpression
