{-# LANGUAGE GADTs #-}
module Language.APSL.AMSL.DataType.MessageInspector where

import Language.APSL.AMSL.DataType.Module

import Language.APSL.Base
import Language.APSL.AMSL.DataType.Expression
import Language.APSL.AMSL.DataType.Types
import Language.APSL.AMSL.DataType.Codecs
import Language.APSL.AMSL.DataType.TypeCodec
import Data.Maybe
import Debug.Trace
import Text.PrettyPrint
import Language.APSL.AMSL.MessagePrettyPrinter

import Control.Monad.Writer

type EnvBuilder a = Writer Env a

pushScope :: Ident -> EnvBuilder a -> EnvBuilder a
pushScope scope envB
  = mapWriter (\(a, e) -> (a, parentScopeEnv scope e)) envB

addVar :: Ident -> ExpValue -> EnvBuilder ()
addVar var val
  = tell (singletonEnv ([],var) val)

msgEnv :: MessageInstance -> Env
msgEnv (MessageInstance tn r x)
  = execWriter $ instanceEnv "msg" (UserType $ Record tn [] r) x

instanceEnv :: Ident -> Type a -> a -> EnvBuilder ()
instanceEnv var tailElem x
  | isJust val
    = addVar var (fromJust val)
  where val = toExpValue tailElem x
instanceEnv var usrTy@(UserType t) x
  = case t of
      Record fn _ r -> recordInstanceEnv var r x
      Union _ _ _ u -> unionInstanceEnv var u x
instanceEnv _ _ _
  = return ()


recordInstanceEnv :: Ident -> Record a -> a -> EnvBuilder ()
recordInstanceEnv _ Empty _ = return ()
recordInstanceEnv var (Field (FieldName name) ftc rest) (x,xs)
  = do
  pushScope var $ instanceEnv name (fieldType ftc) x
  recordInstanceEnv var rest xs

unionInstanceEnv :: Ident -> Union t a -> a -> EnvBuilder ()
unionInstanceEnv _ None _
  = return ()
unionInstanceEnv var (Option _ Nothing ftc _) (Left x)
  = instanceEnv var (fieldType ftc) x
unionInstanceEnv var (Option _ (Just (FieldName name)) ftc _) (Left x)
  = pushScope var $ instanceEnv name (fieldType ftc) x
unionInstanceEnv var (Option _ _ _ others) (Right x)
  = unionInstanceEnv var others x
