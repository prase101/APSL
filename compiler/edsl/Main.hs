module Main where

import Language.APSL.PNSL.DataType.Types
import Language.APSL.AsyncTestEngine.Test
import Language.APSL.AsyncTestEngine.Traversal
import Language.APSL.PNSL.Visualize
import Language.APSL.PNSL.API
import Language.APSL.PNSL.EDSL
import qualified Data.Set as S

main :: IO ()
main
  = if False
      then printTest
      else printWS

pn = compilePN websocketPN
  where theta = "theta"

printTest :: IO ()
printTest
  = do
  --let pn = buildPN testPN "theta"
  let theta = "theta" -- "τ"
  let pn = buildPN thesisPN theta
  i <- visualizationNum
  visualizePN pn
  visualizePN (optimizePN theta pn)
  openVisualizations i (i+2)

printWS
  = do
  let pn = compilePN websocketPN
      pnConf = pnslConf
             $ initialTraversalState pn undefined
  i <- visualizationNum
  visualizePN (buildPN wsPNBuilder "theta")
  visualizePN pn
  openVisualizations i (i+2)
  putStrLn "***********"
  print pnConf

thesisPN
  = do
  markInit
  transition "Login?"
  while "No!" $ do
    transition "Login?"
  transition "Logged in!"
  forks [control, pulseForth, pulseBack]
  transition "Logout?"
  transition "Logged out!"
  markExit

control
  = loop $ do
      transition "Request stuff?"
      transition "Received stuff!"

pulseForth
  = loop $ do
      transition "Server still alive?"
      transition "Server still alive!"

pulseBack
  = loop $ do
      transition "Client still alive!"
      transition "Client still alive?"


testPN
  = do
  loop $ parallel [p1,
                   optional $ transition "Optional!" ] "theta"

p1 = do
  loop $ do
    transition "First!"
    choice (transition "MaybeSecondA!")
           (transition "MaybeSecondB!" )
    choices [transition "CsA!"
            ,transition "CsB!"
            ,transition "CsC!"
            ,transition "CsD!"]
  transition "LeaveLoop!"
