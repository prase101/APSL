module Main where

import Language.APSL.TestEngine.Traversal
import System.IO

-- for now a dummy main that just call the websocket tester
-- myTestWebsocket aggresiveness timeout maximumCycleCount verbosity
main =
  hSetBuffering stdout NoBuffering >>
  hSetBuffering stderr NoBuffering >>
  myTestWebsocket 1 500000 2000 9

