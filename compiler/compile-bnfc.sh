#!/bin/bash

cd src

if [ -f Language/APSL/AMSL/Grammar/.Lex.x ]; then
  mv -v Language/APSL/AMSL/Grammar/.Lex.x Language/APSL/AMSL/Grammar/Lex.x
fi
if [ -f Language/APSL/AMSL/Grammar/.Par.y ]; then
  mv -v Language/APSL/AMSL/Grammar/.Par.y Language/APSL/AMSL/Grammar/Par.y
fi
if [ -f Language/APSL/AISL/Grammar/.Lex.x ]; then
  mv -v Language/APSL/AISL/Grammar/.Lex.x Language/APSL/AISL/Grammar/Lex.x
fi
if [ -f Language/APSL/AISL/Grammar/.Par.y ]; then
  mv -v Language/APSL/AISL/Grammar/.Par.y Language/APSL/AISL/Grammar/Par.y
fi



bnfc -d -p Language.APSL.AMSL Language/APSL/AMSL/Grammar.cf
alex Language/APSL/AMSL/Grammar/Lex.x
happy Language/APSL/AMSL/Grammar/Par.y

bnfc -d -p Language.APSL.AISL Language/APSL/AISL/Grammar.cf
alex Language/APSL/AISL/Grammar/Lex.x
happy Language/APSL/AISL/Grammar/Par.y

mv -v Language/APSL/AMSL/Grammar/Lex.x Language/APSL/AMSL/Grammar/.Lex.x
mv -v Language/APSL/AMSL/Grammar/Par.y Language/APSL/AMSL/Grammar/.Par.y

mv -v Language/APSL/AISL/Grammar/Lex.x Language/APSL/AISL/Grammar/.Lex.x
mv -v Language/APSL/AISL/Grammar/Par.y Language/APSL/AISL/Grammar/.Par.y


