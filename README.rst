.. |copy|   unicode:: U+000A9 .. COPYRIGHT SIGN

APSL: A Protocol Specification Language
=======================================

APSL is a language to formally describe a protocol to facilitate testing. A protocol 
is defined as a set of actors that interact by exchanging messages. APSL describes
each actor in terms of, essentially, a Labelled Transition System (LTS).
APSL has a very expressive message description language, to describe the format
of each message type used by the protocol. Both text-based and binary-based message
formats can be described in APSL.

Once a protocol is described in APSL, its test engine can be used to automatically
test an implementation of an actor in a protocol. APSL will use the actor LTS description
to drive a traversal through the LTS. When a step in the traversal expects the Implementation
Under Test (IUT) to send a message, the test engine will wait for the message and consume it
when it arrives. When a traversal step expects a message to be sent to the IUT, the test
engine will generate a random message of the right type and send it to the IUT.
The direction of the traversal is decided by a traversal strategy. 
The default strategy is to simply randomly choose the next send-to-IUT message types,
whenever more than one choice are possible. 
More sophisticated traversal strategy by writing a custom strategy (we can use the code of the
random strategy as a template).

The engine reports two kinds of error: (1) if a message sent by the IUT is not in a correct
format, and (2) if the IUT sent a wrong message type.

The test engine is connected to the IUT through a "channel". A channel allows the engine to
send and receives messages to/from the IUT. A default TCP socket channel is provided
by the APSL library.

Authors: Tom Tervoort, Rick Klomp, Wishnu Prasetya

Copyright |copy| 2017, Utrecht University.

License: GPL.

Example
-------

A protocol is described by at two modules. One of these is an *interaction module*,
and another is a *message module*. An interaction module is an .aisl file, and
a message module in an .amsl file. An interaction module describes how
each party/actor in the protocol exchanges messages with its environment. 

The example below describes a hypothetical protocol consisting of two actors:
Server and Client. They exchange three types of messages: Ask, Data, and Done.
The client sends Ask to ask the server for Data, and otherwise the client
sends Done when it has enough. ::

        interactions module myP1
        import * from 'myP1.amsl'

        actor Client with
           init state Starting where
              anytime do send Ask next Waiting 
                   or do quit 
           end
           state Waiting where
              on Data do send Ask continue
                   or do send Done next Starting 
                   or do send Done continue
              end
        end

        actor Server with
           init state Serving where
              on Ask  do send Data continue
              on Done do send Done continue
           end
        end

The format of a message can be quite complex. APSL is intended to be able to express
complex message formats. As an example, the message module below defines a quite
complex formats for the three types of the messages used in the above protocol.
It defines, for
example, Ask as a field of two values: a two bits integer followed by a "000000" bit string.
It defines Data as a field of four values, where the second one is the Data's actual
payload, consisting of up to four data-items. ::

        message module myP1

        codec Word32Codec is BigEndian(signed=false, length=32)

        record Header with
           flag     is Integer as BigEndian(signed=false,length=2) 
           reserved is Binary(value=b'000000')
        end

        record DataItem with
           n       is Integer(min=0,max=500) as BigEndian(signed=false,length=32) 
           data    is Binary(length=8 * n)
           padding is Binary(length=8 * (4 - n % 4), char8_pattern=/\0*\1/)
        end

        message Ask with h  is Header(flag=1) end 
        message Done with h is Header(flag=3) end 

        message Data with 
           h       is Header(flag=0)
           payload is List(elem=DataItem, max_length=4) as CountPrefixList(count_codec=Word32Codec)
           hasfoot is Bool  as BoolBits(falsehood_string=X'00', truth_string=X'ff')
           foot    is Optional(is_empty=!has_footer,subject=Text(value='Bye!')) 
                   as OptionalCodec(subject_codec=FixedCountText(encoding='ascii'))
        end

For further explanation on this example: see the paper below.

Further explanation on APSL syntax: see documentation.


Documentation
-------------

The `doc <doc/>`_ directory contains several documentation files:


- `aisl-grammar.tex <doc/amsl-grammar.tex>`_  and `amsl-grammar.tex <doc/amsl-grammar.tex>`_  are automatically generated LaTeX documents from APSL language definition. The first shows the syntax interaction modules. The second shows the syntax of message modules.

- `amsl-language-definition.rst <doc/amsl-language-definition.rst>`_ provides further information on the structure of a message module. When describing a message type, it will typically be modeled as a record with fields. Each field must be types, e.g. an integer or a bit string. Furthermore, we also need to specify the *codec* for the field, which specifies how exactly its value is to be formatted into bit strings. The file `basic-types-codecs.rst <doc/basic-types-codecs.rst>`_ provides a reference of the built-in *types* and *codecs* in APSL.
  
- `APSL paper <doc/APSL_paper.pdf>`_ to gives an overview of APSL.


Using APSL
----------

I have not made a command-line deployment. For now, use ghci. The current top level functions to call can be found in the module `Language.APSL.TestEngine.Traversal <compiler/src/Language/APSL/TestEngine/Traversal.hs>`_. The function to call to do automated testing is ``randomTraversal``. It uses a random traversal strategy defined by the function ``randomStrategy``. Use the later as a template to define a custom traversal strategy. The module also contains another example strategy, that tries to steer the traversal towards a given state. Example session:

1. cd to ``compiler/src``

2. Invoke ghci (Glasgow Haskell Interpreter): ``ghci -i../../fixed-bitstring/`` 

3. In ghci, do this ``:l Language.APSL.TestEngine.Traversal``
    
4. In ghci, invoke `randomTraversal <compiler/src/Language/APSL/TestEngine/Traversal.hs>`_ , passing to it the parameters it needs.

The test engine currently prints various information to the console. It will also save the coverage information
to a file. To print the combined coverage inferred from multiple coverage files (collected from multiple runs),
invoke `printMergedTransitionCoverageInfo <compiler/src/Language/APSL/TestEngine/CoverageMergeUtil.hs>`_, passing
to it the path to the directory where the coverage files are located (the function assumes they are all located
in the same directory).

The produced traversal is also saved in a trace-file. The module `Language.APSL.TestEngine.Replay <compiler/src/Language/APSL/TestEngine/Replay.hs>`_ contains a function to replay a saved trace.



Other stuff
-----------

- The `examples <examples/>`_ directory contains a few example specifications written APSL.

Build instruction
---------

TO DO.

Source code organization
------------------------

- ``Language\APSL\AISL`` : parser related stuffs to parse interaction modules.
- ``Language\APSL\AMSL`` : parser related stuffs to parse message modules.
- ``Language\APSL\Channel`` : implementation of communication channel(s) between the test engine and Implementation Under Test (IUT). This includes APSL's decoder and encoder to decode bit strings into more abstract APSL representation of messages, and the other way around.
- ``Language\APSL\Generator`` : contains APSL message generator. The directory also contains an implementation for tracking message coverage.
- ``Langueg\APSL\TestEngine`` : contains APSL's test engine.


Notes for Developers
--------------------

To change the traversal behavior of the test engine, see how the function `randomTraversal <compiler/src/Language/APSL/TestEngine/Traversal.hs>`_ is defined. Use it as a template. See how it is configured with a traversal strategy.
You then want to define your own traversal strategy. See the definition of `randomStrategy <compiler/src/Language/APSL/TestEngine/Traversal.hs>`_ as an example.

To test out your new strategy without having to use a real protocol, see how `echoTraversal <compiler/src/Language/APSL/TestEngine/Traversal.hs>`_ is defined. You can either adjust it, by configuring it to use your own strategy, or create your own echoTraversal. When invoked, passing to it a path to an APSL interaction module, it will invoke the test engine, using your traversal strategy. However, it will not test any real IUT. Instead, you will act as the IUT. It will interactively inform you in which state the test engine thinks the IUT is. Whenever it wants the IUT to send a message, it will for your command, since you are
now the IUT, to send one. The test engine will inform you what are the message types that would be valid to send at that point.
After loading ``Language.APSL.TestEngine.Traversal`` in ghci, you can invoke echoTraversal for example like this:

   ``echoTraversal "../../examples/" "simple1.aisl" "Server1" 1 10``


OTHER NOTES
-----------

- APSL is implemented in Haskell.

- As a language, APSL is split in two parts: AISL for defining interaction modules, and AMSL for defining message modules. The syntax of both are defined in the BNFC language. The definitions are in two .cf files, located in ``src/Language/APSL/AMSL`` and ``src/Language/APSL/AISL`` directories.

- The ``compile-bnfc.sh`` script reads the BNFC syntax specifications of AISL and AMSL, and will generate raw parsers for them, located in the ``.../AMSL/Grammar`` and ``.../AISL/Grammar`` subdirs. 

  Among other things, a Haskell module `Test.hs` will also be generated. It contains a generated test-file to test the generated AMSL/AISL parsers. There are two Test.hs, one for AMSL and one for AISL. To test with ghci::

    1. cd to src
    2. start ghci: ``ghci path/Test.hs``
    3. in ghci: ``:main --help``
    4. parse an example in ghci: ``:main ../../examples/simple1.amsl``

- The Haskell module ``Language.APSL.AMSL.Compiler`` contains the top-level parser to parse message modules (as opposed to the raw parser generated from BNFC). Similarly, ``Language.APSL.AISL.Compiler`` contains the top-level parser to parse interaction modules.

- The project seems to required a fixed/repaired version the bitstring package. This is included in the project in the ``fixed-bitstring`` dir. When loading APSL modules from ghci, include this dir in the -i option.
