
Basic types and codecs
======================

This document describes the list of basic types and codecs available within AMSL, i.e. the types and codecs that are 
built-in and not defined by the user. Refer to `the language definition <doc/amsl-language-definition.rst>`_ for a 
description of what a (basic) type or codec is and how they are used.

Each basic type has multiple parameters, followed by one or more codecs that can be used with this type, which also 
have parameters. Parameters are optional unless noted otherwise.

Any parameter can be given the argument ``null``, which will result in the same behavior as when the parameter has not 
been assigned at all.

An *expression type* is given for each parameter, describing which expressions are allowed as an argument for it. 
Additionally, the pseudo-type *natural* is used, which describes all nonnegative integers.

Note that it is possible to provide arguments in such a manner that there is no longer any value covered by a type, for 
example ``Integer(min=1, max=0)`` or ``Binary(value=b101, length=4)``. These contradictory arguments are allowed and will 
simply result in an *absurd* type expression that covers an empty set of values. Passing *redundant* arguments (e.g. 
the type expression ``Binary(min_length=0, max_length=10, length=5)``, which is equivalent to ``Binary(length=5)``) is also
accepted. In both cases the compiler may generate a warning, though.


Binary
------

The *Binary* type contains arbitrary sequences of bits. Note that in order to enforce that the number of bits is 
divisible by eight (i.e. so it can be perceived as a *byte* sequence), the ``length_factor`` parameter can be used.
Also note that lengths are in bits, not bytes.



Parameters
~~~~~~~~~~

value (*bits*)
    The sequence must be exactly equal to the given bits.

length (*natural*)
    The fixed number of bits in the string.
    
min_length (*natural*)
    The minimum length in bits.
    
max_length (*natural*)
    The maximum length in bits.
    
length_factor (*natural*)
    The length of the bit sequence should be divisible by this value. Set to 8 in order to only accept byte sequences. 
    May not be zero.
    
bit_pattern (*regex*)
    When encoding the bit sequence as text string of ones and zeros, that string should (in its entirety) match this 
    regular expression. For example, in order to express a sequence of bytes of which the first bit is always zero,one 
    could use the pattern ``/(0[01]{7})*/``.
    
exclude_bit_pattern (*regex*)
    Not a single substring may match this bit pattern. Matches on the same encoding as bit_pattern, but with the 
    notable difference that this regex only tries to match part of the string and not the whole thing. May not use ^ or
    $ tokens to match the beginning or end of input
    
char8_pattern (*regex*)
    When setting this, the amount of bits must be a multiple of eight. When encoding this byte sequence using the 
    *char8* encoding, the encoded string should match this regular expression. The char8 encoding simply treats each 
    byte as a number between 0 and 255 and maps them to the corresponding Unicode code points; this means that char8 is
    a (small) superset of the ASCII and Latin-1. This encoding was originally defined within the `Haskell ByteString 
    library <http://hackage.haskell.org/package/bytestring-0.10.6.0/docs/Data-ByteString-Char8.html>`_. The 
    ``bit_pattern`` example can also be expressed as a ``char8_pattern``: ``/[\0-\x8f]*/``.

    Setting ``bit_pattern`` and ``char8_pattern`` at the same time is currently not supported.
    
exclude_char8_pattern (*regex*)
    Similar to ``exclude_bit_pattern``, but using the char8 representation for matching.

    

Binary codecs
-------------

When the length of the bit sequence is fixed (or can be derived from the context), the bits can be read directly. 
Otherwise, this length needs to be included in some way.

Unlike other basic types, Binary has an implicit default codec: namely ``FixedLengthBinary``.

FixedLengthBinary (default)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This bit sequence is represented as itself. This codec can only be used when the ``value`` and/or ``length`` argument is 
set to a non-``null`` value in the type expression, even when a fixed length could be derived from other arguments.

This codec has no parameter.


LengthPrefixBinary
~~~~~~~~~~~~~~~~~

The sequence is preceded with some bits indicating its length; this prefix encodes an integer called *length*. The 
length of the sequence should be equal to ``length * length_factor + length_offset``.

length_codec (*Integer codec*) [required]
    A codec expression for the *Integer* type, indicating how the length prefix is encoded.
    
length_multiplier (*natural*)
    Multiplier added to the length. Default is 1. It is typical for this to be 8, when the length denotes a number of 
    bytes.
    
length_offset (*integer*)
    Added to the length, multiplied by the factor, in order to obtain the final length of the Binary. Default is 0.
    

TerminatedBinary
~~~~~~~~~~~~~~~~

The end of the Binary is signified by a certain bitstring called the *terminator*; all bits of the input are decoded 
until this terminator is reached. The Binary *must* have been assigned non-``null`` arguments for 
``exclude_bit_pattern`` or ``exclude_char8_pattern``, one of which should match (part of) the terminator; this prevents 
the string from containing the terminator itself, which can not be encoded.

terminator (*bits*) [required]
    The terminator right past the end of the string. It (or a substring) should match the ``exclude_bit_pattern`` 
    and/or ``exclude_char8_pattern`` of the Binary.
    

TODO?: some run-length encoding scheme(s)?

Bool
----

A boolean can have one of two values: ``true`` or ``false``.

Parameters
~~~~~~~~~~

value (*boolean*)
    Fix the value to either ``true`` or ``false``.

Bool codecs
-----------

BoolBits
~~~~~~~~

Specify bitstrings for both ``truth_string`` and ``falsehood_string`` to indicate which exact strings respectively 
stand for true and false. When only setting ``truth_string``, any other bitstring of equal length is interpreted as 
false; the reverse holds for ``falsehood_string``.

Providing neither argument is not allowed; obviously, they should not be identical either.

Two common instances of this codec are ``BoolBits(truth_string=b1,falsehood_string=b0)`` (single boolean bit) and
``BoolBits(falsehood_string=X'00')`` (single byte, where anything nonzero is true).

truth_string (*bits*)
    The bits representing the value ``true``.

falsehood_string (*bits*)
    The bits representing the value ``false``.


Floating
--------

TODO


Integer
-------

Parameters
~~~~~~~~~~

value

min

max

factor

Integer codecs
--------------

BigEndian
~~~~~~~~~

length [required]

signed [required]

offset

multiplier


LittleEndian
~~~~~~~~~~~~

length [required]

signed [required]

offset

multiplier


LengthPrefixInteger
~~~~~~~~~~~~~~~~~~~

length_codec [required]

signed [required]

length_multiplier

length_offset


TextInteger
~~~~~~~~~~~

text_codec [required]

base


TODO?: run-length/Huffman-esque encodings


List
----

Parameters
~~~~~~~~~~

elem [required]

value

count

min_count

max_count

count_factor

exclude_values


List codecs
-----------

FixedCountList
~~~~~~~~~~~~~~

elem_codec


CountPrefixList
~~~~~~~~~~~~~~~

elem_codec

count_codec [required]

count_multiplier (*natural*)
    
count_offset (*integer*)


TerminatedList
~~~~~~~~~~~~~~

elem_codec

terminator


TerminatedUnionList
~~~~~~~~~~~~~~~~~~~

elem_codec

terminator_tag [required]

union_field


Optional
--------

Parameters
~~~~~~~~~~

subject [required]

is_empty [required]


OptionalCodec
~~~~~~~~~~~~~

subject_codec [required]

null_string


Text
----

Parameters
~~~~~~~~~~

value

count

min_count

max_count

count_factor

utf8_length (TODO)

pattern

exclude_pattern

charset


Text codecs
-----------

Text encodings
~~~~~~~~~~~~~~

FixedCountText
~~~~~~~~~~~~~~

encoding [required]

CountPrefixText
~~~~~~~~~~~~~~~

encoding [required]

count_codec [required]

count_multiplier (*natural*)
    
count_offset (*integer*)


TerminatedText
~~~~~~~~~~~~~~

encoding [required]

terminator [required]


TBD: Pointer, Void, Dictionary Date/Timestamps ....


Stacking codecs
===============

DoubleCodec
~~~~~~~~~~~

field [required]

additional [required]


Parameters and codecs for user-defined types
============================================

Records
-------

RecordCodec
~~~~~~~~~~~

Unions
------

tag


TagPrefixUnion
~~~~~~~~~~~~~~

tag_codec


EmbeddedTagUnion
~~~~~~~~~~~~~~~~

tag_codec

offset


ContextTagUnion
~~~~~~~~~~~~~~~

Enumerations
------------

value

