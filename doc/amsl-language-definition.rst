
AMSL: A Message Description Language
====================================

This document describes the design of what I for now have dubbed *AMSL*: a language for describing protocol messages.
It is a reference document that is updated when the language itself is updated. Its intention is to define *how* the 
language works but not *why* it works in that manner: the design rationale will be in a different document.

Language grammar
----------------

The language grammer is defined using the `BNFC parser generator language <http://bnfc.digitalgrammars.com/>`_ and can 
be found in `this file <compiler/src/Language/APSL/AMSL/Grammar.cf>`_. Parser source code and documentation is generated 
from this file.

Comments and whitespace
~~~~~~~~~~~~~~~~~~~~~~~

Note that the language is not whitespace sensitive. Comments start with a ``#`` and continue until the end of the line.


Core concepts
-------------

Modules
~~~~~~~

Each AMSL file contains exactly one *module*. The first non-whitespace and non-comment line of the file should be 
``message module X``, where X is a (preferably lowercase) module name, which should match the file name sans extension.

A module contains one or more *declarations*: definitions of types or codecs (described in the next sections) that 
have a name through which they can be identified. These names should consist of alphanumerical ASCII characters (or 
underscores) and are required to start with a capital letter.

Declarations from one module can be *imported* within another module by using *import statements*, which should be 
below the module name but above any declarations. One can import all declarations from a module, or only an explicitly 
named subset. Other modules are identified with a string literal containing their file path, relative to the location 
of the current module.

By importing them, these declarations become available to the current module and can be referred to. Imports are not 
*transitive*: when module B imports all declarations from module A, and module C imports all from B, the contents of A 
do not become available.

Here is an example of this *module header* syntax::

    # The hypothetical ABCD protocol is an extension of the 'ABC' protocol.
    message module abcd

    # Import all from the ABC message specification.
    import * from 'abc.amsl'

    # Include a number of useful type synonyms.
    import Int32, UInt32, PrintableText from 'lib/types.amsl'

    # Include a record representing a particular set of log-in information.
    import LoginCredentials from 'some/other/protocol.amsl'


Expressions and expression types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TODO
    
    
Types and type expressions
~~~~~~~~~~~~~~~~~~~~~~~~~~

A type describes a *set of abstract values* within a certain domain. Examples of these abstract values are ``false``, 
``42`` and ``"Hello world!"``; which respectively belong to the types ``Bool``, ``Integer`` and ``Text``. These values 
are abstract in the sense that they are not bound to a single *representation* when stored or transmitted as a 
sequence of bytes. An integer, for example, could be represented using either big-endian or little-endian byte order, 
and could either use a fixed size of bytes or somehow encode a dynamic length.

There is a set of *basic types*, which are part of the language definition and detailed in `Basic types and codecs <doc/basic-types-codecs.rst>`_. 
These types do not need to be imported within a module, but are available from the start. 

A user will introduce a new type within a module by declaring one of the following:

- A type definition.
- A record.
- A union.
- An enumeration.
- An extension type.

All of these declarations are described later in this document.

The definitions of these declaration may contain *type epxressions*. Such an expression refers to a type that is 'in 
scope' (i.e. a basic type, an imported type, or a type declared in the same module). There are two kinds of type 
epxressions:

- One that just contains the name of a type, which represents the set of all its values.
- A type name followed by an *named argument list* between parentheses. These arguments each put *restrictions* upon 
  the values in the set. Which *parameters* (i.e. argument names) are supported depend on the type: for example, the 
  ``Integer`` basic type has the paramater *min*: when set, all integers strictly lesser than the argument are removed 
  from the set.


A few examples of type expressions, each using basic types::

    Integer                                                  # All integers.
    Integer(min=0)                                           # The natural numbers.
    Integer(min=1, max=3)                                    # {1,2,3}
    Text(max_count=200)                                      # Text strings not longer than 200 code points.
    Text(count=200)                                          # Text strings that are exactly 200 code points long.
    Text(pattern=/([A-Z][a-z])+/)                            # Strings of one or more alphabetical letters.
    Text(pattern=/[a-z.\-]@[a-z.\-]\.com/, max_count=50)     # A (rather incomplete) set of e-mailaddresses.
    List(elem=Bool, count=100)                               # Lists of 100 boolean values.
    List(elem=Binary(length=10))                             # Lists of any size containing 10-bit strings.


Available parameters for each type are described in the basic type documentation and in the sections below.

Codecs
~~~~~~

A *codec* is a method through which a value within a certain type expression can be *encoded* as a sequence of bytes, 
and how this byte sequence can be *decoded* back into the same value.

A codec basically contains a pair of the following functions:

- ``encode``: converts a value of the appropriate type into a sequence of bytes.
- ``decode``: identifies a prefix of a byte sequence containing an encoded value, and returns this value along with 
  the remaining bytes.

These functions should satisfy two laws:

- Let ``[]`` denote an empty byte sequence. When ``encode`` accepts some value ``x``, then 
  ``decode(encode(x)) == (x, [])``. 
- Let ``~`` denote byte sequence concatenation. When ``decode(a) == (x,b)`` for some ``a``, then 
  ``decode(a ~ c) == (x, b ~ c)`` for any ``c``; this means that it should be possible to distinguish a prefix 
  containing an encoded value from any arbitrary byte sequence that may follow it.

Like with types, there is a set of *basic codecs* (see `Basic types and codecs <doc/basic-types-codecs.rst>`_). Codecs can also be introduced within
a module by declaring codec definitions or codec extensions (both described in later sections).

Codecs are used within *codec expressions*, which are syntactically the same as type expressions. Although, in this 
case arguments do not restrict a set but rather provide more information about the encoding mechanism.

Some examples of codec expressions::

    BoolBits(truth_string=b1)               # Encode a boolean value as a single bit: 1 is true, 0 is false.
    LittleEndian(length=64, signed=true)    # Represent a 64-bit signed integer using two's complement.
    FixedCountText(encoding='utf-16')       # UTF-16 encode a text with some fixed size.
    TerminatedText(encoding='ascii',        # ASCII string of which the end is indicated by a zero byte.
                   terminator="\0")
    CompactIPv4                             # Encode an IPv4-address in 32 bits, instead of the human-readible notation.

Codecs generally only work for particular sets of values, i.e. type expressions. For example, 
``LittleEndian(length=64)`` is a codec for ``Integer``s with a ``min`` and ``max`` parameter for which 
``0 <= min <= max < 2^64``, whereas ``FixedCountText`` works for Texts with their ``count`` parameter set to some 
value.


Type and codec definitions
~~~~~~~~~~~~~~~~~~~~~~~~~~

Users can define new types and codecs that serve as an alias for another type/codec expression.

Some examples of type definitions::

    type Int is Integer                               # When typing out Integer is too much work.
    type UInt16 is Integer(min=0, max=2^16-1)         # Natural numbers which could be encoded in 16 bits.
    type SInt32 is Integer(min=-2^31, max=2^31-1)     # Integers that could be encoded as signed 32-bit words.
    type EMail is Text(pattern=/.+@.+\..+/)           # An e-mail address.
    type MulticastIP is IPAddress(prefix='ff00::/8')  # Multicast IP addresses.
    type Bytes is Binary(unit_length=8)               # Bit sequences of which the length is divisible by eight.

Codec definitions are similar::
    
    codec BE is BigEndian                                                # An abbreviation.
    codec Bit is BoolBits(truth_string=b1)                               # Encode booleans as single bits.
    codec NetworkByteOrder is BE                                         # One definition can be based upon another.
    codec CString is TerminatedText(encoding='ascii', terminator="\0")   # C-style strings.
    codec PText is CountPrefixText(count_codec=BigEndian(length=32))     # Text with length indicated by a 32-bit word.


Given these examples, the expression ``UInt16`` would be equivalent to ``Integer(min=0, max=2^16-1)``. However, a 
defined type may also be parameterized: the permissible arguments are identical to those accepted by the type being 
aliased (in this case ``Integer``), and will have the same semantics. When providing an argument to a parameter that 
has already been assigned within the type definition, the value from the definition will be *overridden*. This means, 
for example that the expression ``UInt16(min=5)`` is equivalent to ``Integer(min=5, max=2^16-1)``.

Some examples of how type/codec expressions are translated::

    SInt32(min=0)  ==> Integer(min=0, max=2^31-1)
    EMail(max_count=100) ==> Text(pattern=/.+@.+\..+/, max_count=100)
    BE(signed=false, offset=1) ==> BigEndian(signed=true, offset=1)
    Bytes(unit_length=2) ==> Binary(unit_length=2)  # Note that this type name no longer makes sense here.


Default codecs
~~~~~~~~~~~~~~

Within a single protocol, values of the same type are generally encoded in the same way. To prevent the user from 
repeatedly having to write out the same codec expression, a default codec expression can be associated with a 
particular type, which is used when no other codec expression is specified.

Default codec definitions are only valid for a single module and will not be imported by other modules. Only one 
default can be assigned to one type per module, and this default applies to the entire module regardless of its 
position in the source file.

Some types (such as ``Binary``) already have a default codec associated with them, so it is define e.g. 
``Binary`` fields without a codec. Specifying a default codec for such a type is allowed, and will override the 
previous default for the current module.

Codecs can also be assigned to, and are particularly useful for, user-defined types. Note that different defaults can 
be assigned to different defined types, *even if they have the same translation*. Given ``type Int is Integer`` and
``type Nat is Integer(min=0)``, this means that expressions using the types ``Integer``, ``Int`` or ``Nat`` can all 
have distinct default codecs. Furthermore, if ``Int`` would not have a default, neglecting to provide a codec for a 
field with an ``Int`` type will result in an error, even if ``Integer`` does have a default codec.

Some example defaults for basic types and examples from the previous sections::

    default IPAddress as TextIP(notation='rfc5952')
    default SInt32 as BE(signed=true)
    default Bool as Bit
    default EMail as PText(encoding='utf-8')


Fields
~~~~~~

A *field* denotes a part of a data structure (records or unions, as described later) that describes a particular part
of a message. A field has three components:

- A *name*, which is an identifier that starts with a lowercase letter. It should be unique within the data structure 
  in which the field is defined (but not neccessarily within the whole module) and can be used within other fields to 
  refer to this one.
- A *type expression* describing the set of values can be stored at a field; this also gives some semantics about what 
  the field means, and also intructs tools how to handle them. This expression is called the *field type*.
- An optional *codec expression* that matched with the type expression and describes how this field should be parsed 
  and serialized. This expression is called the *field codec*.

Some example fields::

    # Name         # Field type                           # Field codec

    _magic      is Text(value='ABCDEFG')                  as FixedCountText(encoding='ascii')
    username    is Text(min_count=1)                      as TerminatedText(encoding='utf-8', terminator="\0")
    age         is Integer(min=0, max=150)                as LittleEndian(signed=false, length=32)
    logged_in   is Bool                                   as FalseZero(length=8)
    session_id  is Optional(is_empty=!logged_in,      
                            subject=Text(count=30))       as OptCodec(subject_codec=FixedCountText(encoding='ascii'))
    identifier  is Binary(length=16)                      # The Binary type has an implicit default codec.


The field type and codec allow a tool to inspect a byte stream that is supposed to start with this field; it can detect 
whether it is indeed correctly formatted, identify how long the prefix containing the field is, parse this field, and 
then validate whether it represents an acceptable value.

Besides validation, a tool could generate code for a field, to aid a protocol implementation in a particular language.
The field codec indicates how this field can be parsed and the field type provides validation code, as well as an 
appropriate type to store the data in: for example, in Java a field with type ``Text(min_count=1)`` could be 
represented by a private ``String`` field, along with a getter and a setter that validates whether the new value is not 
empty. For ``Integer``s, an appropriate primitive integer type could be selected that fits the required range, or a 
``BigInteger`` in case more than 2^64 values are possible.

A field name may start with an underscore: while this does not change how the field is parsed, it does indicate that 
this field is "not interesting" (i.e. it adds no relevant information about the data) and does not neccessarily need to 
be exposed to an implementor. Such fields usually have a fixed value (specified through the ``value`` parameter in the 
field type), such as is the case for 'magic numbers' or reserved fields that must always be zero.


Enumerations
~~~~~~~~~~~~

Another means through which to introduce a new type is by defining an enumeration. An enumeration is simply a (small)
set of user-defined identifiers, that each represent a unique value that can be used throughout the module (and any 
module importing the enumeration definition).

In order to be able to store an enum type, each enumeration type is backed by a *representation type* and each value of 
the enum is associated with a value of that type, which are called *representations*. This representation type 
determines how the enum type can be encoded, but it does not influence the enum's semantics; furthermore, the 
representation values describe how each enum value is supposed to be parsed or serialized, but they are not synonymous 
with them.

Only the following types can be used as representation types:

- ``Binary``
- ``Text``
- ``Integer``
- ``Bool``
- Any other enum type.

See `Basic types and codecs <doc/basic-types-codecs.rst>`_ for a description of enum type parmaters.

An enum type inherits all codecs of its representation type, and each value of the enum is encoded/decoded in the same 
manner as its representation. For example, consider the following enumeration::

    enum CommandType of Text with
        login     as "LOGIN"
        logout    as "LOGOUT"
        nickname  as "NAME"
        speak     as "SAY"
    end

A ``CommandType`` could now be used in a field as follows::

    command is CommandType(values=[login, logout]) as TerminatedText(encoding='ascii', terminator=" ")

In most cases, it is probably preferable to define a default codec for the enumeration, this would also makes it easier 
to change the representation type later.


Composite types and messages
----------------------------

Fields can be combined within *composite types*, of which there are two kinds: records and unions. These are another 
kind of user defined types and can be used within type expression (meaning a value of one composite type may contain 
another). 

Similar to the ``Binary`` type, each composite type comes with an implicit default codec, that could be overridden by 
another user-defined default.

Records
~~~~~~~

A record is a so-called 'product type' that contains a sequence of fields, for example::

    record ArchivedFile with
        indicator   is Text(value="FILE")          as FixedCountText(encoding='ascii')
        filename    is Text(max_count=100)         as CountPrefixText(count_codec=BE(length=8))
        permissions is Binary(length=9)
        size        is Integer(min=0, max=2^64-1)  as BigEndian(length=64)
        create_year is Integer(min=1970, max=2100) as IntText(base=10, count=4, encoding='ascii')
        file_data   is Binary(length=size*8)
        _checksum   is Binary(length=32)
    end

As can be seen in the above example (in the type expression of the ``file_data`` field), expressions within fields may 
refer to the names of fields that are defined earlier. In this case, the ``size`` variable will be substituted by the 
value encoded in the ``size`` field during parsing, so that the parser can find out the ``length`` argument of 
``file_data``'s type, after which it knows how many more bytes to consume.

Since each fields requires a codec, or a type for which a default codec is available, a codec for each record becomes 
immediately available that simply expects to see these fields, in this order, directly following each other. 
Alternative record codecs are available that control alignment and padding.

Since a record declaration introduces a new type, and any other record in the module may include a field with this 
type. A record type may not, directly or indirectly, contain itself, even if wrapped within an ``Optional`` or ``List``
type, meaning recursive records are not supported.

See `Basic types and codecs <doc/basic-types-codecs.rst>`_ for a description of the parameters of a record type.


Unions
~~~~~~

Unions are a 'sum type' that hold the value of one of many fields. They can be used to express fields that could either
contain one type of value or other. The different types that could be stored within the untion are called *options*.
For example::

union PathLookupResult tagged Text of
    "FILE" tags file       is ArchivedFile
    "DIR"  tags dir        is ArchivedDirectory
    "LINK" tags link       is Path
    "NONE" tags not_found  is Text(value="NOT FOUND") as FixedCountText(encoding='ascii')
end

Tagged unions are associated with a *tag type*, which can any of the same types that can be used as enum representation
types. The value of this tag indicates which union option should be selected.

Naming union options is not required, the following would also be valid::

union PathLookupResult tagged Text of
    "FILE" tags ArchivedFile
    "DIR"  tags ArchivedDirectory
    "LINK" tags Path
    "NONE" tags Text(value="NOT FOUND") as FixedCountText(encoding='ascii')
end


When using the ``TagPrefixUnion`` codec, the tag is encoded along with the union object itself. An ``EmbeddedTagUnion``
is similar, except that the tag bytes overlap with the bytes of the union data. When using the ``ContextTagUnion`` 
codec, a ``tag`` argument should be provided that equals a tag value; this tag usually refers to another field within a 
record, but when the tag type is ``Bool``, for example, it could also be some boolean predicate.

An example of a union tagged with a user-defined type and a record using it::

    default Text as CountPrefixText(count_codec=BE(length=8), encoding='utf-8')

    enum HostnameType of Integer with
        domain_name as b001
        ip_address   as b010
        stored_name as b100
    end
    default HostnameType as BigEndian(length=3)

    union Hostname tagged HostnameType of
        domain_name tags Text(pattern=/[a-z\-0-9.]+\.[a-z\-0-9]/, max_count=255)
        ip_address  tags IPAddress as TextIP(notation='rfc5952')
        stored_name tags Text(pattern=/[a-z\-0-9.]+/, max_count=255)
    end

    record Location with
        hostname_flag is HostnameType
        protocol_flag is ProtocolType
        hostname is Hostname(tag=hostname_flag)
        path is Text
    end


Messages
~~~~~~~~

The point of this language is to be able to describe protocol messages. In order to accomplish this, one can simply 
define a record, but substitute ``record`` keyword for ``message``. Now a *message type* has been introduced, which 
behaves identically to a record within AMSL, except that they can not be assigned any other codec than their default.

These message types represent individual data packets that are used for the protocol communication. These message types 
can be imported within protocol interaction specifications written in the AISL language.


Extension types and codecs
--------------------------

TODO

Documentation
-------------

TODO
